#!/usr/bin/perl
# grabbed at http://ryanwark.com/blog/posting-to-the-tumblr-v2-api-in-perl
# - this page disappeared, the server seems off, so it was:

    # convenient callback URL = https://api.tumblr.com/console/calls/user/info


    # Getting an application set up to use OAuth is a multi-step process but basically works like this:
    #
    # Generate consumer key and consumer secret
    # Request request token and token secret
    # Grant application access (requires user intervention)
    # Request access token and token secret
    # Use consumer key, consumer secret, access token and token secret to post content
    #
    # Some services may do most of this work for you, but Tumblr does not. When you register an application, one of the fields you fill in is the ‘callback url’. This is the URL that you will be directed to after you authorize a token for that application. When this happens (and if you’re setting up this application for your own use, it doesn’t matter where you’re redirected to) you’ll be provided with a oauth verifier token (VIA URL parameter). For my own simplicity sake, I put a PHP script on my web server that would echo the contents of that parameter:
    #
    # <?php echo $_GET["oauth_verifier"]; ?>
    #
    # I accidentally found a Perl script https://web.archive.org/web/20130908073950/http://www.commonmap.info/w/index.php/Oauth_perl_examples meant for walking you through the process of getting tokens. For useability sake, I made some slight modifications to the script to make it a little more extensible and you can get it here.
    #
    # Running this script will require that you at least set the consumer key and consumer secret but these are the configurable options.
    
use LWP::UserAgent;
use Net::OAuth;
$Net::OAuth::PROTOCOL_VERSION = Net::OAuth::PROTOCOL_VERSION_1_0A;
use HTTP::Request::Common;
my $ua = LWP::UserAgent->new;

use Getopt::Long;
## get tty input with standard opts.
my $getopt;
my $consumer_key;
my $consumer_secret;
eval {
	$getopt = GetOptions("key=s" => \$consumer_key,
			     "secret=s" => \$consumer_secret);
};
die "Set consumer key with --key arg! Exiting" unless $consumer_key;
die "Set consumer secret with --secret arg! Exiting" unless $consumer_secret;

my $request_url = 'http://www.tumblr.com/oauth/request_token';
my $access_url = 'http://www.tumblr.com/oauth/access_token';
my $authorize_url = 'http://www.tumblr.com/oauth/authorize';
####
my $request =
        Net::OAuth->request('consumer')->new(
          consumer_key => $consumer_key,
          consumer_secret => $consumer_secret,
          request_url => $request_url,
          request_method => 'POST',
          signature_method => 'HMAC-SHA1',
          timestamp => time,
          nonce => nonce(),
        );

$request->sign;

print $request->to_url."\n";
my $res = $ua->request(POST $request->to_url);
my $token;
my $token_secret;
if ($res->is_success) {
  my $response = Net::OAuth->response('request token')->from_post_body($res->content);
  $token=$response->token;
  $token_secret=$response->token_secret;
  print "Got Request Token ", $token, "\n";
  print "Got Request Token Secret ", $token_secret, "\n";
  print "Go to $authorize_url?oauth_token=".$token."\n";
} else {
  die "Something went wrong";
}

print "Go to the above URL, authorize and give me the oauth_verifier token. Then press <ENTER>\n";
my $verifier = <STDIN>;
chomp $verifier;

$request =
        Net::OAuth->request('access token')->new(
          consumer_key => $consumer_key,
          consumer_secret => $consumer_secret,
          token => $token,
          token_secret => $token_secret,
          request_url => $access_url,
          request_method => 'POST',
          signature_method => 'HMAC-SHA1',
          timestamp => time,
          nonce => nonce(),
          verifier => $verifier,
        );

$request->sign;

#print $request->to_url."\n";

$res = $ua->request(POST $request->to_url);
if ($res->is_success) {
  my $response = Net::OAuth->response('access token')->from_post_body($res->content);
  print "Got Access Token ", $response->token, "\n";
  print "Got Access Token Secret ", $response->token_secret, "\n";
} else {
  die "Something went wrong";
}

sub nonce {
  my @a = ('A'..'Z', 'a'..'z', 0..9);
  my $nonce = '';
  for(0..31) {
    $nonce .= $a[rand(scalar(@a))];
  }

  $nonce;
}
