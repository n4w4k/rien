# skip already sourced
[ "`declare -f Y00-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y00-sourced() {
    echo true;
}

# TERM needs to be properly set
#    xterm-256color, rxvt-unicode-256color, etc
# otherwise tput will fail to find any color beside 8 first

# use tput, that is portable, not bash specific like
# http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/index.html
# we used before

# color list
# printf '\e[48;5;%dm ' {0..255}; printf '\e[0m \n'
shell_clear=$(tput sgr0)
shell_bold=$(tput bold)
shell_underline=$(tput smul)
shell_invert=$(tput rev)

# 8 colors
shell_red=$(tput setaf 1)
shell_RED=${shell_red}${bold}
shell_green=$(tput setaf 2)
shell_GREEN=${shell_green}${bold}
shell_yellow=$(tput setaf 3)
shell_YELLOW=${shell_yellow}${bold}
shell_blue=$(tput setaf 4)
shell_BLUE=${shell_blue}${bold}
shell_magenta=$(tput setaf 5)
shell_MAGENTA=${shell_magenta}${bold}
shell_cyan=$(tput setaf 6)
shell_CYAN=${shell_cyan}${bold}
shell_grey=$(tput setaf 8)
shell_GREY=${shell_grey}${bold}

# 256
# http://misc.flogisoft.com/bash/tip_colors_and_formatting
shell_blue256=$(tput setaf 27)
shell_green256=$(tput setaf 77)
shell_violet256=$(tput setaf 141)
shell_pinkish256=$(tput setaf 204)
shell_orange256=$(tput setaf 209)


# terminals might not set USER and HOSTNAME
[ ! -z "$USER" ] && USER=$(id -un)
[ ! -z "$HOSTNAME" ] && HOSTNAME=$(uname -n)


### BASHism stuff from now on
[ -z "$BASH_VERSION" ] && return

# Nice colored prompt (with a space before the pwd, to ease copy/paste)
# 
#
# THIS MUST BE COMMENTED OUT IN /etc/bash.bashrc
#   # set a fancy prompt (non-color, overwrite the one in /etc/profile)
#   PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '

shell_datecolor=${shell_magenta}
shell_pathcolor=${shell_yellow}
shell_usercolor=${shell_green}
shell_rootcolor=${shell_red}
shell_distantcolor=${shell_blue}
shell_containercolor=${shell_CYAN}


if [ $(tput colors) -gt 8 ]; then
    # we have 256 colors and tput knows it
    shell_usercolor=${shell_green256}
    shell_rootcolor=${shell_orange256}
    shell_distantcolor=${shell_blue256}
    shell_containercolor=${shell_violet256}
fi

shell_promptcolor=${shell_usercolor}
shell_hostcolor=${shell_cyan}
shell_user="${USER}"
shell_host="@${HOSTNAME%%.*}"
if [ "`id -u`" == 0 ]; then
    # do not print username if root
    shell_user=""
    shell_host="${HOSTNAME%%.*}"
    # but change prompt color instead
    shell_promptcolor=${shell_rootcolor}
fi
if [ "$SSH_CLIENT" != "" ]; then
    # change @host color if connected from distance
    shell_hostcolor=${shell_distantcolor}
fi
if [ "$container" != "" ]; then
    # change @host color if running within a container (like lxc)
    # (override previous setting)
    shell_hostcolor=${shell_containercolor}
    # if connected over SSH, try to add name or IP of the host, useful to avoid having terminals with meaningless
    # names
    # (otherwise it is safe to assume you wont get confused)
    if [ "$SSH_CLIENT" != "" ]; then
	# these should be set
	#   SSH_CLIENT="192.168.1.10 58068 22"
	#   SSH_CONNECTION="192.168.1.10 58068 192.168.1.1 22"
	# so we could go through every part of SSH_CONNECTION and keep it if missing in SSH_CLIENT
	# but assuming SSH_CONNECTION is consistent, we'll just pick its third value
	SSH_CONNECTION_PUBLICIP=$(echo "$SSH_CONNECTION" | cut -f 3 -d " ")
	# try to resolve the IP to a name
	SSH_CONNECTION_PUBLICHOSTNAME=$(dig +short +tries=1 +time=1 -x "$SSH_CONNECTION_PUBLICIP")
	if [ "$SSH_CONNECTION_PUBLICHOSTNAME" != "" ]; then
	    # print only the short hostname
	    shell_host="${shell_host}^${SSH_CONNECTION_PUBLICHOSTNAME%%.*}"
	else
	    shell_host="${shell_host}^${SSH_CONNECTION_PUBLICIP}"
	fi	
    fi
fi

### BASHism stuff from now on
[ -z "$BASH_VERSION" ] && return

PS1="\[${shell_promptcolor}\]\!\[${shell_clear}\] \[${shell_datecolor}\]\$(date +%H:%M)\[${shell_clear}\] \[${shell_promptcolor}\]${shell_user}\[${shell_clear}\]\[${shell_hostcolor}\]${shell_host}:\[${shell_clear}\] \[${shell_pathcolor}\]\w\[${shell_clear}\]\n\[${shell_invert}\]\[${shell_promptcolor}\]"'\$'"\[${shell_clear}\] "

# update window title only if we have an X terminal
case $TERM in
    xterm*|rxvt*|konsole|aterm|wterm)
	# Why not using directly PS1?: because it mess up other things
	PROMPT_COMMAND='echo -ne "\033]0;${shell_user}${shell_host}: ${PWD/$HOME/~}\007"'
esac	


# EOF
