# skip already sourced
[ "`declare -f Y51-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y51-sourced() {
    echo true;
}

# skip if lxc isnt installed
[ ! -x "`which lxc-ls`" ] && return

# note that stalag13-00-shell handle the parts relevant to
# shell prompt and colors definitions

# command distributed over all host/guests
# with colored hostname
lxc-dcmd() {
    # handle master
    echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_hostcolor}$HOSTNAME:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}$@${shell_clear}]"
    "$@" | sed --null-data s/\\b$HOSTNAME\\b/${shell_hostcolor}$HOSTNAME${shell_clear}/gi;
    
    # loop in active guest
    # (since we share hardware, no point in running anything in
    # parallel)
    guests=($(lxc-ls --active))
    for guest in "${guests[@]}"; do
	echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_containercolor}$guest:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}$@${shell_clear}]"
	lxc-attach -n "$guest" -- "$@"| sed --null-data s/\\b$guest\\b/${shell_containercolor}$guest${shell_clear}/gi;
    done
}
# same but without messing with the output in any way
# (useful to do some cat/echo to | or >
lxc-dcmd-pure () {
    "$@"
    
    # loop in active guest
    guests=($(lxc-ls --active))
    for guest in "${guests[@]}"; do
	lxc-attach -n "$guest" -- "$@"
    done
}


# sort of lxc-dcmd specific apt-get command: so we get completion 
apt-lxc() {
    # handle master
    echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_hostcolor}$HOSTNAME:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}apt-get $@${shell_clear}]"
    apt-get "$@"
    
    # loop in active guest
    # since we share hardware, no point in running anything in
    # parallel
    guests=($(lxc-ls --active))
    for guest in "${guests[@]}"; do
	echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_containercolor}$guest:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}apt-get $@${shell_clear}]"
	lxc-attach -n "$guest" -- apt-get "$@"
    done
}

# sort of lxc-dcmd specific apt command: so we get completion
lxc-apt() {
    # handle master
    echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_hostcolor}$HOSTNAME:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}apt $@${shell_clear}]"
    apt "$@"

    # loop in active guest
    # since we share hardware, no point in running anything in
    # parallel
    guests=($(lxc-ls --active))
    for guest in "${guests[@]}"; do
	        echo -e "[${shell_datecolor}$(date +%H:%M:%S)${shell_clear} ${shell_containercolor}$guest:${shell_clear} ${shell_promptcolor}#${shell_clear} ${shell_invert}apt $@${shell_clear}]"
		lxc-attach -n "$guest" -- apt "$@"
    done
}
 


# EOF
