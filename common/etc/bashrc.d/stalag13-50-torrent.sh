# skip already sourced
[ "`declare -f tcdcheck 2>/dev/null`" ] && return

# Wild assumption?
if [ -d /home/torrent/watch ]; then
    TORRENT_BASEDIR=/home/torrent
    TORRENT_WATCH=$TORRENT_BASEDIR/watch
    TORRENT_DOWNLOAD=$TORRENT_BASEDIR/download
else
    TORRENT_BASEDIR=$LAN
    TORRENT_WATCH=$TORRENT_BASEDIR/torrent
    TORRENT_DOWNLOAD=$TORRENT_BASEDIR/download
fi

# Run
function tcdcheck {
    if [ ! -d "$TORRENT_BASEDIR" ]; then 
	 echo "TORRENT_BASEDIR ($TORRENT_BASEDIR) does not exists."
	 echo "(It should point to the directory that contains watch & download)"
	 return 1
    fi
    return 0
}

alias torwatch='tcdcheck && cd $TORRENT_WATCH'
alias tordown='tcdcheck && cd $TORRENT_DOWNLOAD'
alias torlog='torwatch && tail -n 100 log.txt'
alias torstat='torwatch && cat status.txt'
alias torfinished='torwatch && ls *.trs+'

# EOF
