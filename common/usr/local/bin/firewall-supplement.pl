#!/usr/bin/perl
#
# Copyright (c) 2003-2022 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA
#
# ufw and all are fine to set up firewall rules from scratch.
# A bit a pain in the ass if you want rules to be generated on the fly
# added to prexisting rules, for instance by another software like lxc-net
# 
# this script purpose is only that: setting up extra rules, not managing
# current one, not saving current one, etc.
#
# assumes:
#   - you have two interface, one to the internet and one local

use strict;
use Fcntl ":flock";
use Getopt::Long;
use Sys::Syslog; 
use IO::Interface::Simple;
use feature "switch";

#######################################
### get options, variables setup

# variables
my ($getopt, $help, $booting);
my ($debug, $verbose);
my $iptables = "/sbin/iptables";
my $iptables_cache = "/tmp/firewall-supplement";
my %existing_rules;

my $rc = "/etc/default/firewall-supplement";
my $internet_interface = "eth0";
my $intranetA_interface = "eth1";
my $intranetB_interface = "";
my $loopback_interface = "lo";
my $on_boot = 0;
my $allow_dupes = 0;
my $create_natA = 0;
my $create_natB = 0;

# IP will permanent access through internet 
my @internet_priviledged_ips = ();

# ports opened to the internet interface
#   syntax: port (open), port->IP:port (open and forward)
my @internet_ports = (
    "22", # ssh
    "2222->172.16.0.22:22"); # ssh to local ip through port 2222

# ports opened to the intranet interface
#   syntax: port (open), port->IP:port (open and forward)
my @intranetA_ports = (
    "*", # wildcard accept all
    "22->172.16.0.22"); # ssh to local ip

my @intranetB_ports = @intranetA_ports;


# get config
open(RCFILE, "< $rc");
while(<RCFILE>){
    # ignore comments
    next if /^#/;
    next if /^;/;
    
    # remove quotes
    s/\"//;
    # remove white spaces
    s/\s//;

    # general options
    $on_boot = $1 if /^on_boot=(\d*)$/i;
    $create_natA = $1 if /^create_nat1?=(\d*)$/i;
    $create_natB = $1 if /^create_nat2=(\d*)$/i;

    # internet 
    $internet_interface = $1 if /^internet_interface=(.*)$/i;
    @internet_ports = split(",", $1) if /^internet_ports=(.*)$/i;
    @internet_priviledged_ips = split(",", $1) if /^internet_priviledged_ip=(.*)$/i;
	
    # intranet
    $intranetA_interface = $1 if /^intranet_interface1?=(.*)$/i;
    @intranetA_ports = split(",", $1) if /^intranet_ports1?=(.*)$/i;
    
    # secondary intranet
    $intranetB_interface = $1 if /^intranet_interface2=(.*)$/i;
    @intranetB_ports = split(",", $1) if /^intranet_ports2=(.*)$/i;

    # loopback 
    $loopback_interface = $1 if /^loopback_interface=(.*)$/i;
    
}
close(RCFILE);

# get command line args
eval {
    $getopt = GetOptions("help" => \$help,
			 "debug" => \$debug,
			 "verbose" => \$verbose,
			 "allow-dupes" => \$allow_dupes,
			 "nat" => \$create_natA,
			 "booting" => \$booting);
};

# silently exits if start by boot script will not configure to do so
exit if $booting and ! $on_boot;

if ($help) {
    print STDERR "
  Usage: $0 [OPTIONS]

      Add decent iptables rules to a gateway/router. Nothing overly complicated,
      it mainly filters internet/intranet input and allow to redirect it.
 
    -n,  --nat            Create NAT from $internet_interface to $intranetA_interface.
    -a,  --allow-dupes	  Create rules even it if already exists
    -v,  --verbose        Non silent.
         --debug          Only print what it would do (imply --verbose).
	 
  Config ($rc):	 
  ON_BOOT=$on_boot
  ALLOW_DUPES=$allow_dupes

    internet:
  INTERNET_INTERFACE=$internet_interface
  INTERNET_PORTS=".join(",", @internet_ports)."
  INTERNET_PRIVILEDGED_IP=".join(",", @internet_priviledged_ips)."

    intranet:
  INTRANET_INTERFACE=$intranetA_interface
  INTRANET_PORTS=".join(",", @intranetA_ports)."
  CREATE_NAT=$create_natA

  INTRANET_INTERFACE2=$intranetB_interface
  INTRANET_PORTS2=".join(",", @intranetB_ports)."
  CREATE_NAT2=$create_natB

    loopback:
  LOOPBACK_INTERFACE=$loopback_interface

Author: yeupou\@gnu.org
       http://yeupou.wordpress.com/
";
exit(1);
}

#######################################
### function

sub filter {
    my $rule = join(' ', @_);
    if (!$existing_rules{$rule} or $allow_dupes) {
	# verbose
	print "$iptables $rule\n" if $verbose or $debug;
	# execute unless debug mode
	system($iptables, @_) unless $debug;
	# remember rule
	$existing_rules{$rule} = 1;
    } else {
	comment("\tskip duplicate: $rule") if $verbose or $debug;
    }
}

sub comment {
    # verbose
    print "# ".join(' ', @_)."\n" if $verbose or $debug;
}

sub filter_list {    
    # interface is always first argument
    my ($interface, @request) = @_;

    # conditional access: keep track of dedicated chains
    my %from_ip_chains;
    
    # go thru the list
    for (@request) {
	# might be something like IP->PORT->NEWIP:NEWPORT
	#  (source IP, server PORT, subserver NEWIP:NEWPORT)

	# make sure no white space remains
	s/\s//;
	
	# decompose -> parts
	my ($from_ip, $port_in, $to);
	my (@items)= split("->", $_);
	given (scalar(@items)) {
	    # only one item: it must be a port to open
	    ($port_in) = (@items) when 1;
	    # three items: first must be originating IP, second a port to open, third specific destination
	    ($from_ip, $port_in, $to) = (@items) when 3;
	    # otherwise two, which can be either: originating IP and port to open / port to open and
	    # specific destination
	    default {
		my ($a,) = (@items);
		if ($a =~ /^[0-9]*$/) {
		    # simple test, if first component is numeric only, it is a port with specific destination
		    ($port_in, $to) = (@items);
		} else {
		    # otherwise, it is an IP with the port as second component
		    ($from_ip, $port_in) = (@items);
		}
	    }
	}

	# decompose $to part
	my ($to_port, $to_ip);
	($to_ip, $to_port) = split(":", $to) if $to;

	comment("\t$_ translated into:");
	comment("\t\t from_ip = $from_ip") if $from_ip;
	comment("\t\t port_in = $port_in") if $port_in;
	comment("\t\t   to_ip = $to_ip") if $to_ip;
	comment("\t\t to_port = $to_port") if $to_port;
	
	
	if ($port_in eq '*') {
	    # wilcard *: open all on interface
	    comment("\twildcard: interface fully opened");
	    filter("--append", "INPUT",
		   "--in-interface", $interface,
		   "--jump", "ACCEPT");
	    filter("--append", "OUTPUT",
		   "--out-interface", $interface,
		   "--jump", "ACCEPT");
	    next;
	}

	if ($from_ip) {
	    # IP->...: filter by source IP, in addition to possible further forward rules
	    # https://www.netfilter.org/documentation/HOWTO//packet-filtering-HOWTO-9.html
	    # conditional access: create dedicated chain, if not yet created
	    unless ($from_ip_chains{"lt".$interface.$port_in}) {
		comment("\tfilter interface port $port_in by IP");
		filter("--new", "lt".$interface.$port_in);
		filter("--append", "INPUT",
		       "--protocol", "tcp",
		       "--in-interface", $interface,
		       "--dport", $port_in,
		       "--jump", "lt".$interface.$port_in);
		$from_ip_chains{"lt".$interface.$port_in} = 1;
	    }

	    # add allow rule for the given IP
	    comment("\tallow access to port $port_in to $from_ip");
	    filter("--append", "lt".$interface.$port_in,
		   "--source", $from_ip,
		   "--jump", "RETURN");

	    # continue forward, this is only extra
	}
	
	if ($to_port) {
	    # PORT->IP:PORT
	    comment("\tTCP port $port_in forwarded to $to_ip port $to_port");
	    filter("--table", "nat",
		   "--append", "PREROUTING",
		   "--protocol", "tcp",
		   "--in-interface", $interface,
		   "--dport", $port_in,
		   "--jump", "DNAT",
		   "--to-destination", $to_ip.":".$to_port);
	    next;
	}	
	if ($to_ip) {
	    # PORT->IP
	    comment("\tTCP port $port_in forwarded to $to_ip");
	    filter("--table", "nat",
		   "--append", "PREROUTING",
		   "--protocol", "tcp",
		   "--in-interface", $interface,
		   "--dport", $port_in,
		   "--jump", "DNAT",
		   "--to-destination", $to_ip.":".$port_in);	    
	    next;
	}
	if ($port_in) {
	    # simple port, fully open this specific port
	    # (both TCP and UDP: nowhere else we work on UDP in this script,
	    # seems uncessary so far)
	    comment("\tTCP/UDP port $port_in opened");
	    foreach my $protocol ("tcp", "udp") {
		filter("--append", "INPUT",
		       "--in-interface", $interface,
		       "--protocol", $protocol,
		       "--dport", $port_in,
		       "--jump", "ACCEPT");
	    }
	    next;
	}
	# we should not reach this point, something amiss
       	filter("unable to understand parameters: $_");
    }

    foreach my $chain (keys %from_ip_chains)  {
	comment("\tforbid $chain access to any unspecificied IP");
	filter("--append", $chain,
	       "--jump", "REJECT");
    }
}


#######################################
### run

# disallow concurrent run
open(LOCK, "< $0") or die "Failed to ask lock. Exiting";
flock(LOCK, LOCK_EX | LOCK_NB) or die "Unable to lock. This daemon is already alive. Exiting";

# check if the network interface differs (otherwise it would mean
# setup is broken)
die "Both internet ($internet_interface) and intranet ($intranetA_interface) interface have the same name. How come? Please check:\n\t$0 --help\nExit" if $internet_interface eq $intranetA_interface;
# check if each configured network interface actually exists
# (warning only)
my %interface_isup;
foreach my $interface ($internet_interface, $intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    if (IO::Interface::Simple->new($interface) eq $interface) {
	$interface_isup{$interface} = $interface;
	next;
    } else {
	print "Interface $interface not found. Please check:\n\t$0 --help\n";
    }
}

# log startup
openlog("firewall-supplement", "pid", "LOG_DAEMON");
syslog("info", "initialize with options internet=$internet_interface, intranet1=$intranetA_interface, create_nat1=$create_natA, intranet2=$intranetB_interface, create_nat2=$create_natB, allow_dupes=$allow_dupes") unless $debug;

# read current rules (by reading a /tmp file that should not exist on boot),
# to avoid duplicates.
# (not using iptables-save because the way rules are saved are mangled)
# (not using iptables --check because results seem unreliable)
# note that it might lead to non working rules (if the order is wrong)
# but the alternative is to remove all rules first, which might not be wanted either
if (-r $iptables_cache) {
    open(RULES, "< $iptables_cache ");
    while (<RULES>) {
	next if /^#/;
	next if /^$/;
	chomp();
	$existing_rules{$_} = 1;
    }
    close(RULES);
    comment("read ".(keys %existing_rules)." rules from cache");
    syslog("info", "read ".(keys %existing_rules)." rules from cache") unless $debug;
}

# set default rules: DROP everything in INPUT/FORWARD
comment("set default policy: drop input/forward, accept output");
filter("--policy", "INPUT", "DROP");
filter("--policy", "FORWARD", "DROP");
filter("--policy", "OUTPUT", "ACCEPT");

# set default rules: ACCEPT all on loopback
comment("set default policy: accept traffic on loopback ($loopback_interface)");
filter("--append", "INPUT", "--in-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "OUTPUT", "--out-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "FORWARD", "--in-interface", $loopback_interface, "--jump", "ACCEPT");
filter("--append", "FORWARD", "--out-interface", $loopback_interface, "--jump", "ACCEPT");

# set default rules: ACCEPT INPUT responses 
foreach my $interface ($internet_interface, $intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    unless ($interface_isup{$interface}) {
	comment("set default policy: ignore $interface (not found)");
	next;
    }
    comment("set default policy: accept input responses on $interface");
    filter("--append", "INPUT", 
	   "--in-interface", $interface,
	   "--match", "state",
	   "--state", "ESTABLISHED,RELATED",
	   "--jump", "ACCEPT");
    filter("--append", "OUTPUT", 
	   "--out-interface", $interface,
	   "--match", "state",
	   "--state", "NEW,ESTABLISHED,RELATED",
	   "--jump", "ACCEPT");	
}

# give unconditional access to priviledged IPs on internet interface
foreach my $ip (@internet_priviledged_ips) {
    unless ($interface_isup{$internet_interface}) {
	comment("impossible to accept traffic from priviledged IP $ip through $internet_interface (not found)");
	last;
    }    
    comment("accept traffic from priviledged IP $ip through $internet_interface");
    filter("--append", "INPUT",
	   "--in-interface", $internet_interface,
	   "--source", $ip,
	   "--jump", "ACCEPT");
    filter("--append", "OUTPUT",
	   "--out-interface", $internet_interface,
	   "--destination", $ip,
	   "--jump", "ACCEPT");
}

# create the NAT
foreach my $interface ($intranetA_interface, $intranetB_interface) {
    next if $interface eq "";
    unless ($interface_isup{$interface}) {
	comment("impossible to create the network address translation (NAT) for $interface (not found)");
	next;
    }
    
    if ($create_natA and $interface eq $intranetA_interface or
	$create_natB and $interface eq $intranetB_interface) {
	comment("create the network address translation (NAT) $interface (intranet) <-> $internet_interface (internet)");
	filter("--append", "FORWARD",
	       "--in-interface", $internet_interface,
	       "--out-interface", $interface,
	       "--match", "state",
	       "--state", "ESTABLISHED,RELATED",
	       "--jump", "ACCEPT");
	filter("--append", "FORWARD",
	       "--in-interface", $interface,
	       "--out-interface", $internet_interface,
	       "--match", "state",
	       "--state", "NEW,ESTABLISHED,RELATED",
	       "--jump", "ACCEPT");
	filter("--append", "POSTROUTING",
	       "--table", "nat",
	       "--out-interface", $internet_interface,
	       "--jump", "MASQUERADE");
    } else {
	comment("here we would create the network address translation (NAT) for $interface if we were asked to - we're not");
    }
}

# now deal with each requested port - without overthinking it, if wildcard
# * appears, we wont skip rules apparently irrelevant since then
if ($interface_isup{$internet_interface}) {
    comment("deal specifically with internet ($internet_interface) input");
    filter_list($internet_interface, @internet_ports);
} else {
    comment("impossible to deal specifically with internet ($internet_interface: not found) input");
}

# now deal with each requested port - without overthinking it, if wildcard
# * appears, we wont skip rules apparently irrelevant since then
if ($intranetA_interface ne "" and $interface_isup{$intranetA_interface}) {
    comment("deal specifically with intranet ($intranetA_interface) input");
    filter_list($intranetA_interface, @intranetA_ports);
} else {
    comment("impossible to deal specifically with intranet ($intranetA_interface: not found) input");
}
if ($intranetB_interface ne "" and $interface_isup{$intranetB_interface}) {
    comment("deal specifically with intranet ($intranetB_interface) input");
    filter_list($intranetB_interface, @intranetB_ports);
} else {
    comment("impossible to deal specifically with intranet ($intranetB_interface: not found) input");
}

# finally save in /tmp current rules
die "$iptables_cache is a symlink,  exiting without saving cache..." if -l $iptables_cache;
unless ($debug) {
    syslog("info", "saving ".(keys %existing_rules)." rules to cache");
    comment("saving ".(keys %existing_rules)." rules to cache");
    if (keys %existing_rules) {
	open(RULES, "> $iptables_cache ");
	foreach my $rule (keys %existing_rules)  {
	    print RULES "$rule\n";
	}
	close(RULES);
    }
} else {
    comment("not saving ".(keys %existing_rules)." rules to cache (debug mode)");
}
 
syslog("info", "initialized.") unless $debug;
closelog();

# EOF
