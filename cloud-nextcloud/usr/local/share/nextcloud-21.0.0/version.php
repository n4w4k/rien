<?php 
$OC_Version = array(21,0,0,18);
$OC_VersionString = '21.0.0';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '20.0' => true,
    '21.0' => true,
  ),
  'owncloud' => 
  array (
    '10.5' => true,
  ),
);
$OC_Build = '2021-02-19T08:52:18+00:00 8985b7930653139859002eb3eca2852999ed8f0d';
$vendor = 'nextcloud';
