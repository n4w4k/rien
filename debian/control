Source: rien
Section: admin
Priority: optional
Maintainer: Mathieu Roy <yeupou@gnu.org>
Standards-Version: 3.5.9.0


Package: rien-cloud
Architecture: all
Depends: rien-common, rien-nginx
Description: Cloud setup
 .
 Homepage: https://yeupou.wordpress.com/

Package: rien-cloud-nextcloud
Architecture: all
Replaces: stalag13-utils-cloud
Conflicts: stalag13-utils-cloud
Depends: rien-cloud
Description: Nextcloud files (deps not handled)
 After upgrade:
   su  --shell=/bin/bash www-data
   cd /usr/local/share/nextcloud
   php occ upgrade
 .
 Homepage: https://yeupou.wordpress.com/

Package: rien-common
Architecture: all
Replaces: stalag13-utils
Conflicts: stalag13-utils
Suggests: libimage-exiftool-perl, libtext-unaccent-perl
Depends: rien-keyring, rxvt-unicode-256color | rxvt-unicode, apt-transport-https, perl-base (>=5.6.0), perl-modules, coreutils (>=4.5.0) | fileutils (>=4.1), bash (>=2.04), gzip (>=1), libimage-exiftool-perl, libnet-dns-perl, libfile-find-rule-perl, libapt-pkg-perl, libterm-readkey-perl, libfile-homedir-perl, libtimedate-perl, debfoster, localepurge, etckeeper, libmoose-perl, liburi-encode-perl, libjson-perl, libio-interactive-perl, libhtml-tableextract-perl, libnumber-bytes-human-perl, libterm-readkey-perl, libdate-calc-perl, libcalendar-simple-perl, bash-completion, libio-interface-perl, dnsutils, libtext-unaccent-perl, rxvt-unicode-256color, libconfig-general-perl, libfile-chmod-perl
Description: Generally useful/less stuff
  .
  Homepage: https://yeupou.wordpress.com/

Package: rien-cozy
Architecture: all
Depends: cozy-stack, cozy-nsjail, cozy-couchdb, nodejs (>=12), logrotate, cron, certbot, python3-certbot-dns-gandi, rien-nginx
Description: Cozy cloud on Devuan LXC (without coclyco)
 .
 https://docs.cozy.io/en/tutorials/selfhost-debian/


Package: rien-desktop
Architecture: all
Depends: rien-common
Recommends: keepass2, keepass2-plugin-keepasshttp
Conflicts: stalag13-utils-ahem
Description: Useful/less desktop related scripts. Depends are set as suggestion.
 .
 Homepage: https://yeupou.wordpress.com

Package: rien-fetch
Architecture: all
Depends: links, notmuch, procmail
Recommends: lieer, offlineimap | mbsync | isync
Description: Fetching mails
 .
 Homepage: https://yeupou.wordpress.com/
  

Package: rien-host
Architecture: all
Depends: rien-nginx
Suggests: pdns-server, pdns-recursor, pdns-backend-sqlite3, mandos-client
Provides: dnsmasq-base
Conflicts: dnsmasq-base
Description: Host/master related files
 .
 Homepage: https://yeupou.wordpress.com/

Package: rien-mpc
Replaces: stalag13-utils-mpc
Conflicts: stalag13-utils-mpc
Architecture: all
Depends: rien-common, mpc, libnotify-bin, rxvt-unicode-256color, awesome, xcompmgr, redshift
Suggests: mpd
Description: Minimalistic mpc/mpd music player setup for Raspbian
  Supposed to work with both mpc and mpd on the same device, but theoretically requires only mpc.
  Need write access to /var/lib/mpd/playlists
  .
  Homepage: https://yeupou.wordpress.com/
  
Package: rien-mx
Architecture: all
Depends: rien-common, exim4-config (>=4.92), exim4-daemon-heavy (>=4.92), spamassassin | rspamd, spamc | rspamd, bogofilter | rspamd, dnsutils, procmail, memcached , libcache-memcached-perl, spf-tools-perl, dnsutils, procmail, libmail-imapclient-perl, libmime-tools-perl, libxml-simple-perl, libclass-dbi-mysql-perl, libio-socket-inet6-perl, libio-socket-ip-perl, libperlio-gzip-perl, libmail-mbox-messageparser-perl, unzip, libmail-gnupg-perl, libmime-tools-perl, dovecot-sieve (>=2.3.4), dovecot-core (>=2.3.4), dovecot-imapd (>=2.3.4), dovecot-lmtpd, clamav-daemon, redis-server
Conflicts: stalag13-utils-mx, stalag13-utils-exim
Replaces: stalag13-utils-mx, stalag13-utils-exim
Suggests: unison, offlineimap | mbsync | isync, stunnel4
Description: exim4 greylist, bogofilter and spamassassin setup
 .
 Homepage: https://yeupou.wordpress.com/

Package: rien-nginx
Architecture: all
Depends: nginx (>= 1.14)
Replaces: stalag13-utils-nginx
Conflicts: stalag13-utils-nginx
Description: General purpose nginx configurationfiles
  .
  Homepage

Package: rien-torrent
Architecture: all
Depends: rien-common, logrotate, transmission-cli, libfile-mimeinfo-perl, libfile-slurp-perl, libcapture-tiny-perl
Conflicts: stalag13-utils-torrent
Replaces: stalag13-utils-torrent
Description: Manage transmission via NFS/Samba
  Assume you already have networking file system up
  /home/torrent must be set, check https://yeupou.wordpress.com/?s=transmission
  .
  Homepage: https://yeupou.wordpress.com/

Package: rien-tumblr
Architecture: all
Depends: rien-common, libimage-exiftool-perl, liblwp-authen-oauth-perl, libnet-oauth-perl, libencode-detect-perl, libfont-freetype-perl, libtext-unaccent-perl, imagemagick, toot, ffmpeg
Conflicts: stalag13-utils-tumblr
Replaces: stalag13-utils-tumblr
Suggests: ttf-liberation
Description: Automated image post on Tumblr/Mastodon
 Check https://yeupou.wordpress.com/?s=tumblr beforehand
 .
 Homepage: https://yeupou.wordpress.com/


Package: rien-webmail
Architecture: all
Depends: rainloop | rien-webmail-snappymail | snappymail, rien-nginx, rien-common, php7.3-fpm | php7.4-fpm | php-fpm, php-xml, php-curl, php-gd, php-mbstring, php-json, php-intl, php-opcache | php8.1-opcache | php7.4-opcache, php-pspell, php-readline, php-sqlite3, php-xml, php-redis, php-zip, php-uuid, php-imagick, php-apcu
Conflicts: stalag13-utils-webmail
Replaces: stalag13-utils-webmail
Description: Webmail setup rainloop with nginx
 .
 Homepage: https://yeupou.wordpress.com/

Package: rien-webmail-snappymail
Architecture: all
Depends: php-redis, php-zip, php-uuid, php-imagick, php-apcu
Conflicts: snappymail
Description: Snappymail original files
 .
  Homepage: https://yeupou.wordpress.com/
   
Package: rien-zukowka
Architecture: all
Depends: gpsd, gpsd-clients, cutecom, navit, espeak-ng, mpv, mpd, mpc, awesome, awesome-extra, inotify-tools, libgtk3-perl, rien-desktop, rien-mpc, dmenu, breeze-icon-theme, libproc-processtable-perl, scrot, unclutter, xsel, slock, libconfig-tiny-perl, samba, breeze-icon-theme
Description: To be described
 .
  Homepage: https://zukowka.wordpress.com/
  




