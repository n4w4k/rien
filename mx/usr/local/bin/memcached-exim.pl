#!/usr/bin/perl
#
# mix of
# http://wiki.exim.org/GreylistMemcachedPerl
#  https://poolp.org/posts/2019-12-01/spf-aware-greylisting-and-filter-greylist/

#   "Then, you have the MX that do publish SPF records, which includes all Big Mailer Corps for obvious reasons. And for these, instead of greylisting by source address, we can do an SPF-aware greylisting for the domain.
#    Assuming this is the first mail we receive from an MX, when a new connection arrives, the filter tracks the source IP address. The SMTP session moves forward, initiates an SMTP transaction, and then the client provides the envelope-from (MAIL FROM):
#    At this point, the filter can do an SPF lookup for the envelope-from domain, poolp.org. If it doesn’t find an SPF record, it can simply keep track of the source address and issue a retry request.
#    If it finds an SPF record, it checks if the source IP address is valid. If it is not, then it supposedly doesn’t come from the sender domain, so the filter keeps track of the source address and issues a retry request. A stricter approach could be to reject the sender but I don’t think it’s the goal of a greylisting to check SPF validity.
#    If, however, the source IP address is valid for the SPF record, then instead of keeping track of the source address, the filter keeps track of the domain and issues a retry request."
#
# In the current setup, SPF was checked already. So it is meaningless in (local) DNS extra requests.


use Cache::Memcached;
use strict;
use Mail::SPF;

my $spfd = Mail::SPF::Server->new();
my $memd = _create_memcached();

sub check_greylist { 
    my $senderip = $_[0];
    my $senderdomain = $_[1];
    my $now = time();
    my $timeout = ( $_[2] || 5 ) * 60;      # Minutes to seconds, default 5
    my $expire = ( $_[3] || 10 ) *24*60*60;  # Days to seconds, default 7 (=> 10)

    # first check if the IP was greylisted
    my $key = $senderip;
    my $val = $memd->get($key);

    # then check if the domain was greylisted instead
    unless ($val) {
	# but only if the current IP SPF pass for this domain
	my $spfquery = Mail::SPF::Request->new(ip_address => $senderip, identity => $senderdomain);
	my $spf  = $spfd->process($spfquery);
	if ($spf->is_code('pass')) {
	    $key = $senderdomain;
	    $val = $memd->get($key);
	}
    }
    
    # if we found an entry, either for an IP or domain, proceed to update and
    # eventually accept it
    if ($val) {
	# Update expiration
	$memd->replace($key, $val, $now + $expire);
	# Has exceeded the timeout, don't defer it
	if ($now > $val + $timeout) {
	    #Exim::log_write("PASS GREYLIST: '$key'");
	    if ($key eq $senderdomain) {
		# if we approved a domain, also store/update the current IP so it'll skip further SPF tests
		$memd->set($senderip, $val, $now + $expire);
	    }
	    
	    return(0);
	}
	#Exim::log_write("CONTINUE GREYLIST: '$key' still greylisted for " . ($val + $timeout - $now) . " seconds");
	return(1);
    }
    else {
	# otherwise we have no entry yet
	# if the earlier SPF check passed, the key is the domain
	# otherwise we stick to the IP
	
	$memd->set($key, $now ,$now + $expire);
	#Exim::log_write("SET GREYLIST: '$key'");
	return(1);
    }
}

sub greylist_time {
    my $senderip = $_[0];
    my $senderdomain = $_[1];
    
    my $now = time();
    my $timeout = ( $_[2] || 5 ) * 60;      # Minutes to seconds, default 5

    # find out if the relevant key is the IP or the domain
    my $key = $senderip;
    my $val = $memd->get($key);
    unless ($val) {
	$key = $senderdomain;
	$val = $memd->get($key);
    }
    
    if (my $val = $memd->get($key)) {
	my $left = $val + $timeout - $now;
	$left = sprintf("%0i:%02i", int($left/60), $left % 60);
	return( $left);
    }
    # Should never get here if because this sub should never
    # have been called if there is no greylist record, but 
    # handle it by just printing default
    return( sprintf("%0i:%02i", int($timeout/60), $timeout % 60));
}

sub _create_memcached {
    my $config = "/etc/exim4/memcached.conf";
    my $namespace = 'exim:';  # set default namespace
    my $servers;
    if ( -f $config ) {
	open(my $fh, "<", $config);
	while(<$fh>) {
	    chomp($_);
	    if ( my ($arg,$val) = split(/=/,$_) ) {
		next if ( $arg =~ /#/ );
		$arg =~ s/\s+//;
		$val =~ s/\s+//;
		if ( $arg =~ /\bserver\b/ ) {
		    next if ( $val !~ /^[-\w\d\.]+:\d+$/ );
		    push( @$servers, $val );
		}
		elsif ( $arg =~ /\bnamespace\b/ ) {
		    next if ( $val !~ /^[-\w\d\.]+$/ );
		    $namespace = $val . ":";
		}
	    }
	}
	close $fh;
    }
    $servers ||= [ 'localhost:11211' ];  # Set a default if nothing specified
    my $m = Cache::Memcached->new( {
	'servers'            => $servers,
	'namespace'          => $namespace,
	'debug'              => 0,
	'compress_threshold' => 10_000
				   } );
    return($m);
}
