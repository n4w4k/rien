#; -*-sieve-*-
require ["editheader", "regex", "imap4flags", "vnd.dovecot.execute"];

# simple flagging for easy per-user sorting
# chained, so only a single X-Sieve-Mark is possible


## flag Spam
if anyof (
	  header :regex "X-Spam-Status" "^Yes",
	  header :regex "X-Spam-Flag" "^YES",
	  header :regex "X-Bogosity" "^Spam",
	  header :regex "X-Spam_action" "^reject")
{
  # flag for the mail client
  addflag "Junk";
  # header for further parsing
  addheader "X-Sieve-Mark" "Spam";
  # autolearn
  execute :pipe "sa-learn-spam.sh";
}
## sysadmin
elsif address :localpart ["from", "sender"] ["root", "netdata", "mailer-daemon"]
{
  addheader "X-Sieve-Mark" "Sysadmin";
} 
## institutional 
elsif address :domain :regex ["to", "from", "cc"] ["gouv\.fr$", 
						   "^doctolib\.fr$", 
						   "^ac\-.*.fr$", 
						   "^espace\-citoyen\.net$"]
{
  addheader "X-Sieve-Mark" "Institutional";
}
## social network
elsif address :domain :regex ["to", "from", "cc"] ["^twitter\.",
						   "^facebook\.",
						   "^youtube\.",
						   "^mastodon\.",
						   "instagram\."]
{
  addheader "X-Sieve-Mark" "SocialNetwork";
}
## computer related
elsif address :domain :regex ["to", "from", "cc"] ["debian\.",
						   "devuan\.",
						   "gnu\.",
						   "gitlab\.",
						   "github\."]
{
  addheader "X-Sieve-Mark" "Cpu";
}
## shop alias
elsif address :domain :regex ["to", "from", "cc"] ["^plow\.", "^spm\."]
{
  addheader "X-Sieve-Mark" "Shop";
}


## EOF
