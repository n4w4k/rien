#; -*-sieve-*-
# require ["fileinto", "regex", "imap4flags"];
#
# ## (alternative to marking spam)
# ## redirect Spam and stop processing
# # assume Spam goes into a dedicated Spam box, flagged
# if anyof (
# 	header :regex "X-Spam-Status" "^Yes",
# 	header :regex "X-Spam-Flag" "^YES",
# 	header :regex "X-Bogosity" "^Spam")
# {
# 	addflag "Junk";
# 	#not marking as read to ease false positive issues: addflag "\\Seen";
# 	fileinto "Spam";
# 	stop;
# }

require ["regex", "editheader", "variables"];

# messages sent to specific spam catchall domain will be marked as suspicious
# if sender domain is unrelated to catchall string
if address :domain :regex ["to", "cc", "bcc"] ["^spm\."]
{
  if address :localpart :regex ["to", "cc", "bcc"] ["^(.*)-(for|dla|pour)-\w*"] {
      set "spamdomain" "${1}";
      if address :domain :contains "from" "${spamdomain}"
      {
	  addheader "X-Spam-Domain" "Clear";
      }
      else
      {
	  addheader "X-Spam-Domain" "Suspicious (${spamdomain})";
      }      
    }  
}

# EOF

