#!/bin/sh
# beware: "restarting dovecot holds stderr open, this blocks python"
# (https://stackoverflow.com/questions/48976571/certbot-renewal-hook-wont-finish)
invoke-rc.d dovecot restart 2>/dev/null &
