--[[

   Powerarrow Awesome WM theme
   github.com/lcpz

   adjusted for zukowka.wordpress.com

--]]

local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local dpi   = require("beautiful.xresources").apply_dpi

local math, string, os = math, string, os
local my_table = awful.util.table or gears.table -- 4.{0,1} compatibility

local theme                                     = {}
theme.dir                                       = "/usr/share/awesome/themes/powerarrow-zukowka"
theme.wallpaper                                 = "/usr/share/awesome/themes/default/background.png"
theme.font                                      = "Terminus 13"
theme.fg_normal                                 = "#FEFEFE"
theme.fg_focus                                  = "#32D6FF"
theme.fg_urgent                                 = "#C83F11"
theme.bg_normal                                 = "#222222"
theme.bg_focus                                  = "#1E2320"
theme.bg_urgent                                 = "#3F3F3F"
theme.taglist_fg_focus                          = "#00CCFF"
theme.tasklist_bg_focus                         = "#222222"
theme.tasklist_fg_focus                         = "#00CCFF"
theme.border_width                              = dpi(2)
theme.border_normal                             = "#3F3F3F"
theme.border_focus                              = "#6F6F6F"
theme.border_marked                             = "#CC9393"
theme.titlebar_bg_focus                         = "#3F3F3F"
theme.titlebar_bg_normal                        = "#3F3F3F"
theme.titlebar_bg_focus                         = theme.bg_focus
theme.titlebar_bg_normal                        = theme.bg_normal
theme.titlebar_fg_focus                         = theme.fg_focus
theme.menu_height                               = dpi(22)
theme.menu_width                                = dpi(500)
theme.menu_submenu_icon                         = theme.dir .. "/icons/submenu.png"
theme.awesome_icon                              = theme.dir .. "/icons/zukowka.png"
theme.taglist_squares_sel                       = theme.dir .. "/icons/square_sel.png"
theme.taglist_squares_unsel                     = theme.dir .. "/icons/square_unsel.png"
theme.layout_tile                               = theme.dir .. "/icons/tile.png"
theme.layout_tileleft                           = theme.dir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.dir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.dir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.dir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.dir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.dir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.dir .. "/icons/dwindle.png"
theme.layout_max                                = theme.dir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.dir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.dir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.dir .. "/icons/floating.png"
theme.widget_ac                                 = theme.dir .. "/icons/ac.png"
theme.widget_battery                            = theme.dir .. "/icons/battery.png"
theme.widget_battery_low                        = theme.dir .. "/icons/battery_low.png"
theme.widget_battery_empty                      = theme.dir .. "/icons/battery_empty.png"
theme.widget_brightness                         = theme.dir .. "/icons/brightness.png"
theme.widget_mem                                = theme.dir .. "/icons/mem.png"
theme.widget_cpu                                = theme.dir .. "/icons/cpu.png"
theme.widget_temp                               = theme.dir .. "/icons/temp.png"
theme.widget_net                                = theme.dir .. "/icons/net.png"
theme.widget_hdd                                = theme.dir .. "/icons/hdd.png"
theme.widget_music                              = theme.dir .. "/icons/note.png"
theme.widget_music_on                           = theme.dir .. "/icons/note_on.png"
theme.widget_music_pause                        = theme.widget_music
theme.widget_music_stop                         = theme.dir .. "/icons/stop.png"
theme.widget_vol                                = theme.dir .. "/icons/vol.png"
theme.widget_vol_low                            = theme.dir .. "/icons/vol_low.png"
theme.widget_vol_no                             = theme.dir .. "/icons/vol_no.png"
theme.widget_vol_mute                           = theme.dir .. "/icons/vol_mute.png"
theme.widget_mail                               = theme.dir .. "/icons/mail.png"
theme.widget_mail_on                            = theme.dir .. "/icons/mail_on.png"
theme.widget_task                               = theme.dir .. "/icons/task.png"
theme.widget_scissors                           = theme.dir .. "/icons/scissors.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = 0
theme.titlebar_close_button_focus               = theme.dir .. "/icons/titlebar/close_focus.png"
theme.titlebar_close_button_normal              = theme.dir .. "/icons/titlebar/close_normal.png"
theme.titlebar_ontop_button_focus_active        = theme.dir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active       = theme.dir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive      = theme.dir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive     = theme.dir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_sticky_button_focus_active       = theme.dir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active      = theme.dir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive     = theme.dir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive    = theme.dir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_floating_button_focus_active     = theme.dir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active    = theme.dir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive   = theme.dir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive  = theme.dir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_maximized_button_focus_active    = theme.dir .. "/icons/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active   = theme.dir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.button_voldown = theme.dir .. "/icons/Enlightenment-X/audio-volume-low.png"
theme.button_volup = theme.dir .. "/icons/Enlightenment-X/audio-volume-high.png"
theme.button_play = theme.dir .. "/icons/Enlightenment-X/media-seek-forward.png"
theme.button_pause = theme.dir .. "/icons/Enlightenment-X/media-playback-pause.png"
theme.button_shutdown = theme.dir .. "/icons/Enlightenment-X/system-shutdown.png"
theme.button_resizel = theme.dir .. "/icons/Enlightenment-X/view-fullscreen-left.png"
theme.button_resizer = theme.dir .. "/icons/Enlightenment-X/view-fullscreen-right.png"

local markup = lain.util.markup
local separators = lain.util.separators

---- TOP PANEL
-- Textclock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local clock = awful.widget.watch(
   "date +'%R'", 60,
   function(widget, stdout)
      widget:set_markup(" " .. markup.font(theme.font, stdout))
   end
)

-- ALSA volume
local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.alsa({
      settings = function()
	 if volume_now.status == "off" then
            volicon:set_image(theme.widget_vol_mute)
	 elseif tonumber(volume_now.level) == 0 then
            volicon:set_image(theme.widget_vol_no)
	 elseif tonumber(volume_now.level) <= 50 then
            volicon:set_image(theme.widget_vol_low)
	 else
            volicon:set_image(theme.widget_vol)
	 end

	 widget:set_markup(markup.font(theme.font, " " .. volume_now.level .. "% "))
      end
})
theme.volume.widget:buttons(awful.util.table.join(
                               awful.button({}, 4, function ()
                                     awful.util.spawn("amixer set Master 1%+")
                                     theme.volume.update()
                               end),
                               awful.button({}, 5, function ()
                                     awful.util.spawn("amixer set Master 1%-")
                                     theme.volume.update()
                               end)
))


-- MPD
local musicplr = awful.util.terminal .. " -title Music -g 130x34-320+16 -e ncmpcpp"
local mpdicon = wibox.widget.imagebox(theme.widget_music)
mpdicon:buttons(my_table.join(
		   awful.button({ modkey }, 1, function () awful.spawn.with_shell(musicplr) end),
		   awful.button({ }, 1, function ()
			 os.execute("mpc prev")
			 theme.mpd.update()
		   end),
		   awful.button({ }, 2, function ()
			 os.execute("mpc toggle")
			 theme.mpd.update()
		   end),
		   awful.button({ }, 3, function ()
			 os.execute("mpc next")
			 theme.mpd.update()
end)))
theme.mpd = lain.widget.mpd({
      settings = function()
	 if mpd_now.state == "play" then
            artist = " " .. mpd_now.artist .. " "
            title  = mpd_now.title  .. " "
            mpdicon:set_image(theme.widget_music_on)
            widget:set_markup(markup.font(theme.font, markup("#FF8466", artist) .. " " .. title))
	 elseif mpd_now.state == "pause" then
            widget:set_markup(markup.font(theme.font, " mpd paused "))
            mpdicon:set_image(theme.widget_music_pause)
	 else
            widget:set_text("")
            mpdicon:set_image(theme.widget_music)
	 end
      end
})

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
      settings = function()
	 widget:set_markup(markup.font(theme.font, " " .. cpu_now.usage .. "% "))
      end
})

-- Coretemp (lain, average)
local temp = awful.widget.watch(
   'bash -c "/usr/bin/sensors | grep temp1 | awk -F \'+\' \'{print $2}\'"', 60,
   function(widget, stdout)
      widget:set_markup(" " .. markup.font(theme.font, stdout))
   end
)
local tempicon = wibox.widget.imagebox(theme.widget_temp)

-- Net
-- FIXME: replace by quickfix /GPS  + on click info: wifi / bluetooth / network / GPS
local gpsicon = wibox.widget.imagebox(theme.widget_net)
local gps = awful.widget.watch(
   "/usr/local/bin/gps-info", 30,
   function(widget, stdout)
      widget:set_markup(" " .. markup.font(theme.font, stdout))
   end
)

-- Brigtness
local brighticon = wibox.widget.imagebox(theme.widget_brightness)
-- If you use xbacklight, comment the line with "light -G" and uncomment the line bellow
-- local brightwidget = awful.widget.watch('xbacklight -get', 0.1,
local brightwidget = awful.widget.watch('light -G', 0.1,
					function(widget, stdout, stderr, exitreason, exitcode)
					   local brightness_level = tonumber(string.format("%.0f", stdout))
					   widget:set_markup(markup.font(theme.font, " " .. brightness_level .. "%"))
end)

-- Separators
local arrow = separators.arrow_left

function theme.powerline_rl(cr, width, height)
   local arrow_depth, offset = height/2, 0

   -- Avoid going out of the (potential) clip area
   if arrow_depth < 0 then
      width  =  width + 2*arrow_depth
      offset = -arrow_depth
   end

   cr:move_to(offset + arrow_depth         , 0        )
   cr:line_to(offset + width               , 0        )
   cr:line_to(offset + width - arrow_depth , height/2 )
   cr:line_to(offset + width               , height   )
   cr:line_to(offset + arrow_depth         , height   )
   cr:line_to(offset                       , height/2 )

   cr:close_path()
end

local function pl(widget, bgcolor, padding)
   return wibox.container.background(wibox.container.margin(widget, dpi(22), dpi(22)), bgcolor, theme.powerline_rl)
end

---- BOTTOM PANEL
-- Menu
local mylauncher = awful.widget.button({ image = theme.awesome_icon })
mylauncher:connect_signal("button::press", function() awful.util.mymainmenu:toggle() end)


-- volume/mpd
local mybutton_voldown = awful.widget.launcher({ image = theme.button_voldown,
						 command = "/usr/local/bin/zukowka-shortcuts-cli.pl voldown" })
local mybutton_volup = awful.widget.launcher({ image = theme.button_volup,
						 command = "/usr/local/bin/zukowka-shortcuts-cli.pl volup" })
local mybutton_pause = awful.widget.launcher({ image = theme.button_pause,
						 command = "/usr/local/bin/zukowka-shortcuts-cli.pl pause" })
local mybutton_play = awful.widget.launcher({ image = theme.button_play,
						 command = "/usr/local/bin/zukowka-shortcuts-cli.pl play" })
local mybutton_shutdown = awful.widget.launcher({ image = theme.button_shutdown,
						 command = "/usr/local/bin/zukowka-shortcuts-cli.pl shutdown" })


-- change layout
local mybutton_resizel = wibox.widget{
   image = icon,
   resize = true,
   forced_height = 64,
   widget = wibox.widget.imagebox(theme.button_resizel)
}
mybutton_resizel:connect_signal("button::press",
			      function()
				 awful.tag.incmwfact( 0.25)
			      end
)

local mybutton_resizer = wibox.widget{
   image = icon,
   resize = true,
   forced_height = 64,
   widget = wibox.widget.imagebox(theme.button_resizer)
}
mybutton_resizer:connect_signal("button::press",
			      function()
				 awful.tag.incmwfact(-0.25)
			      end
)

local sep = wibox.widget {
    widget = wibox.widget.separator,
    orientation = "vertical",
    forced_width = 25,
    color = theme.bg_normal
}



---- PANELS SETUP 
function theme.at_screen_connect(s)
   -- If wallpaper is a function, call it with the screen
   local wallpaper = theme.wallpaper
   if type(wallpaper) == "function" then
      wallpaper = wallpaper(s)
   end
   gears.wallpaper.maximized(wallpaper, s, true)

   -- Tags
   awful.tag(awful.util.tagnames, s, awful.layout.layouts)

   -- Create a promptbox for each screen
   s.mypromptbox = awful.widget.prompt()

   -- Create an imagebox widget which will contains an icon indicating which layout we're using.
   -- We need one layoutbox per screen.
   s.mylayoutbox = awful.widget.layoutbox(s)
   s.mylayoutbox:buttons(awful.util.table.join(
			    awful.button({ }, 1, function () awful.layout.inc( 1) end),
			    awful.button({ }, 3, function () awful.layout.inc(-1) end),
			    awful.button({ }, 5, function () awful.layout.inc( 1) end),
			    awful.button({ }, 4, function () awful.layout.inc(-1) end)))

   -- Create a taglist widget
   s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

   -- Create a tasklist widget
   -- FIXME: probably best to have only icons or nothing at all
   s.mytasklist = awful.widget.tasklist(s, function(c,screen) return not awful.widget.tasklist.filter.currenttags(c, screen) end, awful.util.tasklist_buttons)

   -- Create the top wibox
   s.mywibox = awful.wibar({ position = "top", screen = s, height = dpi(14), bg = theme.bg_normal, fg = theme.fg_normal })

   -- Add widgets to the top wibox
   s.mywibox:setup {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
	 layout = wibox.layout.fixed.horizontal,
	 spr,
	 wibox.widget.systray(),
      },
      s.mypromptbox,-- Middle widget
      { -- Right widgets
	 layout = wibox.layout.fixed.horizontal,
	 arrow(theme.bg_normal, "#343434"),
	 wibox.container.background(wibox.container.margin(wibox.widget { mpdicon, theme.mpd.widget, layout = wibox.layout.align.horizontal }, dpi(3), dpi(6)), "#343434"),
	 arrow("#343434", "#CB755B"),
	 wibox.container.background(wibox.container.margin(wibox.widget { volicon, theme.volume.widget, layout  = wibox.layout.align.horizontal }, dpi(3), dpi(6)), "#CB755B"),
	 arrow("#CB755B", "#4B696D"),
	 wibox.container.background(wibox.container.margin(wibox.widget { cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, dpi(3), dpi(4)), "#4B696D"),
	 arrow("#4B696D", "#4B3B51"),
	 wibox.container.background(wibox.container.margin(wibox.widget { tempicon, temp, layout = wibox.layout.align.horizontal }, dpi(4), dpi(4)), "#4B3B51"),
	 arrow("#4B3B51", "#4f2bae"),
	 wibox.container.background(wibox.container.margin(wibox.widget { gpsicon, gps, layout = wibox.layout.align.horizontal }, dpi(4), dpi(4)), "#4f2bae"),	    
	 arrow("#4f2bae", "#777E76"),
	 wibox.container.background(wibox.container.margin(clock, dpi(4), dpi(8)), "#777E76"),
	 arrow("#777E76", "alpha"),
      },
   }
   
   -- Create the bottom wibox
   s.mybottomwibox = awful.wibar({ position = "bottom", screen = s, border_width = 0, height = dpi(64), bg = theme.bg_normal, fg = theme.fg_normal })

   -- Add widgets to the bottom wibox
   s.mybottomwibox:setup {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
	 layout = wibox.layout.fixed.horizontal,
	 mybutton_voldown,
	 mybutton_volup,
	 sep,
	 mybutton_pause,
	 mybutton_play,
	 sep,
	 mybutton_shutdown,
      },
      s.mypromptbox,-- Middle widget
      { -- Right widgets
	 layout = wibox.layout.fixed.horizontal,
	 mybutton_resizel,
	 mybutton_resizer,
	 s.mytaglist,
	 mylauncher,
      },
   }
end

return theme
