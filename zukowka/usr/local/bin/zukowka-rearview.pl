#!/usr/bin/perl
#
# Copyright (c) 2021 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# make sure we have mpv running on a valid webcam

use strict;
use Fcntl qw(:flock);
use Proc::ProcessTable;
use Cwd qw(realpath);

# do not keep track of mpv childs
$SIG{CHLD} = 'IGNORE';

# silently forbid concurrent runs
# (return error, so chained commands stops)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit(1);

my $debug = 0;
my $wait = 10;
$wait = 1 if $debug;

my $mpv_bin = "/usr/bin/mpv";
my @mpv_options = ("--demuxer-lavf-o-set=input_format=mjpeg",
			  "--no-audio",
			  "--vo=gpu",
			  "--profile=low-latency",
			  "--untimed",
			  "--fps=30",
			  "--loop",
			  "--no-border",
			  "--no-osc",
			  "--quiet");
my @mpv_placeholder_options = ("--player-operation-mode=pseudo-gui");



sub debug {
    return unless $debug;
    print $_[0]."\n";
}


# if mpv /dev/video* is running, check if the relevant device is still on
sub mpv_check {
    debug("mpv_check()");
    my $active = 1;
    my $pid;
    while ($active) {
	sleep($wait);
	# reset active flag 
	$active = 0;
	foreach my $process (@{Proc::ProcessTable->new->table}) {
	    # ignore other users processes 
	    next unless $> eq $process->uid;
	    
	    # only the bin and (device) first argument matters
	    my ($bin, $arg1,) = split(" ", $process->cmndline);
	    next unless $bin eq $mpv_bin;
	    next unless $arg1 =~ m/\/dev\/video\d/;
	    $pid = $process->pid;
	    debug("  found command $bin $arg1 (pid $pid)");
	    
	    # the device must still exists
	    next unless -e $arg1;
	    debug("  device $arg1 still exists");

	    $active = 1;	    
	    last;
	}	
    }

    # not active
    # make sure we dont have a stale process
    if ($pid) {
	debug("  kill stale $pid");
	kill(9, $pid);
    }
    # and start mpv
    mpv_start();       
}

# mpv is off, we need to find the relevant device to start it, if any
my $placeholder;
sub mpv_start {
    debug("mpv_start()");
    # use /dev/v4l/by-id/ to identify devices instead of /dev/video* since
    # recent v4l use video10, etc that are not webcam per se
    my @devices = (sort(glob('/dev/v4l/by-id/*')));
    my $device;
    
    # no devices found
    if (@devices < 1) {
	# start mpv in placeholder mode / loop
	debug("  no device");
	unless ($placeholder) {
	    debug("  start mpv placeholder");
	    $placeholder = fork();
	    if (defined $placeholder && $placeholder == 0) {
		exec($mpv_bin, @mpv_placeholder_options);
		exit 0;
	    }
	}
	sleep($wait);
	mpv_start();
    }
    
    # single device might have several devices 
    # (see https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=088ead25524583e2200aa99111bea2f66a86545a )
    # keep it simple and assume we always want the first one,
    # otherwise, it would, for example, require to parse output of v4l2-ctl --list-formats --device=/dev/video0
    ($device) = @devices;
    debug("  found $device, will start mpv as fork");
    # if symlink, resolve the linkl
    $device = realpath($device) if -l $device;
    debug("  device is in fact $device");

    # remove possible mpv placeholder process
    if ($placeholder) {
	debug("  kill stale $placeholder");
	kill(9, $placeholder);
	$placeholder = "";
    }
    
    # fork
    my $pid = fork();
    if (defined $pid && $pid == 0) {
	exec($mpv_bin, $device, @mpv_options);
	exit 0;
    }    
    mpv_check();
}

# start by check if mpv is on
mpv_check();


# EOF
