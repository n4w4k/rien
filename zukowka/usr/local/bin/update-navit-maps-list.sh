#!/bin/bash
BINDIR=/data/maps
XMLDIR=/usr/share/navit/maps
# indiscriminately erase previous config
rm -v $XMLDIR/*.xml
# register existing bins
for bin in $BINDIR/*.bin; do
    echo '<map type="binfile" enabled="yes" data="'$bin'" />' > $XMLDIR/${bin##*/}.xml
done
