#!/usr/bin/perl
#
# Copyright (c) 2021 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Gtk3 '-init';
use Glib qw/TRUE FALSE/;

my $iconset = "/usr/share/icons/breeze-dark";
my %icon = ("volume" => "$iconset/status/symbolic/audio-volume-medium-symbolic.svg",
	    "volume_fallback" => "/usr/share/icons/PiXflat/48x48/status/audio-volume-medium.png",
	    "mpc" => "$iconset/symbolic/media-seek-forward-symbolic.svg",
	    "mpc_fallback" => "/usr/share/icons/Adwaita/48x48/legacy/media-seek-forward.png",
	    "shutdown" => "$iconset/actions/symbolic/system-shutdown-symbolic.svg",
	    "shutdown_fallback" => "/usr/share/icons/PiXflat/48x48/actions/system-shutdown.png");
my $debug = 0;
my $mixermaster = "Master";  # usually "Master", "Digital" on IQaudIODAC

# functions
sub zukowka_icon {
    return $icon{$_[0]} if -e $icon{$_[0]};
    return $icon{$_[0]."_fallback"};
}

sub zukowka_volume {
    my ($widget, $command) = @_;
    system("/usr/bin/notify-send", "2%$command", "--expire-time=200", "--icon=".zukowka_icon("volume"));
    system("/usr/bin/amixer", "sset", "$mixermaster,0", "2%$command") unless $debug;
    return FALSE;
}

sub zukowka_mpc {
    # fool-proof, readd any files and start play
    system("/usr/bin/notify-send", "lecture", "--expire-time=200", "--icon=".zukowka_icon("mpc"));
    system("/usr/bin/mpc", "add", "/") unless $debug;
    system("/usr/bin/mpc", "play") unless $debug;
    system("/usr/bin/mpc", "next") unless $debug;
    return FALSE;
}

sub zukowka_shutdown {
    system("/usr/bin/notify-send", "extinction", "--expire-time=500", "--icon=".zukowka_icon("shutdown"));
    system("/usr/bin/systemctl", "poweroff", "-i") unless $debug;
    Gtk3->main_quit;
    return FALSE;
}

# build window
my $window = Gtk3::Window->new('toplevel');
$window->set_title("zukowka-shortcuts");
$window->set_default_size(640, 50);
$window->signal_connect (delete_event => sub { Gtk3->main_quit });


# volume up
# volume down
my $button_volume_down = Gtk3::Button->new_from_icon_name('audio-volume-low', "LARGE_TOOLBAR");
$button_volume_down->signal_connect (clicked => \&zukowka_volume, "-");
my $button_volume_up = Gtk3::Button->new_from_icon_name('audio-volume-high', "LARGE_TOOLBAR");
$button_volume_up->signal_connect (pressed => \&zukowka_volume, "+");

# next
my $button_play = Gtk3::Button->new_from_icon_name('media-seek-forward', "LARGE_TOOLBAR");
$button_play->signal_connect (pressed => \&zukowka_mpc);

# shutdown
my $button_shutdown = Gtk3::Button->new_from_icon_name('system-shutdown', "LARGE_TOOLBAR");
$button_shutdown->signal_connect (pressed => \&zukowka_shutdown);

# reorganize layout button?



# pack buttons in horizontal bar
my $hbox = Gtk3::HBox->new(0, 1);
$hbox->pack_start($button_volume_down, TRUE, TRUE, 0);
$hbox->pack_start($button_volume_up, TRUE, TRUE, 0);
$hbox->pack_start($button_play, TRUE, TRUE, 0);
$hbox->pack_start($button_shutdown, TRUE, TRUE, 0);
$window->add($hbox);

# print windows
$window->show_all;
Gtk3->main;


# EOF 
