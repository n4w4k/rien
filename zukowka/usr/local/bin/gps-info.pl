#!/usr/bin/perl
# 
# Copyright (c) 2021 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
# based on
# Copyright 2004 through the last date of modification Charles Curley,
# http://www.charlescurley.com/.
# http://www.rtconnect.net/~ccurley/blog/articles/gpsdate_get_the_date_from_a_gps_receiver/index.html
# 
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

use Getopt::Std;
use Net::GPSD3;
use strict;
use File::HomeDir;

# $host is the name or IP address of the host. It may be local or
# remote. e.g: localhost. User configurable at the command line.
my $host = 'localhost';
my $port = 'gpsd(2947)';
my $verbose = 0;
my $redshiftrc = File::HomeDir->my_home."/.config/redshift/redshift.conf";

# Originally from the Net::GPSD3 man page.
sub myHandler {
  my $object = shift;

  # use Data::Dumper qw{Dumper};
  # print Dumper($object);

  # print ("Class is $object->{'class'}\n");

  if ($object->{'class'} eq 'VERSION' && $verbose) {
    # Show the version of the host.
    # print ($object->{'string'} . "\n");
    print "gpsd release: $object->{'release'}, rev: $object->{'rev'}";
    print ", protocol $object->{'proto_major'}.$object->{'proto_minor'}\n";

  } elsif ($object->{'class'} eq 'TPV') {
    # We have a Time, Position, Velocity report
    print $object->{'string'} . "\n" if $verbose;
    if ($object->{'mode'} > 1) {
        # Mode is 3, i.e. a 3D fix.
	print $object->{'mode'}."D, ".int($object->{'speed'}) . " km/h";
	print ", ".int($object->{'altHAE'})." m" if $object->{'mode'} > 2;
	print "\n";
	# update redshift manual config, requiring the following section to make any effect:
	# [redshift]
	# location-provider=manual
	if (-e $redshiftrc) {
	    use Config::Tiny;
	    my $Config = Config::Tiny->read($redshiftrc);
	    $Config->{manual}->{lat} = $object->{'lat'};
	    $Config->{manual}->{lon} = $object->{'lon'};
	    $Config->write($redshiftrc);
	    print "wrote $redshiftrc\n" if $verbose;
	}	
        exit (0);
    } else {
      print "néant\n";
      exit (0);
    }	
  }
}

my $gpsd=Net::GPSD3->new(host=>$host, port=>$port);
$gpsd->addHandler(\&myHandler);
$gpsd->watch;  

exit (0);

# EOF

