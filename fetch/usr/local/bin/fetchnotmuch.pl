#!/usr/bin/perl
#
# Copyright (c) 2022 Mathieu Roy <yeupou--gnu.org>
#        http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, wrte to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Sys::Syslog qw(:standard); 
use Fcntl ':flock';
use File::HomeDir;
use File::Basename;
use Term::ANSIColor qw(:constants);

my $bin_offlineimap = "/usr/bin/offlineimap";
my $bin_gmi = "/usr/bin/gmi";
my $bin_mbsync = "/usr/bin/mbsync";
my $bin_interimap = "/usr/bin/interimap";
my $bin_notmuch = "/usr/bin/notmuch";
my $timelimit = "2month";  # wont take into account mails older than
my $localtag = "Grabbed";  # specific extra tag to grabbed messages
my $search_forward = "not tag:$localtag and date:$timelimit..now";
my $search_cleanup = "not tag:flagged and date:..$timelimit"; # set to 0 if you do not want cleanup


### INIT
# silently forbid concurrent runs, per user
my $lock  = File::HomeDir->my_home()."/.fetchnotmuch.lock";
open my $flock, ">", $lock or die "Failed to ask lock. Exit";
flock($flock, LOCK_EX | LOCK_NB) or exit;

## read config
# get list of initialized boxes as shown in
# with simple syntax: gmi:boxname offlineimap:boxname 
# https://yeupou.wordpress.com/2020/11/09/fetching-mails-from-gmail-com-with-lieer-notmuch-instead-of-fetchmail/
my $rc = File::HomeDir->my_home()."/.fetchnotmuchrc";
die "Unable to read $rc (list of each boxes to deal with, space separated), exiting" unless -r $rc;
open(RCFILE, "< $rc");
my @boxes;
while(<RCFILE>){
    next if /~#/;
    push(@boxes, split(" ", $_));
}
close(RCFILE);

## Now run

# open log
openlog(basename($0), 'nofatal', 'mail');
my $username = getpwuid($<);

# first clean old messages
# (by default, older than 2 month old and not marked as important
if ($search_cleanup) {
    my $count;
    open(FILES, "$bin_notmuch search --format=text --output=files $search_cleanup |");
    while (<FILES>) {
	chomp();
	unlink($_);
	$count++;
    }
    syslog('info', "deleted %s outdated messages on %s behalf", $count, $username) if $count;
}


# Note: right now, run every task for each box; but offlineimap could do the update
# for all boxes at once. To check if X-Envelope-To stays accurate no matter what
# 
# care only for messages that no more than 2 month old (or any other defined $timelimit)
# process any that does not have a specific mark, mark as read and add the specific mark
# not much does not erase
foreach my $entry (@boxes) {
    my ($backend, $box) = split(":", $entry);
    die "Configured $entry broken, exiting" unless $backend and $box;
    chdir(File::HomeDir->my_home()."/mail/$box") or die "Configured $box missing, exiting";    
    print "$box ($backend):\n";
    
    # backend grab
    system($bin_gmi, "pull", "--quiet") if $backend eq "gmi";
    system($bin_offlineimap, "-o", "-a", $box) if $backend eq "offlineimap";
    system($bin_mbsync, "--pull", $box) if $backend eq "mbsync";
    system($bin_notmuch, "new", "--quiet");

    # read and forward mails not previously obtained
    # (not by message-id, because they can be tailored by the sender unlike filename)
    open(FILES, "$bin_notmuch search --format=text --output=files $search_forward |");
    while (<FILES>) {
	chomp();
	my $file = $_;
	
	# read file, forward with procmail
	open(READ, "< $file");
	open(SEND, "| /usr/bin/procmail");

	# add extra header to ease user sieve filtering
	print SEND "X-Envelope-To: $box\n";
	
	# forward content
	while (<READ>) {
	    print SEND $_;
	}
	close(SEND);
	close(READ);

	# log
	syslog('info', "forward %s@%s to %s", basename($file), $box, $username);
	
    }

    # mark as read and specific tag
    # (cannot use $file, so as a global search - there will be mistakes if another process
    # add new files in between)
    system($bin_notmuch, "tag", "+$localtag", "-unread", $search_forward);

    # send back to original server tags
    system($bin_gmi, "push", "--quiet") if $backend eq "gmi";
    system($bin_mbsync, "--push", $box) if $backend eq "mbsync";
}


closelog();
# EOF
