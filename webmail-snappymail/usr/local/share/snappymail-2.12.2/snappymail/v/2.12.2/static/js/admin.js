/* SnappyMail Webmail (c) SnappyMail | Licensed under AGPL v3 */
(function () {
	'use strict';

	/* eslint quote-props: 0 */

	/**
	 * @enum {string}
	 */
	const Capa = {
		GnuPG: 'GNUPG',
		OpenPGP: 'OPEN_PGP',
		Prefetch: 'PREFETCH',
		Contacts: 'CONTACTS',
		Search: 'SEARCH',
		SearchAdv: 'SEARCH_ADV',
		MessageActions: 'MESSAGE_ACTIONS',
		AttachmentsActions: 'ATTACHMENTS_ACTIONS',
		DangerousActions: 'DANGEROUS_ACTIONS',
		Themes: 'THEMES',
		UserBackground: 'USER_BACKGROUND',
		Sieve: 'SIEVE',
		AttachmentThumbnails: 'ATTACHMENT_THUMBNAILS',
		AutoLogout: 'AUTOLOGOUT',
		Kolab: 'KOLAB',
		AdditionalAccounts: 'ADDITIONAL_ACCOUNTS',
		Identities: 'IDENTITIES'
	};

	/**
	 * @enum {string}
	 */
	const Scope = {
		All: 'all',
		None: 'none',
		Contacts: 'Contacts',
		MessageList: 'MessageList',
		FolderList: 'FolderList',
		MessageView: 'MessageView',
		Compose: 'Compose',
		Settings: 'Settings',
		Menu: 'Menu',
		OpenPgpKey: 'OpenPgpKey',
		KeyboardShortcutsHelp: 'KeyboardShortcutsHelp',
		Ask: 'Ask'
	};

	/**
	 * @enum {number}
	 */
	const SaveSettingsStep = {
		Animate: -2,
		Idle: -1,
		TrueResult: 1,
		FalseResult: 0
	};

	/**
	 * @enum {number}
	 */
	const Notification = {
		RequestError: 1,
		RequestAborted: 2,

		// Global
		InvalidToken: 101,
		AuthError: 102,

		// User
		ConnectionError: 104,
		DomainNotAllowed: 109,
		AccountNotAllowed: 110,

		ContactsSyncError: 140,

		CantGetMessageList: 201,
		CantGetMessage: 202,
		CantDeleteMessage: 203,
		CantMoveMessage: 204,
		CantCopyMessage: 205,

		CantSaveMessage: 301,
		CantSendMessage: 302,
		InvalidRecipients: 303,

		CantSaveFilters: 351,
		CantGetFilters: 352,
		CantActivateFiltersScript: 353,
		CantDeleteFiltersScript: 354,
	//	FiltersAreNotCorrect: 355,

		CantCreateFolder: 400,
		CantRenameFolder: 401,
		CantDeleteFolder: 402,
		CantSubscribeFolder: 403,
		CantUnsubscribeFolder: 404,
		CantDeleteNonEmptyFolder: 405,

	//	CantSaveSettings: 501,

		DomainAlreadyExists: 601,

		DemoSendMessageError: 750,
		DemoAccountError: 751,

		AccountAlreadyExists: 801,
		AccountDoesNotExist: 802,
		AccountSwitchFailed: 803,

		MailServerError: 901,
		ClientViewError: 902,
		InvalidInputArgument: 903,

		JsonFalse: 950,
		JsonParse: 952,
	//	JsonTimeout: 953,

		UnknownNotification: 998,
		UnknownError: 999,

		// Admin
		CantInstallPackage: 701,
		CantDeletePackage: 702,
		InvalidPluginPackage: 703,
		UnsupportedPluginPackage: 704,
		CantSavePluginSettings: 705
	};

	let keyScopeFake = Scope.All;

	const doc = document,

		$htmlCL = doc.documentElement.classList,

		elementById = id => doc.getElementById(id),

		Settings = rl.settings,
		SettingsGet = Settings.get,
		SettingsCapa = Settings.capa,

		dropdownVisibility = ko.observable(false).extend({ rateLimit: 0 }),

		moveAction = ko.observable(false),
		leftPanelDisabled = ko.observable(false),

		createElement = (name, attr) => {
			let el = doc.createElement(name);
			attr && Object.entries(attr).forEach(([k,v]) => el.setAttribute(k,v));
			return el;
		},

		fireEvent = (name, detail) => dispatchEvent(new CustomEvent(name, {detail:detail})),

		// keys
		keyScopeReal = ko.observable(Scope.All),
		keyScope = value => {
			if (value) {
				if (Scope.Menu !== value) {
					keyScopeFake = value;
					if (dropdownVisibility()) {
						value = Scope.Menu;
					}
				}
				keyScopeReal(value);
				shortcuts.setScope(value);
			} else {
				return keyScopeFake;
			}
		};

	dropdownVisibility.subscribe(value => {
		if (value) {
			keyScope(Scope.Menu);
		} else if (Scope.Menu === shortcuts.getScope()) {
			keyScope(keyScopeFake);
		}
	});

	leftPanelDisabled.toggle = () => leftPanelDisabled(!leftPanelDisabled());
	leftPanelDisabled.subscribe(value => {
		value && moveAction() && moveAction(false);
		$htmlCL.toggle('rl-left-panel-disabled', value);
	});

	moveAction.subscribe(value => value && leftPanelDisabled() && leftPanelDisabled(false));

	let __themeTimer = 0,
		__themeJson = null;

	const isArray = Array.isArray,
		arrayLength = array => isArray(array) && array.length,
		isFunction = v => typeof v === 'function',
		pString = value => null != value ? '' + value : '',

		forEachObjectValue = (obj, fn) => Object.values(obj).forEach(fn),

		forEachObjectEntry = (obj, fn) => Object.entries(obj).forEach(([key, value]) => fn(key, value)),

		pInt = (value, defaultValue = 0) => {
			value = parseInt(value, 10);
			return isNaN(value) || !isFinite(value) ? defaultValue : value;
		},

		convertThemeName = theme => theme
			.replace(/@custom$/, '')
			.replace(/([A-Z])/g, ' $1')
			.replace(/[^a-zA-Z0-9]+/g, ' ')
			.trim(),

		defaultOptionsAfterRender = (domItem, item) =>
			domItem && item && undefined !== item.disabled
			&& domItem.classList.toggle('disabled', domItem.disabled = item.disabled),

		addObservablesTo = (target, observables) =>
			forEachObjectEntry(observables, (key, value) =>
				target[key] = /*isArray(value) ? ko.observableArray(value) :*/ ko.observable(value) ),

		addComputablesTo = (target, computables) =>
			forEachObjectEntry(computables, (key, fn) => target[key] = ko.computed(fn, {'pure':true})),

		addSubscribablesTo = (target, subscribables) =>
			forEachObjectEntry(subscribables, (key, fn) => target[key].subscribe(fn)),

		inFocus = () => {
			try {
				return doc.activeElement && doc.activeElement.matches(
					'input,textarea,[contenteditable]'
				);
			} catch (e) {
				return false;
			}
		},

		settingsSaveHelperSimpleFunction = (koTrigger, context) =>
			iError => {
				koTrigger.call(context, iError ? SaveSettingsStep.FalseResult : SaveSettingsStep.TrueResult);
				setTimeout(() => koTrigger.call(context, SaveSettingsStep.Idle), 1000);
			},

		changeTheme = (value, themeTrigger = ()=>0) => {
			const themeStyle = elementById('app-theme-style'),
				clearTimer = () => {
					__themeTimer = setTimeout(() => themeTrigger(SaveSettingsStep.Idle), 1000);
					__themeJson = null;
				};

			let url = themeStyle.dataset.href;

			if (url) {
				url = url.toString()
					.replace(/\/-\/[^/]+\/-\//, '/-/' + value + '/-/')
					.replace(/\/Css\/[^/]+\/User\//, '/Css/0/User/')
					.replace(/\/Hash\/[^/]+\//, '/Hash/-/');

				if ('Json/' !== url.substr(-5)) {
					url += 'Json/';
				}

				clearTimeout(__themeTimer);

				themeTrigger(SaveSettingsStep.Animate);

				if (__themeJson) {
					__themeJson.abort();
				}
				let init = {};
				if (window.AbortController) {
					__themeJson = new AbortController();
					init.signal = __themeJson.signal;
				}
				rl.fetchJSON(url, init)
					.then(data => {
						if (2 === arrayLength(data)) {
							themeStyle.textContent = data[1];
							themeStyle.dataset.href = url;
							themeStyle.dataset.theme = data[0];
							themeTrigger(SaveSettingsStep.TrueResult);
						}
					})
					.then(clearTimer, clearTimer);
			}
		},

		getKeyByValue = (o, v) => Object.keys(o).find(key => o[key] === v);

	const ROOT = './',
		HASH_PREFIX = '#/',
		SERVER_PREFIX = './?',
		VERSION = Settings.app('version');
		Settings.app('webVersionPath') || 'snappymail/v/' + VERSION + '/';

		const adminPath = () => rl.adminArea() && !Settings.app('adminHostUse'),

		prefix = () => SERVER_PREFIX + (adminPath() ? Settings.app('adminPath') : '');

	const SUB_QUERY_PREFIX = '&q[]=',

		/**
		 * @param {string=} startupUrl
		 * @returns {string}
		 */
		root = (startupUrl = '') => HASH_PREFIX + pString(startupUrl),

		/**
		 * @returns {string}
		 */
		logoutLink = () => adminPath() ? prefix() : ROOT,

		/**
		 * @param {string} type
		 * @param {string} hash
		 * @param {string=} customSpecSuffix
		 * @returns {string}
		 */
		serverRequestRaw = (type, hash) =>
			SERVER_PREFIX + '/Raw/' + SUB_QUERY_PREFIX + '/'
			+ '0/' // AuthAccountHash ?
			+ (type
				? type + '/' + (hash ? SUB_QUERY_PREFIX + '/' + hash : '')
				: ''),

		/*
			return './?/ProxyExternal/'.Utils::EncodeKeyValuesQ(array(
				'Rnd' => \md5(\microtime(true)),
				'Token' => Utils::GetConnectionToken(),
				'Url' => $sUrl
			)).'/';
	*/

		/**
		 * @param {string} type
		 * @returns {string}
		 */
		serverRequest = type => prefix() + '/' + type + '/' + SUB_QUERY_PREFIX + '/0/',

		/**
		 * @param {string} lang
		 * @param {boolean} isAdmin
		 * @returns {string}
		 */
		langLink = (lang, isAdmin) =>
			SERVER_PREFIX + '/Lang/0/' + (isAdmin ? 'Admin' : 'App') + '/' + encodeURI(lang) + '/' + VERSION + '/',

		/**
		 * @param {string=} screenName = ''
		 * @returns {string}
		 */
		settings = (screenName = '') => HASH_PREFIX + 'settings' + (screenName ? '/' + screenName : '');

	let I18N_DATA = {};

	const
		i18nToNode = element => {
			const key = element.dataset.i18n;
			if (key) {
				if ('[' === key.slice(0, 1)) {
					switch (key.slice(0, 6)) {
						case '[html]':
							element.innerHTML = i18n(key.slice(6));
							break;
						case '[place':
							element.placeholder = i18n(key.slice(13));
							break;
						case '[title':
							element.title = i18n(key.slice(7));
							break;
						// no default
					}
				} else {
					element.textContent = i18n(key);
				}
			}
		},

		init = () => {
			if (rl.I18N) {
				I18N_DATA = rl.I18N;
				Date.defineRelativeTimeFormat(rl.relativeTime || {});
				rl.I18N = null;
				return 1;
			}
		},

		i18nKey = key => key.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase(),

		getNotificationMessage = code => {
			let key = getKeyByValue(Notification, code);
			if (key) {
				key = i18nKey(key).replace('_NOTIFICATION', '_ERROR');
				return I18N_DATA.NOTIFICATIONS[key];
			}
		};

	const trigger = ko.observable(false),

		/**
		 * @param {string} key
		 * @param {Object=} valueList
		 * @param {string=} defaulValue
		 * @returns {string}
		 */
		i18n = (key, valueList, defaulValue) => {
			let result = defaulValue || key;
			if (key) {
				let path = key.split('/');
				if (I18N_DATA[path[0]] && path[1]) {
					result = I18N_DATA[path[0]][path[1]] || result;
				}
			}
			if (valueList) {
				forEachObjectEntry(valueList, (key, value) => {
					result = result.replace('%' + key + '%', value);
				});
			}
			return result;
		},

		/**
		 * @param {Object} elements
		 * @param {boolean=} animate = false
		 */
		i18nToNodes = element =>
			setTimeout(() =>
				element.querySelectorAll('[data-i18n]').forEach(item => i18nToNode(item))
			, 1),

		/**
		 * @param {Function} startCallback
		 * @param {Function=} langCallback = null
		 */
		initOnStartOrLangChange = (startCallback, langCallback = null) => {
			startCallback && startCallback();
			startCallback && trigger.subscribe(startCallback);
			langCallback && trigger.subscribe(langCallback);
		},

		/**
		 * @param {number} code
		 * @param {*=} message = ''
		 * @param {*=} defCode = null
		 * @returns {string}
		 */
		getNotification = (code, message = '', defCode = 0) => {
			code = parseInt(code, 10) || 0;
			if (Notification.ClientViewError === code && message) {
				return message;
			}

			return getNotificationMessage(code)
				|| getNotificationMessage(parseInt(defCode, 10))
				|| '';
		},

		/**
		 * @param {boolean} admin
		 * @param {string} language
		 */
		translatorReload = (admin, language) =>
			new Promise((resolve, reject) => {
				const script = createElement('script');
				script.onload = () => {
					// reload the data
					if (init()) {
						i18nToNodes(doc);
						admin || rl.app.reloadTime();
						trigger(!trigger());
					}
					script.remove();
					resolve();
				};
				script.onerror = () => reject(new Error('Language '+language+' failed'));
				script.src = langLink(language, admin);
		//		script.async = true;
				doc.head.append(script);
			}),

		/**
		 *
		 * @param {string} language
		 * @param {boolean=} isEng = false
		 * @returns {string}
		 */
		convertLangName = (language, isEng = false) =>
			i18n(
				'LANGS_NAMES' + (true === isEng ? '_EN' : '') + '/' + language,
				null,
				language
			);

	init();

	var bootstrap = App => {

		addEventListener('click', ()=>rl.Dropdowns.detectVisibility());

		rl.app = App;
		rl.logoutReload = App.logoutReload;

		rl.i18n = i18n;

		rl.Enums = {
			StorageResultType: {
				Success: 0,
				Error: 1,
				Abort: 2
			}
		};

		rl.Dropdowns = [];
		rl.Dropdowns.register = function(element) { this.push(element); };
		rl.Dropdowns.detectVisibility = (() =>
			dropdownVisibility(!!rl.Dropdowns.find(item => item.classList.contains('show')))
		).debounce(50);

		rl.route = {
			root: () => {
				rl.route.setHash(root(), true);
				rl.route.off();
			},
			reload: () => {
				rl.route.root();
				setTimeout(() => (Settings.app('inIframe') ? parent : window).location.reload(), 100);
			},
			off: () => hasher.active = false,
			on: () => hasher.active = true,
			/**
			 * @param {string} sHash
			 * @param {boolean=} silence = false
			 * @param {boolean=} replace = false
			 * @returns {void}
			 */
			setHash: (hash, silence = false, replace = false) => {
				hash = hash.replace(/^[#/]+/, '');
				hasher.active = !silence;
				hasher[replace ? 'replaceHash' : 'setHash'](hash);
				if (silence) {
					hasher.active = true;
				} else {
					hasher.setHash(hash);
				}
			}
		};

		rl.fetch = (resource, init, postData) => {
			init = Object.assign({
				mode: 'same-origin',
				cache: 'no-cache',
				redirect: 'error',
				referrerPolicy: 'no-referrer',
				credentials: 'same-origin',
				headers: {}
			}, init);
			if (postData) {
				init.method = 'POST';
				init.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				const buildFormData = (formData, data, parentKey) => {
					if (data && typeof data === 'object' && !(data instanceof Date || data instanceof File)) {
						Object.keys(data).forEach(key =>
							buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key)
						);
					} else {
						formData.set(parentKey, data == null ? '' : data);
					}
					return formData;
				};
				postData = (postData instanceof FormData)
					? postData
					: buildFormData(new FormData(), postData);
				postData.set('XToken', Settings.app('token'));
	//			init.body = JSON.stringify(Object.fromEntries(postData));
				init.body = new URLSearchParams(postData);
			}

			return fetch(resource, init);
		};

		rl.fetchJSON = (resource, init, postData) => {
			init = Object.assign({ headers: {} }, init);
			init.headers.Accept = 'application/json';
			return rl.fetch(resource, init, postData).then(response => {
				if (!response.ok) {
					return Promise.reject('Network response error: ' + response.status);
				}
				/* TODO: use this for non-developers?
				response.clone()
				let data = response.text();
				try {
					return JSON.parse(data);
				} catch (e) {
					console.error(e);
	//				console.log(data);
					return Promise.reject(Notification.JsonParse);
					return {
						Result: false,
						ErrorCode: 952, // Notification.JsonParse
						ErrorMessage: e.message,
						ErrorMessageAdditional: data
					}
				}
				*/
				return response.json();
			});
		};

	};

	/**
	 * The value of the pureComputed observable shouldn’t vary based on the
	 * number of evaluations or other “hidden” information. Its value should be
	 * based solely on the values of other observables in the application
	 */
	const koComputable = fn => ko.computed(fn, {'pure':true});

	ko.bindingHandlers.tooltipErrorTip = {
		init: (element, fValueAccessor) => {
			doc.addEventListener('click', () => {
				let value = fValueAccessor();
				ko.isObservable(value) && !ko.isComputed(value) && value('');
				element.removeAttribute('data-rainloopErrorTip');
			});
		},
		update: (element, fValueAccessor) => {
			let value = ko.unwrap(fValueAccessor());
			value = isFunction(value) ? value() : value;
			if (value) {
				setTimeout(() => element.setAttribute('data-rainloopErrorTip', value), 100);
			} else {
				element.removeAttribute('data-rainloopErrorTip');
			}
		}
	};

	ko.bindingHandlers.onEnter = {
		init: (element, fValueAccessor, fAllBindings, viewModel) => {
			let fn = event => {
				if ('Enter' == event.key) {
					element.dispatchEvent(new Event('change'));
					fValueAccessor().call(viewModel);
				}
			};
			element.addEventListener('keydown', fn);
			ko.utils.domNodeDisposal.addDisposeCallback(element, () => element.removeEventListener('keydown', fn));
		}
	};

	ko.bindingHandlers.onSpace = {
		init: (element, fValueAccessor, fAllBindings, viewModel) => {
			let fn = event => {
				if (' ' == event.key) {
					fValueAccessor().call(viewModel, event);
				}
			};
			element.addEventListener('keyup', fn);
			ko.utils.domNodeDisposal.addDisposeCallback(element, () => element.removeEventListener('keyup', fn));
		}
	};

	ko.bindingHandlers.i18nInit = {
		init: element => i18nToNodes(element)
	};

	ko.bindingHandlers.i18nUpdate = {
		update: (element, fValueAccessor) => {
			ko.unwrap(fValueAccessor());
			i18nToNodes(element);
		}
	};

	ko.bindingHandlers.title = {
		update: (element, fValueAccessor) => element.title = ko.unwrap(fValueAccessor())
	};

	ko.bindingHandlers.command = {
		init: (element, fValueAccessor, fAllBindings, viewModel, bindingContext) => {
			const command = fValueAccessor();

			if (!command || !command.enabled || !command.canExecute) {
				throw new Error('Value should be a command');
			}

			ko.bindingHandlers['FORM'==element.nodeName ? 'submit' : 'click'].init(
				element,
				fValueAccessor,
				fAllBindings,
				viewModel,
				bindingContext
			);
		},
		update: (element, fValueAccessor) => {
			const cl = element.classList,
				command = fValueAccessor();

			let disabled = !command.enabled();

			disabled = disabled || !command.canExecute();
			cl.toggle('disabled', disabled);

			if (element.matches('INPUT,TEXTAREA,BUTTON')) {
				element.disabled = disabled;
			}
		}
	};

	ko.bindingHandlers.saveTrigger = {
		init: (element) => {
			let icon = element;
			if (element.matches('input,select,textarea')) {
				element.classList.add('settings-saved-trigger-input');
				element.after(element.saveTriggerIcon = icon = createElement('span'));
			}
			icon.classList.add('settings-save-trigger');
		},
		update: (element, fValueAccessor) => {
			const value = parseInt(ko.unwrap(fValueAccessor()),10);
			let cl = (element.saveTriggerIcon || element).classList;
			if (element.saveTriggerIcon) {
				cl.toggle('saving', value === SaveSettingsStep.Animate);
				cl.toggle('success', value === SaveSettingsStep.TrueResult);
				cl.toggle('error', value === SaveSettingsStep.FalseResult);
			}
			cl = element.classList;
			cl.toggle('success', value === SaveSettingsStep.TrueResult);
			cl.toggle('error', value === SaveSettingsStep.FalseResult);
		}
	};

	// extenders

	ko.extenders.limitedList = (target, limitedList) => {
		const result = ko
				.computed({
					read: target,
					write: (newValue) => {
						const currentValue = ko.unwrap(target),
							list = ko.unwrap(limitedList);

						if (arrayLength(list)) {
							if (list.includes(newValue)) {
								target(newValue);
							} else if (list.includes(currentValue, list)) {
								target(currentValue + ' ');
								target(currentValue);
							} else {
								target(list[0] + ' ');
								target(list[0]);
							}
						} else {
							target('');
						}
					}
				})
				.extend({ notify: 'always' });

		result(target());

		if (!result.valueHasMutated) {
			result.valueHasMutated = () => target.valueHasMutated();
		}

		return result;
	};

	ko.extenders.toggleSubscribeProperty = (target, options) => {
		const prop = options[1];
		if (prop) {
			target.subscribe(
				prev => prev && prev[prop] && prev[prop](false),
				options[0],
				'beforeChange'
			);

			target.subscribe(next => next && next[prop] && next[prop](true), options[0]);
		}

		return target;
	};

	ko.extenders.falseTimeout = (target, option) => {
		target.subscribe((() => target(false)).debounce(parseInt(option, 10) || 0));
		return target;
	};

	// functions

	ko.observable.fn.askDeleteHelper = function() {
		return this.extend({ falseTimeout: 3000, toggleSubscribeProperty: [this, 'askDelete'] });
	};

	let iJsonErrorCount = 0;

	const getURL = (add = '') => serverRequest('Json') + add,

	checkResponseError = data => {
		const err = data ? data.ErrorCode : null;
		if (Notification.InvalidToken === err) {
			alert(getNotification(err));
			rl.logoutReload();
		} else if ([
				Notification.AuthError,
				Notification.ConnectionError,
				Notification.DomainNotAllowed,
				Notification.AccountNotAllowed,
				Notification.MailServerError,
				Notification.UnknownNotification,
				Notification.UnknownError
			].includes(err)
		) {
			if (7 < ++iJsonErrorCount) {
				rl.logoutReload();
			}
		}
	},

	oRequests = {},

	abort = (sAction, bClearOnly) => {
		if (oRequests[sAction]) {
			if (!bClearOnly && oRequests[sAction].abort) {
	//			oRequests[sAction].__aborted = true;
				oRequests[sAction].abort();
			}

			oRequests[sAction] = null;
			delete oRequests[sAction];
		}
	},

	fetchJSON = (action, sGetAdd, params, timeout, jsonCallback) => {
		sGetAdd = pString(sGetAdd);
		params = params || {};
		if (params instanceof FormData) {
			params.set('Action', action);
		} else {
			params.Action = action;
		}
		let init = {};
		if (window.AbortController) {
			abort(action);
			const controller = new AbortController();
			timeout && setTimeout(() => controller.abort(), timeout);
			oRequests[action] = controller;
			init.signal = controller.signal;
		}
		return rl.fetchJSON(getURL(sGetAdd), init, sGetAdd ? null : params).then(jsonCallback);
	};

	class FetchError extends Error
	{
		constructor(code, message) {
			super(message);
			this.code = code || Notification.JsonFalse;
		}
	}

	class AbstractFetchRemote
	{
		abort(sAction, bClearOnly) {
			abort(sAction, bClearOnly);
			return this;
		}

		/**
		 * Allows quicker visual responses to the user.
		 * Can be used to stream lines of json encoded data, but does not work on all servers.
		 * Apache needs 'flushpackets' like in <Proxy "fcgi://...." flushpackets=on></Proxy>
		 */
		streamPerLine(fCallback, sGetAdd) {
			rl.fetch(getURL(sGetAdd))
			.then(response => response.body)
			.then(body => {
				// Firefox TextDecoderStream is not defined
			//	const reader = body.pipeThrough(new TextDecoderStream()).getReader();
				const reader = body.getReader(),
					re = /\r\n|\n|\r/gm,
					utf8decoder = new TextDecoder();
				let buffer = '';
				function processText({ done, value }) {
					buffer += value ? utf8decoder.decode(value, {stream: true}) : '';
					for (;;) {
						let result = re.exec(buffer);
						if (!result) {
							if (done) {
								break;
							}
							reader.read().then(processText);
							return;
						}
						fCallback(buffer.slice(0, result.index));
						buffer = buffer.slice(result.index + 1);
						re.lastIndex = 0;
					}
					if (buffer.length) {
						// last line didn't end in a newline char
						fCallback(buffer);
					}
				}
				reader.read().then(processText);
			});
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sAction
		 * @param {Object=} oParameters
		 * @param {?number=} iTimeout
		 * @param {string=} sGetAdd = ''
		 * @param {Array=} aAbortActions = []
		 */
		request(sAction, fCallback, params, iTimeout, sGetAdd, abortActions) {
			params = params || {};

			const start = Date.now();

			if (sAction && abortActions) {
				abortActions.forEach(actionToAbort => abort(actionToAbort));
			}

			fetchJSON(sAction, sGetAdd,
				params,
				undefined === iTimeout ? 30000 : pInt(iTimeout),
				data => {
					let cached = false;
					if (data && data.Time) {
						cached = pInt(data.Time) > Date.now() - start;
					}

					let iError = 0;
					if (sAction && oRequests[sAction]) {
						if (oRequests[sAction].__aborted) {
							iError = 2;
						}
						abort(sAction, true);
					}

					if (!iError && data) {
	/*
						if (sAction !== data.Action) {
							console.log(sAction + ' !== ' + data.Action);
						}
	*/
						if (data.Result) {
							iJsonErrorCount = 0;
						} else {
							checkResponseError(data);
							iError = data.ErrorCode || Notification.UnknownError;
						}
					}

					fCallback && fCallback(
						iError,
						data,
						cached,
						sAction,
						params
					);
				}
			)
			.catch(err => {
				console.error(err);
				fCallback && fCallback(err.name == 'AbortError' ? 2 : 1);
			});
		}

		/**
		 * @param {?Function} fCallback
		 */
		getPublicKey(fCallback) {
			this.request('GetPublicKey', fCallback);
		}

		setTrigger(trigger, value) {
			if (trigger) {
				value = !!value;
				(isArray(trigger) ? trigger : [trigger]).forEach(fTrigger => {
					fTrigger && fTrigger(value);
				});
			}
		}

		post(action, fTrigger, params, timeOut) {
			this.setTrigger(fTrigger, true);
			return fetchJSON(action, '', params, pInt(timeOut, 30000),
				data => {
					abort(action, true);

					if (!data) {
						return Promise.reject(new FetchError(Notification.JsonParse));
					}
	/*
					let isCached = false, type = '';
					if (data && data.Time) {
						isCached = pInt(data.Time) > microtime() - start;
					}
					// backward capability
					switch (true) {
						case 'success' === textStatus && data && data.Result && action === data.Action:
							type = AbstractFetchRemote.SUCCESS;
							break;
						case 'abort' === textStatus && (!data || !data.__aborted__):
							type = AbstractFetchRemote.ABORT;
							break;
						default:
							type = AbstractFetchRemote.ERROR;
							break;
					}
	*/
					this.setTrigger(fTrigger, false);

					if (!data.Result || action !== data.Action) {
						checkResponseError(data);
						return Promise.reject(new FetchError(
							data ? data.ErrorCode : 0,
							data ? (data.ErrorMessageAdditional || data.ErrorMessage) : ''
						));
					}

					return data;
				}
			);
		}
	}

	Object.assign(AbstractFetchRemote.prototype, {
		SUCCESS : 0,
		ERROR : 1,
		ABORT : 2
	});

	class RemoteAdminFetch extends AbstractFetchRemote {

		/**
		 * @param {?Function} fCallback
		 * @param {?} oData
		 */
		saveConfig(oData, fCallback) {
			this.request('AdminSettingsUpdate', fCallback, oData);
		}

	}

	var Remote = new RemoteAdminFetch();

	class AbstractScreen {
		constructor(screenName, viewModels = []) {
			this.__cross = null;
			this.screenName = screenName;
			this.viewModels = isArray(viewModels) ? viewModels : [];
		}

		/**
		 * @returns {?Array)}
		 */
		routes() {
			return null;
		}

	/*
		onBuild(viewModelDom) {}
		onShow() {}
		onHide() {}
		__started
		__builded
	*/

		/**
		 * @returns {void}
		 */
		onStart() {
			if (!this.__started) {
				this.__started = true;
				const routes = this.routes();
				if (arrayLength(routes)) {
					let route = new Crossroads(),
						fMatcher = (this.onRoute || (()=>0)).bind(this);

					routes.forEach(item => item && route && (route.addRoute(item[0], fMatcher).rules = item[1]));

					this.__cross = route;
				}
			}
		}
	}

	const VIEW_MODELS = [];

	class AbstractSettingsScreen extends AbstractScreen {
		/**
		 * @param {Array} viewModels
		 */
		constructor(viewModels) {
			super('settings', viewModels);

			this.menu = ko.observableArray();

			this.oCurrentSubScreen = null;
		}

		onRoute(subName) {
			let settingsScreen = null,
				viewModelDom = null,
				RoutedSettingsViewModel = VIEW_MODELS.find(
					SettingsViewModel => subName === SettingsViewModel.__rlSettingsData.route
				);

			if (RoutedSettingsViewModel) {
				if (RoutedSettingsViewModel.__builded && RoutedSettingsViewModel.__vm) {
					settingsScreen = RoutedSettingsViewModel.__vm;
				} else {
					const vmPlace = elementById('rl-settings-subscreen');
					if (vmPlace) {
						viewModelDom = createElement('div',{
							id: 'V-Settings-' + RoutedSettingsViewModel.name.replace(/(User|Admin)Settings/,''),
							hidden: ''
						});
						vmPlace.append(viewModelDom);

						settingsScreen = new RoutedSettingsViewModel();
						settingsScreen.viewModelDom = viewModelDom;

						RoutedSettingsViewModel.__dom = viewModelDom;
						RoutedSettingsViewModel.__builded = true;
						RoutedSettingsViewModel.__vm = settingsScreen;

						ko.applyBindingAccessorsToNode(
							viewModelDom,
							{
								i18nInit: true,
								template: () => ({ name: RoutedSettingsViewModel.__rlSettingsData.template })
							},
							settingsScreen
						);

						settingsScreen.onBuild && settingsScreen.onBuild(viewModelDom);
					} else {
						console.log('Cannot find sub settings view model position: SettingsSubScreen');
					}
				}

				if (settingsScreen) {
					setTimeout(() => {
						// hide
						this.onHide();
						// --

						this.oCurrentSubScreen = settingsScreen;

						// show
						settingsScreen.onBeforeShow && settingsScreen.onBeforeShow();
						settingsScreen.viewModelDom.hidden = false;
						settingsScreen.onShow && settingsScreen.onShow();

						this.menu.forEach(item => {
							item.selected(
								item.route === RoutedSettingsViewModel.__rlSettingsData.route
							);
						});

						elementById('rl-settings-subscreen').scrollTop = 0;
						// --
					}, 1);
				}
			} else {
				rl.route.setHash(settings(), false, true);
			}
		}

		onHide() {
			let subScreen = this.oCurrentSubScreen;
			if (subScreen) {
				subScreen.onHide && subScreen.onHide();
				subScreen.viewModelDom.hidden = true;
			}
		}

		onBuild() {
			VIEW_MODELS.forEach(SettingsViewModel => this.menu.push(SettingsViewModel.__rlSettingsData));
		}

		routes() {
			const DefaultViewModel = VIEW_MODELS.find(
					SettingsViewModel => SettingsViewModel.__rlSettingsData.isDefault
				),
				defaultRoute =
					DefaultViewModel ? DefaultViewModel.__rlSettingsData.route : 'general',
				rules = {
					subname: /^(.*)$/,
					normalize_: (rquest, vals) => {
						vals.subname = null == vals.subname ? defaultRoute : pString(vals.subname);
						return [vals.subname];
					}
				};

			return [
				['{subname}/', rules],
				['{subname}', rules],
				['', rules]
			];
		}
	}

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} template
	 * @param {string} labelName
	 * @param {string} route
	 * @param {boolean=} isDefault = false
	 * @returns {void}
	 */
	function settingsAddViewModel(SettingsViewModelClass, template, labelName, route, isDefault = false) {
		SettingsViewModelClass.__rlSettingsData = {
			label: labelName,
			route: route,
			selected: ko.observable(false),
			template: template,
			isDefault: !!isDefault
		};

		VIEW_MODELS.push(SettingsViewModelClass);
	}

	let
		SCREENS = {},
		currentScreen = null,
		defaultScreenName = '';

	const
		autofocus = dom => {
			const af = dom.querySelector('[autofocus]');
			af && af.focus();
		},

		visiblePopups = new Set,

		/**
		 * @param {string} screenName
		 * @returns {?Object}
		 */
		screen = screenName => screenName && null != SCREENS[screenName] ? SCREENS[screenName] : null,

		/**
		 * @param {Function} ViewModelClass
		 * @param {Object=} vmScreen
		 * @returns {*}
		 */
		buildViewModel = (ViewModelClass, vmScreen) => {
			if (ViewModelClass && !ViewModelClass.__builded) {
				let vmDom = null;
				const vm = new ViewModelClass(vmScreen),
					position = vm.viewType || '',
					dialog = ViewType.Popup === position,
					vmPlace = position ? doc.getElementById('rl-' + position.toLowerCase()) : null;

				ViewModelClass.__builded = true;
				ViewModelClass.__vm = vm;

				if (vmPlace) {
					vmDom = dialog
						? Element.fromHTML('<dialog id="V-'+ vm.viewModelTemplateID + '"></dialog>')
						: Element.fromHTML('<div id="V-'+ vm.viewModelTemplateID + '" hidden=""></div>');
					vmPlace.append(vmDom);

					vm.viewModelDom = vmDom;
					ViewModelClass.__dom = vmDom;

					if (ViewType.Popup === position) {
						vm.cancelCommand = vm.closeCommand = createCommand(() => hideScreenPopup(ViewModelClass));

						// Firefox / Safari HTMLDialogElement not defined
						if (!vmDom.showModal) {
							vmDom.classList.add('polyfill');
							vmDom.showModal = () => {
								if (!vmDom.backdrop) {
									vmDom.before(vmDom.backdrop = Element.fromHTML('<div class="dialog-backdrop"></div>'));
								}
								vmDom.setAttribute('open','');
								vmDom.open = true;
								vmDom.returnValue = null;
								vmDom.backdrop.hidden = false;
							};
							vmDom.close = v => {
								vmDom.backdrop.hidden = true;
								vmDom.returnValue = v;
								vmDom.removeAttribute('open', null);
								vmDom.open = false;
							};
						}

						// show/hide popup/modal
						const endShowHide = e => {
							if (e.target === vmDom) {
								if (vmDom.classList.contains('animate')) {
									autofocus(vmDom);
									vm.onShowWithDelay && vm.onShowWithDelay();
								} else {
									vmDom.close();
									vm.onHideWithDelay && vm.onHideWithDelay();
								}
							}
						};

						vm.modalVisibility.subscribe(value => {
							if (value) {
								visiblePopups.add(vm);
								vmDom.style.zIndex = 3000 + (visiblePopups.size * 2);
								vmDom.showModal();
								if (vmDom.backdrop) {
									vmDom.backdrop.style.zIndex = 3000 + visiblePopups.size;
								}
								vm.keyScope.set();
								requestAnimationFrame(() => { // wait just before the next paint
									vmDom.offsetHeight; // force a reflow
									vmDom.classList.add('animate'); // trigger the transitions
								});
							} else {
								visiblePopups.delete(vm);
								vm.onHide && vm.onHide();
								vm.keyScope.unset();
								vmDom.classList.remove('animate'); // trigger the transitions
							}
							arePopupsVisible(0 < visiblePopups.size);
	/*
							// the old ko.bindingHandlers.modal
							const close = vmDom.querySelector('.close'),
								click = () => vm.modalVisibility(false);
							if (close) {
								close.addEventListener('click.koModal', click);
								ko.utils.domNodeDisposal.addDisposeCallback(vmDom, () =>
									close.removeEventListener('click.koModal', click)
								);
							}
	*/
						});
						if ('ontransitionend' in vmDom) {
							vmDom.addEventListener('transitionend', endShowHide);
						} else {
							// For Edge < 79 and mobile browsers
							vm.modalVisibility.subscribe(() => ()=>setTimeout(endShowHide({target:vmDom}), 500));
						}
					}

					ko.applyBindingAccessorsToNode(
						vmDom,
						{
							i18nInit: true,
							template: () => ({ name: vm.viewModelTemplateID })
						},
						vm
					);

					vm.onBuild && vm.onBuild(vmDom);
					if (vm && ViewType.Popup === position) {
						vm.registerPopupKeyDown();
					}

					fireEvent('rl-view-model', vm);
				} else {
					console.log('Cannot find view model position: ' + position);
				}
			}

			return ViewModelClass && ViewModelClass.__vm;
		},

		forEachViewModel = (screen, fn) => {
			screen.viewModels.forEach(ViewModelClass => {
				if (
					ViewModelClass.__vm &&
					ViewModelClass.__dom &&
					ViewType.Popup !== ViewModelClass.__vm.viewType
				) {
					fn(ViewModelClass.__vm, ViewModelClass.__dom);
				}
			});
		},

		hideScreen = (screenToHide, destroy) => {
			screenToHide.onHide && screenToHide.onHide();
			forEachViewModel(screenToHide, (vm, dom) => {
				dom.hidden = true;
				vm.onHide && vm.onHide();
				destroy && vm.viewModelDom.remove();
			});
		},

		/**
		 * @param {Function} ViewModelClassToHide
		 * @returns {void}
		 */
		hideScreenPopup = ViewModelClassToHide => {
			if (ViewModelClassToHide && ViewModelClassToHide.__vm && ViewModelClassToHide.__dom) {
				ViewModelClassToHide.__vm.modalVisibility(false);
			}
		},

		/**
		 * @param {string} screenName
		 * @param {string} subPart
		 * @returns {void}
		 */
		screenOnRoute = (screenName, subPart) => {
			let vmScreen = null,
				isSameScreen = false;

			if (null == screenName || '' == screenName) {
				screenName = defaultScreenName;
			}

			// Close all popups
			for (let vm of visiblePopups) {
				vm.closeCommand();
			}

			if (screenName) {
				vmScreen = screen(screenName);
				if (!vmScreen) {
					vmScreen = screen(defaultScreenName);
					if (vmScreen) {
						subPart = screenName + '/' + subPart;
						screenName = defaultScreenName;
					}
				}

				if (vmScreen && vmScreen.__started) {
					isSameScreen = currentScreen && vmScreen === currentScreen;

					if (!vmScreen.__builded) {
						vmScreen.__builded = true;

						vmScreen.viewModels.forEach(ViewModelClass =>
							buildViewModel(ViewModelClass, vmScreen)
						);

						vmScreen.onBuild && vmScreen.onBuild();
					}

					setTimeout(() => {
						// hide screen
						if (currentScreen && !isSameScreen) {
							hideScreen(currentScreen);
						}
						// --

						currentScreen = vmScreen;

						// show screen
						if (!isSameScreen) {
							vmScreen.onShow && vmScreen.onShow();

							forEachViewModel(vmScreen, (vm, dom) => {
								vm.onBeforeShow && vm.onBeforeShow();
								dom.hidden = false;
								vm.onShow && vm.onShow();
								autofocus(dom);
							});
						}
						// --

						vmScreen.__cross && vmScreen.__cross.parse(subPart);
					}, 1);
				}
			}
		};


	const
		ViewType = {
			Popup: 'Popups',
			Left: 'Left',
			Right: 'Right',
			Content: 'Content'
		},

		/**
		 * @param {Function} fExecute
		 * @param {(Function|boolean|null)=} fCanExecute = true
		 * @returns {Function}
		 */
		createCommand = (fExecute, fCanExecute) => {
			let fResult = () => {
					fResult.canExecute() && fExecute.call(null);
					return false;
				};
			fResult.enabled = ko.observable(true);
			fResult.canExecute = isFunction(fCanExecute)
				? koComputable(() => fResult.enabled() && fCanExecute())
				: fResult.enabled;
			return fResult;
		},

		/**
		 * @param {Function} ViewModelClassToShow
		 * @param {Array=} params
		 * @returns {void}
		 */
		showScreenPopup = (ViewModelClassToShow, params = []) => {
			const vm = buildViewModel(ViewModelClassToShow) && ViewModelClassToShow.__dom && ViewModelClassToShow.__vm;

			if (vm) {
				params = params || [];

				vm.onBeforeShow && vm.onBeforeShow(...params);

				vm.modalVisibility(true);

				vm.onShow && vm.onShow(...params);
			}
		},

		arePopupsVisible = ko.observable(false),

		/**
		 * @param {Array} screensClasses
		 * @returns {void}
		 */
		startScreens = screensClasses => {
			hasher.clear();
			forEachObjectValue(SCREENS, screen => hideScreen(screen, 1));
			SCREENS = {};
			currentScreen = null,
			defaultScreenName = '';

			screensClasses.forEach(CScreen => {
				if (CScreen) {
					const vmScreen = new CScreen(),
						screenName = vmScreen.screenName;
					defaultScreenName || (defaultScreenName = screenName);
					SCREENS[screenName] = vmScreen;
				}
			});

			forEachObjectValue(SCREENS, vmScreen => vmScreen.onStart());

			const cross = new Crossroads();
			cross.addRoute(/^([a-zA-Z0-9-]*)\/?(.*)$/, screenOnRoute);

			hasher.add(cross.parse.bind(cross));
			hasher.init();

			setTimeout(() => $htmlCL.remove('rl-started-trigger'), 100);

			const c = elementById('rl-content'), l = elementById('rl-loading');
			c && (c.hidden = false);
			l && l.remove();
		},

		decorateKoCommands = (thisArg, commands) =>
			forEachObjectEntry(commands, (key, canExecute) => {
				let command = thisArg[key],
					fn = (...args) => fn.enabled() && fn.canExecute() && command.apply(thisArg, args);

		//		fn.__realCanExecute = canExecute;

				fn.enabled = ko.observable(true);

				fn.canExecute = isFunction(canExecute)
					? koComputable(() => fn.enabled() && canExecute.call(thisArg, thisArg))
					: koComputable(() => fn.enabled());

				thisArg[key] = fn;
			});

	ko.decorateCommands = decorateKoCommands;

	class AbstractView {
		constructor(templateID, type)
		{
	//		Object.defineProperty(this, 'viewModelTemplateID', { value: templateID });
			this.viewModelTemplateID = templateID;
			this.viewType = type;
			this.viewModelDom = null;

			this.keyScope = {
				scope: Scope.None,
				previous: Scope.None,
				set: function() {
					this.previous = keyScope();
					keyScope(this.scope);
				},
				unset: function() {
					keyScope(this.previous);
				}
			};
		}

	/*
		onBuild() {}
		onBeforeShow() {}
		onShow() {}
		onHide() {}
	*/

		querySelector(selectors) {
			return this.viewModelDom.querySelector(selectors);
		}

		addObservables(observables) {
			addObservablesTo(this, observables);
		}

		addComputables(computables) {
			addComputablesTo(this, computables);
		}

		addSubscribables(subscribables) {
			addSubscribablesTo(this, subscribables);
		}

	}

	class AbstractViewPopup extends AbstractView
	{
		constructor(name)
		{
			super('Popups' + name, ViewType.Popup);
			if (name in Scope) {
				this.keyScope.scope = Scope[name];
			}
			this.bDisabeCloseOnEsc = false;
			this.modalVisibility = ko.observable(false).extend({ rateLimit: 0 });
		}
	/*
		onShowWithDelay() {}
		onHideWithDelay() {}

		cancelCommand() {}
		closeCommand() {}
	*/
		/**
		 * @returns {void}
		 */
		registerPopupKeyDown() {
			addEventListener('keydown', event => {
				if (event && this.modalVisibility()) {
					if (!this.bDisabeCloseOnEsc && 'Escape' == event.key) {
						this.cancelCommand();
						return false;
					} else if ('Backspace' == event.key && !inFocus()) {
						return false;
					}
				}

				return true;
			});
		}
	}

	AbstractViewPopup.showModal = function(params = []) {
		showScreenPopup(this, params);
	};

	AbstractViewPopup.hidden = function() {
		return !this.__vm || !this.__vm.modalVisibility();
	};

	class AbstractViewCenter extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Content);
		}
	}

	class AbstractViewLeft extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Left);
			this.leftPanelDisabled = leftPanelDisabled;
		}
	}

	class AbstractViewRight extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Right);
		}
	}

	/*
	export class AbstractViewSettings
	{
		onBuild(viewModelDom) {}
		onBeforeShow() {}
		onShow() {}
		onHide() {}
		viewModelDom
	}
	*/

	class AbstractViewLogin extends AbstractViewCenter {
		constructor(templateID) {
			super(templateID);
			this.hideSubmitButton = Settings.app('hideSubmitButton');
			this.formError = ko.observable(false).extend({ falseTimeout: 500 });
		}

		onBuild(dom) {
			dom.classList.add('LoginView');
		}

		onShow() {
			rl.route.off();
		}

		submitForm() {
	//		return false;
		}
	}

	const USER_VIEW_MODELS_HOOKS = [],
		ADMIN_VIEW_MODELS_HOOKS = [];

	/**
	 * @param {Function} callback
	 * @param {string} action
	 * @param {Object=} parameters
	 * @param {?number=} timeout
	 */
	rl.pluginRemoteRequest = (callback, action, parameters, timeout) => {
		rl.app && rl.app.Remote.request('Plugin' + action, callback, parameters, timeout);
	};

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} labelName
	 * @param {string} template
	 * @param {string} route
	 */
	rl.addSettingsViewModel = (SettingsViewModelClass, template, labelName, route) => {
		USER_VIEW_MODELS_HOOKS.push([SettingsViewModelClass, template, labelName, route]);
	};

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} labelName
	 * @param {string} template
	 * @param {string} route
	 */
	rl.addSettingsViewModelForAdmin = (SettingsViewModelClass, template, labelName, route) => {
		ADMIN_VIEW_MODELS_HOOKS.push([SettingsViewModelClass, template, labelName, route]);
	};

	/**
	 * @param {boolean} admin
	 */
	function runSettingsViewModelHooks(admin) {
		(admin ? ADMIN_VIEW_MODELS_HOOKS : USER_VIEW_MODELS_HOOKS).forEach(view => {
			settingsAddViewModel(view[0], view[1], view[2], view[3]);
		});
	}

	/**
	 * @param {string} pluginSection
	 * @param {string} name
	 * @returns {?}
	 */
	rl.pluginSettingsGet = (pluginSection, name) => {
		let plugins = SettingsGet('Plugins');
		plugins = plugins && null != plugins[pluginSection] ? plugins[pluginSection] : null;
		return plugins ? (null == plugins[name] ? null : plugins[name]) : null;
	};

	rl.pluginPopupView = AbstractViewPopup;

	const ThemeStore = {
		themes: ko.observableArray(),
		userBackgroundName: ko.observable(''),
		userBackgroundHash: ko.observable(''),
		isMobile: ko.observable($htmlCL.contains('rl-mobile')),

		populate: function(){
			const themes = Settings.app('themes');

			this.themes(isArray(themes) ? themes : []);
			this.theme(SettingsGet('Theme'));
			if (!this.isMobile()) {
				this.userBackgroundName(SettingsGet('UserBackgroundName'));
				this.userBackgroundHash(SettingsGet('UserBackgroundHash'));
			}

			leftPanelDisabled(this.isMobile());
		}
	};

	ThemeStore.theme = ko.observable('').extend({ limitedList: ThemeStore.themes });

	ThemeStore.isMobile.subscribe(value => $htmlCL.toggle('rl-mobile', value));

	ThemeStore.userBackgroundHash.subscribe(value => {
		if (value) {
			$htmlCL.add('UserBackground');
			doc.body.style.backgroundImage = "url("+serverRequestRaw('UserBackground', value)+")";
		} else {
			$htmlCL.remove('UserBackground');
			doc.body.removeAttribute('style');
		}
	});

	const LanguageStore = {
		languages: ko.observableArray(),
		userLanguage: ko.observable(''),

		populate: function() {
			const aLanguages = Settings.app('languages');
			this.languages(isArray(aLanguages) ? aLanguages : []);
			this.language(SettingsGet('Language'));
			this.userLanguage(SettingsGet('UserLanguage'));
		}
	};

	LanguageStore.language = ko.observable('')
		.extend({ limitedList: LanguageStore.languages });

	class LanguagesPopupView extends AbstractViewPopup {
		constructor() {
			super('Languages');

			this.fLang = null;
			this.userLanguage = ko.observable('');

			this.langs = ko.observableArray();

			this.languages = koComputable(() => {
				const userLanguage = this.userLanguage();
				return this.langs.map(language => ({
					key: language,
					user: language === userLanguage,
					selected: ko.observable(false),
					fullName: convertLangName(language)
				}));
			});

			this.langs.subscribe(() => this.setLanguageSelection());
		}

		languageTooltipName(language) {
			return convertLangName(language, true);
		}

		setLanguageSelection() {
			const currentLang = this.fLang ? ko.unwrap(this.fLang) : '';
			this.languages().forEach(item => item.selected(item.key === currentLang));
		}

		onBeforeShow() {
			this.fLang = null;
			this.userLanguage('');

			this.langs([]);
		}

		onShow(fLanguage, langs, userLanguage) {
			this.fLang = fLanguage;
			this.userLanguage(userLanguage || '');

			this.langs(langs);
		}

		changeLanguage(lang) {
			this.fLang && this.fLang(lang);
			this.cancelCommand();
		}
	}

	class GeneralAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.language = LanguageStore.language;
			this.languages = LanguageStore.languages;

			const aLanguagesAdmin = Settings.app('languagesAdmin');
			this.languagesAdmin = ko.observableArray(isArray(aLanguagesAdmin) ? aLanguagesAdmin : []);
			this.languageAdmin = ko
				.observable(SettingsGet('LanguageAdmin'))
				.extend({ limitedList: this.languagesAdmin });

			this.theme = ThemeStore.theme;
			this.themes = ThemeStore.themes;

			addObservablesTo(this, {
				allowLanguagesOnSettings: !!SettingsGet('AllowLanguagesOnSettings'),
				newMoveToFolder: !!SettingsGet('NewMoveToFolder'),
				attachmentLimitTrigger: SaveSettingsStep.Idle,
				languageTrigger: SaveSettingsStep.Idle,
				themeTrigger: SaveSettingsStep.Idle,
				capaThemes: SettingsCapa(Capa.Themes),
				capaUserBackground: SettingsCapa(Capa.UserBackground),
				capaAdditionalAccounts: SettingsCapa(Capa.AdditionalAccounts),
				capaIdentities: SettingsCapa(Capa.Identities),
				capaAttachmentThumbnails: SettingsCapa(Capa.AttachmentThumbnails),
				dataFolderAccess: false
			});

			this.weakPassword = rl.app.weakPassword;

			/** https://github.com/RainLoop/rainloop-webmail/issues/1924
			if (this.weakPassword) {
				fetch('./data/VERSION?' + Math.random()).then(response => this.dataFolderAccess(response.ok));
			}
			*/

			this.mainAttachmentLimit = ko
				.observable(pInt(SettingsGet('AttachmentLimit')) / (1024 * 1024))
				.extend({ debounce: 500 });

			this.uploadData = SettingsGet('PhpUploadSizes');
			this.uploadDataDesc =
				this.uploadData && (this.uploadData.upload_max_filesize || this.uploadData.post_max_size)
					? [
							this.uploadData.upload_max_filesize
								? 'upload_max_filesize = ' + this.uploadData.upload_max_filesize + '; '
								: '',
							this.uploadData.post_max_size ? 'post_max_size = ' + this.uploadData.post_max_size : ''
					  ].join('')
					: '';

			addComputablesTo(this, {
				themesOptions: () => this.themes.map(theme => ({ optValue: theme, optText: convertThemeName(theme) })),

				languageFullName: () => convertLangName(this.language()),
				languageAdminFullName: () => convertLangName(this.languageAdmin())
			});

			this.languageAdminTrigger = ko.observable(SaveSettingsStep.Idle).extend({ debounce: 100 });

			const fReloadLanguageHelper = (saveSettingsStep) => () => {
					this.languageAdminTrigger(saveSettingsStep);
					setTimeout(() => this.languageAdminTrigger(SaveSettingsStep.Idle), 1000);
				},
				fSaveBoolHelper = key =>
					value => Remote.saveConfig({[key]: value ? 1 : 0});

			addSubscribablesTo(this, {
				mainAttachmentLimit: value =>
					Remote.saveConfig({
						AttachmentLimit: pInt(value)
					}, settingsSaveHelperSimpleFunction(this.attachmentLimitTrigger, this)),

				language: value =>
					Remote.saveConfig({
						Language: value.trim()
					}, settingsSaveHelperSimpleFunction(this.languageTrigger, this)),

				languageAdmin: value => {
					this.languageAdminTrigger(SaveSettingsStep.Animate);
					translatorReload(true, value)
						.then(fReloadLanguageHelper(SaveSettingsStep.TrueResult), fReloadLanguageHelper(SaveSettingsStep.FalseResult))
						.then(() => Remote.saveConfig({
							LanguageAdmin: value.trim()
						}));
				},

				theme: value => {
					changeTheme(value, this.themeTrigger);
					Remote.saveConfig({
						Theme: value.trim()
					}, settingsSaveHelperSimpleFunction(this.themeTrigger, this));
				},

				capaAdditionalAccounts: fSaveBoolHelper('CapaAdditionalAccounts'),

				capaIdentities: fSaveBoolHelper('CapaIdentities'),

				capaAttachmentThumbnails: fSaveBoolHelper('CapaAttachmentThumbnails'),

				capaThemes: fSaveBoolHelper('CapaThemes'),

				capaUserBackground: fSaveBoolHelper('CapaUserBackground'),

				allowLanguagesOnSettings: fSaveBoolHelper('AllowLanguagesOnSettings'),

				newMoveToFolder: fSaveBoolHelper('NewMoveToFolder')
			});
		}

		selectLanguage() {
			showScreenPopup(LanguagesPopupView, [this.language, this.languages(), LanguageStore.userLanguage()]);
		}

		selectLanguageAdmin() {
			showScreenPopup(LanguagesPopupView, [
				this.languageAdmin,
				this.languagesAdmin(),
				SettingsGet('UserLanguageAdmin')
			]);
		}
	}

	const DomainAdminStore = ko.observableArray();

	DomainAdminStore.loading = ko.observable(false);

	DomainAdminStore.fetch = () => {
		DomainAdminStore.loading(true);
		Remote.request('AdminDomainList',
			(iError, data) => {
				DomainAdminStore.loading(false);
				if (!iError) {
					DomainAdminStore(
						data.Result.map(item => {
							item.disabled = ko.observable(item.disabled);
							item.askDelete = ko.observable(false);
							return item;
						})
					);
				}
			}, {
				IncludeAliases: 1
			});
	};

	const domainToParams = oDomain => ({
				Name: oDomain.name(),

				IncHost: oDomain.imapServer(),
				IncPort: oDomain.imapPort(),
				IncSecure: oDomain.imapSecure(),

				UseSieve: oDomain.useSieve() ? 1 : 0,
				SieveHost: oDomain.sieveServer(),
				SievePort: oDomain.sievePort(),
				SieveSecure: oDomain.sieveSecure(),

				OutHost: oDomain.smtpServer(),
				OutPort: oDomain.smtpPort(),
				OutSecure: oDomain.smtpSecure(),
				OutAuth: oDomain.smtpAuth() ? 1 : 0,
				OutUsePhpMail: oDomain.smtpPhpMail() ? 1 : 0
			});

	class DomainPopupView extends AbstractViewPopup {
		constructor() {
			super('Domain');

			this.addObservables(this.getDefaults());
			this.addObservables({
				edit: false,

				saving: false,

				testing: false,
				testingDone: false,
				testingImapError: false,
				testingSieveError: false,
				testingSmtpError: false,
				testingImapErrorDesc: '',
				testingSieveErrorDesc: '',
				testingSmtpErrorDesc: '',

				imapServerFocus: false,
				sieveServerFocus: false,
				smtpServerFocus: false,
			});

			this.addComputables({
				headerText: () => {
					const name = this.name(),
						aliasName = this.aliasName();

					let result = '';

					if (this.edit()) {
						result = i18n('POPUPS_DOMAIN/TITLE_EDIT_DOMAIN', { NAME: name });
						if (aliasName) {
							result += ' ← ' + aliasName;
						}
					} else {
						result = name
								? i18n('POPUPS_DOMAIN/TITLE_ADD_DOMAIN_WITH_NAME', { NAME: name })
								: i18n('POPUPS_DOMAIN/TITLE_ADD_DOMAIN');
					}

					return result;
				},

				domainDesc: () => {
					const name = this.name();
					return !this.edit() && name ? i18n('POPUPS_DOMAIN/NEW_DOMAIN_DESC', { NAME: '*@' + name }) : '';
				},

				domainIsComputed: () => {
					const usePhpMail = this.smtpPhpMail(),
						useSieve = this.useSieve();

					return (
						this.name() &&
						this.imapServer() &&
						this.imapPort() &&
						(useSieve ? this.sieveServer() && this.sievePort() : true) &&
						((this.smtpServer() && this.smtpPort()) || usePhpMail)
					);
				},

				canBeTested: () => !this.testing() && this.domainIsComputed(),
				canBeSaved: () => !this.saving() && this.domainIsComputed()
			});

			this.addSubscribables({
				testingImapError: value => value || this.testingImapErrorDesc(''),
				testingSieveError: value => value || this.testingSieveErrorDesc(''),
				testingSmtpError: value => value || this.testingSmtpErrorDesc(''),

				// smart form improvements
				imapServerFocus: value =>
					value && this.name() && !this.imapServer() && this.imapServer(this.name().replace(/[.]?[*][.]?/g, '')),

				sieveServerFocus: value =>
					value && this.imapServer() && !this.sieveServer() && this.sieveServer(this.imapServer()),

				smtpServerFocus: value => value && this.imapServer() && !this.smtpServer()
					&& this.smtpServer(this.imapServer().replace(/imap/gi, 'smtp')),

				imapSecure: value => {
					if (this.enableSmartPorts()) {
						const port = pInt(this.imapPort());
						switch (pString(value)) {
							case '0':
							case '2':
								if (993 === port) {
									this.imapPort('143');
								}
								break;
							case '1':
								if (143 === port) {
									this.imapPort('993');
								}
								break;
							// no default
						}
					}
				},

				smtpSecure: value => {
					if (this.enableSmartPorts()) {
						const port = pInt(this.smtpPort());
						switch (pString(value)) {
							case '0':
								if (465 === port || 587 === port) {
									this.smtpPort('25');
								}
								break;
							case '1':
								if (25 === port || 587 === port) {
									this.smtpPort('465');
								}
								break;
							case '2':
								if (25 === port || 465 === port) {
									this.smtpPort('587');
								}
								break;
							// no default
						}
					}
				}
			});

			decorateKoCommands(this, {
				createOrAddCommand: self => self.canBeSaved(),
				testConnectionCommand: self => self.canBeTested()
			});
		}

		createOrAddCommand() {
			this.saving(true);
			Remote.request('AdminDomainSave',
				this.onDomainCreateOrSaveResponse.bind(this),
				Object.assign(domainToParams(this), {
					Create: this.edit() ? 0 : 1,

					IncShortLogin: this.imapShortLogin() ? 1 : 0,

					OutShortLogin: this.smtpShortLogin() ? 1 : 0,
					OutSetSender: this.smtpSetSender() ? 1 : 0,

					WhiteList: this.whiteList()
				})
			);
		}

		testConnectionCommand() {
			this.testingDone(false);
			this.testingImapError(false);
			this.testingSieveError(false);
			this.testingSmtpError(false);
			this.testing(true);

			Remote.request('AdminDomainTest',
				(iError, oData) => {
					this.testing(false);
					if (iError) {
						this.testingImapError(true);
						this.testingSieveError(true);
						this.testingSmtpError(true);
					} else {
						this.testingDone(true);
						this.testingImapError(true !== oData.Result.Imap);
						this.testingSieveError(true !== oData.Result.Sieve);
						this.testingSmtpError(true !== oData.Result.Smtp);

						if (this.testingImapError() && oData.Result.Imap) {
							this.testingImapErrorDesc('');
							this.testingImapErrorDesc(oData.Result.Imap);
						}

						if (this.testingSieveError() && oData.Result.Sieve) {
							this.testingSieveErrorDesc('');
							this.testingSieveErrorDesc(oData.Result.Sieve);
						}

						if (this.testingSmtpError() && oData.Result.Smtp) {
							this.testingSmtpErrorDesc('');
							this.testingSmtpErrorDesc(oData.Result.Smtp);
						}
					}
				},
				domainToParams(this)
			);
		}

		onDomainCreateOrSaveResponse(iError) {
			this.saving(false);
			if (iError) {
				this.savingError(getNotification(iError));
			} else {
				DomainAdminStore.fetch();
				this.closeCommand();
			}
		}

		clearTesting() {
			this.testing(false);
			this.testingDone(false);
			this.testingImapError(false);
			this.testingSieveError(false);
			this.testingSmtpError(false);
		}

		onShow(oDomain) {
			this.saving(false);

			this.clearTesting();

			this.clearForm();
			if (oDomain) {
				this.enableSmartPorts(false);

				this.edit(true);

				this.name(oDomain.Name);
				this.imapServer(oDomain.IncHost);
				this.imapPort('' + pInt(oDomain.IncPort));
				this.imapSecure(oDomain.IncSecure);
				this.imapShortLogin(!!oDomain.IncShortLogin);
				this.useSieve(!!oDomain.UseSieve);
				this.sieveServer(oDomain.SieveHost);
				this.sievePort('' + pInt(oDomain.SievePort));
				this.sieveSecure(oDomain.SieveSecure);
				this.smtpServer(oDomain.OutHost);
				this.smtpPort('' + pInt(oDomain.OutPort));
				this.smtpSecure(oDomain.OutSecure);
				this.smtpShortLogin(!!oDomain.OutShortLogin);
				this.smtpAuth(!!oDomain.OutAuth);
				this.smtpSetSender(!!oDomain.OutSetSender);
				this.smtpPhpMail(!!oDomain.OutUsePhpMail);
				this.whiteList(oDomain.WhiteList);
				this.aliasName(oDomain.AliasName);

				this.enableSmartPorts(true);
			}
		}

		getDefaults() {
			return {
				savingError: '',

				name: '',

				imapServer: '',
				imapPort: '143',
				imapSecure: 0,
				imapShortLogin: false,

				useSieve: false,
				sieveServer: '',
				sievePort: '4190',
				sieveSecure: 0,

				smtpServer: '',
				smtpPort: '25',
				smtpSecure: 0,
				smtpShortLogin: false,
				smtpAuth: true,
				smtpSetSender: false,
				smtpPhpMail: false,

				whiteList: '',
				aliasName: '',

				enableSmartPorts: false
			};
		}

		clearForm() {
			this.edit(false);
			forEachObjectEntry(this.getDefaults(), (key, value) => this[key](value));
			this.enableSmartPorts(true);
		}
	}

	class DomainAliasPopupView extends AbstractViewPopup {
		constructor() {
			super('DomainAlias');

			this.addObservables({
				saving: false,
				savingError: '',

				name: '',

				alias: ''
			});

			this.addComputables({
				domains: () => DomainAdminStore.filter(item => item && !item.alias),

				domainsOptions: () => this.domains().map(item => ({ optValue: item.name, optText: item.name })),

				canBeSaved: () => !this.saving() && this.name() && this.alias()
			});

			decorateKoCommands(this, {
				createCommand: self => self.canBeSaved()
			});
		}

		createCommand() {
			this.saving(true);
			Remote.request('AdminDomainAliasSave',
				iError => {
					this.saving(false);
					if (iError) {
						this.savingError(getNotification(iError));
					} else {
						DomainAdminStore.fetch();
						this.closeCommand();
					}
				}, {
					Name: this.name(),
					Alias: this.alias()
				});
		}

		onShow() {
			this.clearForm();
		}

		clearForm() {
			this.saving(false);
			this.savingError('');

			this.name('');

			this.alias('');
		}
	}

	class DomainsAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.domains = DomainAdminStore;

			this.domainForDeletion = ko.observable(null).askDeleteHelper();
		}

		createDomain() {
			showScreenPopup(DomainPopupView);
		}

		createDomainAlias() {
			showScreenPopup(DomainAliasPopupView);
		}

		deleteDomain(domain) {
			DomainAdminStore.remove(domain);
			Remote.domainDelete(DomainAdminStore.fetch, domain.name);
			Remote.request('AdminDomainDelete', DomainAdminStore.fetch, {
				Name: domain.name
			});
		}

		disableDomain(domain) {
			domain.disabled(!domain.disabled());
			Remote.request('AdminDomainDisable', DomainAdminStore.fetch, {
				Name: domain.name,
				Disabled: domain.disabled() ? 1 : 0
			});
		}

		onBuild(oDom) {
			oDom.addEventListener('click', event => {
				let el = event.target.closestWithin('.b-admin-domains-list-table .e-action', oDom);
				el && ko.dataFor(el) && Remote.request('AdminDomainLoad',
					(iError, oData) => iError || showScreenPopup(DomainPopupView, [oData.Result]),
					{
						Name: ko.dataFor(el).name
					}
				);

			});

			DomainAdminStore.fetch();
		}
	}

	class LoginAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			addObservablesTo(this, {
				determineUserLanguage: !!SettingsGet('DetermineUserLanguage'),
				determineUserDomain: !!SettingsGet('DetermineUserDomain'),
				allowLanguagesOnLogin: !!SettingsGet('AllowLanguagesOnLogin'),
				hideSubmitButton: !!Settings.app('hideSubmitButton'),
				defaultDomain: SettingsGet('LoginDefaultDomain'),
				defaultDomainTrigger: SaveSettingsStep.Idle
			});

			addSubscribablesTo(this, {
				determineUserLanguage: value =>
					Remote.saveConfig({
						DetermineUserLanguage: value ? 1 : 0
					}),

				determineUserDomain: value =>
					Remote.saveConfig({
						DetermineUserDomain: value ? 1 : 0
					}),

				allowLanguagesOnLogin: value =>
					Remote.saveConfig({
						AllowLanguagesOnLogin: value ? 1 : 0
					}),

				hideSubmitButton: value =>
					Remote.saveConfig({
						hideSubmitButton: value ? 1 : 0
					}),

				defaultDomain: (value =>
					Remote.saveConfig({
						LoginDefaultDomain: value.trim()
					}, settingsSaveHelperSimpleFunction(this.defaultDomainTrigger, this))
				).debounce(999)
			});
		}
	}

	class ContactsAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.defaultOptionsAfterRender = defaultOptionsAfterRender;

			addObservablesTo(this, {
				enableContacts: !!SettingsGet('ContactsEnable'),
				contactsSync: !!SettingsGet('ContactsSync'),
				contactsType: SettingsGet('ContactsPdoType'),

				pdoDsn: SettingsGet('ContactsPdoDsn'),
				pdoUser: SettingsGet('ContactsPdoUser'),
				pdoPassword: SettingsGet('ContactsPdoPassword'),

				pdoDsnTrigger: SaveSettingsStep.Idle,
				pdoUserTrigger: SaveSettingsStep.Idle,
				pdoPasswordTrigger: SaveSettingsStep.Idle,
				contactsTypeTrigger: SaveSettingsStep.Idle,

				testing: false,
				testContactsSuccess: false,
				testContactsError: false,
				testContactsErrorMessage: ''
			});

			const supportedTypes = SettingsGet('supportedPdoDrivers') || [],
				types = [{
					id:'sqlite',
					name:'SQLite'
				},{
					id:'mysql',
					name:'MySQL'
				},{
					id:'pgsql',
					name:'PostgreSQL'
				}].filter(type => supportedTypes.includes(type.id));

			this.contactsSupported = 0 < types.length;

			this.contactsTypesOptions = types;

			this.mainContactsType = ko
				.computed({
					read: this.contactsType,
					write: value => {
						if (value !== this.contactsType()) {
							if (supportedTypes.includes(value)) {
								this.contactsType(value);
							} else if (types.length) {
								this.contactsType('');
							}
						} else {
							this.contactsType.valueHasMutated();
						}
					}
				})
				.extend({ notify: 'always' });

			addSubscribablesTo(this, {
				enableContacts: value =>
					Remote.saveConfig({
						ContactsEnable: value ? 1 : 0
					}),

				contactsSync: value =>
					Remote.saveConfig({
						ContactsSync: value ? 1 : 0
					}),

				contactsType: value => {
					this.testContactsSuccess(false);
					this.testContactsError(false);
					this.testContactsErrorMessage('');
					Remote.saveConfig({
						ContactsPdoType: value.trim()
					}, settingsSaveHelperSimpleFunction(this.contactsTypeTrigger, this));
				},

				pdoDsn: value =>
					Remote.saveConfig({
						ContactsPdoDsn: value.trim()
					}, settingsSaveHelperSimpleFunction(this.pdoDsnTrigger, this)),

				pdoUser: value =>
					Remote.saveConfig({
						ContactsPdoUser: value.trim()
					}, settingsSaveHelperSimpleFunction(this.pdoUserTrigger, this)),

				pdoPassword: value =>
					Remote.saveConfig({
						ContactsPdoPassword: value.trim()
					}, settingsSaveHelperSimpleFunction(this.pdoPasswordTrigger, this))
			});

			decorateKoCommands(this, {
				testContactsCommand: self => self.pdoDsn() && self.pdoUser()
			});
		}

		testContactsCommand() {
			this.testContactsSuccess(false);
			this.testContactsError(false);
			this.testContactsErrorMessage('');
			this.testing(true);

			Remote.request('AdminContactsTest',
				(iError, data) => {
					this.testContactsSuccess(false);
					this.testContactsError(false);
					this.testContactsErrorMessage('');

					if (!iError && data.Result.Result) {
						this.testContactsSuccess(true);
					} else {
						this.testContactsError(true);
						if (data && data.Result) {
							this.testContactsErrorMessage(data.Result.Message || '');
						} else {
							this.testContactsErrorMessage('');
						}
					}

					this.testing(false);
				}, {
					ContactsPdoType: this.contactsType(),
					ContactsPdoDsn: this.pdoDsn(),
					ContactsPdoUser: this.pdoUser(),
					ContactsPdoPassword: this.pdoPassword()
				}
			);
		}

		onShow() {
			this.testContactsSuccess(false);
			this.testContactsError(false);
			this.testContactsErrorMessage('');
		}
	}

	class SecurityAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.weakPassword = rl.app.weakPassword;

			addObservablesTo(this, {
				useLocalProxyForExternalImages: !!SettingsGet('UseLocalProxyForExternalImages'),

				verifySslCertificate: !!SettingsGet('VerifySslCertificate'),
				allowSelfSigned: !!SettingsGet('AllowSelfSigned'),

				adminLogin: SettingsGet('AdminLogin'),
				adminLoginError: false,
				adminPassword: '',
				adminPasswordNew: '',
				adminPasswordNew2: '',
				adminPasswordNewError: false,
				adminTOTP: SettingsGet('AdminTOTP'),

				adminPasswordUpdateError: false,
				adminPasswordUpdateSuccess: false,

				capaOpenPGP: Settings.capa(Capa.OpenPGP)
			});

			addSubscribablesTo(this, {
				adminPassword: () => {
					this.adminPasswordUpdateError(false);
					this.adminPasswordUpdateSuccess(false);
				},

				adminLogin: () => this.adminLoginError(false),

				adminPasswordNew: () => {
					this.adminPasswordUpdateError(false);
					this.adminPasswordUpdateSuccess(false);
					this.adminPasswordNewError(false);
				},

				adminPasswordNew2: () => {
					this.adminPasswordUpdateError(false);
					this.adminPasswordUpdateSuccess(false);
					this.adminPasswordNewError(false);
				},

				capaOpenPGP: value =>
					Remote.saveConfig({
						CapaOpenPGP: value ? 1 : 0
					}),

				useLocalProxyForExternalImages: value =>
					Remote.saveConfig({
						UseLocalProxyForExternalImages: value ? 1 : 0
					}),

				verifySslCertificate: value => {
					value || this.allowSelfSigned(true);
					Remote.saveConfig({
						VerifySslCertificate: value ? 1 : 0
					});
				},

				allowSelfSigned: value =>
					Remote.saveConfig({
						AllowSelfSigned: value ? 1 : 0
					})
			});

			decorateKoCommands(this, {
				saveNewAdminPasswordCommand: self => self.adminLogin().trim() && self.adminPassword()
			});
		}

		saveNewAdminPasswordCommand() {
			if (!this.adminLogin().trim()) {
				this.adminLoginError(true);
				return false;
			}

			if (this.adminPasswordNew() !== this.adminPasswordNew2()) {
				this.adminPasswordNewError(true);
				return false;
			}

			this.adminPasswordUpdateError(false);
			this.adminPasswordUpdateSuccess(false);

			Remote.request('AdminPasswordUpdate', (iError, data) => {
				if (iError) {
					this.adminPasswordUpdateError(true);
				} else {
					this.adminPassword('');
					this.adminPasswordNew('');
					this.adminPasswordNew2('');

					this.adminPasswordUpdateSuccess(true);

					this.weakPassword(!!data.Result.Weak);
				}
			}, {
				'Login': this.adminLogin(),
				'Password': this.adminPassword(),
				'NewPassword': this.adminPasswordNew(),
				'TOTP': this.adminTOTP()
			});

			return true;
		}

		onHide() {
			this.adminPassword('');
			this.adminPasswordNew('');
			this.adminPasswordNew2('');
		}
	}

	const PackageAdminStore = ko.observableArray();

	PackageAdminStore.real = ko.observable(true);

	PackageAdminStore.loading = ko.observable(false);

	PackageAdminStore.error = ko.observable('');

	PackageAdminStore.fetch = () => {
		PackageAdminStore.loading(true);
		Remote.request('AdminPackagesList', (iError, data) => {
			PackageAdminStore.loading(false);
			if (iError) {
				PackageAdminStore.real(false);
			} else {
				PackageAdminStore.real(!!data.Result.Real);
				PackageAdminStore.error(data.Result.Error);

				const loading = {};
				PackageAdminStore.forEach(item => {
					if (item && item.loading()) {
						loading[item.file] = item;
					}
				});

				let list = [];
				if (isArray(data.Result.List)) {
					list = data.Result.List.filter(v => v).map(item => {
						item.loading = ko.observable(loading[item.file] !== undefined);
						item.enabled = ko.observable(item.enabled);
						return item;
					});
				}

				PackageAdminStore(list);
			}
		});
	};

	class AskPopupView extends AbstractViewPopup {
		constructor() {
			super('Ask');

			this.addObservables({
				askDesc: '',
				yesButton: '',
				noButton: '',
				passphrase: '',
				askPass: false
			});

			this.fYesAction = null;
			this.fNoAction = null;

			this.focusOnShow = true;
			this.bDisabeCloseOnEsc = true;
		}

		yesClick() {
			this.cancelCommand();

			isFunction(this.fYesAction) && this.fYesAction();
		}

		noClick() {
			this.cancelCommand();

			isFunction(this.fNoAction) && this.fNoAction();
		}

		/**
		 * @param {string} sAskDesc
		 * @param {Function=} fYesFunc
		 * @param {Function=} fNoFunc
		 * @param {boolean=} focusOnShow = true
		 * @returns {void}
		 */
		onShow(sAskDesc, fYesFunc = null, fNoFunc = null, focusOnShow = true, askPass = false, btnText = '') {
			this.askDesc(sAskDesc || '');
			this.askPass(askPass);
			this.passphrase('');
			this.yesButton(i18n(btnText || 'POPUPS_ASK/BUTTON_YES'));
			this.noButton(i18n(askPass ? 'GLOBAL/CANCEL' : 'POPUPS_ASK/BUTTON_NO'));
			this.fYesAction = fYesFunc;
			this.fNoAction = fNoFunc;
			this.focusOnShow = focusOnShow ? (askPass ? 'input[type="password"]' : '.buttonYes') : '';
		}

		onShowWithDelay() {
			this.focusOnShow && this.querySelector(this.focusOnShow).focus();
		}

		onBuild() {
	//		shortcuts.add('tab', 'shift', Scope.Ask, () => {
			shortcuts.add('tab,arrowright,arrowleft', '', Scope.Ask, () => {
				let btn = this.querySelector('.buttonYes');
				if (btn.matches(':focus')) {
					btn = this.querySelector('.buttonNo');
				}
				btn.focus();
				return false;
			});

			shortcuts.add('escape', '', Scope.Ask, () => {
				this.noClick();
				return false;
			});
		}
	}

	AskPopupView.password = function(sAskDesc, btnText) {
		return new Promise(resolve => {
			this.showModal([
				sAskDesc,
				() => resolve(this.__vm.passphrase()),
				() => resolve(null),
				true,
				true,
				btnText
			]);
		});
	};

	class PluginPopupView extends AbstractViewPopup {
		constructor() {
			super('Plugin');

			this.addObservables({
				saveError: '',
				id: '',
				name: '',
				readme: ''
			});

			this.config = ko.observableArray();

			this.addComputables({
				hasReadme: () => !!this.readme(),
				hasConfiguration: () => 0 < this.config().length
			});

			this.bDisabeCloseOnEsc = true;
			this.keyScope.scope = Scope.All;

			this.tryToClosePopup = this.tryToClosePopup.debounce(200);

			decorateKoCommands(this, {
				saveCommand: self => self.hasConfiguration()
			});
		}

		saveCommand() {
			const oConfig = {
				Id: this.id(),
				Settings: {}
			};

			this.config.forEach(oItem => {
				let value = oItem.value();
				if (false === value || true === value) {
					value = value ? 1 : 0;
				}
				oConfig.Settings[oItem.Name] = value;
			});

			this.saveError('');
			Remote.request('AdminPluginSettingsUpdate',
				iError => iError
					? this.saveError(getNotification(iError))
					: this.cancelCommand(),
				oConfig);
		}

		onShow(oPlugin) {
			this.id('');
			this.name('');
			this.readme('');
			this.config([]);

			if (oPlugin) {
				this.id(oPlugin.Id);
				this.name(oPlugin.Name);
				this.readme(oPlugin.Readme);

				const config = oPlugin.Config;
				if (arrayLength(config)) {
					this.config(
						config.map(item => {
							item.value = ko.observable(item.value);
							return item;
						})
					);
				}
			}
		}

		tryToClosePopup() {
			if (AskPopupView.hidden()) {
				showScreenPopup(AskPopupView, [
					i18n('POPUPS_ASK/DESC_WANT_CLOSE_THIS_WINDOW'),
					() => this.modalVisibility() && this.cancelCommand()
				]);
			}
		}

		onBuild() {
			shortcuts.add('escape', '', Scope.All, () => {
				if (this.modalVisibility()) {
					this.tryToClosePopup();
					return false;
				}
			});
		}
	}

	class PackagesAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			addObservablesTo(this, {
				packagesError: '',
				enabledPlugins: !!SettingsGet('EnabledPlugins')
			});

			this.packages = PackageAdminStore;

			addComputablesTo(this, {
				packagesCurrent: () => PackageAdminStore.filter(item => item && item.installed && !item.canBeUpdated),
				packagesAvailableForUpdate: () => PackageAdminStore.filter(item => item && item.installed && !!item.canBeUpdated),
				packagesAvailableForInstallation: () => PackageAdminStore.filter(item => item && !item.installed),

				visibility: () => (PackageAdminStore.loading() ? 'visible' : 'hidden')
			});

			this.enabledPlugins.subscribe(value =>
				Remote.saveConfig({
					EnabledPlugins: value ? 1 : 0
				})
			);
		}

		onShow() {
			this.packagesError('');
		}

		onBuild(oDom) {
			PackageAdminStore.fetch();

			oDom.addEventListener('click', event => {
				// configurePlugin
				let el = event.target.closestWithin('.package-configure', oDom),
					data = el ? ko.dataFor(el) : 0;
				data && Remote.request('AdminPluginLoad',
					(iError, data) => iError || showScreenPopup(PluginPopupView, [data.Result]),
					{
						Id: data.id
					}
				);
				// disablePlugin
				el = event.target.closestWithin('.package-active', oDom);
				data = el ? ko.dataFor(el) : 0;
				data && this.disablePlugin(data);
			});
		}

		requestHelper(packageToRequest, install) {
			return (iError, data) => {
				PackageAdminStore.forEach(item => {
					if (item && packageToRequest && item.loading && item.loading() && packageToRequest.file === item.file) {
						packageToRequest.loading(false);
						item.loading(false);
					}
				});

				if (iError) {
					this.packagesError(
						getNotification(install ? Notification.CantInstallPackage : Notification.CantDeletePackage)
						+ (data.ErrorMessage ? ':\n' + data.ErrorMessage : '')
					);
				} else if (data.Result.Reload) {
					location.reload();
				} else {
					PackageAdminStore.fetch();
				}
			};
		}

		deletePackage(packageToDelete) {
			if (packageToDelete) {
				packageToDelete.loading(true);
				Remote.request('AdminPackageDelete',
					this.requestHelper(packageToDelete, false),
					{
						Id: packageToDelete.id
					}
				);
			}
		}

		installPackage(packageToInstall) {
			if (packageToInstall) {
				packageToInstall.loading(true);
				Remote.request('AdminPackageInstall',
					this.requestHelper(packageToInstall, true),
					{
						Id: packageToInstall.id,
						Type: packageToInstall.type,
						File: packageToInstall.file
					},
					60000
				);
			}
		}

		disablePlugin(plugin) {
			let disable = plugin.enabled();
			plugin.enabled(!disable);
			Remote.request('AdminPluginDisable',
			(iError, data) => {
					if (iError) {
						plugin.enabled(disable);
						this.packagesError(
							(Notification.UnsupportedPluginPackage === iError && data && data.ErrorMessage)
							? data.ErrorMessage
							: getNotification(iError)
						);
					}
	//				PackageAdminStore.fetch();
				}, {
					Id: plugin.id,
					Disabled: disable ? 1 : 0
				}
			);
		}

	}

	class AboutAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.version = ko.observable(Settings.app('version'));
			this.phpextensions = ko.observableArray();
		}

		onBuild() {
			Remote.request('AdminPHPExtensions', (iError, data) => iError || this.phpextensions(data.Result));
		}

	}

	class BrandingAdminSettings /*extends AbstractViewSettings*/ {
		constructor() {
			addObservablesTo(this, {
				title: SettingsGet('Title'),
				loadingDesc: SettingsGet('LoadingDescription'),
				faviconUrl: SettingsGet('FaviconUrl'),

				titleTrigger: SaveSettingsStep.Idle,
				loadingDescTrigger: SaveSettingsStep.Idle,
				faviconUrlTrigger: SaveSettingsStep.Idle
			});

			addSubscribablesTo(this, {
				title: (value =>
					Remote.saveConfig({
						Title: value.trim()
					}, settingsSaveHelperSimpleFunction(this.titleTrigger, this))
				).debounce(999),

				loadingDesc: (value =>
					Remote.saveConfig({
						LoadingDescription: value.trim()
					}, settingsSaveHelperSimpleFunction(this.loadingDescTrigger, this))
				).debounce(999),

				faviconUrl: (value =>
					Remote.saveConfig({
						FaviconUrl: value.trim()
					}, settingsSaveHelperSimpleFunction(this.faviconUrlTrigger, this))
				).debounce(999)
			});
		}
	}

	class ConfigAdminSettings /*extends AbstractViewSettings*/ {

		constructor() {
			this.config = ko.observableArray();
		}

		onBeforeShow() {
			Remote.request('AdminSettingsGet', (iError, data) => {
				if (!iError) {
					const cfg = [],
						getInputType = (value, pass) => {
							switch (typeof value)
							{
							case 'boolean': return 'checkbox';
							case 'number': return 'number';
							}
							return pass ? 'password' : 'text';
						};
					forEachObjectEntry(data.Result, (key, items) => {
						const section = {
							name: key,
							items: []
						};
						forEachObjectEntry(items, (skey, item) => {
							section.items.push({
								key: `config[${key}][${skey}]`,
								name: skey,
								value: item[0],
								type: getInputType(item[0], skey.includes('password')),
								comment: item[1]
							});
						});
						cfg.push(section);
					});
					this.config(cfg);
				}
			});
		}

		saveConfig(form) {
			Remote.post('AdminSettingsSet', null, new FormData(form));
		}
	}

	class MenuSettingsAdminView extends AbstractViewLeft {
		/**
		 * @param {?} screen
		 */
		constructor(screen) {
			super('AdminMenu');

			this.menu = screen.menu;
		}

		link(route) {
			return '#/' + route;
		}
	}

	class PaneSettingsAdminView extends AbstractViewRight {
		constructor() {
			super('AdminPane');

			this.version = ko.observable(Settings.app('version'));

			this.leftPanelDisabled = leftPanelDisabled;

			this.adminManLoadingVisibility = koComputable(() => PackageAdminStore.loading() ? 'visible' : 'hidden');
		}

		logoutClick() {
			Remote.request('AdminLogout', () => rl.logoutReload());
		}
	}

	class SettingsAdminScreen extends AbstractSettingsScreen {
		constructor() {
			super([MenuSettingsAdminView, PaneSettingsAdminView]);

			settingsAddViewModel(
				GeneralAdminSettings,
				'AdminSettingsGeneral',
				'TABS_LABELS/LABEL_GENERAL_NAME',
				'general',
				true
			);

			[
				[DomainsAdminSettings, 'Domains'],
				[LoginAdminSettings, 'Login'],
				[BrandingAdminSettings, 'Branding'],
				[ContactsAdminSettings, 'Contacts'],
				[SecurityAdminSettings, 'Security'],
				[PackagesAdminSettings, 'Packages'],
				[ConfigAdminSettings, 'Config'],
				[AboutAdminSettings, 'About'],
			].forEach(item =>
				settingsAddViewModel(
					item[0],
					'AdminSettings'+item[1],
					'TABS_LABELS/LABEL_'+item[1].toUpperCase()+'_NAME',
					item[1].toLowerCase()
				)
			);

			runSettingsViewModelHooks(true);
		}

		onShow() {
			rl.setWindowTitle();
		}
	}

	class LoginAdminView extends AbstractViewLogin {
		constructor() {
			super('AdminLogin');

			this.addObservables({
				login: '',
				password: '',
				totp: '',

				loginError: false,
				passwordError: false,

				submitRequest: false,
				submitError: ''
			});

			this.addSubscribables({
				login: () => this.loginError(false),
				password: () => this.passwordError(false)
			});

			decorateKoCommands(this, {
				submitCommand: self => !self.submitRequest()
			});
		}

		submitCommand(self, event) {
			let form = event.target.form,
				data = new FormData(form),
				valid = form.reportValidity() && fireEvent('sm-admin-login', data);

			this.loginError(!this.login());
			this.passwordError(!this.password());
			this.formError(!valid);

			if (valid) {
				this.submitRequest(true);

				Remote.request('AdminLogin',
					(iError, oData) => {
						fireEvent('sm-admin-login-response', {
							error: iError,
							data: oData
						});
						if (iError) {
							this.submitRequest(false);
							this.submitError(getNotification(iError));
						} else {
							rl.setData(oData.Result);
						}
					},
					data
				);
			}

			return valid;
		}
	}

	class LoginAdminScreen extends AbstractScreen {
		constructor() {
			super('login', [LoginAdminView]);
		}

		onShow() {
			rl.setWindowTitle();
		}
	}

	class AbstractComponent {
		constructor() {
			this.disposable = [];
		}

		dispose() {
			this.disposable.forEach((funcToDispose) => {
				if (funcToDispose && funcToDispose.dispose) {
					funcToDispose.dispose();
				}
			});
		}
	}

	class AbstractInput extends AbstractComponent {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super();

			this.value = params.value || '';
			this.label = params.label || '';
			this.enable = null == params.enable ? true : params.enable;
			this.trigger = params.trigger && params.trigger.subscribe ? params.trigger : null;
			this.placeholder = params.placeholder || '';

			this.labeled = null != params.label;

			let size = 0 < params.size ? 'span' + params.size : '';
			if (this.trigger) {
				const
					classForTrigger = ko.observable(''),
					setTriggerState = value => {
						switch (pInt(value)) {
							case SaveSettingsStep.TrueResult:
								classForTrigger('success');
								break;
							case SaveSettingsStep.FalseResult:
								classForTrigger('error');
								break;
							default:
								classForTrigger('');
								break;
						}
					};

				setTriggerState(this.trigger());

				this.className = koComputable(() =>
					(size + ' settings-saved-trigger-input ' + classForTrigger()).trim()
				);

				this.disposable.push(this.trigger.subscribe(setTriggerState, this));
			} else {
				this.className = size;
			}

			this.disposable.push(this.className);
		}
	}

	class InputComponent extends AbstractInput {}

	class SelectComponent extends AbstractInput {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super(params);

			this.options = params.options || '';

			this.optionsText = params.optionsText || null;
			this.optionsValue = params.optionsValue || null;
			this.optionsCaption = i18n(params.optionsCaption || null);

			this.defaultOptionsAfterRender = defaultOptionsAfterRender;
		}
	}

	class TextAreaComponent extends AbstractInput {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super(params);

			this.rows = params.rows || 5;
			this.spellcheck = !!params.spellcheck;
		}
	}

	class AbstractCheckbox extends AbstractComponent {
		/**
		 * @param {Object} params = {}
		 */
		constructor(params = {}) {
			super();

			this.value = ko.isObservable(params.value) ? params.value
				: ko.observable(!!params.value);

			this.enable = ko.isObservable(params.enable) ? params.enable
				: ko.observable(undefined === params.enable || !!params.enable);

			this.label = params.label || '';
			this.inline = !!params.inline;

			this.labeled = undefined !== params.label;
		}

		click() {
			this.enable() && this.value(!this.value());
		}
	}

	class CheckboxMaterialDesignComponent extends AbstractCheckbox {}

	class CheckboxComponent extends AbstractCheckbox {}

	class AbstractApp {
		/**
		 * @param {RemoteStorage|AdminRemoteStorage} Remote
		 */
		constructor(Remote) {
			this.Remote = Remote;
		}

		logoutReload() {
			const url = logoutLink();

			if (location.href !== url) {
				setTimeout(() => (Settings.app('inIframe') ? parent : window).location.href = url, 100);
			} else {
				rl.route.reload();
			}
		}

		refresh() {
	//		rl.adminArea() || !translatorReload(false, );
			rl.adminArea() || (
				LanguageStore.language(SettingsGet('Language'))
				& ThemeStore.populate()
				& changeTheme(SettingsGet('Theme'))
			);

			this.start();
		}

		bootstart() {
			const register = (key, ClassObject, templateID) => ko.components.register(key, {
					template: { element: templateID || (key + 'Component') },
					viewModel: {
						createViewModel: (params, componentInfo) => {
							params = params || {};

							if (componentInfo && componentInfo.element) {

								i18nToNodes(componentInfo.element);

								if (params.inline) {
									componentInfo.element.style.display = 'inline-block';
								}
							}

							return new ClassObject(params);
						}
					}
				});

			register('Input', InputComponent);
			register('Select', SelectComponent);
			register('TextArea', TextAreaComponent);
			register('Checkbox', CheckboxMaterialDesignComponent, 'CheckboxMaterialDesignComponent');
			register('CheckboxSimple', CheckboxComponent, 'CheckboxComponent');

			initOnStartOrLangChange();

			LanguageStore.populate();
			ThemeStore.populate();

			this.start();
		}
	}

	class AdminApp extends AbstractApp {
		constructor() {
			super(Remote);
			this.weakPassword = ko.observable(false);
		}

		start() {
			if (!Settings.app('adminAllowed')) {
				rl.route.root();
				setTimeout(() => location.href = '/', 1);
			} else if (SettingsGet('Auth')) {
				this.weakPassword(!!SettingsGet('WeakPassword'));
				startScreens([SettingsAdminScreen]);
			} else {
				startScreens([LoginAdminScreen]);
			}
		}
	}

	var App = new AdminApp();

	bootstrap(App);

}());
