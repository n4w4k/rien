/* SnappyMail Webmail (c) SnappyMail | Licensed under AGPL v3 */
(function () {
	'use strict';

	/* eslint quote-props: 0 */

	/**
	 * @enum {string}
	 */
	const Capa = {
		GnuPG: 'GNUPG',
		OpenPGP: 'OPEN_PGP',
		Prefetch: 'PREFETCH',
		Contacts: 'CONTACTS',
		Search: 'SEARCH',
		SearchAdv: 'SEARCH_ADV',
		MessageActions: 'MESSAGE_ACTIONS',
		AttachmentsActions: 'ATTACHMENTS_ACTIONS',
		DangerousActions: 'DANGEROUS_ACTIONS',
		Themes: 'THEMES',
		UserBackground: 'USER_BACKGROUND',
		Sieve: 'SIEVE',
		AttachmentThumbnails: 'ATTACHMENT_THUMBNAILS',
		AutoLogout: 'AUTOLOGOUT',
		Kolab: 'KOLAB',
		AdditionalAccounts: 'ADDITIONAL_ACCOUNTS',
		Identities: 'IDENTITIES'
	};

	/**
	 * @enum {string}
	 */
	const Scope = {
		All: 'all',
		None: 'none',
		Contacts: 'Contacts',
		MessageList: 'MessageList',
		FolderList: 'FolderList',
		MessageView: 'MessageView',
		Compose: 'Compose',
		Settings: 'Settings',
		Menu: 'Menu',
		OpenPgpKey: 'OpenPgpKey',
		KeyboardShortcutsHelp: 'KeyboardShortcutsHelp',
		Ask: 'Ask'
	};

	/**
	 * @enum {number}
	 */
	const UploadErrorCode = {
		Normal: 0,
		FileIsTooBig: 1,
		FilePartiallyUploaded: 3,
		NoFileUploaded: 4,
		MissingTempFolder: 6,
		OnSavingFile: 7,
		FileType: 98,
		Unknown: 99
	};

	/**
	 * @enum {number}
	 */
	const SaveSettingsStep = {
		Animate: -2,
		Idle: -1,
		TrueResult: 1,
		FalseResult: 0
	};

	/**
	 * @enum {number}
	 */
	const Notification = {
		RequestError: 1,
		RequestAborted: 2,

		// Global
		InvalidToken: 101,
		AuthError: 102,

		// User
		ConnectionError: 104,
		DomainNotAllowed: 109,
		AccountNotAllowed: 110,

		ContactsSyncError: 140,

		CantGetMessageList: 201,
		CantGetMessage: 202,
		CantDeleteMessage: 203,
		CantMoveMessage: 204,
		CantCopyMessage: 205,

		CantSaveMessage: 301,
		CantSendMessage: 302,
		InvalidRecipients: 303,

		CantSaveFilters: 351,
		CantGetFilters: 352,
		CantActivateFiltersScript: 353,
		CantDeleteFiltersScript: 354,
	//	FiltersAreNotCorrect: 355,

		CantCreateFolder: 400,
		CantRenameFolder: 401,
		CantDeleteFolder: 402,
		CantSubscribeFolder: 403,
		CantUnsubscribeFolder: 404,
		CantDeleteNonEmptyFolder: 405,

	//	CantSaveSettings: 501,

		DomainAlreadyExists: 601,

		DemoSendMessageError: 750,
		DemoAccountError: 751,

		AccountAlreadyExists: 801,
		AccountDoesNotExist: 802,
		AccountSwitchFailed: 803,

		MailServerError: 901,
		ClientViewError: 902,
		InvalidInputArgument: 903,

		JsonFalse: 950,
		JsonParse: 952,
	//	JsonTimeout: 953,

		UnknownNotification: 998,
		UnknownError: 999,

		// Admin
		CantInstallPackage: 701,
		CantDeletePackage: 702,
		InvalidPluginPackage: 703,
		UnsupportedPluginPackage: 704,
		CantSavePluginSettings: 705
	};

	let keyScopeFake = Scope.All;

	const

		doc = document,

		$htmlCL = doc.documentElement.classList,

		elementById = id => doc.getElementById(id),

		exitFullscreen = () => getFullscreenElement() && (doc.exitFullscreen || doc.webkitExitFullscreen).call(doc),
		getFullscreenElement = () => doc.fullscreenElement || doc.webkitFullscreenElement,

		Settings = rl.settings,
		SettingsGet = Settings.get,
		SettingsCapa = Settings.capa,

		dropdownVisibility = ko.observable(false).extend({ rateLimit: 0 }),

		moveAction = ko.observable(false),
		leftPanelDisabled = ko.observable(false),

		createElement = (name, attr) => {
			let el = doc.createElement(name);
			attr && Object.entries(attr).forEach(([k,v]) => el.setAttribute(k,v));
			return el;
		},

		fireEvent = (name, detail) => dispatchEvent(new CustomEvent(name, {detail:detail})),

		// keys
		keyScopeReal = ko.observable(Scope.All),
		keyScope = value => {
			if (value) {
				if (Scope.Menu !== value) {
					keyScopeFake = value;
					if (dropdownVisibility()) {
						value = Scope.Menu;
					}
				}
				keyScopeReal(value);
				shortcuts.setScope(value);
			} else {
				return keyScopeFake;
			}
		};

	dropdownVisibility.subscribe(value => {
		if (value) {
			keyScope(Scope.Menu);
		} else if (Scope.Menu === shortcuts.getScope()) {
			keyScope(keyScopeFake);
		}
	});

	leftPanelDisabled.toggle = () => leftPanelDisabled(!leftPanelDisabled());
	leftPanelDisabled.subscribe(value => {
		value && moveAction() && moveAction(false);
		$htmlCL.toggle('rl-left-panel-disabled', value);
	});

	moveAction.subscribe(value => value && leftPanelDisabled() && leftPanelDisabled(false));

	let __themeTimer = 0,
		__themeJson = null;

	const
		isArray = Array.isArray,
		arrayLength = array => isArray(array) && array.length,
		isFunction = v => typeof v === 'function',
		pString = value => null != value ? '' + value : '',

		forEachObjectValue = (obj, fn) => Object.values(obj).forEach(fn),

		forEachObjectEntry = (obj, fn) => Object.entries(obj).forEach(([key, value]) => fn(key, value)),

		pInt = (value, defaultValue = 0) => {
			value = parseInt(value, 10);
			return isNaN(value) || !isFinite(value) ? defaultValue : value;
		},

		convertThemeName = theme => theme
			.replace(/@custom$/, '')
			.replace(/([A-Z])/g, ' $1')
			.replace(/[^a-zA-Z0-9]+/g, ' ')
			.trim(),

		defaultOptionsAfterRender = (domItem, item) =>
			domItem && item && undefined !== item.disabled
			&& domItem.classList.toggle('disabled', domItem.disabled = item.disabled),

		addObservablesTo = (target, observables) =>
			forEachObjectEntry(observables, (key, value) =>
				target[key] = /*isArray(value) ? ko.observableArray(value) :*/ ko.observable(value) ),

		addComputablesTo = (target, computables) =>
			forEachObjectEntry(computables, (key, fn) => target[key] = ko.computed(fn, {'pure':true})),

		addSubscribablesTo = (target, subscribables) =>
			forEachObjectEntry(subscribables, (key, fn) => target[key].subscribe(fn)),

		inFocus = () => {
			try {
				return doc.activeElement && doc.activeElement.matches(
					'input,textarea,[contenteditable]'
				);
			} catch (e) {
				return false;
			}
		},

		// unescape(encodeURIComponent()) makes the UTF-16 DOMString to an UTF-8 string
		b64EncodeJSON = data => btoa(unescape(encodeURIComponent(JSON.stringify(data)))),
	/* 	// Without deprecated 'unescape':
		b64EncodeJSON = data => btoa(encodeURIComponent(JSON.stringify(data)).replace(
			/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode('0x' + p1)
	    )),
	*/
		b64EncodeJSONSafe = data => b64EncodeJSON(data).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, ''),

		settingsSaveHelperSimpleFunction = (koTrigger, context) =>
			iError => {
				koTrigger.call(context, iError ? SaveSettingsStep.FalseResult : SaveSettingsStep.TrueResult);
				setTimeout(() => koTrigger.call(context, SaveSettingsStep.Idle), 1000);
			},

		changeTheme = (value, themeTrigger = ()=>0) => {
			const themeStyle = elementById('app-theme-style'),
				clearTimer = () => {
					__themeTimer = setTimeout(() => themeTrigger(SaveSettingsStep.Idle), 1000);
					__themeJson = null;
				};

			let url = themeStyle.dataset.href;

			if (url) {
				url = url.toString()
					.replace(/\/-\/[^/]+\/-\//, '/-/' + value + '/-/')
					.replace(/\/Css\/[^/]+\/User\//, '/Css/0/User/')
					.replace(/\/Hash\/[^/]+\//, '/Hash/-/');

				if ('Json/' !== url.substr(-5)) {
					url += 'Json/';
				}

				clearTimeout(__themeTimer);

				themeTrigger(SaveSettingsStep.Animate);

				if (__themeJson) {
					__themeJson.abort();
				}
				let init = {};
				if (window.AbortController) {
					__themeJson = new AbortController();
					init.signal = __themeJson.signal;
				}
				rl.fetchJSON(url, init)
					.then(data => {
						if (2 === arrayLength(data)) {
							themeStyle.textContent = data[1];
							themeStyle.dataset.href = url;
							themeStyle.dataset.theme = data[0];
							themeTrigger(SaveSettingsStep.TrueResult);
						}
					})
					.then(clearTimer, clearTimer);
			}
		},

		getKeyByValue = (o, v) => Object.keys(o).find(key => o[key] === v);

	const
		ROOT = './',
		HASH_PREFIX = '#/',
		SERVER_PREFIX = './?',
		VERSION = Settings.app('version'),
		VERSION_PREFIX = Settings.app('webVersionPath') || 'snappymail/v/' + VERSION + '/',

		adminPath = () => rl.adminArea() && !Settings.app('adminHostUse'),

		prefix = () => SERVER_PREFIX + (adminPath() ? Settings.app('adminPath') : '');

	const SUB_QUERY_PREFIX = '&q[]=',

		/**
		 * @param {string=} startupUrl
		 * @returns {string}
		 */
		root = (startupUrl = '') => HASH_PREFIX + pString(startupUrl),

		/**
		 * @returns {string}
		 */
		logoutLink = () => adminPath() ? prefix() : ROOT,

		/**
		 * @param {string} type
		 * @param {string} hash
		 * @param {string=} customSpecSuffix
		 * @returns {string}
		 */
		serverRequestRaw = (type, hash) =>
			SERVER_PREFIX + '/Raw/' + SUB_QUERY_PREFIX + '/'
			+ '0/' // AuthAccountHash ?
			+ (type
				? type + '/' + (hash ? SUB_QUERY_PREFIX + '/' + hash : '')
				: ''),

		/**
		 * @param {string} download
		 * @param {string=} customSpecSuffix
		 * @returns {string}
		 */
		attachmentDownload = (download, customSpecSuffix) =>
			serverRequestRaw('Download', download),

		proxy = url =>
			SERVER_PREFIX + '/ProxyExternal/' + btoa(url).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, ''),
	/*
			return './?/ProxyExternal/'.Utils::EncodeKeyValuesQ(array(
				'Rnd' => \md5(\microtime(true)),
				'Token' => Utils::GetConnectionToken(),
				'Url' => $sUrl
			)).'/';
	*/

		/**
		 * @param {string} type
		 * @returns {string}
		 */
		serverRequest = type => prefix() + '/' + type + '/' + SUB_QUERY_PREFIX + '/0/',

		/**
		 * @param {string} lang
		 * @param {boolean} isAdmin
		 * @returns {string}
		 */
		langLink = (lang, isAdmin) =>
			SERVER_PREFIX + '/Lang/0/' + (isAdmin ? 'Admin' : 'App') + '/' + encodeURI(lang) + '/' + VERSION + '/',

		/**
		 * @param {string} path
		 * @returns {string}
		 */
		staticLink = path => VERSION_PREFIX + 'static/' + path,

		/**
		 * @param {string} theme
		 * @returns {string}
		 */
		themePreviewLink = theme => {
			let prefix = VERSION_PREFIX;
			if ('@custom' === theme.slice(-7)) {
				theme = theme.slice(0, theme.length - 7).trim();
				prefix = Settings.app('webPath') || '';
			}

			return prefix + 'themes/' + encodeURI(theme) + '/images/preview.png';
		},

		/**
		 * @param {string} inboxFolderName = 'INBOX'
		 * @returns {string}
		 */
		mailbox = (inboxFolderName = 'INBOX') => HASH_PREFIX + 'mailbox/' + inboxFolderName,

		/**
		 * @param {string=} screenName = ''
		 * @returns {string}
		 */
		settings = (screenName = '') => HASH_PREFIX + 'settings' + (screenName ? '/' + screenName : ''),

		/**
		 * @param {string} folder
		 * @param {number=} page = 1
		 * @param {string=} search = ''
		 * @param {number=} threadUid = 0
		 * @returns {string}
		 */
		mailBox = (folder, page, search, threadUid) => {
			let result = [HASH_PREFIX + 'mailbox'];

			if (folder) {
				result.push(folder + (threadUid ? '~' + threadUid : ''));
			}

			page = pInt(page, 1);
			if (1 < page) {
				result.push('p' + page);
			}

			search = pString(search);
			if (search) {
				result.push(encodeURI(search));
			}

			return result.join('/');
		};

	let I18N_DATA = {};

	const
		i18nToNode = element => {
			const key = element.dataset.i18n;
			if (key) {
				if ('[' === key.slice(0, 1)) {
					switch (key.slice(0, 6)) {
						case '[html]':
							element.innerHTML = i18n(key.slice(6));
							break;
						case '[place':
							element.placeholder = i18n(key.slice(13));
							break;
						case '[title':
							element.title = i18n(key.slice(7));
							break;
						// no default
					}
				} else {
					element.textContent = i18n(key);
				}
			}
		},

		init = () => {
			if (rl.I18N) {
				I18N_DATA = rl.I18N;
				Date.defineRelativeTimeFormat(rl.relativeTime || {});
				rl.I18N = null;
				return 1;
			}
		},

		i18nKey = key => key.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase(),

		getNotificationMessage = code => {
			let key = getKeyByValue(Notification, code);
			if (key) {
				key = i18nKey(key).replace('_NOTIFICATION', '_ERROR');
				return I18N_DATA.NOTIFICATIONS[key];
			}
		};

	const
		trigger = ko.observable(false),

		/**
		 * @param {string} key
		 * @param {Object=} valueList
		 * @param {string=} defaulValue
		 * @returns {string}
		 */
		i18n = (key, valueList, defaulValue) => {
			let result = defaulValue || key;
			if (key) {
				let path = key.split('/');
				if (I18N_DATA[path[0]] && path[1]) {
					result = I18N_DATA[path[0]][path[1]] || result;
				}
			}
			if (valueList) {
				forEachObjectEntry(valueList, (key, value) => {
					result = result.replace('%' + key + '%', value);
				});
			}
			return result;
		},

		/**
		 * @param {Object} elements
		 * @param {boolean=} animate = false
		 */
		i18nToNodes = element =>
			setTimeout(() =>
				element.querySelectorAll('[data-i18n]').forEach(item => i18nToNode(item))
			, 1),

		/**
		 * @param {Function} startCallback
		 * @param {Function=} langCallback = null
		 */
		initOnStartOrLangChange = (startCallback, langCallback = null) => {
			startCallback && startCallback();
			startCallback && trigger.subscribe(startCallback);
			langCallback && trigger.subscribe(langCallback);
		},

		/**
		 * @param {number} code
		 * @param {*=} message = ''
		 * @param {*=} defCode = null
		 * @returns {string}
		 */
		getNotification = (code, message = '', defCode = 0) => {
			code = parseInt(code, 10) || 0;
			if (Notification.ClientViewError === code && message) {
				return message;
			}

			return getNotificationMessage(code)
				|| getNotificationMessage(parseInt(defCode, 10))
				|| '';
		},

		/**
		 * @param {*} code
		 * @returns {string}
		 */
		getUploadErrorDescByCode = code => {
			let key = getKeyByValue(UploadErrorCode, parseInt(code, 10));
			return i18n('UPLOAD/ERROR_' + (key ? i18nKey(key) : 'UNKNOWN'));
		},

		/**
		 * @param {boolean} admin
		 * @param {string} language
		 */
		translatorReload = (admin, language) =>
			new Promise((resolve, reject) => {
				const script = createElement('script');
				script.onload = () => {
					// reload the data
					if (init()) {
						i18nToNodes(doc);
						admin || rl.app.reloadTime();
						trigger(!trigger());
					}
					script.remove();
					resolve();
				};
				script.onerror = () => reject(new Error('Language '+language+' failed'));
				script.src = langLink(language, admin);
		//		script.async = true;
				doc.head.append(script);
			}),

		/**
		 *
		 * @param {string} language
		 * @param {boolean=} isEng = false
		 * @returns {string}
		 */
		convertLangName = (language, isEng = false) =>
			i18n(
				'LANGS_NAMES' + (true === isEng ? '_EN' : '') + '/' + language,
				null,
				language
			);

	init();

	var bootstrap = App => {

		addEventListener('click', ()=>rl.Dropdowns.detectVisibility());

		rl.app = App;
		rl.logoutReload = App.logoutReload;

		rl.i18n = i18n;

		rl.Enums = {
			StorageResultType: {
				Success: 0,
				Error: 1,
				Abort: 2
			}
		};

		rl.Dropdowns = [];
		rl.Dropdowns.register = function(element) { this.push(element); };
		rl.Dropdowns.detectVisibility = (() =>
			dropdownVisibility(!!rl.Dropdowns.find(item => item.classList.contains('show')))
		).debounce(50);

		rl.route = {
			root: () => {
				rl.route.setHash(root(), true);
				rl.route.off();
			},
			reload: () => {
				rl.route.root();
				setTimeout(() => (Settings.app('inIframe') ? parent : window).location.reload(), 100);
			},
			off: () => hasher.active = false,
			on: () => hasher.active = true,
			/**
			 * @param {string} sHash
			 * @param {boolean=} silence = false
			 * @param {boolean=} replace = false
			 * @returns {void}
			 */
			setHash: (hash, silence = false, replace = false) => {
				hash = hash.replace(/^[#/]+/, '');
				hasher.active = !silence;
				hasher[replace ? 'replaceHash' : 'setHash'](hash);
				if (silence) {
					hasher.active = true;
				} else {
					hasher.setHash(hash);
				}
			}
		};

		rl.fetch = (resource, init, postData) => {
			init = Object.assign({
				mode: 'same-origin',
				cache: 'no-cache',
				redirect: 'error',
				referrerPolicy: 'no-referrer',
				credentials: 'same-origin',
				headers: {}
			}, init);
			if (postData) {
				init.method = 'POST';
				init.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
				const buildFormData = (formData, data, parentKey) => {
					if (data && typeof data === 'object' && !(data instanceof Date || data instanceof File)) {
						Object.keys(data).forEach(key =>
							buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key)
						);
					} else {
						formData.set(parentKey, data == null ? '' : data);
					}
					return formData;
				};
				postData = (postData instanceof FormData)
					? postData
					: buildFormData(new FormData(), postData);
				postData.set('XToken', Settings.app('token'));
	//			init.body = JSON.stringify(Object.fromEntries(postData));
				init.body = new URLSearchParams(postData);
			}

			return fetch(resource, init);
		};

		rl.fetchJSON = (resource, init, postData) => {
			init = Object.assign({ headers: {} }, init);
			init.headers.Accept = 'application/json';
			return rl.fetch(resource, init, postData).then(response => {
				if (!response.ok) {
					return Promise.reject('Network response error: ' + response.status);
				}
				/* TODO: use this for non-developers?
				response.clone()
				let data = response.text();
				try {
					return JSON.parse(data);
				} catch (e) {
					console.error(e);
	//				console.log(data);
					return Promise.reject(Notification.JsonParse);
					return {
						Result: false,
						ErrorCode: 952, // Notification.JsonParse
						ErrorMessage: e.message,
						ErrorMessageAdditional: data
					}
				}
				*/
				return response.json();
			});
		};

	};

	/**
	 * The value of the pureComputed observable shouldn’t vary based on the
	 * number of evaluations or other “hidden” information. Its value should be
	 * based solely on the values of other observables in the application
	 */
	const koComputable = fn => ko.computed(fn, {'pure':true});

	ko.bindingHandlers.tooltipErrorTip = {
		init: (element, fValueAccessor) => {
			doc.addEventListener('click', () => {
				let value = fValueAccessor();
				ko.isObservable(value) && !ko.isComputed(value) && value('');
				element.removeAttribute('data-rainloopErrorTip');
			});
		},
		update: (element, fValueAccessor) => {
			let value = ko.unwrap(fValueAccessor());
			value = isFunction(value) ? value() : value;
			if (value) {
				setTimeout(() => element.setAttribute('data-rainloopErrorTip', value), 100);
			} else {
				element.removeAttribute('data-rainloopErrorTip');
			}
		}
	};

	ko.bindingHandlers.onEnter = {
		init: (element, fValueAccessor, fAllBindings, viewModel) => {
			let fn = event => {
				if ('Enter' == event.key) {
					element.dispatchEvent(new Event('change'));
					fValueAccessor().call(viewModel);
				}
			};
			element.addEventListener('keydown', fn);
			ko.utils.domNodeDisposal.addDisposeCallback(element, () => element.removeEventListener('keydown', fn));
		}
	};

	ko.bindingHandlers.onSpace = {
		init: (element, fValueAccessor, fAllBindings, viewModel) => {
			let fn = event => {
				if (' ' == event.key) {
					fValueAccessor().call(viewModel, event);
				}
			};
			element.addEventListener('keyup', fn);
			ko.utils.domNodeDisposal.addDisposeCallback(element, () => element.removeEventListener('keyup', fn));
		}
	};

	ko.bindingHandlers.i18nInit = {
		init: element => i18nToNodes(element)
	};

	ko.bindingHandlers.i18nUpdate = {
		update: (element, fValueAccessor) => {
			ko.unwrap(fValueAccessor());
			i18nToNodes(element);
		}
	};

	ko.bindingHandlers.title = {
		update: (element, fValueAccessor) => element.title = ko.unwrap(fValueAccessor())
	};

	ko.bindingHandlers.command = {
		init: (element, fValueAccessor, fAllBindings, viewModel, bindingContext) => {
			const command = fValueAccessor();

			if (!command || !command.enabled || !command.canExecute) {
				throw new Error('Value should be a command');
			}

			ko.bindingHandlers['FORM'==element.nodeName ? 'submit' : 'click'].init(
				element,
				fValueAccessor,
				fAllBindings,
				viewModel,
				bindingContext
			);
		},
		update: (element, fValueAccessor) => {
			const cl = element.classList,
				command = fValueAccessor();

			let disabled = !command.enabled();

			disabled = disabled || !command.canExecute();
			cl.toggle('disabled', disabled);

			if (element.matches('INPUT,TEXTAREA,BUTTON')) {
				element.disabled = disabled;
			}
		}
	};

	ko.bindingHandlers.saveTrigger = {
		init: (element) => {
			let icon = element;
			if (element.matches('input,select,textarea')) {
				element.classList.add('settings-saved-trigger-input');
				element.after(element.saveTriggerIcon = icon = createElement('span'));
			}
			icon.classList.add('settings-save-trigger');
		},
		update: (element, fValueAccessor) => {
			const value = parseInt(ko.unwrap(fValueAccessor()),10);
			let cl = (element.saveTriggerIcon || element).classList;
			if (element.saveTriggerIcon) {
				cl.toggle('saving', value === SaveSettingsStep.Animate);
				cl.toggle('success', value === SaveSettingsStep.TrueResult);
				cl.toggle('error', value === SaveSettingsStep.FalseResult);
			}
			cl = element.classList;
			cl.toggle('success', value === SaveSettingsStep.TrueResult);
			cl.toggle('error', value === SaveSettingsStep.FalseResult);
		}
	};

	// extenders

	ko.extenders.limitedList = (target, limitedList) => {
		const result = ko
				.computed({
					read: target,
					write: (newValue) => {
						const currentValue = ko.unwrap(target),
							list = ko.unwrap(limitedList);

						if (arrayLength(list)) {
							if (list.includes(newValue)) {
								target(newValue);
							} else if (list.includes(currentValue, list)) {
								target(currentValue + ' ');
								target(currentValue);
							} else {
								target(list[0] + ' ');
								target(list[0]);
							}
						} else {
							target('');
						}
					}
				})
				.extend({ notify: 'always' });

		result(target());

		if (!result.valueHasMutated) {
			result.valueHasMutated = () => target.valueHasMutated();
		}

		return result;
	};

	ko.extenders.toggleSubscribeProperty = (target, options) => {
		const prop = options[1];
		if (prop) {
			target.subscribe(
				prev => prev && prev[prop] && prev[prop](false),
				options[0],
				'beforeChange'
			);

			target.subscribe(next => next && next[prop] && next[prop](true), options[0]);
		}

		return target;
	};

	ko.extenders.falseTimeout = (target, option) => {
		target.subscribe((() => target(false)).debounce(parseInt(option, 10) || 0));
		return target;
	};

	// functions

	ko.observable.fn.askDeleteHelper = function() {
		return this.extend({ falseTimeout: 3000, toggleSubscribeProperty: [this, 'askDelete'] });
	};

	const
		tpl = createElement('template'),
		htmlre = /[&<>"']/g,
		htmlmap = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#x27;'
		},

		replaceWithChildren = node => node.replaceWith(...[...node.childNodes]),

		// Strip utm_* tracking
		stripTracking = text => text.replace(/(\?|&amp;|&)utm_[a-z]+=[^&?#]*/si, '$1');

	const

		/**
		 * @param {string} text
		 * @returns {string}
		 */
		encodeHtml = text => (text && text.toString ? text.toString() : '' + text).replace(htmlre, m => htmlmap[m]),

		/**
		 * Clears the Message Html for viewing
		 * @param {string} text
		 * @returns {string}
		 */
		cleanHtml = (html, contentLocationUrls, removeColors) => {
			const useProxy = !!SettingsGet('UseLocalProxyForExternalImages'),
				result = {
					hasExternals: false,
					foundCIDs: [],
					foundContentLocationUrls: []
				},

				// convert body attributes to CSS
				tasks = {
					link: value => {
						if (/^#[a-fA-Z0-9]{3,6}$/.test(value)) {
							tpl.content.querySelectorAll('a').forEach(node => node.style.color = value);
						}
					},
					text: (value, node) => node.style.color = value,
					topmargin: (value, node) => node.style.marginTop = pInt(value) + 'px',
					leftmargin: (value, node) => node.style.marginLeft = pInt(value) + 'px',
					bottommargin: (value, node) => node.style.marginBottom = pInt(value) + 'px',
					rightmargin: (value, node) => node.style.marginRight = pInt(value) + 'px'
				};

	//		if (static::Config()->Get('labs', 'strict_html_parser', true))
			let
				value,
				allowedAttributes = [
					// defaults
					'name',
					'dir', 'lang', 'style', 'title',
					'background', 'bgcolor', 'alt', 'height', 'width', 'src', 'href',
					'border', 'bordercolor', 'charset', 'direction', 'language',
					// a
					'coords', 'download', 'hreflang', 'shape',
					// body
					'alink', 'bgproperties', 'bottommargin', 'leftmargin', 'link', 'rightmargin', 'text', 'topmargin', 'vlink',
					'marginwidth', 'marginheight', 'offset',
					// button,
					'disabled', 'type', 'value',
					// col
					'align', 'valign',
					// font
					'color', 'face', 'size',
					// form
					'novalidate',
					// hr
					'noshade',
					// img
					'hspace', 'sizes', 'srcset', 'vspace', 'usemap',
					// input, textarea
					'checked', 'max', 'min', 'maxlength', 'multiple', 'pattern', 'placeholder', 'readonly',
						'required', 'step', 'wrap',
					// label
					'for',
					// meter
					'low', 'high', 'optimum',
					// ol
					'reversed', 'start',
					// option
					'selected', 'label',
					// table
					'cols', 'rows', 'frame', 'rules', 'summary', 'cellpadding', 'cellspacing',
					// th
					'abbr', 'scope',
					// td
					'axis', 'colspan', 'rowspan', 'headers', 'nowrap'
				],
				disallowedAttributes = [
					'id', 'class', 'contenteditable', 'designmode', 'formaction', 'manifest', 'action',
					'data-bind', 'data-reactid', 'xmlns', 'srcset',
					'fscommand', 'seeksegmenttime'
				],
				disallowedTags = [
					'HEAD','STYLE','SVG','SCRIPT','TITLE','LINK','BASE','META',
					'INPUT','OUTPUT','SELECT','BUTTON','TEXTAREA',
					'BGSOUND','KEYGEN','SOURCE','OBJECT','EMBED','APPLET','IFRAME','FRAME','FRAMESET','VIDEO','AUDIO','AREA','MAP'
				];

			tpl.innerHTML = html
				.replace(/(<pre[^>]*>)([\s\S]*?)(<\/pre>)/gi, aMatches => {
					return (aMatches[1] + aMatches[2].trim() + aMatches[3].trim()).replace(/\r?\n/g, '<br>');
				})
				// GetDomFromText
				.replace('<o:p></o:p>', '')
				.replace('<o:p>', '<span>')
				.replace('</o:p>', '</span>')
				// \MailSo\Base\HtmlUtils::ClearFastTags
				.replace(/<!doctype[^>]*>/i, '')
				.replace(/<\?xml [^>]*\?>/i, '')
				.trim();
			html = '';

			// \MailSo\Base\HtmlUtils::ClearComments()
			// https://github.com/the-djmaze/snappymail/issues/187
			const nodeIterator = document.createNodeIterator(tpl.content, NodeFilter.SHOW_COMMENT);
			while (nodeIterator.nextNode()) {
				nodeIterator.referenceNode.remove();
			}

			tpl.content.querySelectorAll('*').forEach(oElement => {
				const name = oElement.tagName,
					oStyle = oElement.style,
					hasAttribute = name => oElement.hasAttribute(name),
					getAttribute = name => hasAttribute(name) ? oElement.getAttribute(name).trim() : '',
					setAttribute = (name, value) => oElement.setAttribute(name, value),
					delAttribute = name => oElement.removeAttribute(name);

				// \MailSo\Base\HtmlUtils::ClearTags()
				if (disallowedTags.includes(name)
				 || 'none' == oStyle.display
				 || 'hidden' == oStyle.visibility
	//			 || (oStyle.lineHeight && 1 > parseFloat(oStyle.lineHeight)
	//			 || (oStyle.maxHeight && 1 > parseFloat(oStyle.maxHeight)
	//			 || (oStyle.maxWidth && 1 > parseFloat(oStyle.maxWidth)
	//			 || ('0' === oStyle.opacity
				) {
					oElement.remove();
					return;
				}
	//			if (['CENTER','FORM'].includes(name)) {
				if ('FORM' === name) {
					replaceWithChildren(oElement);
					return;
				}
	/*
				// Idea to allow CSS
				if ('STYLE' === name) {
					msgId = '#rl-msg-061eb4d647771be4185943ce91f0039d';
					oElement.textContent = oElement.textContent
						.replace(/[^{}]+{/g, m => msgId + ' ' + m.replace(',', ', '+msgId+' '))
						.replace(/(background-)color:[^};]+/g, '');
					return;
				}
	*/
				if ('BODY' === name) {
					forEachObjectEntry(tasks, (name, cb) => {
						if (hasAttribute(name)) {
							cb(getAttribute(name), oElement);
							delAttribute(name);
						}
					});
				}

				else if ('TABLE' === name && hasAttribute('width')) {
					value = getAttribute('width');
					if (!value.includes('%')) {
						delAttribute('width');
						oStyle.maxWidth = value + 'px';
						oStyle.width = '100%';
					}
				}

				else if ('A' === name) {
					value = oElement.href;
					value = stripTracking(value);
					if (!/^([a-z]+):/i.test(value) && '//' !== value.slice(0, 2)) {
						setAttribute('data-x-broken-href', value);
						delAttribute('href');
					} else {
						setAttribute('target', '_blank');
						setAttribute('rel', 'external nofollow noopener noreferrer');
					}
					setAttribute('tabindex', '-1');
				}

				if (oElement.hasAttributes()) {
					let i = oElement.attributes.length;
					while (i--) {
						let sAttrName = oElement.attributes[i].name.toLowerCase();
						if (!allowedAttributes.includes(sAttrName)
						 || 'on' === sAttrName.slice(0, 2)
						 || 'form' === sAttrName.slice(0, 4)
	//					 || 'data-' === sAttrName.slice(0, 5)
	//					 || sAttrName.includes(':')
						 || disallowedAttributes.includes(sAttrName))
						{
							delAttribute(sAttrName);
						}
					}
				}

				// SVG xlink:href
				/*
				if (hasAttribute('xlink:href')) {
					delAttribute('xlink:href');
				}
				*/

				let skipStyle = false;
				if (hasAttribute('src')) {
					value = getAttribute('src');
					delAttribute('src');

					if ('IMG' === name
						&& (('' != getAttribute('height') && 3 > pInt(getAttribute('height')))
							|| ('' != getAttribute('width') && 3 > pInt(getAttribute('width')))
							|| [
								'email.microsoftemail.com/open',
								'github.com/notifications/beacon/',
								'mandrillapp.com/track/open',
								'list-manage.com/track/open'
							].filter(uri => value.toLowerCase().includes(uri)).length
					)) {
						skipStyle = true;
						setAttribute('style', 'display:none');
						setAttribute('data-x-hidden-src', value);
					}
					else if (contentLocationUrls[value])
					{
						setAttribute('data-x-src-location', value);
						result.foundContentLocationUrls.push(value);
					}
					else if ('cid:' === value.slice(0, 4))
					{
						setAttribute('data-x-src-cid', value.slice(4));
						result.foundCIDs.push(value.slice(4));
					}
					else if (/^https?:\/\//i.test(value) || '//' === value.slice(0, 2))
					{
						setAttribute('data-x-src', useProxy ? proxy(value) : value);
						result.hasExternals = true;
					}
					else if ('data:image/' === value.slice(0, 11))
					{
						setAttribute('src', value);
					}
					else
					{
						setAttribute('data-x-broken-src', value);
					}
				}

				if (hasAttribute('background')) {
					oStyle.backgroundImage = 'url("' + getAttribute('background') + '")';
					delAttribute('background');
				}

				if (hasAttribute('bgcolor')) {
					oStyle.backgroundColor = getAttribute('bgcolor');
					delAttribute('bgcolor');
				}

				if (hasAttribute('color')) {
					oStyle.color = getAttribute('color');
					delAttribute('color');
				}

				if (!skipStyle) {
	/*
					if ('fixed' === oStyle.position) {
						oStyle.position = 'absolute';
					}
	*/
					oStyle.removeProperty('behavior');
					oStyle.removeProperty('cursor');
					oStyle.removeProperty('min-width');

					const urls = {
						cid: [],    // 'data-x-style-cid'
						remote: [], // 'data-x-style-url'
						broken: []  // 'data-x-broken-style-src'
					};
					['backgroundImage', 'listStyleImage', 'content'].forEach(property => {
						if (oStyle[property]) {
							let value = oStyle[property],
								found = value.match(/url\s*\(([^)]+)\)/gi);
							if (found) {
								oStyle[property] = null;
								found = found[0].replace(/^["'\s]+|["'\s]+$/g, '');
								let lowerUrl = found.toLowerCase();
								if ('cid:' === lowerUrl.slice(0, 4)) {
									found = found.slice(4);
									urls.cid[property] = found;
									result.foundCIDs.push(found);
								} else if (/http[s]?:\/\//.test(lowerUrl) || '//' === found.slice(0, 2)) {
									result.hasExternals = true;
									urls.remote[property] = useProxy ? proxy(found) : found;
								} else if ('data:image/' === lowerUrl.slice(0, 11)) {
									oStyle[property] = value;
								} else {
									urls.broken[property] = found;
								}
							}
						}
					});
	//				oStyle.removeProperty('background-image');
	//				oStyle.removeProperty('list-style-image');

					if (urls.cid.length) {
						setAttribute('data-x-style-cid', JSON.stringify(urls.cid));
					}
					if (urls.remote.length) {
						setAttribute('data-x-style-url', JSON.stringify(urls.remote));
					}
					if (urls.broken.length) {
						setAttribute('data-x-style-broken-urls', JSON.stringify(urls.broken));
					}

					if (11 > pInt(oStyle.fontSize)) {
						oStyle.removeProperty('font-size');
					}

					// Removes background and color
					// Many e-mails incorrectly only define one, not both
					// And in dark theme mode this kills the readability
					if (removeColors) {
						oStyle.removeProperty('background-color');
						oStyle.removeProperty('background-image');
						oStyle.removeProperty('color');
					}
				}
			});

	//		return tpl.content.firstChild;
			result.html = tpl.innerHTML;
			return result;
		},

		/**
		 * @param {string} html
		 * @returns {string}
		 */
		htmlToPlain = html => {
			const
				hr = '⎯'.repeat(64),
				forEach = (selector, fn) => tpl.content.querySelectorAll(selector).forEach(fn),
				blockquotes = node => {
					let bq;
					while ((bq = node.querySelector('blockquote'))) {
						// Convert child blockquote first
						blockquotes(bq);
						// Convert blockquote
						bq.innerHTML = '\n' + ('\n' + bq.innerHTML.replace(/\n{3,}/gm, '\n\n').trim() + '\n').replace(/^/gm, '&gt; ');
						replaceWithChildren(bq);
					}
				};

			html = html
				.replace(/<pre[^>]*>([\s\S]*?)<\/pre>/gim, (...args) =>
					1 < args.length ? args[1].toString().replace(/\n/g, '<br>') : '')
				.replace(/\r?\n/g, '')
				.replace(/\s+/gm, ' ');

			while (/<(div|tr)[\s>]/i.test(html)) {
				html = html.replace(/\n*<(div|tr)(\s[\s\S]*?)?>\n*/gi, '\n');
			}
			while (/<\/(div|tr)[\s>]/i.test(html)) {
				html = html.replace(/\n*<\/(div|tr)(\s[\s\S]*?)?>\n*/gi, '\n');
			}

			tpl.innerHTML = html
				.replace(/<t[dh](\s[\s\S]*?)?>/gi, '\t')
				.replace(/<\/tr(\s[\s\S]*?)?>/gi, '\n');

			// Convert line-breaks
			forEach('br', br => br.replaceWith('\n'));

			// lines
			forEach('hr', node => node.replaceWith(`\n\n${hr}\n\n`));

			// headings
			forEach('h1,h2,h3,h4,h5,h6', h => h.replaceWith(`\n\n${'#'.repeat(h.tagName[1])} ${h.textContent}\n\n`));

			// paragraphs
			forEach('p', node => {
				node.prepend('\n\n');
				node.after('\n\n');
			});

			// proper indenting and numbering of (un)ordered lists
			forEach('ol,ul', node => {
				let prefix = '',
					parent = node,
					ordered = 'OL' == node.tagName,
					i = 0;
				while (parent && parent.parentNode && parent.parentNode.closest) {
					parent = parent.parentNode.closest('ol,ul');
					parent && (prefix = '    ' + prefix);
				}
				node.querySelectorAll(':scope > li').forEach(li => {
					li.prepend('\n' + prefix + (ordered ? `${++i}. ` : ' * '));
				});
				node.prepend('\n\n');
				node.after('\n\n');
			});

			// Convert anchors
			forEach('a', a => a.replaceWith(a.textContent + ' ' + a.href));

			// Bold
			forEach('b,strong', b => b.replaceWith(`**${b.textContent}**`));
			// Italic
			forEach('i,em', i => i.replaceWith(`*${i.textContent}*`));

			// Blockquotes must be last
			blockquotes(tpl.content);

			return (tpl.content.textContent || '').replace(/\n{3,}/gm, '\n\n').trim();
		},

		/**
		 * @param {string} plain
		 * @param {boolean} findEmailAndLinksInText = false
		 * @returns {string}
		 */
		plainToHtml = plain => {
			plain = stripTracking(plain)
				.toString()
				.replace(/\r/g, '')
				.replace(/^>[> ]>+/gm, ([match]) => (match ? match.replace(/[ ]+/g, '') : match));

			let bIn = false,
				bDo = true,
				bStart = true,
				aNextText = [],
				aText = plain.split('\n');

			do {
				bDo = false;
				aNextText = [];
				aText.forEach(sLine => {
					bStart = '>' === sLine.slice(0, 1);
					if (bStart && !bIn) {
						bDo = true;
						bIn = true;
						aNextText.push('~~~blockquote~~~');
						aNextText.push(sLine.slice(1));
					} else if (!bStart && bIn) {
						if (sLine) {
							bIn = false;
							aNextText.push('~~~/blockquote~~~');
							aNextText.push(sLine);
						} else {
							aNextText.push(sLine);
						}
					} else if (bStart && bIn) {
						aNextText.push(sLine.slice(1));
					} else {
						aNextText.push(sLine);
					}
				});

				if (bIn) {
					bIn = false;
					aNextText.push('~~~/blockquote~~~');
				}

				aText = aNextText;
			} while (bDo);

			return aText.join('\n')
				// .replace(/~~~\/blockquote~~~\n~~~blockquote~~~/g, '\n')
				.replace(/&/g, '&amp;')
				.replace(/>/g, '&gt;')
				.replace(/</g, '&lt;')
				.replace(/~~~blockquote~~~\s*/g, '<blockquote>')
				.replace(/\s*~~~\/blockquote~~~/g, '</blockquote>')
				.replace(/\n/g, '<br>');
		};

	class HtmlEditor {
		/**
		 * @param {Object} element
		 * @param {Function=} onBlur
		 * @param {Function=} onReady
		 * @param {Function=} onModeChange
		 */
		constructor(element, onBlur = null, onReady = null, onModeChange = null) {
			this.blurTimer = 0;

			this.onBlur = onBlur;
			this.onModeChange = onModeChange;

			if (element) {
				let editor;

				onReady = onReady ? [onReady] : [];
				this.onReady = fn => onReady.push(fn);
				const readyCallback = () => {
					this.editor = editor;
					this.onReady = fn => fn();
					onReady.forEach(fn => fn());
				};

				if (rl.createWYSIWYG) {
					editor = rl.createWYSIWYG(element, readyCallback);
				}
				if (!editor) {
					editor = new SquireUI(element);
					setTimeout(readyCallback, 1);
				}

				editor.on('blur', () => this.blurTrigger());
				editor.on('focus', () => this.blurTimer && clearTimeout(this.blurTimer));
				editor.on('mode', () => {
					this.blurTrigger();
					this.onModeChange && this.onModeChange(!this.isPlain());
				});
			}
		}

		blurTrigger() {
			if (this.onBlur) {
				clearTimeout(this.blurTimer);
				this.blurTimer = setTimeout(() => this.onBlur && this.onBlur(), 200);
			}
		}

		/**
		 * @returns {boolean}
		 */
		isHtml() {
			return this.editor ? !this.isPlain() : false;
		}

		/**
		 * @returns {boolean}
		 */
		isPlain() {
			return this.editor ? 'plain' === this.editor.mode : false;
		}

		/**
		 * @returns {void}
		 */
		clearCachedSignature() {
			this.onReady(() => this.editor.execCommand('insertSignature', {
				clearCache: true
			}));
		}

		/**
		 * @param {string} signature
		 * @param {bool} html
		 * @param {bool} insertBefore
		 * @returns {void}
		 */
		setSignature(signature, html, insertBefore = false) {
			this.onReady(() => this.editor.execCommand('insertSignature', {
				isHtml: html,
				insertBefore: insertBefore,
				signature: signature
			}));
		}

		/**
		 * @param {boolean=} wrapIsHtml = false
		 * @returns {string}
		 */
		getData() {
			let result = '';
			if (this.editor) {
				try {
					if (this.isPlain() && this.editor.plugins.plain && this.editor.__plain) {
						result = this.editor.__plain.getRawData();
					} else {
						result = this.editor.getData();
					}
				} catch (e) {} // eslint-disable-line no-empty
			}
			return result;
		}

		/**
		 * @returns {string}
		 */
		getDataWithHtmlMark() {
			return (this.isHtml() ? ':HTML:' : '') + this.getData();
		}

		modeWysiwyg() {
			this.onReady(() => this.editor.setMode('wysiwyg'));
		}
		modePlain() {
			this.onReady(() => this.editor.setMode('plain'));
		}

		setHtmlOrPlain(text) {
			if (':HTML:' === text.slice(0, 6)) {
				this.setHtml(text.slice(6));
			} else {
				this.setPlain(text);
			}
		}

		setData(mode, data) {
			this.onReady(() => {
				const editor = this.editor;
				this.clearCachedSignature();
				try {
					editor.setMode(mode);
					if (this.isPlain() && editor.plugins.plain && editor.__plain) {
						editor.__plain.setRawData(data);
					} else {
						editor.setData(data);
					}
				} catch (e) { console.error(e); }
			});
		}

		setHtml(html) {
			this.setData('wysiwyg', html/*.replace(/<p[^>]*><\/p>/gi, '')*/);
		}

		setPlain(txt) {
			this.setData('plain', txt);
		}

		focus() {
			this.onReady(() => this.editor.focus());
		}

		hasFocus() {
			try {
				return this.editor && !!this.editor.focusManager.hasFocus;
			} catch (e) {
				return false;
			}
		}

		blur() {
			this.onReady(() => this.editor.focusManager.blur(true));
		}

		clear() {
			this.onReady(() => this.isPlain() ? this.setPlain('') : this.setHtml(''));
		}
	}

	rl.Utils = {
		htmlToPlain: htmlToPlain,
		plainToHtml: plainToHtml
	};

	function timestampToString(timeStampInUTC, formatStr) {
		const now = Date.now(),
			time = 0 < timeStampInUTC ? Math.min(now, timeStampInUTC * 1000) : (0 === timeStampInUTC ? now : 0);

		if (31536000000 < time) {
			const m = new Date(time);
			switch (formatStr) {
				case 'FROMNOW':
					return m.fromNow();
				case 'SHORT': {
					if (4 >= (now - time) / 3600000)
						return m.fromNow();
					const mt = m.getTime(), date = new Date,
						dt = date.setHours(0,0,0,0);
					if (mt > dt)
						return i18n('MESSAGE_LIST/TODAY_AT', {TIME: m.format('LT')});
					if (mt > dt - 86400000)
						return i18n('MESSAGE_LIST/YESTERDAY_AT', {TIME: m.format('LT')});
					if (date.getFullYear() === m.getFullYear())
						return m.format('d M');
					return m.format('LL');
				}
				case 'FULL':
					return m.format('LLL');
				default:
					return m.format(formatStr);
			}
		}

		return '';
	}

	function timeToNode(element, time) {
		try {
			if (time) {
				element.dateTime = (new Date(time * 1000)).format('Y-m-d\\TH:i:s');
			} else {
				time = Date.parse(element.dateTime) / 1000;
			}

			let key = element.dataset.momentFormat;
			if (key) {
				element.textContent = timestampToString(time, key);
			}

			if ((key = element.dataset.momentFormatTitle)) {
				element.title = timestampToString(time, key);
			}
		} catch (e) {
			// prevent knockout crashes
			console.error(e);
		}
	}

	function dispose(disposable) {
		if (disposable && isFunction(disposable.dispose)) {
			disposable.dispose();
		}
	}

	function typeCast(curValue, newValue) {
		if (null != curValue) {
			switch (typeof curValue)
			{
			case 'boolean': return 0 != newValue && !!newValue;
			case 'number': return isFinite(newValue) ? parseFloat(newValue) : 0;
			case 'string': return null != newValue ? '' + newValue : '';
			case 'object':
				if (curValue.constructor.reviveFromJson) {
					return curValue.constructor.reviveFromJson(newValue);
				}
				if (isArray(curValue) && !isArray(newValue))
					return [];
			}
		}
		return newValue;
	}

	class AbstractModel {
		constructor() {
	/*
			if (new.target === AbstractModel) {
				throw new Error("Can't instantiate AbstractModel!");
			}
	*/
			this.subscribables = [];
		}

		addObservables(observables) {
			addObservablesTo(this, observables);
		}

		addComputables(computables) {
			addComputablesTo(this, computables);
		}

		addSubscribables(subscribables) {
			forEachObjectEntry(subscribables, (key, fn) => this.subscribables.push( this[key].subscribe(fn) ) );
		}

		/** Called by delegateRunOnDestroy */
		onDestroy() {
			/** dispose ko subscribables */
			this.subscribables.forEach(dispose);
			/** clear object entries */
	//		forEachObjectEntry(this, (key, value) => {
			forEachObjectValue(this, value => {
				/** clear CollectionModel */
				let arr = ko.isObservableArray(value) ? value() : value;
				arr && arr.onDestroy && value.onDestroy();
				/** destroy ko.observable/ko.computed? */
				dispose(value);
				/** clear object value */
	//			this[key] = null; // TODO: issue with Contacts view
			});
	//		this.subscribables = [];
		}

		/**
		 * @static
		 * @param {FetchJson} json
		 * @returns {boolean}
		 */
		static validJson(json) {
			return !!(json && ('Object/'+this.name.replace('Model', '') === json['@Object']));
		}

		/**
		 * @static
		 * @param {FetchJson} json
		 * @returns {*Model}
		 */
		static reviveFromJson(json) {
			let obj = this.validJson(json) ? new this() : null;
			obj && obj.revivePropertiesFromJson(json);
			return obj;
		}

		revivePropertiesFromJson(json) {
			let model = this.constructor;
			if (!model.validJson(json)) {
				return false;
			}
			forEachObjectEntry(json, (key, value) => {
				if ('@' !== key[0]) try {
					key = key[0].toLowerCase() + key.slice(1);
					switch (typeof this[key])
					{
					case 'function':
						if (ko.isObservable(this[key])) {
							this[key](typeCast(this[key](), value));
	//						console.log('Observable ' + (typeof this[key]()) + ' ' + (model.name) + '.' + key + ' revived');
						}
	//					else console.log(model.name + '.' + key + ' is a function');
						break;
					case 'boolean':
					case 'number':
					case 'object':
					case 'string':
						this[key] = typeCast(this[key], value);
						break;
						// fall through
					case 'undefined':
					default:
	//					console.log((typeof this[key])+' '+(model.name)+'.'+key+' not revived');
					}
				} catch (e) {
					console.log(model.name + '.' + key);
					console.error(e);
				}
			});
			return true;
		}

	}

	/**
	 * Parses structured e-mail addresses from an address field
	 *
	 * Example:
	 *
	 *    "Name <address@domain>"
	 *
	 * will be converted to
	 *
	 *     [{name: "Name", address: "address@domain"}]
	 *
	 * @param {String} str Address field
	 * @return {Array} An array of address objects
	 */
	function addressparser(str) {
		var tokenizer = new Tokenizer(str);
		var tokens = tokenizer.tokenize();
		var addresses = [];
		var address = [];
		var parsedAddresses = [];

		tokens.forEach(token => {
			if (token.type === 'operator' && (token.value === ',' || token.value === ';')) {
				if (address.length) {
					addresses.push(address);
				}
				address = [];
			} else {
				address.push(token);
			}
		});

		if (address.length) {
			addresses.push(address);
		}

		addresses.forEach(address => {
			address = _handleAddress(address);
			if (address.length) {
				parsedAddresses = parsedAddresses.concat(address);
			}
		});

		return parsedAddresses;
	}

	/**
	 * Converts tokens for a single address into an address object
	 *
	 * @param {Array} tokens Tokens object
	 * @return {Object} Address object
	 */
	function _handleAddress(tokens) {
		var isGroup = false;
		var state = 'text';
		var address = void 0;
		var addresses = [];
		var data = {
			address: [],
			comment: [],
			group: [],
			text: []
		};

		// Filter out <addresses>, (comments) and regular text
		tokens.forEach(token => {
			if (token.type === 'operator') {
				switch (token.value) {
					case '<':
						state = 'address';
						break;
					case '(':
						state = 'comment';
						break;
					case ':':
						state = 'group';
						isGroup = true;
						break;
					default:
						state = 'text';
				}
			} else if (token.value) {
				data[state].push(token.value);
			}
		});

		// If there is no text but a comment, replace the two
		if (!data.text.length && data.comment.length) {
			data.text = data.comment;
			data.comment = [];
		}

		if (isGroup) {
			// http://tools.ietf.org/html/rfc2822#appendix-A.1.3
			data.text = data.text.join(' ');
			addresses.push({
				name: data.text || address && address.name,
				group: data.group.length ? addressparser(data.group.join(',')) : []
			});
		} else {
			// If no address was found, try to detect one from regular text
			if (!data.address.length && data.text.length) {
				var i = data.text.length;
				while (i--) {
					if (data.text[i].match(/^[^@\s]+@[^@\s]+$/)) {
						data.address = data.text.splice(i, 1);
						break;
					}
				}

				// still no address
				if (!data.address.length) {
					i = data.text.length;
					while (i--) {
						data.text[i] = data.text[i].replace(/\s*\b[^@\s]+@[^@\s]+\b\s*/, address => {
							if (!data.address.length) {
								data.address = [address.trim()];
								return '';
							}
							return address.trim();
						});
						if (data.address.length) {
							break;
						}
					}
				}
			}

			// If there's still is no text but a comment exixts, replace the two
			if (!data.text.length && data.comment.length) {
				data.text = data.comment;
				data.comment = [];
			}

			// Keep only the first address occurence, push others to regular text
			if (data.address.length > 1) {
				data.text = data.text.concat(data.address.splice(1));
			}

			// Join values with spaces
			data.text = data.text.join(' ');
			data.address = data.address.join(' ');

			if (!data.address && isGroup) {
				return [];
			}
			address = {
				address: data.address || data.text || '',
				name: data.text || data.address || ''
			};

			if (address.address === address.name) {
				if ((address.address || '').match(/@/)) {
					address.name = '';
				} else {
					address.address = '';
				}
			}

			addresses.push(address);
		}

		return addresses;
	}

	/*
	 * Operator tokens and which tokens are expected to end the sequence
	 */
	var OPERATORS = {
	  '"': '"',
	  '(': ')',
	  '<': '>',
	  ',': '',
	  // Groups are ended by semicolons
	  ':': ';',
	  // Semicolons are not a legal delimiter per the RFC2822 grammar other
	  // than for terminating a group, but they are also not valid for any
	  // other use in this context.  Given that some mail clients have
	  // historically allowed the semicolon as a delimiter equivalent to the
	  // comma in their UI, it makes sense to treat them the same as a comma
	  // when used outside of a group.
	  ';': ''
	};

	class Tokenizer
	{
		constructor(str) {
			this.str = (str || '').toString();
			this.operatorCurrent = '';
			this.operatorExpecting = '';
			this.node = null;
			this.escaped = false;
			this.list = [];
		}

		tokenize() {
			var list = [];
			[...this.str].forEach(c => this.checkChar(c));

			this.list.forEach(node => {
				node.value = (node.value || '').toString().trim();
				if (node.value) {
					list.push(node);
				}
			});

			return list;
		}

		checkChar(chr) {
			if ((chr in OPERATORS || chr === '\\') && this.escaped) {
				this.escaped = false;
			} else if (this.operatorExpecting && chr === this.operatorExpecting) {
				this.node = {
					type: 'operator',
					value: chr
				};
				this.list.push(this.node);
				this.node = null;
				this.operatorExpecting = '';
				this.escaped = false;
				return;
			} else if (!this.operatorExpecting && chr in OPERATORS) {
				this.node = {
					type: 'operator',
					value: chr
				};
				this.list.push(this.node);
				this.node = null;
				this.operatorExpecting = OPERATORS[chr];
				this.escaped = false;
				return;
			}

			if (!this.escaped && chr === '\\') {
				this.escaped = true;
				return;
			}

			if (!this.node) {
				this.node = {
					type: 'text',
					value: ''
				};
				this.list.push(this.node);
			}

			if (this.escaped && chr !== '\\') {
				this.node.value += '\\';
			}

			this.node.value += chr;
			this.escaped = false;
		}
	}

	class EmailModel extends AbstractModel {
		/**
		 * @param {string=} email = ''
		 * @param {string=} name = ''
		 * @param {string=} dkimStatus = 'none'
		 * @param {string=} dkimValue = ''
		 */
		constructor(email = '', name = '', dkimStatus = 'none', dkimValue = '') {
			super();
			this.email = email;
			this.name = name;
			this.dkimStatus = dkimStatus;
			this.dkimValue = dkimValue;

			this.clearDuplicateName();
		}

		/**
		 * @static
		 * @param {FetchJsonEmail} json
		 * @returns {?EmailModel}
		 */
		static reviveFromJson(json) {
			const email = super.reviveFromJson(json);
			email && email.clearDuplicateName();
			return email;
		}

		/**
		 * @returns {void}
		 */
		clear() {
			this.email = '';
			this.name = '';

			this.dkimStatus = 'none';
			this.dkimValue = '';
		}

		/**
		 * @returns {boolean}
		 */
		validate() {
			return this.name || this.email;
		}

		/**
		 * @param {boolean} withoutName = false
		 * @returns {string}
		 */
		hash(withoutName = false) {
			return '#' + (withoutName ? '' : this.name) + '#' + this.email + '#';
		}

		/**
		 * @returns {void}
		 */
		clearDuplicateName() {
			if (this.name === this.email) {
				this.name = '';
			}
		}

		/**
		 * @param {string} query
		 * @returns {boolean}
		 */
		search(query) {
			return (this.name + ' ' + this.email).toLowerCase().includes(query.toLowerCase());
		}

		/**
		 * @param {boolean} friendlyView = false
		 * @param {boolean=} wrapWithLink = false
		 * @param {boolean=} useEncodeHtml = false
		 * @returns {string}
		 */
		toLine(friendlyView, wrapWithLink, useEncodeHtml) {
			let result = '',
				toLink = (to, txt) => '<a href="mailto:' + to + '" target="_blank" tabindex="-1">' + encodeHtml(txt) + '</a>';
			if (this.email) {
				if (friendlyView && this.name) {
					result = wrapWithLink
						? toLink(
							encodeHtml(this.email) + '?to=' + encodeURIComponent('"' + this.name + '" <' + this.email + '>'),
							this.name
						)
						: (useEncodeHtml ? encodeHtml(this.name) : this.name);
				} else {
					result = this.email;
					if (this.name) {
						if (wrapWithLink) {
							result =
								encodeHtml('"' + this.name + '" <')
								+ toLink(
									encodeHtml(this.email) + '?to=' + encodeURIComponent('"' + this.name + '" <' + this.email + '>'),
									result
								)
								+ encodeHtml('>');
						} else {
							result = '"' + this.name + '" <' + result + '>';
							if (useEncodeHtml) {
								result = encodeHtml(result);
							}
						}
					} else if (wrapWithLink) {
						result = toLink(encodeHtml(this.email), this.email);
					}
				}
			}

			return result;
		}

		static splitEmailLine(line) {
			const parsedResult = addressparser(line);
			if (parsedResult.length) {
				const result = [];
				let exists = false;
				parsedResult.forEach((item) => {
					const address = item.address
						? new EmailModel(item.address.replace(/^[<]+(.*)[>]+$/g, '$1'), item.name || '')
						: null;

					if (address && address.email) {
						exists = true;
					}

					result.push(address ? address.toLine(false) : item.name);
				});

				return exists ? result : null;
			}

			return null;
		}

		static parseEmailLine(line) {
			const parsedResult = addressparser(line);
			if (parsedResult.length) {
				return parsedResult.map(item =>
					item.address ? new EmailModel(item.address.replace(/^[<]+(.*)[>]+$/g, '$1'), item.name || '') : null
				).filter(v => v);
			}

			return [];
		}

		/**
		 * @param {string} emailAddress
		 * @returns {boolean}
		 */
		parse(emailAddress) {
			emailAddress = emailAddress.trim();
			if (!emailAddress) {
				return false;
			}

			const result = addressparser(emailAddress);
			if (result.length) {
				this.name = result[0].name || '';
				this.email = result[0].address || '';
				this.clearDuplicateName();

				return true;
			}

			return false;
		}
	}

	const contentType = 'snappymail/emailaddress',
		getAddressKey = li => li && li.emailaddress && li.emailaddress.key;

	let dragAddress, datalist;

	// mailbox-list
	class EmailAddressesComponent {

		constructor(element, options) {

			if (!datalist) {
				datalist = createElement('datalist',{id:"emailaddresses-datalist"});
				doc.body.append(datalist);
			}

			var self = this,
				// In Chrome we have no access to dataTransfer.getData unless it's the 'drop' event
				// In Chrome Mobile dataTransfer.types.includes(contentType) fails, only text/plain is set
				validDropzone = () => dragAddress && dragAddress.li.parentNode !== self.ul,
				fnDrag = e => validDropzone() && e.preventDefault();

			self.element = element;

			self.options = Object.assign({

				focusCallback : null,

				// simply passing an autoComplete source (array, string or function) will instantiate autocomplete functionality
				autoCompleteSource : '',

				onChange : null
			}, options);

			self._chosenValues = [];

			self._lastEdit = '';

			// Create the elements
			self.ul = createElement('ul',{class:"emailaddresses"});

			self.ul.addEventListener('click', e => self._focus(e));
			self.ul.addEventListener('dblclick', e => self._editTag(e));
			self.ul.addEventListener("dragenter", fnDrag);
			self.ul.addEventListener("dragover", fnDrag);
			self.ul.addEventListener("drop", e => {
				if (validDropzone() && dragAddress.value) {
					e.preventDefault();
					dragAddress.source._removeDraggedTag(dragAddress.li);
					self._parseValue(dragAddress.value);
				}
			});

			self.input = createElement('input',{type:"text", list:datalist.id,
				autocomplete:"off", autocorrect:"off", autocapitalize:"off", spellcheck:"false"});

			self.input.addEventListener('focus', () => self._focusTrigger(true));
			self.input.addEventListener('blur', () => {
				// prevent autoComplete menu click from causing a false 'blur'
				self._parseInput(true);
				self._focusTrigger(false);
			});
			self.input.addEventListener('keydown', e => {
				if ('Backspace' === e.key || 'ArrowLeft' === e.key) {
					// if our input contains no value and backspace has been pressed, select the last tag
					var lastTag = self.inputCont.previousElementSibling,
						input = self.input;
					if (lastTag && (!input.value
						|| (('selectionStart' in input) && input.selectionStart === 0 && input.selectionEnd === 0))
					) {
						e.preventDefault();
						lastTag.querySelector('a').focus();
					}
					self._updateDatalist();
				} else if (e.key == 'Enter') {
					e.preventDefault();
					self._parseInput(true);
				}
			});
			self.input.addEventListener('input', () => {
				self._parseInput();
				self._updateDatalist();
			});
			self.input.addEventListener('focus', () => self.input.value || self._resetDatalist());

			// define starting placeholder
			if (element.placeholder) {
				self.input.placeholder = element.placeholder;
			}

			self.inputCont = createElement('li',{class:"emailaddresses-input"});
			self.inputCont.append(self.input);
			self.ul.append(self.inputCont);

			element.replaceWith(self.ul);

			// if instantiated input already contains a value, parse that junk
			if (element.value.trim()) {
				self._parseValue(element.value);
			}

			self._updateDatalist = self.options.autoCompleteSource
				? (() => {
					let value = self.input.value.trim();
					if (datalist.inputValue !== value) {
						datalist.inputValue = value;
						value.length && self.options.autoCompleteSource(
							{term:value},
							items => {
								self._resetDatalist();
								items && items.forEach(item => datalist.append(new Option(item)));
							}
						);
					}
				}).throttle(500)
				: () => 0;
		}

		_focusTrigger(bValue) {
			this.ul.classList.toggle('emailaddresses-focused', bValue);
			this.options.focusCallback(bValue);
		}

		_resetDatalist() {
			datalist.textContent = '';
		}

		_parseInput(force) {
			let val = this.input.value;
			if (force || val.includes(',') || val.includes(';')) {
				this._parseValue(val) && (this.input.value = '');
			}
			this._resizeInput();
		}

		_parseValue(val) {
			if (val) {
				var self = this,
					values = [];

				const v = val.trim(),
					hook = (v && [',', ';', '\n'].includes(v.slice(-1)))
						 ? EmailModel.splitEmailLine(val)
						 : null;

				values = (hook || [val]).map(value => EmailModel.parseEmailLine(value))
						.flat(Infinity)
						.map(item => (item.toLine ? [item.toLine(false), item] : [item, null]));

				if (values.length) {
					self._setChosen(values);
					return true;
				}
			}
		}

		// the input dynamically resizes based on the length of its value
		_resizeInput() {
			let input = this.input;
			if (input.clientWidth < input.scrollWidth) {
				input.style.width = Math.min(500, Math.max(200, input.scrollWidth)) + 'px';
			}
		}

		_editTag(ev) {
			var li = ev.target.closest('li'),
				tagKey = getAddressKey(li);

			if (!tagKey) {
				return true;
			}

			var self = this,
				tagName = '',
				oPrev = null,
				next = false
			;

			self._chosenValues.forEach(v => {
				if (v.key === tagKey) {
					tagName = v.value;
					next = true;
				} else if (next && !oPrev) {
					oPrev = v;
				}
			});

			if (oPrev)
			{
				self._lastEdit = oPrev.value;
			}

			li.after(self.inputCont);

			self.input.value = tagName;
			setTimeout(() => self.input.select(), 100);

			self._removeTag(ev, li);
			self._resizeInput(ev);
		}

		_setChosen(valArr) {
			var self = this;

			if (!isArray(valArr)){
				return false;
			}

			valArr.forEach(a => {
				var v = a[0].trim(),
					exists = false,
					lastIndex = -1,
					obj = {
						key : '',
						obj : null,
						value : ''
					};

				self._chosenValues.forEach((vv, kk) => {
					if (vv.value === self._lastEdit) {
						lastIndex = kk;
					}

					vv.value === v && (exists = true);
				});

				if (v !== '' && a && a[1] && !exists) {

					obj.key = 'mi_' + Math.random().toString( 16 ).slice( 2, 10 );
					obj.value = v;
					obj.obj = a[1];

					if (-1 < lastIndex) {
						self._chosenValues.splice(lastIndex, 0, obj);
					} else {
						self._chosenValues.push(obj);
					}

					self._lastEdit = '';
					self._renderTags();
				}
			});

			if (valArr.length === 1 && valArr[0] === '' && self._lastEdit !== '') {
				self._lastEdit = '';
				self._renderTags();
			}

			self._setValue(self._buildValue());
		}

		_buildValue() {
			return this._chosenValues.map(v => v.value).join(',');
		}

		_setValue(value) {
			if (this.element.value !== value) {
				this.element.value = value;
				this.options.onChange(value);
			}
		}

		_renderTags() {
			let self = this;
			[...self.ul.children].forEach(node => node !== self.inputCont && node.remove());

			self._chosenValues.forEach(v => {
				if (v.obj) {
					let li = createElement('li',{title:v.obj.toLine(false, false, true),draggable:'true'}),
						el = createElement('span');
					el.append(v.obj.toLine(true, false, true));
					li.append(el);

					el = createElement('a',{href:'#', class:'ficon'});
					el.append('✖');
					el.addEventListener('click', e => self._removeTag(e, li));
					el.addEventListener('focus', () => li.className = 'emailaddresses-selected');
					el.addEventListener('blur', () => li.className = null);
					el.addEventListener('keydown', e => {
						switch (e.key) {
							case 'Delete':
							case 'Backspace':
								self._removeTag(e, li);
								break;

							// 'e' - edit tag (removes tag and places value into visible input
							case 'e':
							case 'Enter':
								self._editTag(e);
								break;

							case 'ArrowLeft':
								// select the previous tag or input if no more tags exist
								var previous = el.closest('li').previousElementSibling;
								if (previous.matches('li')) {
									previous.querySelector('a').focus();
								} else {
									self.focus();
								}
								break;

							case 'ArrowRight':
								// select the next tag or input if no more tags exist
								var next = el.closest('li').nextElementSibling;
								if (next !== this.inputCont) {
									next.querySelector('a').focus();
								} else {
									this.focus();
								}
								break;

							case 'ArrowDown':
								self._focus(e);
								break;
						}
					});
					li.append(el);

					li.emailaddress = v;

					li.addEventListener("dragstart", e => {
						dragAddress = {
							source: self,
							li: li,
							value: li.emailaddress.obj.toLine()
						};
	//					e.dataTransfer.setData(contentType, li.emailaddress.obj.toLine());
						e.dataTransfer.setData('text/plain', contentType);
	//					e.dataTransfer.setDragImage(li, 0, 0);
						e.dataTransfer.effectAllowed = 'move';
						li.style.opacity = 0.25;
					});
					li.addEventListener("dragend", () => {
						dragAddress = null;
						li.style.cssText = '';
					});

					self.inputCont.before(li);
				}
			});
		}

		_removeTag(ev, li) {
			ev.preventDefault();

			var key = getAddressKey(li),
				self = this,
				indexFound = self._chosenValues.findIndex(v => key === v.key);

			indexFound > -1 && self._chosenValues.splice(indexFound, 1);

			self._setValue(self._buildValue());

			li.remove();
			setTimeout(() => self.input.focus(), 100);
		}

		_removeDraggedTag(li) {
			var
				key = getAddressKey(li),
				self = this,
				indexFound = self._chosenValues.findIndex(v => key === v.key)
			;
			if (-1 < indexFound) {
				self._chosenValues.splice(indexFound, 1);
				self._setValue(self._buildValue());
			}

			li.remove();
		}

		focus () {
			this.input.focus();
		}

		blur() {
			this.input.blur();
		}

		_focus(ev) {
			var li = ev.target.closest('li');
			if (getAddressKey(li)) {
				li.querySelector('a').focus();
			} else {
				this.focus();
			}
		}

		set value(value) {
			var self = this;
			if (self.element.value !== value) {
	//			self.input.value = '';
	//			self._resizeInput();
				self._chosenValues = [];
				self._renderTags();
				self._parseValue(self.element.value = value);
			}
		}
	}

	const ThemeStore = {
		themes: ko.observableArray(),
		userBackgroundName: ko.observable(''),
		userBackgroundHash: ko.observable(''),
		isMobile: ko.observable($htmlCL.contains('rl-mobile')),

		populate: function(){
			const themes = Settings.app('themes');

			this.themes(isArray(themes) ? themes : []);
			this.theme(SettingsGet('Theme'));
			if (!this.isMobile()) {
				this.userBackgroundName(SettingsGet('UserBackgroundName'));
				this.userBackgroundHash(SettingsGet('UserBackgroundHash'));
			}

			leftPanelDisabled(this.isMobile());
		}
	};

	ThemeStore.theme = ko.observable('').extend({ limitedList: ThemeStore.themes });

	ThemeStore.isMobile.subscribe(value => $htmlCL.toggle('rl-mobile', value));

	ThemeStore.userBackgroundHash.subscribe(value => {
		if (value) {
			$htmlCL.add('UserBackground');
			doc.body.style.backgroundImage = "url("+serverRequestRaw('UserBackground', value)+")";
		} else {
			$htmlCL.remove('UserBackground');
			doc.body.removeAttribute('style');
		}
	});

	const rlContentType = 'snappymail/action',

		// In Chrome we have no access to dataTransfer.getData unless it's the 'drop' event
		// In Chrome Mobile dataTransfer.types.includes(rlContentType) fails, only text/plain is set
		getDragAction = () => dragData ? dragData.action : false,
		setDragAction = (e, action, effect, data, img) => {
			dragData = {
				action: action,
				data: data
			};
	//		e.dataTransfer.setData(rlContentType, action);
			e.dataTransfer.setData('text/plain', rlContentType+'/'+action);
			e.dataTransfer.setDragImage(img, 0, 0);
			e.dataTransfer.effectAllowed = effect;
		},

		dragTimer = {
			id: 0,
			stop: () => clearTimeout(dragTimer.id),
			start: fn => dragTimer.id = setTimeout(fn, 500)
		};

	let dragImage,
		dragData;

	ko.bindingHandlers.editor = {
		init: (element, fValueAccessor) => {
			let editor = null;

			const fValue = fValueAccessor(),
				fUpdateEditorValue = () => fValue && fValue.__editor && fValue.__editor.setHtmlOrPlain(fValue()),
				fUpdateKoValue = () => fValue && fValue.__editor && fValue(fValue.__editor.getDataWithHtmlMark()),
				fOnReady = () => {
					fValue.__editor = editor;
					fUpdateEditorValue();
				};

			if (ko.isObservable(fValue) && HtmlEditor) {
				editor = new HtmlEditor(element, fUpdateKoValue, fOnReady, fUpdateKoValue);

				fValue.__fetchEditorValue = fUpdateKoValue;

				fValue.subscribe(fUpdateEditorValue);

				// ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
				// });
			}
		}
	};

	let ttn = (element, fValueAccessor) => timeToNode(element, ko.unwrap(fValueAccessor()));
	ko.bindingHandlers.moment = {
		init: ttn,
		update: ttn
	};

	ko.bindingHandlers.emailsTags = {
		init: (element, fValueAccessor, fAllBindings) => {
			const fValue = fValueAccessor();

			element.addresses = new EmailAddressesComponent(element, {
				focusCallback: value => fValue.focused && fValue.focused(!!value),
				autoCompleteSource: fAllBindings.get('autoCompleteSource'),
				onChange: value => fValue(value)
			});

			if (fValue.focused && fValue.focused.subscribe) {
				fValue.focused.subscribe(value =>
					element.addresses[value ? 'focus' : 'blur']()
				);
			}
		},
		update: (element, fValueAccessor) => {
			element.addresses.value = ko.unwrap(fValueAccessor());
		}
	};

	// Start dragging selected messages
	ko.bindingHandlers.dragmessages = {
		init: (element, fValueAccessor) => {
			element.addEventListener("dragstart", e => {
				let data = fValueAccessor()(e);
				dragImage || (dragImage = elementById('messagesDragImage'));
				if (data && dragImage && !ThemeStore.isMobile()) {
					dragImage.querySelector('.text').textContent = data.uids.length;
					let img = dragImage.querySelector('i');
					img.classList.toggle('icon-copy', e.ctrlKey);
					img.classList.toggle('icon-mail', !e.ctrlKey);

					// Else Chrome doesn't show it
					dragImage.style.left = e.clientX + 'px';
					dragImage.style.top = e.clientY + 'px';
					dragImage.style.right = 'auto';

					setDragAction(e, 'messages', e.ctrlKey ? 'copy' : 'move', data, dragImage);

					// Remove the Chrome visibility
					dragImage.style.cssText = '';
				} else {
					e.preventDefault();
				}

			}, false);
			element.addEventListener("dragend", () => dragData = null);
			element.setAttribute('draggable', true);
		}
	};

	// Drop selected messages on folder
	ko.bindingHandlers.dropmessages = {
		init: (element, fValueAccessor) => {
			const folder = fValueAccessor(),
	//			folder = ko.dataFor(element),
				fnStop = e => {
					e.preventDefault();
					element.classList.remove('droppableHover');
					dragTimer.stop();
				},
				fnHover = e => {
					if ('messages' === getDragAction()) {
						fnStop(e);
						element.classList.add('droppableHover');
						if (folder && folder.collapsed()) {
							dragTimer.start(() => {
								folder.collapsed(false);
								rl.app.setExpandedFolder(folder.fullName, true);
							}, 500);
						}
					}
				};
			element.addEventListener("dragenter", fnHover);
			element.addEventListener("dragover", fnHover);
			element.addEventListener("dragleave", fnStop);
			element.addEventListener("drop", e => {
				fnStop(e);
				if ('messages' === getDragAction() && ['move','copy'].includes(e.dataTransfer.effectAllowed)) {
					let data = dragData.data;
					if (folder && data && data.folder && isArray(data.uids)) {
						rl.app.moveMessagesToFolder(data.folder, data.uids, folder.fullName, data.copy && e.ctrlKey);
					}
				}
			});
		}
	};

	ko.bindingHandlers.sortableItem = {
		init: (element, fValueAccessor) => {
			let options = ko.unwrap(fValueAccessor()) || {},
				parent = element.parentNode,
				fnHover = e => {
					if ('sortable' === getDragAction()) {
						e.preventDefault();
						let node = (e.target.closest ? e.target : e.target.parentNode).closest('[draggable]');
						if (node && node !== dragData.data && parent.contains(node)) {
							let rect = node.getBoundingClientRect();
							if (rect.top + (rect.height / 2) <= e.clientY) {
								if (node.nextElementSibling !== dragData.data) {
									node.after(dragData.data);
								}
							} else if (node.previousElementSibling !== dragData.data) {
								node.before(dragData.data);
							}
						}
					}
				};
			element.addEventListener("dragstart", e => {
				dragData = {
					action: 'sortable',
					element: element
				};
				setDragAction(e, 'sortable', 'move', element, element);
				element.style.opacity = 0.25;
			});
			element.addEventListener("dragend", e => {
				element.style.opacity = null;
				if ('sortable' === getDragAction()) {
					dragData.data.style.cssText = '';
					let row = parent.rows[options.list.indexOf(ko.dataFor(element))];
					if (row != dragData.data) {
						row.before(dragData.data);
					}
					dragData = null;
				}
			});
			if (!parent.sortable) {
				parent.sortable = true;
				parent.addEventListener("dragenter", fnHover);
				parent.addEventListener("dragover", fnHover);
				parent.addEventListener("drop", e => {
					if ('sortable' === getDragAction()) {
						e.preventDefault();
						let data = ko.dataFor(dragData.data),
							from = options.list.indexOf(data),
							to = [...parent.children].indexOf(dragData.data);
						if (from != to) {
							let arr = options.list();
							arr.splice(to, 0, ...arr.splice(from, 1));
							options.list(arr);
						}
						dragData = null;
						options.afterMove && options.afterMove();
					}
				});
			}
		}
	};

	ko.bindingHandlers.initDom = {
		init: (element, fValueAccessor) => fValueAccessor()(element)
	};

	ko.bindingHandlers.onEsc = {
		init: (element, fValueAccessor, fAllBindings, viewModel) => {
			let fn = event => {
				if ('Escape' == event.key) {
					element.dispatchEvent(new Event('change'));
					fValueAccessor().call(viewModel);
				}
			};
			element.addEventListener('keyup', fn);
			ko.utils.domNodeDisposal.addDisposeCallback(element, () => element.removeEventListener('keyup', fn));
		}
	};

	ko.bindingHandlers.registerBootstrapDropdown = {
		init: element => {
			rl.Dropdowns.register(element);
			element.ddBtn = new BSN.Dropdown(element.querySelector('.dropdown-toggle'));
		}
	};

	ko.bindingHandlers.openDropdownTrigger = {
		update: (element, fValueAccessor) => {
			if (ko.unwrap(fValueAccessor())) {
				const el = element.ddBtn;
				el.open || el.toggle();
	//			el.focus();

				rl.Dropdowns.detectVisibility();
				fValueAccessor()(false);
			}
		}
	};

	/* eslint quote-props: 0 */

	/**
	 * @enum {number}
	 */
	const FolderType = {
		User: 0,
		Inbox: 1,
		Sent: 2,
		Drafts: 3,
		Spam: 4, // JUNK
		Trash: 5,
		Archive: 6,
		NotSpam: 80
	};

	/**
	 * @enum {string}
	 */
	const FolderMetadataKeys = {
		// RFC 5464
		Comment: '/private/comment',
		CommentShared: '/shared/comment',
		// RFC 6154
		SpecialUse: '/private/specialuse',
		// Kolab
		KolabFolderType: '/private/vendor/kolab/folder-type',
		KolabFolderTypeShared: '/shared/vendor/kolab/folder-type'
	};

	/**
	 * @enum {string}
	 */
	const FolderSortMode = {
		DateDesc: '', // default 'REVERSE DATE'
		DateAsc: 'DATE',
		FromDesc: 'REVERSE FROM',
		FromAsc: 'FROM',
		SizeDesc: 'REVERSE SIZE',
		SizeAsc: 'SIZE',
		SubjectDesc: 'REVERSE SUBJECT',
		SubjectAsc: 'SUBJECT'
	//	ToDesc: 'REVERSE TO',
	//	ToAsc: 'TO',
	};

	/**
	 * @enum {string}
	 */
	const ComposeType = {
		Empty: 'empty',
		Reply: 'reply',
		ReplyAll: 'replyall',
		Forward: 'forward',
		ForwardAsAttachment: 'forward-as-attachment',
		Draft: 'draft',
		EditAsNew: 'editasnew'
	};

	/**
	 * @enum {number}
	 */
	const SetSystemFoldersNotification = {
		None: 0,
		Sent: 1,
		Draft: 2,
		Spam: 3,
		Trash: 4,
		Archive: 5
	};

	/**
	 * @enum {number}
	 */
	const ClientSideKeyName = {
		ExpandedFolders: 3,
		FolderListSize: 4,
		MessageListSize: 5,
		LastReplyAction: 6,
		LastSignMe: 7,
		MessageHeaderFullInfo: 9,
		MessageAttachmentControls: 10
	};

	/**
	 * @enum {number}
	 */
	const MessageSetAction = {
		SetSeen: 0,
		UnsetSeen: 1,
		SetFlag: 2,
		UnsetFlag: 3
	};

	/**
	 * @enum {number}
	 */
	const MessagePriority = {
		Low: 5,
		Normal: 3,
		High: 1
	};

	/**
	 * @enum {string}
	 */
	const EditorDefaultType = {
		Html: 'Html',
		Plain: 'Plain',
		HtmlForced: 'HtmlForced',
		PlainForced: 'PlainForced'
	};

	/**
	 * @enum {number}
	 */
	const Layout = {
		NoPreview: 0,
		SidePreview: 1,
		BottomPreview: 2
	};

	const UNUSED_OPTION_VALUE = '__UNUSE__';

	let FOLDERS_CACHE = {},
		FOLDERS_NAME_CACHE = {},
		MESSAGE_FLAGS_CACHE = {},
		NEW_MESSAGE_CACHE = {},
		REQUESTED_MESSAGE_CACHE = {},
		inboxFolderName = 'INBOX';

	const /**
		 * @param {string} folderFullName
		 * @param {string} uid
		 * @returns {string}
		 */
		getMessageKey = (folderFullName, uid) => `${folderFullName}#${uid}`,

		/**
		 * @param {string} folder
		 * @param {string} uid
		 */
		addRequestedMessage = (folder, uid) => REQUESTED_MESSAGE_CACHE[getMessageKey(folder, uid)] = true,

		/**
		 * @param {string} folder
		 * @param {string} uid
		 * @returns {boolean}
		 */
		hasRequestedMessage = (folder, uid) => true === REQUESTED_MESSAGE_CACHE[getMessageKey(folder, uid)],

		/**
		 * @param {string} folderFullName
		 * @param {string} uid
		 */
		addNewMessageCache = (folderFullName, uid) => NEW_MESSAGE_CACHE[getMessageKey(folderFullName, uid)] = true,

		/**
		 * @param {string} folderFullName
		 * @param {string} uid
		 */
		hasNewMessageAndRemoveFromCache = (folderFullName, uid) => {
			if (NEW_MESSAGE_CACHE[getMessageKey(folderFullName, uid)]) {
				NEW_MESSAGE_CACHE[getMessageKey(folderFullName, uid)] = null;
				return true;
			}
			return false;
		},

		/**
		 * @returns {void}
		 */
		clearNewMessageCache = () => NEW_MESSAGE_CACHE = {},

		/**
		 * @returns {string}
		 */
		getFolderInboxName = () => inboxFolderName,

		/**
		 * @returns {string}
		 */
		setFolderInboxName = name => inboxFolderName = name,

		/**
		 * @param {string} folderHash
		 * @returns {string}
		 */
		getFolderFullName = folderHash =>
			folderHash && FOLDERS_NAME_CACHE[folderHash] ? FOLDERS_NAME_CACHE[folderHash] : '',

		/**
		 * @param {string} folderHash
		 * @param {string} folderFullName
		 * @param {?FolderModel} folder
		 */
		setFolder = folder => {
			folder.hash = '';
			FOLDERS_CACHE[folder.fullName] = folder;
			FOLDERS_NAME_CACHE[folder.fullNameHash] = folder.fullName;
		},

		/**
		 * @param {string} folderFullName
		 * @returns {string}
		 */
		getFolderHash = folderFullName =>
			FOLDERS_CACHE[folderFullName] ? FOLDERS_CACHE[folderFullName].hash : '',

		/**
		 * @param {string} folderFullName
		 * @param {string} folderHash
		 */
		setFolderHash = (folderFullName, folderHash) =>
			FOLDERS_CACHE[folderFullName] && (FOLDERS_CACHE[folderFullName].hash = folderHash),

		/**
		 * @param {string} folderFullName
		 * @returns {string}
		 */
		getFolderUidNext = folderFullName =>
			FOLDERS_CACHE[folderFullName] ? FOLDERS_CACHE[folderFullName].uidNext : 0,

		/**
		 * @param {string} folderFullName
		 * @param {string} uidNext
		 */
		setFolderUidNext = (folderFullName, uidNext) =>
			FOLDERS_CACHE[folderFullName] && (FOLDERS_CACHE[folderFullName].uidNext = uidNext),

		/**
		 * @param {string} folderFullName
		 * @returns {?FolderModel}
		 */
		getFolderFromCacheList = folderFullName =>
			FOLDERS_CACHE[folderFullName] ? FOLDERS_CACHE[folderFullName] : null,

		/**
		 * @param {string} folderFullName
		 */
		removeFolderFromCacheList = folderFullName => delete FOLDERS_CACHE[folderFullName];

	class MessageFlagsCache
	{
		/**
		 * @param {string} folderFullName
		 * @param {string} uid
		 * @param {string} flag
		 * @returns {bool}
		 */
		static hasFlag(folderFullName, uid, flag) {
			return MESSAGE_FLAGS_CACHE[folderFullName]
				&& MESSAGE_FLAGS_CACHE[folderFullName][uid]
				&& MESSAGE_FLAGS_CACHE[folderFullName][uid].includes(flag);
		}

		/**
		 * @param {string} folderFullName
		 * @param {string} uid
		 * @returns {?Array}
		 */
		static getFor(folderFullName, uid) {
			return MESSAGE_FLAGS_CACHE[folderFullName] && MESSAGE_FLAGS_CACHE[folderFullName][uid];
		}

		/**
		 * @param {string} folderFullName
		 * @param {string} uid
		 * @param {Array} flagsCache
		 */
		static setFor(folderFullName, uid, flags) {
			if (isArray(flags)) {
				if (!MESSAGE_FLAGS_CACHE[folderFullName]) {
					MESSAGE_FLAGS_CACHE[folderFullName] = {};
				}
				MESSAGE_FLAGS_CACHE[folderFullName][uid] = flags;
			}
		}

		/**
		 * @param {string} folderFullName
		 */
		static clearFolder(folderFullName) {
			MESSAGE_FLAGS_CACHE[folderFullName] = {};
		}

		/**
		 * @param {(MessageModel|null)} message
		 */
		static initMessage(message) {
			if (message) {
				const uid = message.uid,
					flags = this.getFor(message.folder, uid),
					thread = message.threads;

				if (isArray(flags)) {
					message.flags(flags);
				}

				if (thread.length) {
					const unseenSubUid = thread.find(iSubUid =>
						(uid !== iSubUid) && !this.hasFlag(message.folder, iSubUid, '\\seen')
					);

					const flaggedSubUid = thread.find(iSubUid =>
						(uid !== iSubUid) && this.hasFlag(message.folder, iSubUid, '\\flagged')
					);

					message.hasUnseenSubMessage(!!unseenSubUid);
					message.hasFlaggedSubMessage(!!flaggedSubUid);
				}
			}
		}

		/**
		 * @param {(MessageModel|null)} message
		 */
		static store(message) {
			if (message) {
				this.setFor(message.folder, message.uid, message.flags());
			}
		}

		/**
		 * @param {string} folder
		 * @param {string} uid
		 * @param {number} setAction
		 */
		static storeBySetAction(folder, uid, setAction) {
			let flags = this.getFor(folder, uid) || [];
			const
				unread = flags.includes('\\seen') ? 0 : 1,
				add = item => flags.includes(item) || flags.push(item),
				remove = item => flags = flags.filter(flag => flag != item);

			switch (setAction) {
				case MessageSetAction.SetSeen:
					add('\\seen');
					break;
				case MessageSetAction.UnsetSeen:
					remove('\\seen');
					break;
				case MessageSetAction.SetFlag:
					add('\\flagged');
					break;
				case MessageSetAction.UnsetFlag:
					remove('\\flagged');
					break;
				// no default
			}

			this.setFor(folder, uid, flags);

			return unread;
		}

	}

	//import Remote from 'Remote/User/Fetch'; Circular dependency

	const FolderUserStore = new class {
		constructor() {
			const self = this;
			addObservablesTo(self, {
				/**
				 * To use "checkable" option in /#/settings/folders
				 * When true, getNextFolderNames only lists system and "checkable" folders
				 * and affects the update of unseen count
				 * Auto set to true when amount of folders > folderSpecLimit to prevent requests overload,
				 * see application.ini [labs] folders_spec_limit
				 */
				displaySpecSetting: false,

	//			sortMode: '',

				quotaLimit: 0,
				quotaUsage: 0,

				sentFolder: '',
				draftsFolder: '',
				spamFolder: '',
				trashFolder: '',
				archiveFolder: '',

				folderListOptimized: false,
				folderListError: '',

				foldersLoading: false,
				foldersCreating: false,
				foldersDeleting: false,
				foldersRenaming: false,

				foldersInboxUnreadCount: 0
			});

			self.sortMode = ko.observable('').extend({ limitedList: Object.values(FolderSortMode) });

			self.namespace = '';

			self.folderList = ko.observableArray(/*new FolderCollectionModel*/);

			self.capabilities = ko.observableArray();

			self.currentFolder = ko.observable(null).extend({ toggleSubscribeProperty: [self, 'selected'] });

			addComputablesTo(self, {

				draftsFolderNotEnabled: () => !self.draftsFolder() || UNUSED_OPTION_VALUE === self.draftsFolder(),

				currentFolderFullName: () => (self.currentFolder() ? self.currentFolder().fullName : ''),
				currentFolderFullNameHash: () => (self.currentFolder() ? self.currentFolder().fullNameHash : ''),

				foldersChanging: () =>
					self.foldersLoading() | self.foldersCreating() | self.foldersDeleting() | self.foldersRenaming(),

				folderListSystemNames: () => {
					const list = [getFolderInboxName()],
					others = [self.sentFolder(), self.draftsFolder(), self.spamFolder(), self.trashFolder(), self.archiveFolder()];

					self.folderList().length &&
						others.forEach(name => name && UNUSED_OPTION_VALUE !== name && list.push(name));

					return list;
				},

				folderListSystem: () =>
					self.folderListSystemNames().map(name => getFolderFromCacheList(name)).filter(v => v)
			});

			const
				fRemoveSystemFolderType = (observable) => () => {
					const folder = getFolderFromCacheList(observable());
					folder && folder.type(FolderType.User);
				},
				fSetSystemFolderType = type => value => {
					const folder = getFolderFromCacheList(value);
					folder && folder.type(type);
				};

			self.sentFolder.subscribe(fRemoveSystemFolderType(self.sentFolder), self, 'beforeChange');
			self.draftsFolder.subscribe(fRemoveSystemFolderType(self.draftsFolder), self, 'beforeChange');
			self.spamFolder.subscribe(fRemoveSystemFolderType(self.spamFolder), self, 'beforeChange');
			self.trashFolder.subscribe(fRemoveSystemFolderType(self.trashFolder), self, 'beforeChange');
			self.archiveFolder.subscribe(fRemoveSystemFolderType(self.archiveFolder), self, 'beforeChange');

			addSubscribablesTo(self, {
				sentFolder: fSetSystemFolderType(FolderType.Sent),
				draftsFolder: fSetSystemFolderType(FolderType.Drafts),
				spamFolder: fSetSystemFolderType(FolderType.Spam),
				trashFolder: fSetSystemFolderType(FolderType.Trash),
				archiveFolder: fSetSystemFolderType(FolderType.Archive)
			});

			self.quotaPercentage = koComputable(() => {
				const quota = self.quotaLimit(), usage = self.quotaUsage();
				return 0 < quota ? Math.ceil((usage / quota) * 100) : 0;
			});
		}

		/**
		 * If the IMAP server supports SORT, METADATA
		 */
		hasCapability(name) {
			return this.capabilities().includes(name);
		}

		/**
		 * @returns {Array}
		 */
		getNextFolderNames(ttl) {
			const result = [],
				limit = 10,
				utc = Date.now(),
				timeout = utc - ttl,
				timeouts = [],
				bDisplaySpecSetting = this.displaySpecSetting(),
				fSearchFunction = (list) => {
					list.forEach(folder => {
						if (
							folder &&
							folder.selectable() &&
							folder.exists &&
							timeout > folder.expires &&
							(folder.isSystemFolder() || (folder.subscribed() && (folder.checkable() || !bDisplaySpecSetting)))
						) {
							timeouts.push([folder.expires, folder.fullName]);
						}

						if (folder && folder.subFolders.length) {
							fSearchFunction(folder.subFolders());
						}
					});
				};

			fSearchFunction(this.folderList());

			timeouts.sort((a, b) => (a[0] < b[0]) ? -1 : (a[0] > b[0] ? 1 : 0));

			timeouts.find(aItem => {
				const folder = getFolderFromCacheList(aItem[1]);
				if (folder) {
					folder.expires = utc;
					result.push(aItem[1]);
				}

				return limit <= result.length;
			});

			return result.filter((value, index, self) => self.indexOf(value) == index);
		}

		saveSystemFolders(folders) {
			folders = folders || {
				Sent: FolderUserStore.sentFolder(),
				Drafts: FolderUserStore.draftsFolder(),
				Spam: FolderUserStore.spamFolder(),
				Trash: FolderUserStore.trashFolder(),
				Archive: FolderUserStore.archiveFolder()
			};
			forEachObjectEntry(folders, (k,v)=>Settings.set(k+'Folder',v));
			rl.app.Remote.request('SystemFoldersUpdate', null, folders);
		}
	};

	const SettingsUserStore = new class {
		constructor() {
			const self = this;

			self.layout = ko
				.observable(1)
				.extend({ limitedList: Object.values(Layout) });

			self.editorDefaultType = ko.observable('Html').extend({
				limitedList: [
					EditorDefaultType.Html,
					EditorDefaultType.Plain,
					EditorDefaultType.HtmlForced,
					EditorDefaultType.PlainForced
				]
			});

			self.messagesPerPage = ko.observable(25).extend({ debounce: 999 });

			self.messageReadDelay = ko.observable(5).extend({ debounce: 999 });

			addObservablesTo(self, {
				viewHTML: 1,
				showImages: 0,
				removeColors: 0,
				useCheckboxesInList: 1,
				allowDraftAutosave: 1,
				useThreads: 0,
				replySameFolder: 0,
				hideUnsubscribed: 0,
				autoLogout: 0
			});

			self.init();

			self.usePreviewPane = koComputable(() => Layout.NoPreview !== self.layout() && !ThemeStore.isMobile());

			const toggleLayout = () => {
				const value = ThemeStore.isMobile() ? Layout.NoPreview : self.layout();
				$htmlCL.toggle('rl-no-preview-pane', Layout.NoPreview === value);
				$htmlCL.toggle('rl-side-preview-pane', Layout.SidePreview === value);
				$htmlCL.toggle('rl-bottom-preview-pane', Layout.BottomPreview === value);
				fireEvent('rl-layout', value);
			};
			self.layout.subscribe(toggleLayout);
			ThemeStore.isMobile.subscribe(toggleLayout);
			toggleLayout();

			let iAutoLogoutTimer;
			self.delayLogout = (() => {
				clearTimeout(iAutoLogoutTimer);
				if (0 < self.autoLogout() && !SettingsGet('AccountSignMe')) {
					iAutoLogoutTimer = setTimeout(
						rl.app.logout,
						self.autoLogout() * 60000
					);
				}
			}).throttle(5000);
		}

		init() {
			const self = this;
			self.editorDefaultType(SettingsGet('EditorDefaultType'));

			self.layout(pInt(SettingsGet('Layout')));
			self.messagesPerPage(pInt(SettingsGet('MessagesPerPage')));
			self.messageReadDelay(pInt(SettingsGet('MessageReadDelay')));
			self.autoLogout(pInt(SettingsGet('AutoLogout')));

			self.viewHTML(SettingsGet('ViewHTML'));
			self.showImages(SettingsGet('ShowImages'));
			self.removeColors(SettingsGet('RemoveColors'));
			self.useCheckboxesInList(SettingsGet('UseCheckboxesInList'));
			self.allowDraftAutosave(SettingsGet('AllowDraftAutosave'));
			self.useThreads(SettingsGet('UseThreads'));
			self.replySameFolder(SettingsGet('ReplySameFolder'));

			self.hideUnsubscribed(SettingsGet('HideUnsubscribed'));
		}
	};

	const
		win = window,
		CLIENT_SIDE_STORAGE_INDEX_NAME = 'rlcsc',
		sName = 'localStorage',
		getStorage = () => {
			try {
				const value = localStorage.getItem(CLIENT_SIDE_STORAGE_INDEX_NAME);
				return value ? JSON.parse(value) : null;
			} catch (e) {
				return null;
			}
		};

	// Storage
	try {
		win[sName].setItem(sName, '');
		win[sName].getItem(sName);
		win[sName].removeItem(sName);
	} catch (e) {
		console.error(e);
		// initialise if there's already data
		let data = document.cookie.match(/(^|;) ?localStorage=([^;]+)/);
		data = data ? decodeURIComponent(data[2]) : null;
		data = data ? JSON.parse(data) : {};
		win[sName] = {
			getItem: key => data[key] == null ? null : data[key],
			setItem: (key, value) => {
				data[key] = ''+value; // forces the value to a string
				document.cookie = sName+'='+encodeURIComponent(JSON.stringify(data))
					+"; expires="+((new Date(Date.now()+(365*24*60*60*1000))).toGMTString())
					+"; path=/; samesite=strict";
			}
		};
	}

	/**
	 * @param {number} key
	 * @param {*} data
	 * @returns {boolean}
	 */
	function set(key, data) {
		const storageResult = getStorage() || {};
		storageResult['p' + key] = data;

		try {
			localStorage.setItem(CLIENT_SIDE_STORAGE_INDEX_NAME, JSON.stringify(storageResult));
			return true;
		} catch (e) {
			return false;
		}
	}

	/**
	 * @param {number} key
	 * @returns {*}
	 */
	function get(key) {
		try {
			return (getStorage() || {})['p' + key];
		} catch (e) {
			return null;
		}
	}

	const

	sortFolders = folders => {
		try {
			let collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
			folders.sort((a, b) =>
				a.isInbox() ? -1 : (b.isInbox() ? 1 : collator.compare(a.fullName, b.fullName))
			);
		} catch (e) {
			console.error(e);
		}
	},

	/**
	 * @param {string} link
	 * @returns {boolean}
	 */
	download = (link, name = "") => {
		if (ThemeStore.isMobile()) {
			open(link, '_self');
			focus();
		} else {
			const oLink = createElement('a');
			oLink.href = link;
			oLink.target = '_blank';
			oLink.download = name;
			doc.body.appendChild(oLink).click();
			oLink.remove();
		}
	},

	/**
	 * @param {Array=} aDisabled
	 * @param {Array=} aHeaderLines
	 * @param {Function=} fDisableCallback
	 * @param {Function=} fRenameCallback
	 * @param {boolean=} bNoSelectSelectable Used in FolderCreatePopupView
	 * @returns {Array}
	 */
	folderListOptionsBuilder = (
		aDisabled,
		aHeaderLines,
		fRenameCallback,
		fDisableCallback,
		bNoSelectSelectable,
		aList = FolderUserStore.folderList()
	) => {
		const
			aResult = [],
			sDeepPrefix = '\u00A0\u00A0\u00A0',
			// FolderSystemPopupView should always be true
			showUnsubscribed = fRenameCallback ? !SettingsUserStore.hideUnsubscribed() : true,

			foldersWalk = folders => {
				folders.forEach(oItem => {
					if (showUnsubscribed || oItem.hasSubscriptions() || !oItem.exists) {
						aResult.push({
							id: oItem.fullName,
							name:
								sDeepPrefix.repeat(oItem.deep) +
								fRenameCallback(oItem),
							system: false,
							disabled: !bNoSelectSelectable && (
								!oItem.selectable() ||
								aDisabled.includes(oItem.fullName) ||
								fDisableCallback(oItem))
						});
					}

					if (oItem.subFolders.length) {
						foldersWalk(oItem.subFolders());
					}
				});
			};


		fDisableCallback = fDisableCallback || (() => false);
		fRenameCallback = fRenameCallback || (oItem => oItem.name());
		isArray(aDisabled) || (aDisabled = []);

		isArray(aHeaderLines) && aHeaderLines.forEach(line =>
			aResult.push({
				id: line[0],
				name: line[1],
				system: false,
				disabled: false
			})
		);

		foldersWalk(aList);

		return aResult;
	},

	/**
	 * Call the Model/CollectionModel onDestroy() to clear knockout functions/objects
	 * @param {Object|Array} objectOrObjects
	 * @returns {void}
	 */
	delegateRunOnDestroy = (objectOrObjects) => {
		objectOrObjects && (isArray(objectOrObjects) ? objectOrObjects : [objectOrObjects]).forEach(
			obj => obj.onDestroy && obj.onDestroy()
		);
	},

	/**
	 * @returns {function}
	 */
	computedPaginatorHelper = (koCurrentPage, koPageCount) => {
		return () => {
			const currentPage = koCurrentPage(),
				pageCount = koPageCount(),
				result = [],
				fAdd = (index, push = true, customName = '') => {
					const data = {
						current: index === currentPage,
						name: customName ? customName.toString() : index.toString(),
						custom: !!customName,
						title: customName ? index.toString() : '',
						value: index.toString()
					};

					if (push) {
						result.push(data);
					} else {
						result.unshift(data);
					}
				};

			let prev = 0,
				next = 0,
				limit = 2;

			if (1 < pageCount || (0 < pageCount && pageCount < currentPage)) {
				if (pageCount < currentPage) {
					fAdd(pageCount);
					prev = pageCount;
					next = pageCount;
				} else {
					if (3 >= currentPage || pageCount - 2 <= currentPage) {
						limit += 2;
					}

					fAdd(currentPage);
					prev = currentPage;
					next = currentPage;
				}

				while (0 < limit) {
					--prev;
					++next;

					if (0 < prev) {
						fAdd(prev, false);
						--limit;
					}

					if (pageCount >= next) {
						fAdd(next, true);
						--limit;
					} else if (0 >= prev) {
						break;
					}
				}

				if (3 === prev) {
					fAdd(2, false);
				} else if (3 < prev) {
					fAdd(Math.round((prev - 1) / 2), false, '…');
				}

				if (pageCount - 2 === next) {
					fAdd(pageCount - 1, true);
				} else if (pageCount - 2 > next) {
					fAdd(Math.round((pageCount + next) / 2), true, '…');
				}

				// first and last
				if (1 < prev) {
					fAdd(1, false);
				}

				if (pageCount > next) {
					fAdd(pageCount, true);
				}
			}

			return result;
		};
	},

	/**
	 * @param {string} mailToUrl
	 * @returns {boolean}
	 */
	mailToHelper = (mailToUrl) => {
		if (
			mailToUrl &&
			'mailto:' ===
				mailToUrl
					.toString()
					.slice(0, 7)
					.toLowerCase()
		) {
			mailToUrl = mailToUrl.toString().slice(7);

			let to = [],
				params = {};

			const email = mailToUrl.replace(/\?.+$/, ''),
				query = mailToUrl.replace(/^[^?]*\?/, ''),
				toEmailModel = value => null != value ? EmailModel.parseEmailLine(decodeURIComponent(value)) : null;

			query.split('&').forEach(temp => {
				temp = temp.split('=');
				params[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
			});

			if (null != params.to) {
				to = Object.values(
					toEmailModel(email + ',' + params.to).reduce((result, value) => {
						if (value) {
							if (result[value.email]) {
								if (!result[value.email].name) {
									result[value.email] = value;
								}
							} else {
								result[value.email] = value;
							}
						}
						return result;
					}, {})
				);
			} else {
				to = EmailModel.parseEmailLine(email);
			}

			showMessageComposer([
				ComposeType.Empty,
				null,
				to,
				toEmailModel(params.cc),
				toEmailModel(params.bcc),
				null == params.subject ? null : decodeURIComponent(params.subject),
				null == params.body ? null : plainToHtml(decodeURIComponent(params.body))
			]);

			return true;
		}

		return false;
	},

	showMessageComposer = (params = []) =>
	{
		rl.app.showMessageComposer(params);
	},

	initFullscreen = (el, fn) =>
	{
		let event = 'fullscreenchange';
		if (!el.requestFullscreen && el.webkitRequestFullscreen) {
			el.requestFullscreen = el.webkitRequestFullscreen;
			event = 'webkit'+event;
		}
		if (el.requestFullscreen) {
			el.addEventListener(event, fn);
			return el;
		}
	},

	setLayoutResizer = (source, target, sClientSideKeyName, mode) =>
	{
		if (source.layoutResizer && source.layoutResizer.mode != mode) {
			target.removeAttribute('style');
			source.removeAttribute('style');
		}
	//	source.classList.toggle('resizable', mode);
		if (mode) {
			const length = get(sClientSideKeyName+mode);
			if (!source.layoutResizer) {
				const resizer = createElement('div', {'class':'resizer'}),
					size = {},
					store = () => {
						if ('Width' == resizer.mode) {
							target.style.left = source.offsetWidth + 'px';
							set(resizer.key+resizer.mode, source.offsetWidth);
						} else {
							target.style.top = (4 + source.offsetTop + source.offsetHeight) + 'px';
							set(resizer.key+resizer.mode, source.offsetHeight);
						}
					},
					cssint = s => {
						let value = getComputedStyle(source, null)[s].replace('px', '');
						if (value.includes('%')) {
							value = source.parentElement['offset'+resizer.mode]
								* value.replace('%', '') / 100;
						}
						return parseFloat(value);
					};
				source.layoutResizer = resizer;
				source.append(resizer);
				resizer.addEventListener('mousedown', {
					handleEvent: function(e) {
						if ('mousedown' == e.type) {
							const lmode = resizer.mode.toLowerCase();
							e.preventDefault();
							size.pos = ('width' == lmode) ? e.pageX : e.pageY;
							size.min = cssint('min-'+lmode);
							size.max = cssint('max-'+lmode);
							size.org = cssint(lmode);
							addEventListener('mousemove', this);
							addEventListener('mouseup', this);
						} else if ('mousemove' == e.type) {
							const lmode = resizer.mode.toLowerCase(),
								length = size.org + (('width' == lmode ? e.pageX : e.pageY) - size.pos);
							if (length >= size.min && length <= size.max ) {
								source.style[lmode] = length + 'px';
								source.observer || store();
							}
						} else if ('mouseup' == e.type) {
							removeEventListener('mousemove', this);
							removeEventListener('mouseup', this);
						}
					}
				});
				source.observer = window.ResizeObserver ? new ResizeObserver(store) : null;
			}
			source.layoutResizer.mode = mode;
			source.layoutResizer.key = sClientSideKeyName;
			source.observer && source.observer.observe(source, { box: 'border-box' });
			if (length) {
				source.style[mode] = length + 'px';
			}
		} else {
			source.observer && source.observer.disconnect();
		}
	};

	let notificator = null,
		player = null,
		canPlay = type => player && !!player.canPlayType(type).replace('no', ''),

		audioCtx = window.AudioContext || window.webkitAudioContext,

		play = (url, name) => {
			if (player) {
				player.src = url;
				player.play();
				name = name.trim();
				fireEvent('audio.start', name.replace(/\.([a-z0-9]{3})$/, '') || 'audio');
			}
		},

		createNewObject = () => {
			try {
				const player = new Audio;
				if (player.canPlayType && player.pause && player.play) {
					player.preload = 'none';
					player.loop = false;
					player.autoplay = false;
					player.muted = false;
					return player;
				}
			} catch (e) {
				console.error(e);
			}
			return null;
		},

		// The AudioContext is not allowed to start.
		// It must be resumed (or created) after a user gesture on the page. https://goo.gl/7K7WLu
		// Setup listeners to attempt an unlock
		unlockEvents = [
			'click','dblclick',
			'contextmenu',
			'auxclick',
			'mousedown','mouseup',
			'pointerup',
			'touchstart','touchend',
			'keydown','keyup'
		],
		unlock = () => {
			if (audioCtx) {
				console.log('AudioContext ' + audioCtx.state);
				audioCtx.resume();
			}
			unlockEvents.forEach(type => doc.removeEventListener(type, unlock, true));
	//		setTimeout(()=>Audio.playNotification(1),1);
		};

	if (audioCtx) {
		audioCtx = audioCtx ? new audioCtx : null;
		audioCtx.onstatechange = unlock;
	}
	unlockEvents.forEach(type => doc.addEventListener(type, unlock, true));

	/**
	 * Browsers can't play without user interaction
	 */

	const SMAudio = new class {
		constructor() {
			player || (player = createNewObject());

			this.supported = !!player;
			this.supportedMp3 = canPlay('audio/mpeg;');
			this.supportedWav = canPlay('audio/wav; codecs="1"');
			this.supportedOgg = canPlay('audio/ogg; codecs="vorbis"');
			if (player) {
				const stopFn = () => this.pause();
				player.addEventListener('ended', stopFn);
				player.addEventListener('error', stopFn);
				addEventListener('audio.api.stop', stopFn);
			}
		}

		paused() {
			return !player || player.paused;
		}

		stop() {
			this.pause();
		}

		pause() {
			player && player.pause();
			fireEvent('audio.stop');
		}

		playMp3(url, name) {
			this.supportedMp3 && play(url, name);
		}

		playOgg(url, name) {
			this.supportedOgg && play(url, name);
		}

		playWav(url, name) {
			this.supportedWav && play(url, name);
		}

		playNotification(silent) {
			if ('running' == audioCtx.state && (this.supportedMp3 || this.supportedOgg)) {
				notificator = notificator || createNewObject();
				if (notificator) {
					notificator.src = staticLink('sounds/'
						+ SettingsGet('NotificationSound')
						+ (this.supportedMp3 ? '.mp3' : '.ogg'));
					notificator.volume = silent ? 0.01 : 1;
					notificator.play();
				}
			} else {
				console.log('No audio: ' + audioCtx.state);
			}
		}
	};

	/**
	 * Might not work due to the new ServiceWorkerRegistration.showNotification
	 */
	const HTML5Notification = window.Notification,
		HTML5NotificationStatus = () => (HTML5Notification && HTML5Notification.permission) || 'denied',
		NotificationsDenied = () => 'denied' === HTML5NotificationStatus(),
		NotificationsGranted = () => 'granted' === HTML5NotificationStatus(),
		dispatchMessage = data => {
			focus();
			if (data.Folder && data.Uid) {
				fireEvent('mailbox.message.show', data);
			} else if (data.Url) {
				rl.route.setHash(data.Url);
			}
		};

	let DesktopNotifications = false,
		WorkerNotifications = navigator.serviceWorker;

	// Are Notifications supported in the service worker?
	if (WorkerNotifications && ServiceWorkerRegistration && ServiceWorkerRegistration.prototype.showNotification) {
		/* Listen for close requests from the ServiceWorker */
		WorkerNotifications.addEventListener('message', event => {
			const obj = JSON.parse(event.data);
			obj && 'notificationclick' === obj.action && dispatchMessage(obj.data);
		});
	} else {
		WorkerNotifications = null;
		console.log('ServiceWorker Notifications not supported');
	}

	const NotificationUserStore = new class {
		constructor() {
			addObservablesTo(this, {
				enableSoundNotification: false,

				enableDesktopNotification: false,/*.extend({ notify: 'always' })*/

				isDesktopNotificationAllowed: !NotificationsDenied()
			});

			this.enableDesktopNotification.subscribe(value => {
				DesktopNotifications = !!value;
				if (value && HTML5Notification && !NotificationsGranted()) {
					HTML5Notification.requestPermission(() =>
						this.isDesktopNotificationAllowed(!NotificationsDenied())
					);
				}
			});
		}

		/**
		 * Used with SoundNotification setting
		 */
		playSoundNotification(skipSetting) {
			if (skipSetting ? true : this.enableSoundNotification()) {
				SMAudio.playNotification();
			}
		}

		/**
		 * Used with DesktopNotifications setting
		 */
		displayDesktopNotification(title, text, messageData, imageSrc) {
			if (DesktopNotifications && NotificationsGranted()) {
				const options = {
					body: text,
					icon: imageSrc || staticLink('css/images/icon-message-notification.png'),
					data: messageData
				};
				if (messageData && messageData.Uid) {
					options.tag = messageData.Uid;
				}
				if (WorkerNotifications) {
					// Service-Worker-Allowed HTTP header to allow the scope.
					WorkerNotifications.register('/serviceworker.js')
	//				WorkerNotifications.register(Links.staticLink('js/serviceworker.js'), {scope:'/'})
					.then(() =>
						WorkerNotifications.ready.then(registration =>
							/* Show the notification */
							registration
								.showNotification(title, options)
								.then(() =>
									registration.getNotifications().then((/*notifications*/) => {
										/* Send an empty message so the Worker knows who the client is */
										registration.active.postMessage('');
									})
								)
						)
					)
					.catch(e => console.error(e));
				} else {
					const notification = new HTML5Notification(title, options);
					notification.show && notification.show();
					notification.onclick = messageData ? () => dispatchMessage(messageData) : null;
					setTimeout(() => notification.close(), 7000);
				}
			}
		}
	};

	const AccountUserStore = {
		accounts: ko.observableArray(),
		loading: ko.observable(false).extend({ debounce: 100 }),

		getEmailAddresses: () => AccountUserStore.accounts.map(item => item.email)
	};

	addObservablesTo(AccountUserStore, {
		email: '',
		signature: ''
	});

	const AppUserStore = {
		allowContacts: () => !!SettingsGet('ContactsIsAllowed')
	};

	addObservablesTo(AppUserStore, {
		focusedState: Scope.None,

		threadsAllowed: false,

		composeInEdit: false
	});

	AppUserStore.focusedState.subscribe(value => {
		switch (value) {
			case Scope.MessageList:
			case Scope.MessageView:
			case Scope.FolderList:
				keyScope(value);
				ThemeStore.isMobile() && leftPanelDisabled(Scope.FolderList !== value);
				break;
		}
		['FolderList','MessageList','MessageView'].forEach(name => {
			let dom = elementById('V-Mail'+name);
			dom && dom.classList.toggle('focused', name === value);
		});
	});

	let iJsonErrorCount = 0;

	const getURL = (add = '') => serverRequest('Json') + add,

	checkResponseError = data => {
		const err = data ? data.ErrorCode : null;
		if (Notification.InvalidToken === err) {
			alert(getNotification(err));
			rl.logoutReload();
		} else if ([
				Notification.AuthError,
				Notification.ConnectionError,
				Notification.DomainNotAllowed,
				Notification.AccountNotAllowed,
				Notification.MailServerError,
				Notification.UnknownNotification,
				Notification.UnknownError
			].includes(err)
		) {
			if (7 < ++iJsonErrorCount) {
				rl.logoutReload();
			}
		}
	},

	oRequests = {},

	abort = (sAction, bClearOnly) => {
		if (oRequests[sAction]) {
			if (!bClearOnly && oRequests[sAction].abort) {
	//			oRequests[sAction].__aborted = true;
				oRequests[sAction].abort();
			}

			oRequests[sAction] = null;
			delete oRequests[sAction];
		}
	},

	fetchJSON = (action, sGetAdd, params, timeout, jsonCallback) => {
		sGetAdd = pString(sGetAdd);
		params = params || {};
		if (params instanceof FormData) {
			params.set('Action', action);
		} else {
			params.Action = action;
		}
		let init = {};
		if (window.AbortController) {
			abort(action);
			const controller = new AbortController();
			timeout && setTimeout(() => controller.abort(), timeout);
			oRequests[action] = controller;
			init.signal = controller.signal;
		}
		return rl.fetchJSON(getURL(sGetAdd), init, sGetAdd ? null : params).then(jsonCallback);
	};

	class FetchError extends Error
	{
		constructor(code, message) {
			super(message);
			this.code = code || Notification.JsonFalse;
		}
	}

	class AbstractFetchRemote
	{
		abort(sAction, bClearOnly) {
			abort(sAction, bClearOnly);
			return this;
		}

		/**
		 * Allows quicker visual responses to the user.
		 * Can be used to stream lines of json encoded data, but does not work on all servers.
		 * Apache needs 'flushpackets' like in <Proxy "fcgi://...." flushpackets=on></Proxy>
		 */
		streamPerLine(fCallback, sGetAdd) {
			rl.fetch(getURL(sGetAdd))
			.then(response => response.body)
			.then(body => {
				// Firefox TextDecoderStream is not defined
			//	const reader = body.pipeThrough(new TextDecoderStream()).getReader();
				const reader = body.getReader(),
					re = /\r\n|\n|\r/gm,
					utf8decoder = new TextDecoder();
				let buffer = '';
				function processText({ done, value }) {
					buffer += value ? utf8decoder.decode(value, {stream: true}) : '';
					for (;;) {
						let result = re.exec(buffer);
						if (!result) {
							if (done) {
								break;
							}
							reader.read().then(processText);
							return;
						}
						fCallback(buffer.slice(0, result.index));
						buffer = buffer.slice(result.index + 1);
						re.lastIndex = 0;
					}
					if (buffer.length) {
						// last line didn't end in a newline char
						fCallback(buffer);
					}
				}
				reader.read().then(processText);
			});
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sAction
		 * @param {Object=} oParameters
		 * @param {?number=} iTimeout
		 * @param {string=} sGetAdd = ''
		 * @param {Array=} aAbortActions = []
		 */
		request(sAction, fCallback, params, iTimeout, sGetAdd, abortActions) {
			params = params || {};

			const start = Date.now();

			if (sAction && abortActions) {
				abortActions.forEach(actionToAbort => abort(actionToAbort));
			}

			fetchJSON(sAction, sGetAdd,
				params,
				undefined === iTimeout ? 30000 : pInt(iTimeout),
				data => {
					let cached = false;
					if (data && data.Time) {
						cached = pInt(data.Time) > Date.now() - start;
					}

					let iError = 0;
					if (sAction && oRequests[sAction]) {
						if (oRequests[sAction].__aborted) {
							iError = 2;
						}
						abort(sAction, true);
					}

					if (!iError && data) {
	/*
						if (sAction !== data.Action) {
							console.log(sAction + ' !== ' + data.Action);
						}
	*/
						if (data.Result) {
							iJsonErrorCount = 0;
						} else {
							checkResponseError(data);
							iError = data.ErrorCode || Notification.UnknownError;
						}
					}

					fCallback && fCallback(
						iError,
						data,
						cached,
						sAction,
						params
					);
				}
			)
			.catch(err => {
				console.error(err);
				fCallback && fCallback(err.name == 'AbortError' ? 2 : 1);
			});
		}

		/**
		 * @param {?Function} fCallback
		 */
		getPublicKey(fCallback) {
			this.request('GetPublicKey', fCallback);
		}

		setTrigger(trigger, value) {
			if (trigger) {
				value = !!value;
				(isArray(trigger) ? trigger : [trigger]).forEach(fTrigger => {
					fTrigger && fTrigger(value);
				});
			}
		}

		post(action, fTrigger, params, timeOut) {
			this.setTrigger(fTrigger, true);
			return fetchJSON(action, '', params, pInt(timeOut, 30000),
				data => {
					abort(action, true);

					if (!data) {
						return Promise.reject(new FetchError(Notification.JsonParse));
					}
	/*
					let isCached = false, type = '';
					if (data && data.Time) {
						isCached = pInt(data.Time) > microtime() - start;
					}
					// backward capability
					switch (true) {
						case 'success' === textStatus && data && data.Result && action === data.Action:
							type = AbstractFetchRemote.SUCCESS;
							break;
						case 'abort' === textStatus && (!data || !data.__aborted__):
							type = AbstractFetchRemote.ABORT;
							break;
						default:
							type = AbstractFetchRemote.ERROR;
							break;
					}
	*/
					this.setTrigger(fTrigger, false);

					if (!data.Result || action !== data.Action) {
						checkResponseError(data);
						return Promise.reject(new FetchError(
							data ? data.ErrorCode : 0,
							data ? (data.ErrorMessageAdditional || data.ErrorMessage) : ''
						));
					}

					return data;
				}
			);
		}
	}

	Object.assign(AbstractFetchRemote.prototype, {
		SUCCESS : 0,
		ERROR : 1,
		ABORT : 2
	});

	class AbstractCollectionModel extends Array
	{
		constructor() {
	/*
			if (new.target === AbstractCollectionModel) {
				throw new Error("Can't instantiate AbstractCollectionModel!");
			}
	*/
			super();
		}

		onDestroy() {
			this.forEach(item => item.onDestroy && item.onDestroy());
		}

		/**
		 * @static
		 * @param {FetchJson} json
		 * @returns {*CollectionModel}
		 */
		static reviveFromJson(json, itemCallback) {
			const result = new this();
			if (json) {
				if ('Collection/'+this.name.replace('Model', '') === json['@Object']) {
					forEachObjectEntry(json, (key, value) => '@' !== key[0] && (result[key] = value));
					json = json['@Collection'];
				}
				if (isArray(json)) {
					json.forEach(item => {
						item && itemCallback && (item = itemCallback(item, result));
						item && result.push(item);
					});
				}
			}
			return result;
		}

	}

	class EmailCollectionModel extends AbstractCollectionModel
	{
		/**
		 * @param {?Array} json
		 * @returns {EmailCollectionModel}
		 */
		static reviveFromJson(items) {
			return super.reviveFromJson(items, email => EmailModel.reviveFromJson(email));
		}

		/**
		 * @param {boolean=} friendlyView = false
		 * @param {boolean=} wrapWithLink = false
		 * @returns {string}
		 */
		toString(friendlyView = false, wrapWithLink = false) {
			const result = [];
			this.forEach(email => result.push(email.toLine(friendlyView, wrapWithLink)));
			return result.join(', ');
		}

		/**
		 * @returns {string}
		 */
		toStringClear() {
			const result = [];
			this.forEach(email => {
				if (email && email.email && email.name) {
					result.push(email.email);
				}
			});
			return result.join(', ');
		}
	}

	/* eslint key-spacing: 0 */

	const
		cache = {},
		app = 'application/',
		msOffice = app+'vnd.openxmlformats-officedocument.',
		openDoc = app+'vnd.oasis.opendocument.',
		sizes = ['B', 'KiB', 'MiB', 'GiB', 'TiB'],
		lowerCase = text => text.toLowerCase().trim(),

		exts = {
			eml: 'message/rfc822',
			mime: 'message/rfc822',
			vcard: 'text/vcard',
			vcf: 'text/vcard',
			htm: 'text/html',
			html: 'text/html',
			csv: 'text/csv',
			ics: 'text/calendar',
			xml: 'text/xml',
			json: app+'json',
			asc: app+'pgp-signature',
			p10: app+'pkcs10',
			p7c: app+'pkcs7-mime',
			p7m: app+'pkcs7-mime',
			p7s: app+'pkcs7-signature',
			torrent: app+'x-bittorrent',

			// scripts
			js: app+'javascript',
			pl: 'text/perl',
			css: 'text/css',
			asp: 'text/asp',
			php: app+'x-php',

			// images
			jpg: 'image/jpeg',
			ico: 'image/x-icon',
			tif: 'image/tiff',
			svg: 'image/svg+xml',
			svgz: 'image/svg+xml',

			// archives
			zip: app+'zip',
			'7z': app+'x-7z-compressed',
			rar: app+'x-rar-compressed',
			cab: app+'vnd.ms-cab-compressed',
			gz: app+'x-gzip',
			tgz: app+'x-gzip',
			bz: app+'x-bzip',
			bz2: app+'x-bzip2',
			deb: app+'x-debian-package',

			// audio
			mp3: 'audio/mpeg',
			wav: 'audio/x-wav',
			mp4a: 'audio/mp4',
			weba: 'audio/webm',
			m3u: 'audio/x-mpegurl',

			// video
			qt: 'video/quicktime',
			mov: 'video/quicktime',
			wmv: 'video/windows-media',
			avi: 'video/x-msvideo',
			'3gp': 'video/3gpp',
			'3g2': 'video/3gpp2',
			mp4v: 'video/mp4',
			mpg4: 'video/mp4',
			ogv: 'video/ogg',
			m4v: 'video/x-m4v',
			asf: 'video/x-ms-asf',
			asx: 'video/x-ms-asf',
			wm: 'video/x-ms-wm',
			wmx: 'video/x-ms-wmx',
			wvx: 'video/x-ms-wvx',
			movie: 'video/x-sgi-movie',

			// adobe
			pdf: app+'pdf',
			psd: 'image/vnd.adobe.photoshop',
			ai: app+'postscript',
			eps: app+'postscript',
			ps: app+'postscript',

			// ms office
			doc: app+'msword',
			rtf: app+'rtf',
			xls: app+'vnd.ms-excel',
			ppt: app+'vnd.ms-powerpoint',
			docx: msOffice+'wordprocessingml.document',
			xlsx: msOffice+'spreadsheetml.sheet',
			dotx: msOffice+'wordprocessingml.template',
			pptx: msOffice+'presentationml.presentation',

			// open office
			odt: openDoc+'text',
			ods: openDoc+'spreadsheet',
			odp: openDoc+'presentation'
		};

	const FileType = {
		Unknown: 'unknown',
		Text: 'text',
		Code: 'code',
		Eml: 'eml',
		Word: 'word',
		Pdf: 'pdf',
		Image: 'image',
		Audio: 'audio',
		Video: 'video',
		Spreadsheet: 'spreadsheet',
		Presentation: 'presentation',
		Certificate: 'certificate',
		Archive: 'archive'
	};

	const FileInfo = {
		/**
		 * @param {string} fileName
		 * @returns {string}
		 */
		getExtension: fileName => {
			fileName = lowerCase(fileName);
			const result = fileName.split('.').pop();
			return result === fileName ? '' : result;
		},

		getContentType: fileName => {
			fileName = lowerCase(fileName);
			if ('winmail.dat' === fileName) {
				return app + 'ms-tnef';
			}
			let ext = fileName.split('.').pop();
			if (/^(txt|text|def|list|in|ini|log|sql|cfg|conf)$/.test(ext))
				return 'text/plain';
			if (/^(mpe?g|mpe|m1v|m2v)$/.test(ext))
				return 'video/mpeg';
			if (/^aif[cf]?$/.test(ext))
				return 'audio/aiff';
			if (/^(aac|flac|midi|ogg)$/.test(ext))
				return 'audio/'+ext;
			if (/^(h26[134]|jpgv|mp4|webm)$/.test(ext))
				return 'video/'+ext;
			if (/^(otf|sfnt|ttf|woff2?)$/.test(ext))
				return 'font/'+ext;
			if (/^(png|jpeg|gif|tiff|webp)$/.test(ext))
				return 'image/'+ext;

			return exts[ext] || app+'octet-stream';
		},

		/**
		 * @param {string} sExt
		 * @param {string} sMimeType
		 * @returns {string}
		 */
		getType: (ext, mimeType) => {
			ext = lowerCase(ext);
			mimeType = lowerCase(mimeType).replace('csv/plain', 'text/csv');

			let key = ext + mimeType;
			if (cache[key]) {
				return cache[key];
			}

			let result = FileType.Unknown;
			const mimeTypeParts = mimeType.split('/'),
				type = mimeTypeParts[1].replace('x-','').replace('-compressed',''),
				match = str => mimeType.includes(str),
				archive = /^(zip|7z|tar|rar|gzip|bzip|bzip2)$/;

			switch (true) {
				case 'image' == mimeTypeParts[0] || ['png', 'jpg', 'jpeg', 'gif', 'webp'].includes(ext):
					result = FileType.Image;
					break;
				case 'audio' == mimeTypeParts[0] || ['mp3', 'ogg', 'oga', 'wav'].includes(ext):
					result = FileType.Audio;
					break;
				case 'video' == mimeTypeParts[0] || 'mkv' == ext || 'avi' == ext:
					result = FileType.Video;
					break;
				case ['php', 'js', 'css', 'xml', 'html'].includes(ext) || 'text/html' == mimeType:
					result = FileType.Code;
					break;
				case 'eml' == ext || ['message/delivery-status', 'message/rfc822'].includes(mimeType):
					result = FileType.Eml;
					break;
				case 'text' == mimeTypeParts[0] || 'txt' == ext || 'log' == ext:
					result = FileType.Text;
					break;
				case archive.test(type) || archive.test(ext):
					result = FileType.Archive;
					break;
				case 'pdf' == type || 'pdf' == ext:
					result = FileType.Pdf;
					break;
				case [app+'pgp-signature', app+'pgp-keys'].includes(mimeType)
					|| ['asc', 'pem', 'ppk'].includes(ext)
					|| [app+'pkcs7-signature'].includes(mimeType) || 'p7s' == ext:
					result = FileType.Certificate;
					break;
				case match(msOffice+'.wordprocessingml') || match(openDoc+'.text') || match('vnd.ms-word')
					|| ['rtf', 'msword', 'vnd.msword'].includes(type):
					result = FileType.Word;
					break;
				case match(msOffice+'.spreadsheetml') || match(openDoc+'.spreadsheet') || match('ms-excel'):
					result = FileType.Spreadsheet;
					break;
				case match(msOffice+'.presentationml') || match(openDoc+'.presentation') || match('ms-powerpoint'):
					result = FileType.Presentation;
					break;
				// no default
			}

			return cache[key] = result;
		},

		/**
		 * @param {string} sFileType
		 * @returns {string}
		 */
		getTypeIconClass: fileType => {
			let result = 'icon-file';
			switch (fileType) {
				case FileType.Text:
				case FileType.Eml:
				case FileType.Pdf:
				case FileType.Word:
					return result + '-text';
				case FileType.Code:
				case FileType.Image:
				case FileType.Audio:
				case FileType.Video:
				case FileType.Archive:
				case FileType.Certificate:
				case FileType.Spreadsheet:
				case FileType.Presentation:
					return result + '-' + fileType;
			}
			return result;
		},

		getIconClass: (ext, mime) => FileInfo.getTypeIconClass(FileInfo.getType(ext, mime)),

		/**
		 * @param {string} sFileType
		 * @returns {string}
		 */
		getAttachmentsIconClass: data => {
			if (arrayLength(data)) {
				let icons = data
					.map(item => item ? FileInfo.getIconClass(FileInfo.getExtension(item.fileName), item.mimeType) : '')
					.validUnique();

				return (icons && 1 === icons.length && 'icon-file' !== icons[0])
					 ? icons[0]
					 : 'icon-attachment';
			}

			return '';
		},

		friendlySize: bytes => {
			bytes = parseInt(bytes, 10) || 0;
			let i = Math.floor(Math.log(bytes) / Math.log(1024));
			return (bytes / Math.pow(1024, i)).toFixed(2>i ? 0 : 1) + ' ' + sizes[i];
		}

	};

	class AttachmentModel extends AbstractModel {
		constructor() {
			super();

			this.checked = ko.observable(false);

			this.mimeType = '';
			this.fileName = '';
			this.fileNameExt = '';
			this.fileType = FileType.Unknown;
			this.friendlySize = '';
			this.isThumbnail = false;
			this.cid = '';
			this.contentLocation = '';
			this.download = '';
			this.folder = '';
			this.uid = '';
			this.url = '';
			this.mimeIndex = '';
			this.framed = false;

			this.addObservables({
				isInline: false,
				isLinked: false
			});
		}

		/**
		 * @static
		 * @param {FetchJsonAttachment} json
		 * @returns {?AttachmentModel}
		 */
		static reviveFromJson(json) {
			const attachment = super.reviveFromJson(json);
			if (attachment) {
				attachment.friendlySize = FileInfo.friendlySize(json.EstimatedSize);

				attachment.fileNameExt = FileInfo.getExtension(attachment.fileName);
				attachment.fileType = FileInfo.getType(attachment.fileNameExt, attachment.mimeType);
			}
			return attachment;
		}

		contentId() {
			return this.cid.replace(/^<+|>+$/g, '');
		}

		/**
		 * @returns {boolean}
		 */
		isImage() {
			return FileType.Image === this.fileType;
		}

		/**
		 * @returns {boolean}
		 */
		isMp3() {
			return FileType.Audio === this.fileType && 'mp3' === this.fileNameExt;
		}

		/**
		 * @returns {boolean}
		 */
		isOgg() {
			return FileType.Audio === this.fileType && ('oga' === this.fileNameExt || 'ogg' === this.fileNameExt);
		}

		/**
		 * @returns {boolean}
		 */
		isWav() {
			return FileType.Audio === this.fileType && 'wav' === this.fileNameExt;
		}

		/**
		 * @returns {boolean}
		 */
		hasThumbnail() {
			return this.isThumbnail;
		}

		/**
		 * @returns {boolean}
		 */
		isText() {
			return FileType.Text === this.fileType || FileType.Eml === this.fileType;
		}

		/**
		 * @returns {boolean}
		 */
		pdfPreview() {
			return null != navigator.mimeTypes['application/pdf'] && FileType.Pdf === this.fileType;
		}

		/**
		 * @returns {boolean}
		 */
		hasPreview() {
			return this.isImage() || this.pdfPreview() || this.isText();
		}

		/**
		 * @returns {boolean}
		 */
		hasPreplay() {
			return (
				(SMAudio.supportedMp3 && this.isMp3()) ||
				(SMAudio.supportedOgg && this.isOgg()) ||
				(SMAudio.supportedWav && this.isWav())
			);
		}

		/**
		 * @returns {string}
		 */
		linkDownload() {
			return this.url || attachmentDownload(this.download);
		}

		/**
		 * @returns {string}
		 */
		linkPreview() {
			return this.url || serverRequestRaw('View', this.download);
		}

		/**
		 * @returns {string}
		 */
		linkThumbnailPreviewStyle() {
			return this.hasThumbnail() ? 'background:url(' + serverRequestRaw('ViewThumbnail', this.download) + ')' : '';
		}

		/**
		 * @returns {string}
		 */
		linkPreviewMain() {
			let result = '';
			switch (true) {
				case this.isImage():
				case this.pdfPreview():
					result = this.linkPreview();
					break;
				case this.isText():
					result = serverRequestRaw('ViewAsPlain', this.download);
					break;
				// no default
			}

			return result;
		}

		/**
		 * @param {AttachmentModel} attachment
		 * @param {*} event
		 * @returns {boolean}
		 */
		eventDragStart(attachment, event) {
			const localEvent = event.originalEvent || event;
			if (attachment && localEvent && localEvent.dataTransfer && localEvent.dataTransfer.setData) {
				let link = this.linkDownload();
				if ('http' !== link.slice(0, 4)) {
					link = location.protocol + '//' + location.host + location.pathname + link;
				}
				localEvent.dataTransfer.setData('DownloadURL', this.mimeType + ':' + this.fileName + ':' + link);
			}

			return true;
		}

		/**
		 * @returns {string}
		 */
		iconClass() {
			return FileInfo.getTypeIconClass(this.fileType);
		}
	}

	class AttachmentCollectionModel extends AbstractCollectionModel
	{
		/**
		 * @param {?Array} json
		 * @returns {AttachmentCollectionModel}
		 */
		static reviveFromJson(items) {
			return super.reviveFromJson(items, attachment => AttachmentModel.reviveFromJson(attachment));
	/*
			const attachments = super.reviveFromJson(items, attachment => AttachmentModel.reviveFromJson(attachment));
			if (attachments) {
				attachments.InlineCount = attachments.reduce((accumulator, a) => accumulator + (a.isInline ? 1 : 0), 0);
			}
			return attachments;
	*/
		}

		/**
		 * @returns {boolean}
		 */
		hasVisible() {
			return !!this.filter(item => !item.isLinked()).length;
		}

		/**
		 * @param {string} cid
		 * @returns {*}
		 */
		findByCid(cid) {
			cid = cid.replace(/^<+|>+$/g, '');
			return this.find(item => cid === item.contentId());
		}
	}

	var PreviewHTML = "<html>\n<head>\n\t<meta charset=\"utf-8\">\n\t<title></title>\n\t<style>\nhtml, body {\n\tmargin: 0;\n\tpadding: 0;\n}\n\nheader {\n\tbackground: rgba(125,128,128,0.3);\n\tborder-bottom: 1px solid #888;\n}\n\nheader h1 {\n\tfont-size: 120%;\n}\n\nheader * {\n\tmargin: 5px 0;\n}\n\nheader time {\n\tfloat: right;\n}\n\nblockquote {\n\tborder-left: 2px solid rgba(125,128,128,0.5);\n\tmargin: 0;\n\tpadding: 0 0 0 10px;\n}\n\npre {\n\twhite-space: pre-wrap;\n\tword-wrap: break-word;\n\tword-break: normal;\n}\n\nbody > * {\n\tpadding: 0.5em 1em;\n}\n\t</style>\n</head>\n<body></body>\n</html>\n";

	const
		// eslint-disable-next-line max-len
		url = /(^|[\s\n]|\/?>)(https:\/\/[-A-Z0-9+\u0026\u2019#/%?=()~_|!:,.;]*[-A-Z0-9+\u0026#/%=~()_|])/gi,
		// eslint-disable-next-line max-len
		email$1 = /(^|[\s\n]|\/?>)((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x21\x23-\x5b\x5d-\x7f]|\\[\x21\x23-\x5b\x5d-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x21-\x5a\x53-\x7f]|\\[\x21\x23-\x5b\x5d-\x7f])+)\]))/gi,

		hcont = Element.fromHTML('<div area="hidden" style="position:absolute;left:-5000px"></div>'),
		getRealHeight = el => {
			hcont.innerHTML = el.outerHTML;
			const result = hcont.clientHeight;
			hcont.innerHTML = '';
			return result;
		},

		replyHelper = (emails, unic, localEmails) => {
			emails.forEach(email => {
				if (undefined === unic[email.email]) {
					unic[email.email] = true;
					localEmails.push(email);
				}
			});
		};

	doc.body.append(hcont);

	class MessageModel extends AbstractModel {
		constructor() {
			super();

			this._reset();

			this.addObservables({
				subject: '',
				plain: '',
				html: '',
				size: 0,
				spamScore: 0,
				spamResult: '',
				isSpam: false,
				hasVirus: null, // or boolean when scanned
				dateTimeStampInUTC: 0,
				priority: MessagePriority.Normal,

				senderEmailsString: '',
				senderClearEmailsString: '',

				deleted: false,

				focused: false,
				selected: false,
				checked: false,

				isHtml: false,
				hasImages: false,
				hasExternals: false,

				pgpSigned: null,
				pgpEncrypted: null,
				isPgpEncrypted: false,
				pgpVerified: null,

				readReceipt: '',

				hasUnseenSubMessage: false,
				hasFlaggedSubMessage: false
			});

			this.attachments = ko.observableArray(new AttachmentCollectionModel);
			this.threads = ko.observableArray();
			this.unsubsribeLinks = ko.observableArray();
			this.flags = ko.observableArray();

			this.addComputables({
				attachmentIconClass: () => FileInfo.getAttachmentsIconClass(this.attachments()),
				threadsLen: () => this.threads().length,
				isImportant: () => MessagePriority.High === this.priority(),
				hasAttachments: () => this.attachments().hasVisible(),

				isDeleted: () => this.flags().includes('\\deleted'),
				isUnseen: () => !this.flags().includes('\\seen') /* || this.flags().includes('\\unseen')*/,
				isFlagged: () => this.flags().includes('\\flagged'),
				isAnswered: () => this.flags().includes('\\answered'),
				isForwarded: () => this.flags().includes('$forwarded'),
				isReadReceipt: () => this.flags().includes('$mdnsent')
	//			isJunk: () => this.flags().includes('$junk') && !this.flags().includes('$nonjunk'),
	//			isPhishing: () => this.flags().includes('$phishing')
			});
		}

		_reset() {
			this.folder = '';
			this.uid = 0;
			this.hash = '';
			this.requestHash = '';
			this.emails = [];
			this.from = new EmailCollectionModel;
			this.to = new EmailCollectionModel;
			this.cc = new EmailCollectionModel;
			this.bcc = new EmailCollectionModel;
			this.replyTo = new EmailCollectionModel;
			this.deliveredTo = new EmailCollectionModel;
			this.body = null;
			this.draftInfo = [];
			this.messageId = '';
			this.inReplyTo = '';
			this.references = '';
		}

		clear() {
			this._reset();
			this.subject('');
			this.html('');
			this.plain('');
			this.size(0);
			this.spamScore(0);
			this.spamResult('');
			this.isSpam(false);
			this.hasVirus(null);
			this.dateTimeStampInUTC(0);
			this.priority(MessagePriority.Normal);

			this.senderEmailsString('');
			this.senderClearEmailsString('');

			this.deleted(false);

			this.selected(false);
			this.checked(false);

			this.isHtml(false);
			this.hasImages(false);
			this.hasExternals(false);
			this.attachments(new AttachmentCollectionModel);

			this.pgpSigned(null);
			this.pgpEncrypted(null);
			this.isPgpEncrypted(false);
			this.pgpVerified(null);

			this.priority(MessagePriority.Normal);
			this.readReceipt('');

			this.threads([]);
			this.unsubsribeLinks([]);

			this.hasUnseenSubMessage(false);
			this.hasFlaggedSubMessage(false);
		}

		spamStatus() {
			let spam = this.spamResult();
			return spam ? i18n(this.isSpam() ? 'GLOBAL/SPAM' : 'GLOBAL/NOT_SPAM') + ': ' + spam : '';
		}

		/**
		 * @param {Array} properties
		 * @returns {Array}
		 */
		getEmails(properties) {
			return properties.reduce((carry, property) => carry.concat(this[property]), []).map(
				oItem => oItem ? oItem.email : ''
			).validUnique();
		}

		/**
		 * @returns {string}
		 */
		friendlySize() {
			return FileInfo.friendlySize(this.size());
		}

		computeSenderEmail() {
			const list = [FolderUserStore.sentFolder(), FolderUserStore.draftsFolder()].includes(this.folder) ? 'to' : 'from';
			this.senderEmailsString(this[list].toString(true));
			this.senderClearEmailsString(this[list].toStringClear());
		}

		/**
		 * @param {FetchJsonMessage} json
		 * @returns {boolean}
		 */
		revivePropertiesFromJson(json) {
			if ('Priority' in json && ![MessagePriority.High, MessagePriority.Low].includes(json.Priority)) {
				json.Priority = MessagePriority.Normal;
			}
			if (super.revivePropertiesFromJson(json)) {
	//			this.foundCIDs = isArray(json.FoundCIDs) ? json.FoundCIDs : [];
	//			this.attachments(AttachmentCollectionModel.reviveFromJson(json.Attachments, this.foundCIDs));

				this.computeSenderEmail();
			}
		}

		/**
		 * @returns {boolean}
		 */
		hasUnsubsribeLinks() {
			return this.unsubsribeLinks().length;
		}

		/**
		 * @returns {string}
		 */
		getFirstUnsubsribeLink() {
			return this.unsubsribeLinks()[0] || '';
		}

		/**
		 * @param {boolean} friendlyView
		 * @param {boolean=} wrapWithLink
		 * @returns {string}
		 */
		fromToLine(friendlyView, wrapWithLink) {
			return this.from.toString(friendlyView, wrapWithLink);
		}

		/**
		 * @returns {string}
		 */
		fromDkimData() {
			let result = ['none', ''];
			if (1 === arrayLength(this.from) && this.from[0] && this.from[0].dkimStatus) {
				result = [this.from[0].dkimStatus, this.from[0].dkimValue || ''];
			}

			return result;
		}

		/**
		 * @param {boolean} friendlyView
		 * @param {boolean=} wrapWithLink
		 * @returns {string}
		 */
		toToLine(friendlyView, wrapWithLink) {
			return this.to.toString(friendlyView, wrapWithLink);
		}

		/**
		 * @param {boolean} friendlyView
		 * @param {boolean=} wrapWithLink
		 * @returns {string}
		 */
		ccToLine(friendlyView, wrapWithLink) {
			return this.cc.toString(friendlyView, wrapWithLink);
		}

		/**
		 * @param {boolean} friendlyView
		 * @param {boolean=} wrapWithLink
		 * @returns {string}
		 */
		bccToLine(friendlyView, wrapWithLink) {
			return this.bcc.toString(friendlyView, wrapWithLink);
		}

		/**
		 * @param {boolean} friendlyView
		 * @param {boolean=} wrapWithLink
		 * @returns {string}
		 */
		replyToToLine(friendlyView, wrapWithLink) {
			return this.replyTo.toString(friendlyView, wrapWithLink);
		}

		/**
		 * @return string
		 */
		lineAsCss() {
			let classes = [];
			forEachObjectEntry({
				deleted: this.deleted(),
				'deleted-mark': this.isDeleted(),
				selected: this.selected(),
				checked: this.checked(),
				flagged: this.isFlagged(),
				unseen: this.isUnseen(),
				answered: this.isAnswered(),
				forwarded: this.isForwarded(),
				focused: this.focused(),
				important: this.isImportant(),
				withAttachments: !!this.attachments().length,
				emptySubject: !this.subject(),
				// hasChildrenMessage: 1 < this.threadsLen(),
				hasUnseenSubMessage: this.hasUnseenSubMessage(),
				hasFlaggedSubMessage: this.hasFlaggedSubMessage()
			}, (key, value) => value && classes.push(key));
			return classes.join(' ');
		}

		/**
		 * @returns {string}
		 */
		fromAsSingleEmail() {
			return isArray(this.from) && this.from[0] ? this.from[0].email : '';
		}

		/**
		 * @returns {string}
		 */
		viewLink() {
			return serverRequestRaw('ViewAsPlain', this.requestHash);
		}

		/**
		 * @returns {string}
		 */
		downloadLink() {
			return serverRequestRaw('Download', this.requestHash);
		}

		/**
		 * @param {Object} excludeEmails
		 * @param {boolean=} last = false
		 * @returns {Array}
		 */
		replyEmails(excludeEmails, last) {
			const result = [],
				unic = undefined === excludeEmails ? {} : excludeEmails;

			replyHelper(this.replyTo, unic, result);
			if (!result.length) {
				replyHelper(this.from, unic, result);
			}

			if (!result.length && !last) {
				return this.replyEmails({}, true);
			}

			return result;
		}

		/**
		 * @param {Object} excludeEmails
		 * @param {boolean=} last = false
		 * @returns {Array.<Array>}
		 */
		replyAllEmails(excludeEmails, last) {
			let data = [];
			const toResult = [],
				ccResult = [],
				unic = undefined === excludeEmails ? {} : excludeEmails;

			replyHelper(this.replyTo, unic, toResult);
			if (!toResult.length) {
				replyHelper(this.from, unic, toResult);
			}

			replyHelper(this.to, unic, toResult);
			replyHelper(this.cc, unic, ccResult);

			if (!toResult.length && !last) {
				data = this.replyAllEmails({}, true);
				return [data[0], ccResult];
			}

			return [toResult, ccResult];
		}

		viewHtml() {
			const body = this.body;
			if (body && this.html()) {
				const contentLocationUrls = {},
					oAttachments = this.attachments();

				// Get contentLocationUrls
				oAttachments.forEach(oAttachment => {
					if (oAttachment.cid && oAttachment.contentLocation) {
						contentLocationUrls[oAttachment.contentId()] = oAttachment.contentLocation;
					}
				});

				let result = cleanHtml(this.html(), contentLocationUrls, SettingsUserStore.removeColors());
				this.hasExternals(result.hasExternals);
	//			this.hasInternals = result.foundCIDs.length || result.foundContentLocationUrls.length;
				this.hasImages(body.rlHasImages = !!result.hasExternals);

				// Hide valid inline attachments in message view 'attachments' section
				oAttachments.forEach(oAttachment => {
					let cid = oAttachment.contentId(),
						found = result.foundCIDs.includes(cid);
					oAttachment.isInline(found);
					oAttachment.isLinked(found || result.foundContentLocationUrls.includes(oAttachment.contentLocation));
				});

				body.innerHTML = result.html;

				body.classList.toggle('html', 1);
				body.classList.toggle('plain', 0);

				// Drop Microsoft Office style properties
				const rgbRE = /rgb\((\d+),\s*(\d+),\s*(\d+)\)/g,
					hex = n => ('0' + parseInt(n).toString(16)).slice(-2);
				body.querySelectorAll('[style*=mso]').forEach(el =>
					el.setAttribute('style', el.style.cssText.replace(rgbRE, (m,r,g,b) => '#' + hex(r) + hex(g) + hex(b)))
				);

				// showInternalImages
				const findAttachmentByCid = cid => this.attachments().findByCid(cid);
				body.querySelectorAll('[data-x-src-cid],[data-x-src-location],[data-x-style-cid]').forEach(el => {
					const data = el.dataset;
					if (data.xSrcCid) {
						const attachment = findAttachmentByCid(data.xSrcCid);
						if (attachment && attachment.download) {
							el.src = attachment.linkPreview();
						}
					} else if (data.xSrcLocation) {
						const attachment = this.attachments.find(item => data.xSrcLocation === item.contentLocation)
							|| findAttachmentByCid(data.xSrcLocation);
						if (attachment && attachment.download) {
							el.loading = 'lazy';
							el.src = attachment.linkPreview();
						}
					} else if (data.xStyleCid) {
						forEachObjectEntry(JSON.parse(data.xStyleCid), (name, cid) => {
							const attachment = findAttachmentByCid(cid);
							if (attachment && attachment.linkPreview && name) {
								el.style[name] = "url('" + attachment.linkPreview() + "')";
							}
						});
					}
				});

				if (SettingsUserStore.showImages()) {
					this.showExternalImages();
				}

				this.isHtml(true);
				this.initView();
				return true;
			}
		}

		viewPlain() {
			const body = this.body;
			if (body && this.plain()) {
				body.classList.toggle('html', 0);
				body.classList.toggle('plain', 1);
				body.innerHTML = plainToHtml(
					this.plain()
						.replace(/-----BEGIN PGP (SIGNED MESSAGE-----(\r?\n[a-z][^\r\n]+)+|SIGNATURE-----[\s\S]*)/, '')
						.trim()
				)
					.replace(url, '$1<a href="$2" target="_blank">$2</a>')
					.replace(email$1, '$1<a href="mailto:$2">$2</a>');
				this.isHtml(false);
				this.hasImages(false);
				this.initView();
				return true;
			}
		}

		initView() {
			// init BlockquoteSwitcher
			this.body.querySelectorAll('blockquote:not(.rl-bq-switcher)').forEach(node => {
				if (node.textContent.trim() && !node.parentNode.closest('blockquote')) {
					let h = node.clientHeight || getRealHeight(node);
					if (0 === h || 100 < h) {
						const el = Element.fromHTML('<span class="rlBlockquoteSwitcher">•••</span>');
						node.classList.add('rl-bq-switcher','hidden-bq');
						node.before(el);
						el.addEventListener('click', () => node.classList.toggle('hidden-bq'));
					}
				}
			});
		}

		viewPopupMessage(print) {
			const timeStampInUTC = this.dateTimeStampInUTC() || 0,
				ccLine = this.ccToLine(false),
				m = 0 < timeStampInUTC ? new Date(timeStampInUTC * 1000) : null,
				win = open(''),
				sdoc = win.document;
			let subject = encodeHtml(this.subject()),
				mode = this.isHtml() ? 'div' : 'pre',
				cc = ccLine ? `<div>${encodeHtml(i18n('GLOBAL/CC'))}: ${encodeHtml(ccLine)}</div>` : '',
				style = getComputedStyle(doc.querySelector('.messageView')),
				prop = property => style.getPropertyValue(property);
			sdoc.write(PreviewHTML
				.replace('<title>', '<title>'+subject)
				// eslint-disable-next-line max-len
				.replace('<body>', `<body style="background-color:${prop('background-color')};color:${prop('color')}"><header><h1>${subject}</h1><time>${encodeHtml(m ? m.format('LLL') : '')}</time><div>${encodeHtml(this.fromToLine(false))}</div><div>${encodeHtml(i18n('GLOBAL/TO'))}: ${encodeHtml(this.toToLine(false))}</div>${cc}</header><${mode}>${this.bodyAsHTML()}</${mode}>`)
			);
			sdoc.close();

			if (print) {
				setTimeout(() => win.print(), 100);
			}
		}

		/**
		 * @param {boolean=} print = false
		 */
		popupMessage() {
			this.viewPopupMessage(false);
		}

		printMessage() {
			this.viewPopupMessage(true);
		}

		/**
		 * @returns {string}
		 */
		generateUid() {
			return this.folder + '/' + this.uid;
		}

		/**
		 * @param {MessageModel} message
		 * @returns {MessageModel}
		 */
		populateByMessageListItem(message) {
			this.clear();

			if (message) {
				this.folder = message.folder;
				this.uid = message.uid;
				this.hash = message.hash;
				this.requestHash = message.requestHash;
				this.subject(message.subject());
				this.plain(message.plain());
				this.html(message.html());

				this.size(message.size());
				this.spamScore(message.spamScore());
				this.spamResult(message.spamResult());
				this.isSpam(message.isSpam());
				this.hasVirus(message.hasVirus());
				this.dateTimeStampInUTC(message.dateTimeStampInUTC());
				this.priority(message.priority());

				this.hasExternals(message.hasExternals());

				this.emails = message.emails;

				this.from = message.from;
				this.to = message.to;
				this.cc = message.cc;
				this.bcc = message.bcc;
				this.replyTo = message.replyTo;
				this.deliveredTo = message.deliveredTo;
				this.unsubsribeLinks(message.unsubsribeLinks);

				this.flags(message.flags());

				this.priority(message.priority());

				this.selected(message.selected());
				this.checked(message.checked());
				this.attachments(message.attachments());

				this.threads(message.threads());
			}

			this.computeSenderEmail();

			return this;
		}

		showExternalImages() {
			const body = this.body;
			if (body && this.hasImages()) {
				this.hasImages(false);
				body.rlHasImages = false;

				let attr = 'data-x-src';
				body.querySelectorAll('[' + attr + ']').forEach(node => {
					if (node.matches('img')) {
						node.loading = 'lazy';
					}
					node.src = node.getAttribute(attr);
				});

				body.querySelectorAll('[data-x-style-url]').forEach(node => {
					forEachObjectEntry(JSON.parse(node.dataset.xStyleUrl), (name, url) => node.style[name] = "url('" + url + "')");
				});
			}
		}

		/**
		 * @returns {string}
		 */
		bodyAsHTML() {
	//		if (this.body && !this.body.querySelector('iframe[src*=decrypt]')) {
			if (this.body && !this.body.querySelector('iframe')) {
				let clone = this.body.cloneNode(true);
				clone.querySelectorAll('blockquote.rl-bq-switcher').forEach(
					node => node.classList.remove('rl-bq-switcher','hidden-bq')
				);
				clone.querySelectorAll('.rlBlockquoteSwitcher').forEach(
					node => node.remove()
				);
				return clone.innerHTML;
			}
			return this.html() || plainToHtml(this.plain());
		}

		/**
		 * @returns {string}
		 */
		flagHash() {
			return [
				this.deleted(),
				this.isDeleted(),
				this.isUnseen(),
				this.isFlagged(),
				this.isAnswered(),
				this.isForwarded(),
				this.isReadReceipt()
			].join(',');
		}

	}

	class MessageCollectionModel extends AbstractCollectionModel
	{
	/*
		constructor() {
			super();
			this.Filtered
			this.Folder
			this.FolderHash
			this.Limit
			this.MessageCount
			this.MessageUnseenCount
			this.MessageResultCount
			this.NewMessages
			this.Offset
			this.Search
			this.ThreadUid
			this.UidNext
		}
	*/

		/**
		 * @param {?Object} json
		 * @returns {MessageCollectionModel}
		 */
		static reviveFromJson(object, cached) {
			let newCount = 0;
			return super.reviveFromJson(object, message => {
				message = MessageModel.reviveFromJson(message);
				if (message) {
					if (hasNewMessageAndRemoveFromCache(message.folder, message.uid) && 5 >= newCount) {
						++newCount;
					}

					message.deleted(false);

					cached ? MessageFlagsCache.initMessage(message) : MessageFlagsCache.store(message);
					return message;
				}
			});
		}
	}

	//import Remote from 'Remote/User/Fetch'; Circular dependency

	const
		isChecked = item => item.checked();

	let MessageSeenTimer;

	const MessageUserStore = new class {
		constructor() {
			this.staticMessage = new MessageModel();

			this.list = ko.observableArray().extend({ debounce: 0 });

			addObservablesTo(this, {
				listCount: 0,
				listSearch: '',
				listThreadUid: 0,
				listPage: 1,
				listPageBeforeThread: 1,
				listError: '',

				listEndHash: '',
				listEndThreadUid: 0,

				listLoading: false,
				// Happens when message(s) removed from list
				listIsIncomplete: false,

				selectorMessageSelected: null,
				selectorMessageFocused: null,

				// message viewer
				message: null,
				messageViewTrigger: false,
				messageError: '',
				messageLoading: false,
				messageFullScreenMode: false,

				// Cache mail bodies
				messagesBodiesDom: null,
				messageActiveDom: null
			});

			this.listDisableAutoSelect = ko.observable(false).extend({ falseTimeout: 500 });

			// Computed Observables

			addComputablesTo(this, {
				listIsLoading: () => {
					const value = this.listLoading() | this.listIsIncomplete();
					$htmlCL.toggle('list-loading', value);
					return value;
				},

				listPageCount: () => Math.max(1, Math.ceil(this.listCount() / SettingsUserStore.messagesPerPage())),

				mainMessageListSearch: {
					read: this.listSearch,
					write: value => rl.route.setHash(
						mailBox(FolderUserStore.currentFolderFullNameHash(), 1, value.toString().trim(), this.listThreadUid())
					)
				},

				isMessageSelected: () => null !== this.message(),

				listCheckedOrSelected: () => {
					const
						selectedMessage = this.selectorMessageSelected(),
						focusedMessage = this.selectorMessageFocused(),
						checked = this.list.filter(item => isChecked(item) || item === selectedMessage);
					return checked.length ? checked : (focusedMessage ? [focusedMessage] : []);
				},

				listCheckedOrSelectedUidsWithSubMails: () => {
					let result = [];
					this.listCheckedOrSelected().forEach(message => {
						result.push(message.uid);
						if (1 < message.threadsLen()) {
							result = result.concat(message.threads()).unique();
						}
					});
					return result;
				}
			});

			this.listChecked = koComputable(() => this.list.filter(isChecked))
				.extend({ rateLimit: 0 });

			this.hasCheckedMessages = koComputable(() => !!this.list.find(isChecked))
				.extend({ rateLimit: 0 });

			this.hasCheckedOrSelected = koComputable(() => !!(this.selectorMessageSelected()
					|| this.selectorMessageFocused()
					|| this.list.find(item => item.checked())))
				.extend({ rateLimit: 50 });

			// Subscribers

			addSubscribablesTo(this, {
				message: message => {
					clearTimeout(MessageSeenTimer);
					if (message) {
						if (!SettingsUserStore.usePreviewPane()) {
							AppUserStore.focusedState(Scope.MessageView);
						}
					} else {
						AppUserStore.focusedState(Scope.MessageList);

						this.messageFullScreenMode(false);
						this.hideMessageBodies();
					}
				},

				isMessageSelected: value => elementById('rl-right').classList.toggle('message-selected', value)
			});

			this.purgeMessageBodyCache = this.purgeMessageBodyCache.throttle(30000);
		}

		purgeMessageBodyCache() {
			const messagesDom = this.messagesBodiesDom(),
				children = messagesDom && messagesDom.children;
			if (children) {
				while (15 < children.length) {
					children[0].remove();
				}
			}
		}

		initUidNextAndNewMessages(folder, uidNext, newMessages) {
			if (getFolderInboxName() === folder && uidNext) {
				if (arrayLength(newMessages)) {
					newMessages.forEach(item => addNewMessageCache(folder, item.Uid));

					NotificationUserStore.playSoundNotification();

					const len = newMessages.length;
					if (3 < len) {
						NotificationUserStore.displayDesktopNotification(
							AccountUserStore.email(),
							i18n('MESSAGE_LIST/NEW_MESSAGE_NOTIFICATION', {
								COUNT: len
							}),
							{ Url: mailBox(newMessages[0].Folder) }
						);
					} else {
						newMessages.forEach(item => {
							NotificationUserStore.displayDesktopNotification(
								EmailCollectionModel.reviveFromJson(item.From).toString(),
								item.Subject,
								{ Folder: item.Folder, Uid: item.Uid }
							);
						});
					}
				}

				setFolderUidNext(folder, uidNext);
			}
		}

		hideMessageBodies() {
			const messagesDom = this.messagesBodiesDom();
			messagesDom && Array.from(messagesDom.children).forEach(el => el.hidden = true);
		}

		/**
		 * @param {string} fromFolderFullName
		 * @param {Array} uidForRemove
		 * @param {string=} toFolderFullName = ''
		 * @param {boolean=} copy = false
		 */
		removeMessagesFromList(fromFolderFullName, uidForRemove, toFolderFullName = '', copy = false) {
			uidForRemove = uidForRemove.map(mValue => pInt(mValue));

			let unseenCount = 0,
				messageList = this.list,
				currentMessage = this.message();

			const trashFolder = FolderUserStore.trashFolder(),
				spamFolder = FolderUserStore.spamFolder(),
				fromFolder = getFolderFromCacheList(fromFolderFullName),
				toFolder = toFolderFullName ? getFolderFromCacheList(toFolderFullName) : null,
				messages =
					FolderUserStore.currentFolderFullName() === fromFolderFullName
						? messageList.filter(item => item && uidForRemove.includes(pInt(item.uid)))
						: [];

			messages.forEach(item => {
				if (item && item.isUnseen()) {
					++unseenCount;
				}
			});

			if (fromFolder && !copy) {
				fromFolder.messageCountAll(
					0 <= fromFolder.messageCountAll() - uidForRemove.length ? fromFolder.messageCountAll() - uidForRemove.length : 0
				);

				if (0 < unseenCount) {
					fromFolder.messageCountUnread(
						0 <= fromFolder.messageCountUnread() - unseenCount ? fromFolder.messageCountUnread() - unseenCount : 0
					);
				}
			}

			if (toFolder) {
				if (trashFolder === toFolder.fullName || spamFolder === toFolder.fullName) {
					unseenCount = 0;
				}

				toFolder.messageCountAll(toFolder.messageCountAll() + uidForRemove.length);
				if (0 < unseenCount) {
					toFolder.messageCountUnread(toFolder.messageCountUnread() + unseenCount);
				}

				toFolder.actionBlink(true);
			}

			if (messages.length) {
				if (copy) {
					messages.forEach(item => item.checked(false));
				} else {
					this.listIsIncomplete(true);

					messages.forEach(item => {
						if (currentMessage && currentMessage.hash === item.hash) {
							currentMessage = null;
							this.message(null);
						}

						item.deleted(true);
					});

					setTimeout(() => messages.forEach(item => messageList.remove(item)), 350);
				}
			}

			if (fromFolderFullName) {
				setFolderHash(fromFolderFullName, '');
			}

			if (toFolderFullName) {
				setFolderHash(toFolderFullName, '');
			}

			if (this.listThreadUid()) {
				if (
					messageList.length &&
					!!messageList.find(item => !!(item && item.deleted() && item.uid == this.listThreadUid()))
				) {
					const message = messageList.find(item => item && !item.deleted());
					if (message && this.listThreadUid() != message.uid) {
						this.listThreadUid(message.uid);

						rl.route.setHash(
							mailBox(
								FolderUserStore.currentFolderFullNameHash(),
								this.listPage(),
								this.listSearch(),
								this.listThreadUid()
							),
							true,
							true
						);
					} else if (!message) {
						if (1 < this.listPage()) {
							this.listPage(this.listPage() - 1);

							rl.route.setHash(
								mailBox(
									FolderUserStore.currentFolderFullNameHash(),
									this.listPage(),
									this.listSearch(),
									this.listThreadUid()
								),
								true,
								true
							);
						} else {
							this.listThreadUid(0);

							rl.route.setHash(
								mailBox(
									FolderUserStore.currentFolderFullNameHash(),
									this.listPageBeforeThread(),
									this.listSearch()
								),
								true,
								true
							);
						}
					}
				}
			}
		}

		setMessage(data, cached, oMessage) {
			let isNew = false,
				json = data && data.Result,
				message = oMessage || this.message();

			if (
				json &&
				MessageModel.validJson(json) &&
				message &&
				message.folder === json.Folder
			) {
				const threads = message.threads(),
					messagesDom = this.messagesBodiesDom();
				if (!oMessage && message.uid != json.Uid && threads.includes(json.Uid)) {
					message = MessageModel.reviveFromJson(json);
					if (message) {
						message.threads(threads);
						MessageFlagsCache.initMessage(message);

						this.message(this.staticMessage.populateByMessageListItem(message));
						message = this.message();

						isNew = true;
					}
				}

				if (message && message.uid == json.Uid) {
					oMessage || this.messageError('');
	/*
					if (cached) {
						delete json.Flags;
					}
	*/
					isNew || message.revivePropertiesFromJson(json);
					addRequestedMessage(message.folder, message.uid);
					if (messagesDom) {
						let id = 'rl-msg-' + message.hash.replace(/[^a-zA-Z0-9]/g, ''),
							body = elementById(id);
						if (body) {
							message.body = body;
							message.isHtml(body.classList.contains('html'));
							message.hasImages(body.rlHasImages);
						} else {
							body = Element.fromHTML('<div id="' + id + '" hidden="" class="b-text-part '
								+ (message.pgpSigned() ? ' openpgp-signed' : '')
								+ (message.isPgpEncrypted() ? ' openpgp-encrypted' : '')
								+ '">'
								+ '</div>');
							message.body = body;
							if (!SettingsUserStore.viewHTML() || !message.viewHtml()) {
								message.viewPlain();
							}

							this.purgeMessageBodyCache();
						}

						messagesDom.append(body);

						oMessage || this.messageActiveDom(message.body);

						oMessage || this.hideMessageBodies();

						oMessage || (message.body.hidden = false);
						oMessage && message.viewPopupMessage();
					}

					MessageFlagsCache.initMessage(message);
					if (message.isUnseen()) {
						MessageSeenTimer = setTimeout(
							() => rl.app.messageListAction(message.folder, MessageSetAction.SetSeen, [message]),
							SettingsUserStore.messageReadDelay() * 1000 // seconds
						);
					}

					if (message && isNew) {
						let selectedMessage = this.selectorMessageSelected();
						if (
							selectedMessage &&
							(message.folder !== selectedMessage.folder || message.uid != selectedMessage.uid)
						) {
							this.selectorMessageSelected(null);
							if (1 === this.list.length) {
								this.selectorMessageFocused(null);
							}
						} else if (!selectedMessage) {
							selectedMessage = this.list.find(
								subMessage =>
									subMessage &&
									subMessage.folder === message.folder &&
									subMessage.uid == message.uid
							);

							if (selectedMessage) {
								this.selectorMessageSelected(selectedMessage);
								this.selectorMessageFocused(selectedMessage);
							}
						}
					}
				}
			}
		}

		selectMessage(oMessage) {
			if (oMessage) {
				this.message(this.staticMessage.populateByMessageListItem(oMessage));
				this.populateMessageBody(this.message());
			} else {
				this.message(null);
			}
		}

		selectMessageByFolderAndUid(sFolder, iUid) {
			if (sFolder && iUid) {
				this.message(this.staticMessage.populateByMessageListItem(null));
				this.message().folder = sFolder;
				this.message().uid = iUid;

				this.populateMessageBody(this.message());
			} else {
				this.message(null);
			}
		}

		populateMessageBody(oMessage, preload) {
			if (oMessage) {
				preload || this.hideMessageBodies();
				preload || this.messageLoading(true);
				rl.app.Remote.message((iError, oData, bCached) => {
					if (iError) {
						if (Notification.RequestAborted !== iError && !preload) {
							this.message(null);
							this.messageError(getNotification(iError));
						}
					} else {
						this.setMessage(oData, bCached, preload ? oMessage : null);
					}
					preload || this.messageLoading(false);
				}, oMessage.folder, oMessage.uid);
			}
		}

		/**
		 * @param {Array} list
		 * @returns {string}
		 */
		calculateMessageListHash(list) {
			return list.map(message => message.hash + '_' + message.threadsLen() + '_' + message.flagHash()).join(
				'|'
			);
		}

		reloadFlagsAndCachedMessage() {
			this.list.forEach(message => MessageFlagsCache.initMessage(message));
			MessageFlagsCache.initMessage(this.message());
			this.messageViewTrigger(!this.messageViewTrigger());
		}

		setMessageList(data, cached) {
			const collection = MessageCollectionModel.reviveFromJson(data.Result, cached);
			if (collection) {
				let unreadCountChange = false;

				const
					folder = getFolderFromCacheList(collection.Folder);

				if (folder && !cached) {
					folder.expires = Date.now();

					setFolderHash(collection.Folder, collection.FolderHash);

					if (null != collection.MessageCount) {
						folder.messageCountAll(collection.MessageCount);
					}

					if (null != collection.MessageUnseenCount) {
						if (pInt(folder.messageCountUnread()) !== pInt(collection.MessageUnseenCount)) {
							unreadCountChange = true;
							MessageFlagsCache.clearFolder(folder.fullName);
						}

						folder.messageCountUnread(collection.MessageUnseenCount);
					}

					this.initUidNextAndNewMessages(folder.fullName, collection.UidNext, collection.NewMessages);
				}

				this.listCount(collection.MessageResultCount);
				this.listSearch(pString(collection.Search));
				this.listPage(Math.ceil(collection.Offset / SettingsUserStore.messagesPerPage() + 1));
				this.listThreadUid(collection.ThreadUid);

				this.listEndHash(
					collection.Folder +
					'|' + collection.Search +
					'|' + this.listThreadUid() +
					'|' + this.listPage()
				);
				this.listEndThreadUid(collection.ThreadUid);
				const message = this.message();
				if (message && collection.Folder !== message.folder) {
					this.message(null);
				}

				this.listDisableAutoSelect(true);

				this.list(collection);
				this.listIsIncomplete(false);

				clearNewMessageCache();

				if (folder && (cached || unreadCountChange || SettingsUserStore.useThreads())) {
					rl.app.folderInformation(folder.fullName, collection);
				}
			} else {
				this.listCount(0);
				this.list([]);
				this.listError(getNotification(Notification.CantGetMessageList));
			}
		}
	};

	//import { mailBox } from 'Common/Links';

	const
		isPosNumeric = value => null != value && /^[0-9]*$/.test(value.toString()),

		normalizeFolder = sFolderFullName => ('' === sFolderFullName
			|| UNUSED_OPTION_VALUE === sFolderFullName
			|| null !== getFolderFromCacheList(sFolderFullName))
				? sFolderFullName
				: '',

		SystemFolders = {
			Inbox:   0,
			Sent:    0,
			Drafts:  0,
			Spam:    0,
			Trash:   0,
			Archive: 0
		};

	class FolderCollectionModel extends AbstractCollectionModel
	{
	/*
		constructor() {
			super();
			this.CountRec
			this.IsThreadsSupported
			this.Namespace;
			this.Optimized
			this.SystemFolders
			this.Capabilities
		}
	*/

		/**
		 * @param {?Object} json
		 * @returns {FolderCollectionModel}
		 */
		static reviveFromJson(object) {
			const expandedFolders = get(ClientSideKeyName.ExpandedFolders);
			if (object && object.SystemFolders) {
				forEachObjectEntry(SystemFolders, key =>
					SystemFolders[key] = SettingsGet(key+'Folder') || object.SystemFolders[FolderType[key]]
				);
			}

			const result = super.reviveFromJson(object, oFolder => {
				let oCacheFolder = getFolderFromCacheList(oFolder.FullName),
					type = FolderType[getKeyByValue(SystemFolders, oFolder.FullName)];

				if (!oCacheFolder) {
					oCacheFolder = FolderModel.reviveFromJson(oFolder);
					if (!oCacheFolder)
						return null;

					if (1 == type) {
						oCacheFolder.type(FolderType.Inbox);
						setFolderInboxName(oFolder.FullName);
					}
					setFolder(oCacheFolder);
				}

				if (1 < type) {
					oCacheFolder.type(type);
				}

				oCacheFolder.collapsed(!expandedFolders
					|| !isArray(expandedFolders)
					|| !expandedFolders.includes(oCacheFolder.fullName));

				if (oFolder.Extended) {
					if (oFolder.Extended.Hash) {
						setFolderHash(oCacheFolder.fullName, oFolder.Extended.Hash);
					}

					if (null != oFolder.Extended.MessageCount) {
						oCacheFolder.messageCountAll(oFolder.Extended.MessageCount);
					}

					if (null != oFolder.Extended.MessageUnseenCount) {
						oCacheFolder.messageCountUnread(oFolder.Extended.MessageUnseenCount);
					}
				}
				return oCacheFolder;
			});

			let i = result.length;
			if (i) {
				sortFolders(result);
				try {
					while (i--) {
						let folder = result[i], parent = getFolderFromCacheList(folder.parentName);
						if (parent) {
							parent.subFolders.unshift(folder);
							result.splice(i,1);
						}
					}
				} catch (e) {
					console.error(e);
				}
			}

			return result;
		}

		storeIt() {
			FolderUserStore.displaySpecSetting(Settings.app('folderSpecLimit') < this.CountRec);

			if (!(
					SettingsGet('SentFolder') +
					SettingsGet('DraftsFolder') +
					SettingsGet('SpamFolder') +
					SettingsGet('TrashFolder') +
					SettingsGet('ArchiveFolder')
				)
			) {
				FolderUserStore.saveSystemFolders(SystemFolders);
			}

			FolderUserStore.folderList(this);

			FolderUserStore.namespace = this.Namespace;

			AppUserStore.threadsAllowed(!!(Settings.app('useImapThread') && this.IsThreadsSupported));

			FolderUserStore.folderListOptimized(!!this.Optimized);
			FolderUserStore.quotaUsage(this.quotaUsage);
			FolderUserStore.quotaLimit(this.quotaLimit);
			FolderUserStore.capabilities(this.Capabilities);

			FolderUserStore.sentFolder(normalizeFolder(SystemFolders.Sent));
			FolderUserStore.draftsFolder(normalizeFolder(SystemFolders.Drafts));
			FolderUserStore.spamFolder(normalizeFolder(SystemFolders.Spam));
			FolderUserStore.trashFolder(normalizeFolder(SystemFolders.Trash));
			FolderUserStore.archiveFolder(normalizeFolder(SystemFolders.Archive));

	//		FolderUserStore.folderList.valueHasMutated();
		}

	}

	function getKolabFolderName(type)
	{
		const types = {
			configuration: 'CONFIGURATION',
			event: 'CALENDAR',
			contact: 'CONTACTS',
			task: 'TASKS',
			note: 'NOTES',
			file: 'FILES',
			journal: 'JOURNAL'
		};
		return types[type] ? 'Kolab ' + i18n('SETTINGS_FOLDERS/TYPE_' + types[type]) : '';
	}

	function getSystemFolderName(type, def)
	{
		switch (type) {
			case FolderType.Inbox:
			case FolderType.Sent:
			case FolderType.Drafts:
			case FolderType.Trash:
			case FolderType.Archive:
				return i18n('FOLDER_LIST/' + getKeyByValue(FolderType, type).toUpperCase() + '_NAME');
			case FolderType.Spam:
				return i18n('GLOBAL/SPAM');
			// no default
		}
		return def;
	}

	class FolderModel extends AbstractModel {
		constructor() {
			super();

			this.fullName = '';
			this.delimiter = '';
			this.deep = 0;
			this.expires = 0;
			this.metadata = {};

			this.exists = true;

	//		this.hash = '';
	//		this.uidNext = 0;

			this.addObservables({
				name: '',
				type: FolderType.User,
				selectable: false,

				focused: false,
				selected: false,
				edited: false,
				subscribed: true,
				checkable: false, // Check for new messages
				askDelete: false,

				nameForEdit: '',
				errorMsg: '',

				privateMessageCountAll: 0,
				privateMessageCountUnread: 0,

				kolabType: null,

				collapsed: true
			});

			this.addSubscribables({
				kolabType: sValue => this.metadata[FolderMetadataKeys.KolabFolderType] = sValue
			});

			this.subFolders = ko.observableArray(new FolderCollectionModel);
			this.actionBlink = ko.observable(false).extend({ falseTimeout: 1000 });
		}

		/**
		 * For url safe '/#/mailbox/...' path
		 */
		get fullNameHash() {
			return this.fullName.replace(/[^a-z0-9._-]+/giu, b64EncodeJSONSafe);
	//		return /^[a-z0-9._-]+$/iu.test(this.fullName) ? this.fullName : b64EncodeJSONSafe(this.fullName);
		}

		/**
		 * @static
		 * @param {FetchJsonFolder} json
		 * @returns {?FolderModel}
		 */
		static reviveFromJson(json) {
			const folder = super.reviveFromJson(json);
			if (folder) {
				const path = folder.fullName.split(folder.delimiter),
					type = (folder.metadata[FolderMetadataKeys.KolabFolderType]
						|| folder.metadata[FolderMetadataKeys.KolabFolderTypeShared]
						|| ''
					).split('.')[0];

				folder.deep = path.length - 1;
				path.pop();
				folder.parentName = path.join(folder.delimiter);

				type && 'mail' != type && folder.kolabType(type);

				folder.messageCountAll = koComputable({
						read: folder.privateMessageCountAll,
						write: (iValue) => {
							if (isPosNumeric(iValue)) {
								folder.privateMessageCountAll(iValue);
							} else {
								folder.privateMessageCountAll.valueHasMutated();
							}
						}
					})
					.extend({ notify: 'always' });

				folder.messageCountUnread = koComputable({
						read: folder.privateMessageCountUnread,
						write: (value) => {
							if (isPosNumeric(value)) {
								folder.privateMessageCountUnread(value);
							} else {
								folder.privateMessageCountUnread.valueHasMutated();
							}
						}
					})
					.extend({ notify: 'always' });

				folder.addComputables({

					isInbox: () => FolderType.Inbox === folder.type(),

					isFlagged: () => FolderUserStore.currentFolder() === folder
						&& MessageUserStore.listSearch().includes('flagged'),

					hasVisibleSubfolders: () => !!folder.subFolders().find(folder => folder.visible()),

					hasSubscriptions: () => folder.subscribed() | !!folder.subFolders().find(
							oFolder => {
								const subscribed = oFolder.hasSubscriptions();
								return !oFolder.isSystemFolder() && subscribed;
							}
						),

					canBeEdited: () => FolderType.User === folder.type() && folder.exists/* && folder.selectable()*/,

					isSystemFolder: () => FolderType.User !== folder.type() | !!folder.kolabType(),

					canBeSelected: () => folder.selectable() && !folder.isSystemFolder(),

					canBeDeleted: () => folder.canBeSelected() && folder.exists,

					canBeSubscribed: () => folder.selectable()
						&& !(folder.isSystemFolder() | !SettingsUserStore.hideUnsubscribed()),

					/**
					 * Folder is visible when:
					 * - hasVisibleSubfolders()
					 * Or when all below conditions are true:
					 * - selectable()
					 * - subscribed() OR hideUnsubscribed = false
					 * - FolderType.User
					 * - not kolabType()
					 */
					visible: () => {
						const selectable = folder.canBeSelected(),
							visible = (folder.subscribed() | !SettingsUserStore.hideUnsubscribed()) && selectable;
						return folder.hasVisibleSubfolders() | visible;
					},

					hidden: () => !folder.selectable() && (folder.isSystemFolder() | !folder.hasVisibleSubfolders()),

					printableUnreadCount: () => {
						const count = folder.messageCountAll(),
							unread = folder.messageCountUnread(),
							type = folder.type();

						if (count) {
							if (FolderType.Drafts === type) {
								return count;
							}
							if (
								unread &&
								FolderType.Trash !== type &&
								FolderType.Archive !== type &&
								FolderType.Sent !== type
							) {
								return unread;
							}
						}

						return null;
					},

					localName: () => {
						let name = folder.name();
						if (folder.isSystemFolder()) {
							trigger();
							name = getSystemFolderName(folder.type(), name);
						}
						return name;
					},

					manageFolderSystemName: () => {
						if (folder.isSystemFolder()) {
							trigger();
							let suffix = getSystemFolderName(folder.type(), getKolabFolderName(folder.kolabType()));
							if (folder.name() !== suffix && 'inbox' !== suffix.toLowerCase()) {
								return '(' + suffix + ')';
							}
						}
						return '';
					},

					hasUnreadMessages: () => 0 < folder.messageCountUnread() && folder.printableUnreadCount(),

					hasSubscribedUnreadMessagesSubfolders: () =>
						!!folder.subFolders().find(
							folder => folder.hasUnreadMessages() | folder.hasSubscribedUnreadMessagesSubfolders()
						)

	//				,href: () => folder.canBeSelected() && mailBox(folder.fullNameHash)
				});

				folder.addSubscribables({
					name: value => folder.nameForEdit(value),

					edited: value => value && folder.nameForEdit(folder.name()),

					messageCountUnread: unread => {
						if (FolderType.Inbox === folder.type()) {
							fireEvent('mailbox.inbox-unread-count', unread);
						}
					}
				});
			}
			return folder;
		}

		/**
		 * @returns {string}
		 */
		collapsedCss() {
			return 'e-collapsed-sign ' + (this.hasVisibleSubfolders()
				? (this.collapsed() ? 'icon-right-mini' : 'icon-down-mini')
				: 'icon-none'
			);
		}

		/**
		 * @returns {string}
		 */
		printableFullName() {
			return this.fullName.replace(this.delimiter, ' / ');
		}
	}

	class RemoteUserFetch extends AbstractFetchRemote {

		/**
		 * @param {Function} fCallback
		 * @param {object} params
		 * @param {boolean=} bSilent = false
		 */
		messageList(fCallback, params, bSilent = false) {
			const
				sFolderFullName = pString(params.Folder),
				folderHash = getFolderHash(sFolderFullName);

			params = Object.assign({
				Offset: 0,
				Limit: SettingsUserStore.messagesPerPage(),
				Search: '',
				UidNext: getFolderUidNext(sFolderFullName), // Used to check for new messages
				Sort: FolderUserStore.sortMode(),
				Hash: folderHash + SettingsGet('AccountHash')
			}, params);
			params.Folder = sFolderFullName;
			if (AppUserStore.threadsAllowed() && SettingsUserStore.useThreads()) {
				params.UseThreads = 1;
			} else {
				params.ThreadUid = 0;
			}

			let sGetAdd = '';

			if (folderHash && (!params.Search || !params.Search.includes('is:'))) {
				sGetAdd = 'MessageList/' +
					SUB_QUERY_PREFIX +
					'/' +
					b64EncodeJSONSafe(params);
				params = {};
			}

			this.request('MessageList',
				fCallback,
				params,
				30000,
				sGetAdd,
				bSilent ? [] : ['MessageList']
			);
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sFolderFullName
		 * @param {number} iUid
		 * @returns {boolean}
		 */
		message(fCallback, sFolderFullName, iUid) {
			sFolderFullName = pString(sFolderFullName);
			iUid = pInt(iUid);

			if (getFolderFromCacheList(sFolderFullName) && 0 < iUid) {
				this.request('Message',
					fCallback,
					{},
					null,
					'Message/' +
						SUB_QUERY_PREFIX +
						'/' +
						b64EncodeJSONSafe([
							sFolderFullName,
							iUid,
							AppUserStore.threadsAllowed() && SettingsUserStore.useThreads() ? 1 : 0,
							SettingsGet('AccountHash')
						]),
					['Message']
				);

				return true;
			}

			return false;
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} folder
		 * @param {Array=} list = []
		 */
		folderInformation(fCallback, folder, list = []) {
			let fetch = !arrayLength(list);
			const uids = [];

			if (!fetch) {
				list.forEach(messageListItem => {
					if (!MessageFlagsCache.getFor(messageListItem.folder, messageListItem.uid)) {
						uids.push(messageListItem.uid);
					}

					if (messageListItem.threads.length) {
						messageListItem.threads.forEach(uid => {
							if (!MessageFlagsCache.getFor(messageListItem.folder, uid)) {
								uids.push(uid);
							}
						});
					}
				});
				fetch = uids.length;
			}

			if (fetch) {
				this.request('FolderInformation', fCallback, {
					Folder: folder,
					FlagsUids: uids,
					UidNext: getFolderUidNext(folder) // Used to check for new messages
				});
			} else if (SettingsUserStore.useThreads()) {
				MessageUserStore.reloadFlagsAndCachedMessage();
			}
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sFolderFullName
		 * @param {boolean} bSetSeen
		 * @param {Array} aThreadUids = null
		 */
		messageSetSeenToAll(sFolderFullName, bSetSeen, aThreadUids = null) {
			this.request('MessageSetSeenToAll', null, {
				Folder: sFolderFullName,
				SetAction: bSetSeen ? 1 : 0,
				ThreadUids: aThreadUids ? aThreadUids.join(',') : ''
			});
		}

		/**
		 * @param {?Function} fCallback
		 * @param {Object} oData
		 */
		saveSettings(fCallback, oData) {
			this.request('SettingsUpdate', fCallback, oData);
		}

		/**
		 * @param {string} key
		 * @param {?scalar} value
		 * @param {?Function} fCallback
		 */
		saveSetting(key, value, fCallback) {
			this.saveSettings(fCallback, {
				[key]: value
			});
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sFolderFullName
		 * @param {boolean} bSubscribe
		 */
		folderSetMetadata(fCallback, sFolderFullName, sKey, sValue) {
			this.request('FolderSetMetadata', fCallback, {
				Folder: sFolderFullName,
				Key: sKey,
				Value: sValue
			});
		}

		/**
		 * @param {?Function} fCallback
		 * @param {string} sQuery
		 * @param {number} iPage
		 */
		suggestions(fCallback, sQuery, iPage) {
			this.request('Suggestions',
				fCallback,
				{
					Query: sQuery,
					Page: iPage
				},
				null,
				'',
				['Suggestions']
			);
		}

		/**
		 * @param {?Function} fCallback
		 */
		foldersReload(fCallback) {
	//		clearTimeout(this.foldersTimeout);
			this.abort('Folders')
				.post('Folders', FolderUserStore.foldersLoading)
				.then(data => {
					data = FolderCollectionModel.reviveFromJson(data.Result);
					data && data.storeIt();
					fCallback && fCallback(true);
					// Repeat every 15 minutes?
	//				this.foldersTimeout = setTimeout(() => this.foldersReload(), 900000);
				})
				.catch(() => fCallback && setTimeout(fCallback, 1, false));
		}

	/*
		folderMove(sPrevFolderFullName, sNewFolderFullName, bSubscribe) {
			return this.post('FolderMove', FolderUserStore.foldersRenaming, {
				Folder: sPrevFolderFullName,
				NewFolder: sNewFolderFullName,
				Subscribe: bSubscribe ? 1 : 0
			});
		}
	*/
		attachmentsActions(sAction, aHashes, fTrigger) {
			return this.post('AttachmentsActions', fTrigger, {
				Do: sAction,
				Hashes: aHashes
			});
		}
	}

	var Remote = new RemoteUserFetch();

	const ContactUserStore = ko.observableArray();

	ContactUserStore.loading = ko.observable(false).extend({ debounce: 200 });
	ContactUserStore.importing = ko.observable(false).extend({ debounce: 200 });
	ContactUserStore.syncing = ko.observable(false).extend({ debounce: 200 });

	addObservablesTo(ContactUserStore, {
		allowSync: false, // Admin setting
		enableSync: false,
		syncUrl: '',
		syncUser: '',
		syncPass: ''
	});

	/**
	 * @param {Function} fResultFunc
	 * @returns {void}
	 */
	ContactUserStore.sync = fResultFunc => {
		if (ContactUserStore.enableSync()
		 && !ContactUserStore.importing()
		 && !ContactUserStore.syncing()
		) {
			ContactUserStore.syncing(true);
			Remote.request('ContactsSync', (iError, oData) => {
				ContactUserStore.syncing(false);
				fResultFunc && fResultFunc(iError, oData);
			}, null, 200000);
		}
	};

	ContactUserStore.init = () => {
		let value = !!SettingsGet('ContactsSyncIsAllowed');
		ContactUserStore.allowSync(value);
		if (value) {
			ContactUserStore.enableSync(!!SettingsGet('EnableContactsSync'));
			ContactUserStore.syncUrl(SettingsGet('ContactsSyncUrl'));
			ContactUserStore.syncUser(SettingsGet('ContactsSyncUser'));
			ContactUserStore.syncPass(SettingsGet('ContactsSyncPassword'));
			setTimeout(ContactUserStore.sync, 10000);
			value = pInt(SettingsGet('ContactsSyncInterval'));
			value = 5 <= value ? (320 >= value ? value : 320) : 20;
			setInterval(ContactUserStore.sync, value * 60000 + 5000);
		}
	};

	const IdentityUserStore = ko.observableArray();

	IdentityUserStore.getIDS = () => IdentityUserStore.map(item => (item ? item.id() : null))
		.filter(value => null !== value);
	IdentityUserStore.loading = ko.observable(false).extend({ debounce: 100 });

	let
		SCREENS = {},
		currentScreen = null,
		defaultScreenName = '';

	const
		autofocus = dom => {
			const af = dom.querySelector('[autofocus]');
			af && af.focus();
		},

		visiblePopups = new Set,

		/**
		 * @param {string} screenName
		 * @returns {?Object}
		 */
		screen = screenName => screenName && null != SCREENS[screenName] ? SCREENS[screenName] : null,

		/**
		 * @param {Function} ViewModelClass
		 * @param {Object=} vmScreen
		 * @returns {*}
		 */
		buildViewModel = (ViewModelClass, vmScreen) => {
			if (ViewModelClass && !ViewModelClass.__builded) {
				let vmDom = null;
				const vm = new ViewModelClass(vmScreen),
					position = vm.viewType || '',
					dialog = ViewType.Popup === position,
					vmPlace = position ? doc.getElementById('rl-' + position.toLowerCase()) : null;

				ViewModelClass.__builded = true;
				ViewModelClass.__vm = vm;

				if (vmPlace) {
					vmDom = dialog
						? Element.fromHTML('<dialog id="V-'+ vm.viewModelTemplateID + '"></dialog>')
						: Element.fromHTML('<div id="V-'+ vm.viewModelTemplateID + '" hidden=""></div>');
					vmPlace.append(vmDom);

					vm.viewModelDom = vmDom;
					ViewModelClass.__dom = vmDom;

					if (ViewType.Popup === position) {
						vm.cancelCommand = vm.closeCommand = createCommand(() => hideScreenPopup(ViewModelClass));

						// Firefox / Safari HTMLDialogElement not defined
						if (!vmDom.showModal) {
							vmDom.classList.add('polyfill');
							vmDom.showModal = () => {
								if (!vmDom.backdrop) {
									vmDom.before(vmDom.backdrop = Element.fromHTML('<div class="dialog-backdrop"></div>'));
								}
								vmDom.setAttribute('open','');
								vmDom.open = true;
								vmDom.returnValue = null;
								vmDom.backdrop.hidden = false;
							};
							vmDom.close = v => {
								vmDom.backdrop.hidden = true;
								vmDom.returnValue = v;
								vmDom.removeAttribute('open', null);
								vmDom.open = false;
							};
						}

						// show/hide popup/modal
						const endShowHide = e => {
							if (e.target === vmDom) {
								if (vmDom.classList.contains('animate')) {
									autofocus(vmDom);
									vm.onShowWithDelay && vm.onShowWithDelay();
								} else {
									vmDom.close();
									vm.onHideWithDelay && vm.onHideWithDelay();
								}
							}
						};

						vm.modalVisibility.subscribe(value => {
							if (value) {
								visiblePopups.add(vm);
								vmDom.style.zIndex = 3000 + (visiblePopups.size * 2);
								vmDom.showModal();
								if (vmDom.backdrop) {
									vmDom.backdrop.style.zIndex = 3000 + visiblePopups.size;
								}
								vm.keyScope.set();
								requestAnimationFrame(() => { // wait just before the next paint
									vmDom.offsetHeight; // force a reflow
									vmDom.classList.add('animate'); // trigger the transitions
								});
							} else {
								visiblePopups.delete(vm);
								vm.onHide && vm.onHide();
								vm.keyScope.unset();
								vmDom.classList.remove('animate'); // trigger the transitions
							}
							arePopupsVisible(0 < visiblePopups.size);
	/*
							// the old ko.bindingHandlers.modal
							const close = vmDom.querySelector('.close'),
								click = () => vm.modalVisibility(false);
							if (close) {
								close.addEventListener('click.koModal', click);
								ko.utils.domNodeDisposal.addDisposeCallback(vmDom, () =>
									close.removeEventListener('click.koModal', click)
								);
							}
	*/
						});
						if ('ontransitionend' in vmDom) {
							vmDom.addEventListener('transitionend', endShowHide);
						} else {
							// For Edge < 79 and mobile browsers
							vm.modalVisibility.subscribe(() => ()=>setTimeout(endShowHide({target:vmDom}), 500));
						}
					}

					ko.applyBindingAccessorsToNode(
						vmDom,
						{
							i18nInit: true,
							template: () => ({ name: vm.viewModelTemplateID })
						},
						vm
					);

					vm.onBuild && vm.onBuild(vmDom);
					if (vm && ViewType.Popup === position) {
						vm.registerPopupKeyDown();
					}

					fireEvent('rl-view-model', vm);
				} else {
					console.log('Cannot find view model position: ' + position);
				}
			}

			return ViewModelClass && ViewModelClass.__vm;
		},

		forEachViewModel = (screen, fn) => {
			screen.viewModels.forEach(ViewModelClass => {
				if (
					ViewModelClass.__vm &&
					ViewModelClass.__dom &&
					ViewType.Popup !== ViewModelClass.__vm.viewType
				) {
					fn(ViewModelClass.__vm, ViewModelClass.__dom);
				}
			});
		},

		hideScreen = (screenToHide, destroy) => {
			screenToHide.onHide && screenToHide.onHide();
			forEachViewModel(screenToHide, (vm, dom) => {
				dom.hidden = true;
				vm.onHide && vm.onHide();
				destroy && vm.viewModelDom.remove();
			});
		},

		/**
		 * @param {Function} ViewModelClassToHide
		 * @returns {void}
		 */
		hideScreenPopup = ViewModelClassToHide => {
			if (ViewModelClassToHide && ViewModelClassToHide.__vm && ViewModelClassToHide.__dom) {
				ViewModelClassToHide.__vm.modalVisibility(false);
			}
		},

		/**
		 * @param {string} screenName
		 * @param {string} subPart
		 * @returns {void}
		 */
		screenOnRoute = (screenName, subPart) => {
			let vmScreen = null,
				isSameScreen = false;

			if (null == screenName || '' == screenName) {
				screenName = defaultScreenName;
			}

			// Close all popups
			for (let vm of visiblePopups) {
				vm.closeCommand();
			}

			if (screenName) {
				vmScreen = screen(screenName);
				if (!vmScreen) {
					vmScreen = screen(defaultScreenName);
					if (vmScreen) {
						subPart = screenName + '/' + subPart;
						screenName = defaultScreenName;
					}
				}

				if (vmScreen && vmScreen.__started) {
					isSameScreen = currentScreen && vmScreen === currentScreen;

					if (!vmScreen.__builded) {
						vmScreen.__builded = true;

						vmScreen.viewModels.forEach(ViewModelClass =>
							buildViewModel(ViewModelClass, vmScreen)
						);

						vmScreen.onBuild && vmScreen.onBuild();
					}

					setTimeout(() => {
						// hide screen
						if (currentScreen && !isSameScreen) {
							hideScreen(currentScreen);
						}
						// --

						currentScreen = vmScreen;

						// show screen
						if (!isSameScreen) {
							vmScreen.onShow && vmScreen.onShow();

							forEachViewModel(vmScreen, (vm, dom) => {
								vm.onBeforeShow && vm.onBeforeShow();
								dom.hidden = false;
								vm.onShow && vm.onShow();
								autofocus(dom);
							});
						}
						// --

						vmScreen.__cross && vmScreen.__cross.parse(subPart);
					}, 1);
				}
			}
		};


	const
		ViewType = {
			Popup: 'Popups',
			Left: 'Left',
			Right: 'Right',
			Content: 'Content'
		},

		/**
		 * @param {Function} fExecute
		 * @param {(Function|boolean|null)=} fCanExecute = true
		 * @returns {Function}
		 */
		createCommand = (fExecute, fCanExecute) => {
			let fResult = () => {
					fResult.canExecute() && fExecute.call(null);
					return false;
				};
			fResult.enabled = ko.observable(true);
			fResult.canExecute = isFunction(fCanExecute)
				? koComputable(() => fResult.enabled() && fCanExecute())
				: fResult.enabled;
			return fResult;
		},

		/**
		 * @param {Function} ViewModelClassToShow
		 * @param {Array=} params
		 * @returns {void}
		 */
		showScreenPopup = (ViewModelClassToShow, params = []) => {
			const vm = buildViewModel(ViewModelClassToShow) && ViewModelClassToShow.__dom && ViewModelClassToShow.__vm;

			if (vm) {
				params = params || [];

				vm.onBeforeShow && vm.onBeforeShow(...params);

				vm.modalVisibility(true);

				vm.onShow && vm.onShow(...params);
			}
		},

		arePopupsVisible = ko.observable(false),

		/**
		 * @param {Array} screensClasses
		 * @returns {void}
		 */
		startScreens = screensClasses => {
			hasher.clear();
			forEachObjectValue(SCREENS, screen => hideScreen(screen, 1));
			SCREENS = {};
			currentScreen = null,
			defaultScreenName = '';

			screensClasses.forEach(CScreen => {
				if (CScreen) {
					const vmScreen = new CScreen(),
						screenName = vmScreen.screenName;
					defaultScreenName || (defaultScreenName = screenName);
					SCREENS[screenName] = vmScreen;
				}
			});

			forEachObjectValue(SCREENS, vmScreen => vmScreen.onStart());

			const cross = new Crossroads();
			cross.addRoute(/^([a-zA-Z0-9-]*)\/?(.*)$/, screenOnRoute);

			hasher.add(cross.parse.bind(cross));
			hasher.init();

			setTimeout(() => $htmlCL.remove('rl-started-trigger'), 100);

			const c = elementById('rl-content'), l = elementById('rl-loading');
			c && (c.hidden = false);
			l && l.remove();
		},

		decorateKoCommands = (thisArg, commands) =>
			forEachObjectEntry(commands, (key, canExecute) => {
				let command = thisArg[key],
					fn = (...args) => fn.enabled() && fn.canExecute() && command.apply(thisArg, args);

		//		fn.__realCanExecute = canExecute;

				fn.enabled = ko.observable(true);

				fn.canExecute = isFunction(canExecute)
					? koComputable(() => fn.enabled() && canExecute.call(thisArg, thisArg))
					: koComputable(() => fn.enabled());

				thisArg[key] = fn;
			});

	ko.decorateCommands = decorateKoCommands;

	class AbstractView {
		constructor(templateID, type)
		{
	//		Object.defineProperty(this, 'viewModelTemplateID', { value: templateID });
			this.viewModelTemplateID = templateID;
			this.viewType = type;
			this.viewModelDom = null;

			this.keyScope = {
				scope: Scope.None,
				previous: Scope.None,
				set: function() {
					this.previous = keyScope();
					keyScope(this.scope);
				},
				unset: function() {
					keyScope(this.previous);
				}
			};
		}

	/*
		onBuild() {}
		onBeforeShow() {}
		onShow() {}
		onHide() {}
	*/

		querySelector(selectors) {
			return this.viewModelDom.querySelector(selectors);
		}

		addObservables(observables) {
			addObservablesTo(this, observables);
		}

		addComputables(computables) {
			addComputablesTo(this, computables);
		}

		addSubscribables(subscribables) {
			addSubscribablesTo(this, subscribables);
		}

	}

	class AbstractViewPopup extends AbstractView
	{
		constructor(name)
		{
			super('Popups' + name, ViewType.Popup);
			if (name in Scope) {
				this.keyScope.scope = Scope[name];
			}
			this.bDisabeCloseOnEsc = false;
			this.modalVisibility = ko.observable(false).extend({ rateLimit: 0 });
		}
	/*
		onShowWithDelay() {}
		onHideWithDelay() {}

		cancelCommand() {}
		closeCommand() {}
	*/
		/**
		 * @returns {void}
		 */
		registerPopupKeyDown() {
			addEventListener('keydown', event => {
				if (event && this.modalVisibility()) {
					if (!this.bDisabeCloseOnEsc && 'Escape' == event.key) {
						this.cancelCommand();
						return false;
					} else if ('Backspace' == event.key && !inFocus()) {
						return false;
					}
				}

				return true;
			});
		}
	}

	AbstractViewPopup.showModal = function(params = []) {
		showScreenPopup(this, params);
	};

	AbstractViewPopup.hidden = function() {
		return !this.__vm || !this.__vm.modalVisibility();
	};

	class AbstractViewCenter extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Content);
		}
	}

	class AbstractViewLeft extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Left);
			this.leftPanelDisabled = leftPanelDisabled;
		}
	}

	class AbstractViewRight extends AbstractView
	{
		constructor(templateID)
		{
			super(templateID, ViewType.Right);
		}
	}

	/*
	export class AbstractViewSettings
	{
		onBuild(viewModelDom) {}
		onBeforeShow() {}
		onShow() {}
		onHide() {}
		viewModelDom
	}
	*/

	class AbstractViewLogin extends AbstractViewCenter {
		constructor(templateID) {
			super(templateID);
			this.hideSubmitButton = Settings.app('hideSubmitButton');
			this.formError = ko.observable(false).extend({ falseTimeout: 500 });
		}

		onBuild(dom) {
			dom.classList.add('LoginView');
		}

		onShow() {
			rl.route.off();
		}

		submitForm() {
	//		return false;
		}
	}

	class OpenPgpKeyPopupView extends AbstractViewPopup {
		constructor() {
			super('OpenPgpKey');

			this.addObservables({
				key: '',
				keyDom: null
			});
		}

		selectKey() {
			const el = this.keyDom();
			if (el) {
				let sel = getSelection(),
					range = doc.createRange();
				sel.removeAllRanges();
				range.selectNodeContents(el);
				sel.addRange(range);
			}
			if (navigator.clipboard) {
				navigator.clipboard.writeText(this.key()).then(
					() => console.log('Copied to clipboard'),
					err => console.error(err)
				);
			}
		}

		onShow(openPgpKey) {
			// TODO: show more info
			this.key(openPgpKey ? openPgpKey.armor : '');
	/*
			this.key = key;
			const aEmails = [];
			if (key.users) {
				key.users.forEach(user => user.userID.email && aEmails.push(user.userID.email));
			}
			this.id = key.getKeyID().toHex();
			this.fingerprint = key.getFingerprint();
			this.can_encrypt = !!key.getEncryptionKey();
			this.can_sign = !!key.getSigningKey();
			this.emails = aEmails;
			this.armor = armor;
			this.askDelete = ko.observable(false);
			this.openForDeletion = ko.observable(null).askDeleteHelper();

			key.id = key.subkeys[0].keyid;
			key.fingerprint = key.subkeys[0].fingerprint;
			key.uids.forEach(uid => uid.email && aEmails.push(uid.email));
			key.emails = aEmails;
			"disabled": false,
			"expired": false,
			"revoked": false,
			"is_secret": true,
			"can_sign": true,
			"can_decrypt": true
			"can_verify": true
			"can_encrypt": true,
			"uids": [
				{
					"name": "demo",
					"comment": "",
					"email": "demo@snappymail.eu",
					"uid": "demo <demo@snappymail.eu>",
					"revoked": false,
					"invalid": false
				}
			],
			"subkeys": [
				{
					"fingerprint": "2C223F20EA2ADB4CB68F81D95F3A5CDC09AD8AE3",
					"keyid": "5F3A5CDC09AD8AE3",
					"timestamp": 1643381672,
					"expires": 0,
					"is_secret": false,
					"invalid": false,
					"can_encrypt": false,
					"can_sign": true,
					"disabled": false,
					"expired": false,
					"revoked": false,
					"can_certify": true,
					"can_authenticate": false,
					"is_qualified": false,
					"is_de_vs": false,
					"pubkey_algo": 303,
					"length": 256,
					"keygrip": "5A1A6C7310D0508C68E8E74F15068301E83FD1AE",
					"is_cardkey": false,
					"curve": "ed25519"
				},
				{
					"fingerprint": "3CD720549D8833872C267D08F1230DCE2A561ADE",
					"keyid": "F1230DCE2A561ADE",
					"timestamp": 1643381672,
					"expires": 0,
					"is_secret": false,
					"invalid": false,
					"can_encrypt": true,
					"can_sign": false,
					"disabled": false,
					"expired": false,
					"revoked": false,
					"can_certify": false,
					"can_authenticate": false,
					"is_qualified": false,
					"is_de_vs": false,
					"pubkey_algo": 302,
					"length": 256,
					"keygrip": "886921A7E06BE56F8E8C51797BB476BB26DF21BF",
					"is_cardkey": false,
					"curve": "cv25519"
				}
			]
	*/
		}

		onBuild() {
			shortcuts.add('a', 'meta', Scope.OpenPgpKey, () => {
				this.selectKey();
				return false;
			});
		}
	}

	class AskPopupView extends AbstractViewPopup {
		constructor() {
			super('Ask');

			this.addObservables({
				askDesc: '',
				yesButton: '',
				noButton: '',
				passphrase: '',
				askPass: false
			});

			this.fYesAction = null;
			this.fNoAction = null;

			this.focusOnShow = true;
			this.bDisabeCloseOnEsc = true;
		}

		yesClick() {
			this.cancelCommand();

			isFunction(this.fYesAction) && this.fYesAction();
		}

		noClick() {
			this.cancelCommand();

			isFunction(this.fNoAction) && this.fNoAction();
		}

		/**
		 * @param {string} sAskDesc
		 * @param {Function=} fYesFunc
		 * @param {Function=} fNoFunc
		 * @param {boolean=} focusOnShow = true
		 * @returns {void}
		 */
		onShow(sAskDesc, fYesFunc = null, fNoFunc = null, focusOnShow = true, askPass = false, btnText = '') {
			this.askDesc(sAskDesc || '');
			this.askPass(askPass);
			this.passphrase('');
			this.yesButton(i18n(btnText || 'POPUPS_ASK/BUTTON_YES'));
			this.noButton(i18n(askPass ? 'GLOBAL/CANCEL' : 'POPUPS_ASK/BUTTON_NO'));
			this.fYesAction = fYesFunc;
			this.fNoAction = fNoFunc;
			this.focusOnShow = focusOnShow ? (askPass ? 'input[type="password"]' : '.buttonYes') : '';
		}

		onShowWithDelay() {
			this.focusOnShow && this.querySelector(this.focusOnShow).focus();
		}

		onBuild() {
	//		shortcuts.add('tab', 'shift', Scope.Ask, () => {
			shortcuts.add('tab,arrowright,arrowleft', '', Scope.Ask, () => {
				let btn = this.querySelector('.buttonYes');
				if (btn.matches(':focus')) {
					btn = this.querySelector('.buttonNo');
				}
				btn.focus();
				return false;
			});

			shortcuts.add('escape', '', Scope.Ask, () => {
				this.noClick();
				return false;
			});
		}
	}

	AskPopupView.password = function(sAskDesc, btnText) {
		return new Promise(resolve => {
			this.showModal([
				sAskDesc,
				() => resolve(this.__vm.passphrase()),
				() => resolve(null),
				true,
				true,
				btnText
			]);
		});
	};

	const
		askPassphrase$1 = async (privateKey, btnTxt = 'LABEL_SIGN') =>
			await AskPopupView.password('GnuPG key<br>' + privateKey.id + ' ' + privateKey.emails[0], 'OPENPGP/'+btnTxt),

		findGnuPGKey = (keys, query, sign) =>
			keys.find(key =>
				key[sign ? 'can_sign' : 'can_decrypt']
				&& (key.emails.includes(query) || key.subkeys.find(key => query == key.keyid || query == key.fingerprint))
			);

	const GnuPGUserStore = new class {
		constructor() {
			/**
			 * PECL gnupg / PEAR Crypt_GPG
			 * [ {email, can_encrypt, can_sign}, ... ]
			 */
			this.keyring;
			this.publicKeys = ko.observableArray();
			this.privateKeys = ko.observableArray();
		}

		loadKeyrings() {
			this.keyring = null;
			this.publicKeys([]);
			this.privateKeys([]);
			Remote.request('GnupgGetKeys',
				(iError, oData) => {
					if (oData && oData.Result) {
						this.keyring = oData.Result;
						const initKey = (key, isPrivate) => {
							const aEmails = [];
							key.id = key.subkeys[0].keyid;
							key.fingerprint = key.subkeys[0].fingerprint;
							key.uids.forEach(uid => uid.email && aEmails.push(uid.email));
							key.emails = aEmails;
							key.askDelete = ko.observable(false);
							key.openForDeletion = ko.observable(null).askDeleteHelper();
							key.remove = () => {
								if (key.askDelete()) {
									Remote.request('GnupgDeleteKey',
										(iError, oData) => {
											if (oData && oData.Result) {
												if (isPrivate) {
													this.privateKeys.remove(key);
												} else {
													this.publicKeys.remove(key);
												}
												delegateRunOnDestroy(key);
											}
										}, {
											KeyId: key.id,
											isPrivate: isPrivate
										}
									);
								}
							};
							key.view = () => {
								const fetch = pass => Remote.request('GnupgExportKey',
										(iError, oData) => {
											if (oData && oData.Result) {
												key.armor = oData.Result;
												showScreenPopup(OpenPgpKeyPopupView, [key]);
											}
										}, {
											KeyId: key.id,
											isPrivate: isPrivate,
											Passphrase: pass
										}
									);
								if (isPrivate) {
									askPassphrase$1(key, 'POPUP_VIEW_TITLE').then(passphrase => {
										(null !== passphrase) && fetch(passphrase);
									});
								} else {
									fetch('');
								}
							};
							return key;
						};
						this.publicKeys(oData.Result.public.map(key => initKey(key, 0)));
						this.privateKeys(oData.Result.private.map(key => initKey(key, 1)));
						console.log('gnupg ready');
					}
				}
			);
		}

		/**
		 * @returns {boolean}
		 */
		isSupported() {
			return SettingsCapa(Capa.GnuPG);
		}

		importKey(key, callback) {
			Remote.request('GnupgImportKey',
				(iError, oData) => {
					if (oData && oData.Result) ;
					callback && callback(iError, oData);
				}, {
					Key: key
				}
			);
		}

		/**
			keyPair.privateKey
			keyPair.publicKey
			keyPair.revocationCertificate
			keyPair.onServer
			keyPair.inGnuPG
		 */
		storeKeyPair(keyPair, callback) {
			Remote.request('PgpStoreKeyPair',
				(iError, oData) => {
					if (oData && oData.Result) ;
					callback && callback(iError, oData);
				}, keyPair
			);
		}

		/**
		 * Checks if verifying/encrypting a message is possible with given email addresses.
		 */
		hasPublicKeyForEmails(recipients) {
			const count = recipients.length,
				length = count ? recipients.filter(email =>
	//				(key.can_verify || key.can_encrypt) &&
					this.publicKeys.find(key => key.emails.includes(email))
				).length : 0;
			return length && length === count;
		}

		getPublicKeyFingerprints(recipients) {
			const fingerprints = [];
			recipients.forEach(email => {
				fingerprints.push(this.publicKeys.find(key => key.emails.includes(email)).fingerprint);
			});
			return fingerprints;
		}

		getPrivateKeyFor(query, sign) {
			return findGnuPGKey(this.privateKeys, query, sign);
		}

		async decrypt(message) {
			const
				pgpInfo = message.pgpEncrypted();
			if (pgpInfo) {
				let ids = [message.to[0].email].concat(pgpInfo.KeyIds),
					i = ids.length, key;
				while (i--) {
					key = findGnuPGKey(this.privateKeys, ids[i]);
					if (key) {
						break;
					}
				}
				if (key) {
					// Also check message.from[0].email
					let params = {
						Folder: message.folder,
						Uid: message.uid,
						PartId: pgpInfo.PartId,
						KeyId: key.id,
						Passphrase: await askPassphrase$1(key, 'BUTTON_DECRYPT'),
						Data: '' // message.plain() optional
					};
					if (null !== params.Passphrase) {
						const result = await Remote.post('GnupgDecrypt', null, params);
						if (result && result.Result) {
							return result.Result;
						}
					}
				}
			}
		}

		async verify(message) {
			let data = message.pgpSigned(); // { BodyPartId: "1", SigPartId: "2", MicAlg: "pgp-sha256" }
			if (data) {
				data = { ...data }; // clone
	//			const sender = message.from[0].email;
	//			let mode = await this.hasPublicKeyForEmails([sender]);
				data.Folder = message.folder;
				data.Uid = message.uid;
				if (data.BodyPart) {
					data.BodyPart = data.BodyPart.raw;
					data.SigPart = data.SigPart.body;
				}
				let response = await Remote.post('MessagePgpVerify', null, data);
				if (response && response.Result) {
					return {
						fingerprint: response.Result.fingerprint,
						success: 0 == response.Result.status // GOODSIG
					};
				}
			}
		}

		async sign(privateKey) {
			return await askPassphrase$1(privateKey);
		}

	};

	/**
	 * OpenPGP.js
	 */

	const
		findOpenPGPKey = (keys, query/*, sign*/) =>
			keys.find(key =>
				key.emails.includes(query) || query == key.id || query == key.fingerprint
			),

		askPassphrase = async (privateKey, btnTxt = 'LABEL_SIGN') =>
			await AskPopupView.password('OpenPGP.js key<br>' + privateKey.id + ' ' + privateKey.emails[0], 'OPENPGP/'+btnTxt),

		/**
		 * OpenPGP.js v5 removed the localStorage (keyring)
		 * This should be compatible with the old OpenPGP.js v2
		 */
		publicKeysItem = 'openpgp-public-keys',
		privateKeysItem = 'openpgp-private-keys',
		storage = window.localStorage,
		loadOpenPgpKeys = async itemname => {
			let keys = [], key,
				armoredKeys = JSON.parse(storage.getItem(itemname)),
				i = arrayLength(armoredKeys);
			while (i--) {
				key = await openpgp.readKey({armoredKey:armoredKeys[i]});
				if (!key.err) {
					keys.push(new OpenPgpKeyModel(armoredKeys[i], key));
				}
			}
			return keys;
		},
		storeOpenPgpKeys = (keys, section) => {
			let armoredKeys = keys.map(item => item.armor);
			if (armoredKeys.length) {
				storage.setItem(section, JSON.stringify(armoredKeys));
			} else {
				storage.removeItem(section);
			}
		};

	class OpenPgpKeyModel {
		constructor(armor, key) {
			this.key = key;
			const aEmails = [];
			if (key.users) {
				key.users.forEach(user => user.userID.email && aEmails.push(user.userID.email));
			}
			this.id = key.getKeyID().toHex().toUpperCase();
			this.fingerprint = key.getFingerprint();
			this.can_encrypt = !!key.getEncryptionKey();
			this.can_sign = !!key.getSigningKey();
			this.emails = aEmails;
			this.armor = armor;
			this.askDelete = ko.observable(false);
			this.openForDeletion = ko.observable(null).askDeleteHelper();
	//		key.getUserIDs()
	//		key.getPrimaryUser()
		}

		view() {
			showScreenPopup(OpenPgpKeyPopupView, [this]);
		}

		remove() {
			if (this.askDelete()) {
				if (this.key.isPrivate()) {
					OpenPGPUserStore.privateKeys.remove(this);
					storeOpenPgpKeys(OpenPGPUserStore.privateKeys, privateKeysItem);
				} else {
					OpenPGPUserStore.publicKeys.remove(this);
					storeOpenPgpKeys(OpenPGPUserStore.publicKeys, publicKeysItem);
				}
				delegateRunOnDestroy(this);
			}
		}
	/*
		toJSON() {
			return this.armor;
		}
	*/
	}

	const OpenPGPUserStore = new class {
		constructor() {
			this.publicKeys = ko.observableArray();
			this.privateKeys = ko.observableArray();
		}

		loadKeyrings() {
			loadOpenPgpKeys(publicKeysItem).then(keys => {
				this.publicKeys(keys || []);
				console.log('openpgp.js public keys loaded');
			});
			loadOpenPgpKeys(privateKeysItem).then(keys => {
				this.privateKeys(keys || []);
				console.log('openpgp.js private keys loaded');
			});
		}

		/**
		 * @returns {boolean}
		 */
		isSupported() {
			return !!window.openpgp;
		}

		importKey(armoredKey) {
			openpgp.readKey({armoredKey:armoredKey}).then(key => {
				if (!key.err) {
					if (key.isPrivate()) {
						this.privateKeys.push(new OpenPgpKeyModel(armoredKey, key));
						storeOpenPgpKeys(this.privateKeys, privateKeysItem);
					} else {
						this.publicKeys.push(new OpenPgpKeyModel(armoredKey, key));
						storeOpenPgpKeys(this.publicKeys, publicKeysItem);
					}
				}
			});
		}

		/**
			keyPair.privateKey
			keyPair.publicKey
			keyPair.revocationCertificate
		 */
		storeKeyPair(keyPair) {
			openpgp.readKey({armoredKey:keyPair.publicKey}).then(key => {
				this.publicKeys.push(new OpenPgpKeyModel(keyPair.publicKey, key));
				storeOpenPgpKeys(this.publicKeys, publicKeysItem);
			});
			openpgp.readKey({armoredKey:keyPair.privateKey}).then(key => {
				this.privateKeys.push(new OpenPgpKeyModel(keyPair.privateKey, key));
				storeOpenPgpKeys(this.privateKeys, privateKeysItem);
			});
		}

		/**
		 * Checks if verifying/encrypting a message is possible with given email addresses.
		 */
		hasPublicKeyForEmails(recipients) {
			const count = recipients.length,
				length = count ? recipients.filter(email =>
					this.publicKeys().find(key => key.emails.includes(email))
				).length : 0;
			return length && length === count;
		}

		getPrivateKeyFor(query/*, sign*/) {
			return findOpenPGPKey(this.privateKeys, query/*, sign*/);
		}

		/**
		 * https://docs.openpgpjs.org/#encrypt-and-decrypt-string-data-with-pgp-keys
		 */
		async decrypt(armoredText, sender)
		{
			const message = await openpgp.readMessage({ armoredMessage: armoredText }),
				privateKeys = this.privateKeys(),
				msgEncryptionKeyIDs = message.getEncryptionKeyIDs().map(key => key.bytes);
			// Find private key that can decrypt message
			let i = privateKeys.length, privateKey;
			while (i--) {
				if ((await privateKeys[i].key.getDecryptionKeys()).find(
					key => msgEncryptionKeyIDs.includes(key.getKeyID().bytes)
				)) {
					privateKey = privateKeys[i];
					break;
				}
			}
			if (privateKey) try {
				const passphrase = await askPassphrase(privateKey, 'BUTTON_DECRYPT');

				if (null !== passphrase) {
					const
						publicKey = findOpenPGPKey(this.publicKeys, sender/*, sign*/),
						decryptedKey = await openpgp.decryptKey({
							privateKey: privateKey.key,
							passphrase
						});

					return await openpgp.decrypt({
						message,
						verificationKeys: publicKey && publicKey.key,
	//					expectSigned: true,
	//					signature: '', // Detached signature
						decryptionKeys: decryptedKey
					});
				}
			} catch (err) {
				alert(err);
				console.error(err);
			}
		}

		/**
		 * https://docs.openpgpjs.org/#sign-and-verify-cleartext-messages
		 */
		async verify(message) {
			const data = message.pgpSigned(), // { BodyPartId: "1", SigPartId: "2", MicAlg: "pgp-sha256" }
				publicKey = this.publicKeys().find(key => key.emails.includes(message.from[0].email));
			if (data && publicKey) {
				data.Folder = message.folder;
				data.Uid = message.uid;
				data.GnuPG = 0;
				let response;
				if (data.SigPartId) {
					response = await Remote.post('MessagePgpVerify', null, data);
				} else if (data.BodyPart) {
					response = { Result: { text: data.BodyPart.raw, signature: data.SigPart.body } };
				} else {
					response = { Result: { text: message.plain(), signature: null } };
				}
				if (response) {
					const signature = response.Result.signature
						? await openpgp.readSignature({ armoredSignature: response.Result.signature })
						: null;
					const signedMessage = signature
						? await openpgp.createMessage({ text: response.Result.text })
						: await openpgp.readCleartextMessage({ cleartextMessage: response.Result.text });
	//				(signature||signedMessage).getSigningKeyIDs();
					let result = await openpgp.verify({
						message: signedMessage,
						verificationKeys: publicKey.key,
	//					expectSigned: true, // !!detachedSignature
						signature: signature
					});
					return {
						fingerprint: publicKey.fingerprint,
						success: result && !!result.signatures.length
					};
				}
			}
		}

		/**
		 * https://docs.openpgpjs.org/global.html#sign
		 */
		async sign(text, privateKey, detached) {
			const passphrase = await askPassphrase(privateKey);
			if (null !== passphrase) {
				privateKey = await openpgp.decryptKey({
					privateKey: privateKey.key,
					passphrase
				});
				const message = detached
					? await openpgp.createMessage({ text: text })
					: await openpgp.createCleartextMessage({ text: text });
				return await openpgp.sign({
					message: message,
					signingKeys: privateKey,
					detached: !!detached
				});
			}
			throw 'Sign cancelled';
		}

		/**
		 * https://docs.openpgpjs.org/global.html#encrypt
		 */
		async encrypt(text, recipients, signPrivateKey) {
			const count = recipients.length;
			recipients = recipients.map(email => this.publicKeys().find(key => key.emails.includes(email))).filter(key => key);
			if (count === recipients.length) {
				if (signPrivateKey) {
					const passphrase = await askPassphrase(signPrivateKey);
					if (null === passphrase) {
						return;
					}
					signPrivateKey = await openpgp.decryptKey({
						privateKey: signPrivateKey.key,
						passphrase
					});
				}
				return await openpgp.encrypt({
					message: await openpgp.createMessage({ text: text }),
					encryptionKeys: recipients.map(pkey => pkey.key),
					signingKeys: signPrivateKey
	//				signature
				});
			}
			throw 'Encrypt failed';
		}

	};

	const
		BEGIN_PGP_MESSAGE = '-----BEGIN PGP MESSAGE-----',
	//	BEGIN_PGP_SIGNATURE = '-----BEGIN PGP SIGNATURE-----',
	//	BEGIN_PGP_SIGNED = '-----BEGIN PGP SIGNED MESSAGE-----',

		PgpUserStore = new class {
			constructor() {
				// https://mailvelope.github.io/mailvelope/Keyring.html
				this.mailvelopeKeyring = null;
			}

			init() {
				if (SettingsCapa(Capa.OpenPGP) && window.crypto && crypto.getRandomValues) {
					const script = createElement('script', {src:staticLink('js/min/openpgp.min.js')});
					script.onload = () => this.loadKeyrings();
					script.onerror = () => {
						this.loadKeyrings();
						console.error(script.src);
					};
					doc.head.append(script);
				} else {
					this.loadKeyrings();
				}
			}

			loadKeyrings(identifier) {
				identifier = identifier || SettingsGet('Email');
				if (window.mailvelope) {
					const fn = keyring => {
							this.mailvelopeKeyring = keyring;
							console.log('mailvelope ready');
						};
					mailvelope.getKeyring().then(fn, err => {
						if (identifier) {
							// attempt to create a new keyring for this app/user
							mailvelope.createKeyring(identifier).then(fn, err => console.error(err));
						} else {
							console.error(err);
						}
					});
					addEventListener('mailvelope-disconnect', event => {
						alert('Mailvelope is updated to version ' + event.detail.version + '. Reload page');
					}, false);
				} else {
					addEventListener('mailvelope', () => this.loadKeyrings(identifier));
				}

				if (OpenPGPUserStore.isSupported()) {
					OpenPGPUserStore.loadKeyrings();
				}

				if (SettingsCapa(Capa.GnuPG)) {
					GnuPGUserStore.loadKeyrings();
				}
			}

			/**
			 * @returns {boolean}
			 */
			isSupported() {
				return !!(OpenPGPUserStore.isSupported() || GnuPGUserStore.isSupported() || window.mailvelope);
			}

			/**
			 * @returns {boolean}
			 */
			isEncrypted(text) {
				return 0 === text.trim().indexOf(BEGIN_PGP_MESSAGE);
			}

			async mailvelopeHasPublicKeyForEmails(recipients) {
				const
					keyring = this.mailvelopeKeyring,
					mailvelope = keyring && await keyring.validKeyForAddress(recipients)
						/*.then(LookupResult => Object.entries(LookupResult))*/,
					entries = mailvelope && Object.entries(mailvelope);
				return entries && entries.filter(value => value[1]).length === recipients.length;
			}

			/**
			 * Checks if verifying/encrypting a message is possible with given email addresses.
			 * Returns the first library that can.
			 */
			async hasPublicKeyForEmails(recipients) {
				const count = recipients.length;
				if (count) {
					if (GnuPGUserStore.hasPublicKeyForEmails(recipients)) {
						return 'gnupg';
					}
					if (OpenPGPUserStore.hasPublicKeyForEmails(recipients)) {
						return 'openpgp';
					}
				}
				return false;
			}

			async getMailvelopePrivateKeyFor(email/*, sign*/) {
				let keyring = this.mailvelopeKeyring;
				if (keyring && await keyring.hasPrivateKey({email:email})) {
					return ['mailvelope', email];
				}
				return false;
			}

			/**
			 * Checks if signing a message is possible with given email address.
			 * Returns the first library that can.
			 */
			async getKeyForSigning(email) {
				let key = OpenPGPUserStore.getPrivateKeyFor(email, 1);
				if (key) {
					return ['openpgp', key];
				}

				key = GnuPGUserStore.getPrivateKeyFor(email, 1);
				if (key) {
					return ['gnupg', key];
				}

		//		return await this.getMailvelopePrivateKeyFor(email, 1);
			}

			async decrypt(message) {
				const sender = message.from[0].email,
					armoredText = message.plain();

				if (!this.isEncrypted(armoredText)) {
					return;
				}

				// Try OpenPGP.js
				let result = await OpenPGPUserStore.decrypt(armoredText, sender);
				if (result) {
					return result;
				}

				// Try Mailvelope (does not support inline images)
				try {
					let key = await this.getMailvelopePrivateKeyFor(message.to[0].email);
					if (key) {
						/**
						* https://mailvelope.github.io/mailvelope/Mailvelope.html#createEncryptedFormContainer
						* Creates an iframe to display an encrypted form
						*/
		//				mailvelope.createEncryptedFormContainer('#mailvelope-form');
						/**
						* https://mailvelope.github.io/mailvelope/Mailvelope.html#createDisplayContainer
						* Creates an iframe to display the decrypted content of the encrypted mail.
						*/
						const body = message.body;
						body.textContent = '';
						result = await mailvelope.createDisplayContainer(
							'#'+body.id,
							armoredText,
							this.mailvelopeKeyring,
							{
								senderAddress: sender
							}
						);
						if (result) {
							if (result.error && result.error.message) {
								if ('PWD_DIALOG_CANCEL' !== result.error.code) {
									alert(result.error.code + ': ' + result.error.message);
								}
							} else {
								body.classList.add('mailvelope');
								return;
							}
						}
					}
				} catch (err) {
					console.error(err);
				}

				// Now try GnuPG
				return GnuPGUserStore.decrypt(message);
			}

			async verify(message) {
				const signed = message.pgpSigned();
				if (signed) {
					const sender = message.from[0].email,
						gnupg = GnuPGUserStore.hasPublicKeyForEmails([sender]),
						openpgp = OpenPGPUserStore.hasPublicKeyForEmails([sender]);
					// Detached signature use GnuPG first, else we must download whole message
					if (gnupg && signed.SigPartId) {
						return GnuPGUserStore.verify(message);
					}
					if (openpgp) {
						return OpenPGPUserStore.verify(message);
					}
					if (gnupg) {
						return GnuPGUserStore.verify(message);
					}
					// Mailvelope can't
					// https://github.com/mailvelope/mailvelope/issues/434
				}
			}

			/**
			 * Returns headers that should be added to an outgoing email.
			 * So far this is only the autocrypt header.
			 */
		/*
			this.mailvelopeKeyring.additionalHeadersForOutgoingEmail(headers)
			this.mailvelopeKeyring.addSyncHandler(syncHandlerObj)
			this.mailvelopeKeyring.createKeyBackupContainer(selector, options)
			this.mailvelopeKeyring.createKeyGenContainer(selector, {
		//		userIds: [],
				keySize: 4096
			})

			this.mailvelopeKeyring.exportOwnPublicKey(emailAddr).then(<AsciiArmored, Error>)
			this.mailvelopeKeyring.importPublicKey(armored)

			// https://mailvelope.github.io/mailvelope/global.html#SyncHandlerObject
			this.mailvelopeKeyring.addSyncHandler({
				uploadSync
				downloadSync
				backup
				restore
			});
		*/

		};

	class AccountModel extends AbstractModel {
		/**
		 * @param {string} email
		 * @param {boolean=} canBeDelete = true
		 * @param {number=} count = 0
		 */
		constructor(email/*, count = 0*/, isAdditional = true) {
			super();

			this.email = email;

			this.addObservables({
	//			count: count || 0,
				askDelete: false,
				isAdditional: isAdditional
			});
		}

	}

	class IdentityModel extends AbstractModel {
		/**
		 * @param {string} id
		 * @param {string} email
		 */
		constructor(id, email) {
			super();

			this.addObservables({
				id: id || '',
				email: email,
				name: '',

				replyTo: '',
				bcc: '',

				signature: '',
				signatureInsertBefore: false,

				askDelete: false
			});

			this.canBeDeleted = koComputable(() => !!this.id());
		}

		/**
		 * @returns {string}
		 */
		formattedName() {
			const name = this.name(),
				email = this.email();

			return name ? name + ' <' + email + '>' : email;
		}
	}

	class AbstractScreen {
		constructor(screenName, viewModels = []) {
			this.__cross = null;
			this.screenName = screenName;
			this.viewModels = isArray(viewModels) ? viewModels : [];
		}

		/**
		 * @returns {?Array)}
		 */
		routes() {
			return null;
		}

	/*
		onBuild(viewModelDom) {}
		onShow() {}
		onHide() {}
		__started
		__builded
	*/

		/**
		 * @returns {void}
		 */
		onStart() {
			if (!this.__started) {
				this.__started = true;
				const routes = this.routes();
				if (arrayLength(routes)) {
					let route = new Crossroads(),
						fMatcher = (this.onRoute || (()=>0)).bind(this);

					routes.forEach(item => item && route && (route.addRoute(item[0], fMatcher).rules = item[1]));

					this.__cross = route;
				}
			}
		}
	}

	const LanguageStore = {
		languages: ko.observableArray(),
		userLanguage: ko.observable(''),

		populate: function() {
			const aLanguages = Settings.app('languages');
			this.languages(isArray(aLanguages) ? aLanguages : []);
			this.language(SettingsGet('Language'));
			this.userLanguage(SettingsGet('UserLanguage'));
		}
	};

	LanguageStore.language = ko.observable('')
		.extend({ limitedList: LanguageStore.languages });

	class LanguagesPopupView extends AbstractViewPopup {
		constructor() {
			super('Languages');

			this.fLang = null;
			this.userLanguage = ko.observable('');

			this.langs = ko.observableArray();

			this.languages = koComputable(() => {
				const userLanguage = this.userLanguage();
				return this.langs.map(language => ({
					key: language,
					user: language === userLanguage,
					selected: ko.observable(false),
					fullName: convertLangName(language)
				}));
			});

			this.langs.subscribe(() => this.setLanguageSelection());
		}

		languageTooltipName(language) {
			return convertLangName(language, true);
		}

		setLanguageSelection() {
			const currentLang = this.fLang ? ko.unwrap(this.fLang) : '';
			this.languages().forEach(item => item.selected(item.key === currentLang));
		}

		onBeforeShow() {
			this.fLang = null;
			this.userLanguage('');

			this.langs([]);
		}

		onShow(fLanguage, langs, userLanguage) {
			this.fLang = fLanguage;
			this.userLanguage(userLanguage || '');

			this.langs(langs);
		}

		changeLanguage(lang) {
			this.fLang && this.fLang(lang);
			this.cancelCommand();
		}
	}

	const SignMeOff = 0,
		SignMeOn = 1,
		SignMeUnused = 2;


	class LoginUserView extends AbstractViewLogin {
		constructor() {
			super('Login');

			this.addObservables({
				loadingDesc: SettingsGet('LoadingDescription'),

				email: SettingsGet('DevEmail'),
				password: SettingsGet('DevPassword'),
				signMe: false,

				emailError: false,
				passwordError: false,

				submitRequest: false,
				submitError: '',
				submitErrorAddidional: '',

				langRequest: false,

				signMeType: SignMeUnused
			});

			this.allowLanguagesOnLogin = !!SettingsGet('AllowLanguagesOnLogin');

			this.language = LanguageStore.language;
			this.languages = LanguageStore.languages;

			this.bSendLanguage = false;

			this.addComputables({

				languageFullName: () => convertLangName(this.language()),

				signMeVisibility: () => SignMeUnused !== this.signMeType()
			});

			this.addSubscribables({
				email: () => this.emailError(false),

				password: () => this.passwordError(false),

				submitError: value => value || this.submitErrorAddidional(''),

				signMeType: iValue => this.signMe(SignMeOn === iValue),

				language: value => {
					this.langRequest(true);
					translatorReload(false, value).then(
						() => {
							this.langRequest(false);
							this.bSendLanguage = true;
						},
						() => this.langRequest(false)
					);
				}
			});

			if (SettingsGet('AdditionalLoginError') && !this.submitError()) {
				this.submitError(SettingsGet('AdditionalLoginError'));
			}

			decorateKoCommands(this, {
				submitCommand: self => !self.submitRequest()
			});
		}

		submitCommand(self, event) {
			let form = event.target.form,
				data = new FormData(form),
				valid = form.reportValidity() && fireEvent('sm-user-login', data);

			this.emailError(!this.email());
			this.passwordError(!this.password());
			this.formError(!valid);

			if (valid) {
				this.submitRequest(true);
				data.set('Language', this.bSendLanguage ? this.language() : '');
				data.set('SignMe', this.signMe() ? 1 : 0);
				Remote.request('Login',
					(iError, oData) => {
						fireEvent('sm-user-login-response', {
							error: iError,
							data: oData
						});
						if (iError) {
							this.submitRequest(false);
							if (Notification.InvalidInputArgument == iError) {
								iError = Notification.AuthError;
							}
							this.submitError(getNotification(iError, (oData ? oData.ErrorMessage : ''),
								Notification.UnknownNotification));
							this.submitErrorAddidional((oData && oData.ErrorMessageAdditional) || '');
						} else {
							rl.setData(oData.Result);
	//						rl.route.reload();
						}
					},
					data
				);

				set(ClientSideKeyName.LastSignMe, this.signMe() ? '-1-' : '-0-');
			}

			return valid;
		}

		onBuild(dom) {
			super.onBuild(dom);

			const signMeLocal = get(ClientSideKeyName.LastSignMe),
				signMe = (SettingsGet('SignMe') || '').toLowerCase();

			switch (signMe) {
				case 'defaultoff':
				case 'defaulton':
					this.signMeType(
						'defaulton' === signMe ? SignMeOn : SignMeOff
					);

					switch (signMeLocal) {
						case '-1-':
							this.signMeType(SignMeOn);
							break;
						case '-0-':
							this.signMeType(SignMeOff);
							break;
						// no default
					}

					break;
				default:
					this.signMeType(SignMeUnused);
					break;
			}
		}

		selectLanguage() {
			showScreenPopup(LanguagesPopupView, [this.language, this.languages(), LanguageStore.userLanguage()]);
		}
	}

	class LoginUserScreen extends AbstractScreen {
		constructor() {
			super('login', [LoginUserView]);
		}

		onShow() {
			rl.setWindowTitle();
		}
	}

	class KeyboardShortcutsHelpPopupView extends AbstractViewPopup {
		constructor() {
			super('KeyboardShortcutsHelp');
			this.metaKey = shortcuts.getMetaKey();
		}

		onBuild(dom) {
			const tabs = dom.querySelectorAll('.tabs input'),
				last = tabs.length - 1;

	//		shortcuts.add('tab', 'shift',
			shortcuts.add('tab,arrowleft,arrowright', '',
				Scope.KeyboardShortcutsHelp,
				event => {
					let next = 0;
					tabs.forEach((node, index) => {
						if (node.matches(':checked')) {
							if (['Tab','ArrowRight'].includes(event.key)) {
								next = index < last ? index+1 : 0;
							} else {
								next = index ? index-1 : last;
							}
						}
					});
					tabs[next].checked = true;
					return false;
				}
			);
		}
	}

	class AccountPopupView extends AbstractViewPopup {
		constructor() {
			super('Account');

			this.addObservables({
				isNew: true,

				email: '',
				password: '',

				emailError: false,
				passwordError: false,

				submitRequest: false,
				submitError: '',
				submitErrorAdditional: ''
			});

			this.email.subscribe(() => this.emailError(false));

			this.password.subscribe(() => this.passwordError(false));

			decorateKoCommands(this, {
				addAccountCommand: self => !self.submitRequest()
			});
		}

		addAccountCommand() {
			this.emailError(!this.email().trim());
			this.passwordError(!this.password().trim());

			if (this.emailError() || this.passwordError()) {
				return false;
			}

			this.submitRequest(true);

			Remote.request('AccountSetup', (iError, data) => {
					this.submitRequest(false);
					if (iError) {
						this.submitError(getNotification(iError));
						this.submitErrorAdditional((data && data.ErrorMessageAdditional) || '');
					} else {
						rl.app.accountsAndIdentities();
						this.cancelCommand();
					}
				}, {
					Email: this.email(),
					Password: this.password(),
					New: this.isNew() ? 1 : 0
				}
			);

			return true;
		}

		onShow(account) {
			if (account && account.isAdditional()) {
				this.isNew(false);
				this.email(account.email);
			} else {
				this.isNew(true);
				this.email('');
			}
			this.password('');

			this.emailError(false);
			this.passwordError(false);

			this.submitRequest(false);
			this.submitError('');
			this.submitErrorAdditional('');
		}
	}

	/*
		oCallbacks:
			ItemSelect
			MiddleClick
			AutoSelect
			ItemGetUid
			UpOrDown
	*/

	class Selector {
		/**
		 * @param {koProperty} koList
		 * @param {koProperty} koSelectedItem
		 * @param {koProperty} koFocusedItem
		 * @param {string} sItemSelector
		 * @param {string} sItemCheckedSelector
		 * @param {string} sItemFocusedSelector
		 */
		constructor(
			koList,
			koSelectedItem,
			koFocusedItem,
			sItemSelector,
			sItemCheckedSelector,
			sItemFocusedSelector
		) {
			this.list = koList;
			this.listChecked = koComputable(() => this.list.filter(item => item.checked())).extend({ rateLimit: 0 });
			this.isListChecked = koComputable(() => 0 < this.listChecked().length);

			this.focusedItem = koFocusedItem || ko.observable(null);
			this.selectedItem = koSelectedItem || ko.observable(null);

			this.selectedItemUseCallback = true;

			this.iSelectNextHelper = 0;
			this.iFocusedNextHelper = 0;
			this.oContentScrollable;

			this.sItemSelector = sItemSelector;
			this.sItemCheckedSelector = sItemCheckedSelector;
			this.sItemFocusedSelector = sItemFocusedSelector;

			this.sLastUid = '';
			this.oCallbacks = {};

			const itemSelectedThrottle = (item => this.itemSelected(item)).debounce(300);

			this.listChecked.subscribe((items) => {
				if (items.length) {
					if (null === this.selectedItem()) {
						if (this.selectedItem.valueHasMutated) {
							this.selectedItem.valueHasMutated();
						}
					} else {
						this.selectedItem(null);
					}
				} else if (this.autoSelect() && this.focusedItem()) {
					this.selectedItem(this.focusedItem());
				}
			}, this);

			this.selectedItem.subscribe((item) => {
				if (item) {
					if (this.isListChecked()) {
						this.listChecked().forEach(subItem => subItem.checked(false));
					}

					if (this.selectedItemUseCallback) {
						itemSelectedThrottle(item);
					}
				} else if (this.selectedItemUseCallback) {
					this.itemSelected(null);
				}
			}, this);

			this.selectedItem = this.selectedItem.extend({ toggleSubscribeProperty: [this, 'selected'] });
			this.focusedItem = this.focusedItem.extend({ toggleSubscribeProperty: [null, 'focused'] });

			this.focusedItem.subscribe(item => item && (this.sLastUid = this.getItemUid(item)), this);

			let aCache = [],
				aCheckedCache = [],
				mFocused = null,
				mSelected = null;

			this.list.subscribe(
				items => {
					if (isArray(items)) {
						items.forEach(item => {
							if (item) {
								const uid = this.getItemUid(item);

								aCache.push(uid);
								item.checked() && aCheckedCache.push(uid);
								if (null === mFocused && item.focused()) {
									mFocused = uid;
								}
								if (null === mSelected && item.selected()) {
									mSelected = uid;
								}
							}
						});
					}
				},
				this,
				'beforeChange'
			);

			this.list.subscribe((aItems) => {
				let temp = null,
					getNext = false,
					isNextFocused = mFocused,
					isChecked = false,
					isSelected = false,
					len = 0;

				const uids = [];

				this.selectedItemUseCallback = false;

				this.focusedItem(null);
				this.selectedItem(null);

				if (isArray(aItems)) {
					len = aCheckedCache.length;

					aItems.forEach(item => {
						const uid = this.getItemUid(item);
						uids.push(uid);

						if (null !== mFocused && mFocused === uid) {
							this.focusedItem(item);
							mFocused = null;
						}

						if (0 < len && aCheckedCache.includes(uid)) {
							isChecked = true;
							item.checked(true);
							--len;
						}

						if (!isChecked && null !== mSelected && mSelected === uid) {
							isSelected = true;
							this.selectedItem(item);
							mSelected = null;
						}
					});

					this.selectedItemUseCallback = true;

					if (!isChecked && !isSelected && this.autoSelect()) {
						if (this.focusedItem()) {
							this.selectedItem(this.focusedItem());
						} else if (aItems.length) {
							if (null !== isNextFocused) {
								getNext = false;
								isNextFocused = aCache.find(sUid => {
									if (getNext && uids.includes(sUid)) {
										return sUid;
									}
									if (isNextFocused === sUid) {
										getNext = true;
									}
									return false;
								});

								if (isNextFocused) {
									temp = aItems.find(oItem => isNextFocused === this.getItemUid(oItem));
								}
							}

							this.selectedItem(temp || null);
							this.focusedItem(this.selectedItem());
						}
					}

					if (
						(0 !== this.iSelectNextHelper || 0 !== this.iFocusedNextHelper) &&
						aItems.length &&
						!this.focusedItem()
					) {
						temp = null;
						if (0 !== this.iFocusedNextHelper) {
							temp = aItems[-1 === this.iFocusedNextHelper ? aItems.length - 1 : 0] || null;
						}

						if (!temp && 0 !== this.iSelectNextHelper) {
							temp = aItems[-1 === this.iSelectNextHelper ? aItems.length - 1 : 0] || null;
						}

						if (temp) {
							if (0 !== this.iSelectNextHelper) {
								this.selectedItem(temp || null);
							}

							this.focusedItem(temp || null);

							this.scrollToFocused();

							setTimeout(this.scrollToFocused, 100);
						}

						this.iSelectNextHelper = 0;
						this.iFocusedNextHelper = 0;
					}
				}

				aCache = [];
				aCheckedCache = [];
				mFocused = null;
				mSelected = null;
			});
		}

		itemSelected(item) {
			if (this.isListChecked()) {
				item || (this.oCallbacks.ItemSelect || (()=>0))(null);
			} else if (item) {
				(this.oCallbacks.ItemSelect || (()=>0))(item);
			}
		}

		unselect() {
			this.selectedItem(null);
			this.focusedItem(null);
		}

		init(contentScrollable, keyScope = 'all') {
			this.oContentScrollable = contentScrollable;

			if (contentScrollable) {
				let getItem = selector => {
					let el = event.target.closestWithin(selector, contentScrollable);
					return el ? ko.dataFor(el) : null;
				};

				contentScrollable.addEventListener('click', event => {
					let el = event.target.closestWithin(this.sItemSelector, contentScrollable);
					el && this.actionClick(ko.dataFor(el), event);

					const item = getItem(this.sItemCheckedSelector);
					if (item) {
						if (event.shiftKey) {
							this.actionClick(item, event);
						} else {
							this.focusedItem(item);
							item.checked(!item.checked());
						}
					}
				});

				contentScrollable.addEventListener('auxclick', event => {
					if (1 == event.button) {
						const item = getItem(this.sItemSelector);
						if (item) {
							this.focusedItem(item);
							(this.oCallbacks.MiddleClick || (()=>0))(item);
						}
					}
				});

				shortcuts.add('enter,open', '', keyScope, () => {
					const focused = this.focusedItem();
					if (focused && !focused.selected()) {
						this.actionClick(focused);
						return false;
					}
				});

				shortcuts.add('arrowup,arrowdown', 'meta', keyScope, () => false);

				shortcuts.add('arrowup,arrowdown', 'shift', keyScope, event => {
					this.newSelectPosition(event.key, true);
					return false;
				});
				shortcuts.add('arrowup,arrowdown,home,end,pageup,pagedown,space', '', keyScope, event => {
					this.newSelectPosition(event.key, false);
					return false;
				});
			}
		}

		/**
		 * @returns {boolean}
		 */
		autoSelect() {
			return !!(this.oCallbacks.AutoSelect || (()=>1))();
		}

		/**
		 * @param {Object} oItem
		 * @returns {string}
		 */
		getItemUid(item) {
			let uid = '';

			const getItemUidCallback = this.oCallbacks.ItemGetUid || null;
			if (getItemUidCallback && item) {
				uid = getItemUidCallback(item);
			}

			return uid.toString();
		}

		/**
		 * @param {string} sEventKey
		 * @param {boolean} bShiftKey
		 * @param {boolean=} bForceSelect = false
		 */
		newSelectPosition(sEventKey, bShiftKey, bForceSelect) {
			let isArrow = 'ArrowUp' === sEventKey || 'ArrowDown' === sEventKey,
				result = null;

			const pageStep = 10,
				list = this.list(),
				listLen = list.length,
				focused = this.focusedItem();

			if (listLen) {
				if (focused) {
					if (isArrow) {
						let i = list.indexOf(focused);
						if ('ArrowUp' == sEventKey) {
							i > 0 && (result = list[i-1]);
						} else if (++i < listLen) {
							result = list[i];
						}
						result || (this.oCallbacks.UpOrDown || (()=>0))('ArrowUp' === sEventKey);
					} else if ('Home' === sEventKey) {
						result = list[0];
					} else if ('End' === sEventKey) {
						result = list[list.length - 1];
					} else if ('PageDown' === sEventKey) {
						let i = list.indexOf(focused);
						if (i < listLen - 1) {
							result = list[Math.min(i + pageStep, listLen - 1)];
						}
					} else if ('PageUp' === sEventKey) {
						let i = list.indexOf(focused);
						if (i > 0) {
							result = list[Math.max(0, i - pageStep)];
						}
					}
				} else if (
					'Home' == sEventKey ||
					'PageUp' == sEventKey
				) {
					result = list[0];
				} else if (
					'End' === sEventKey ||
					'PageDown' === sEventKey
				) {
					result = list[list.length - 1];
				}
			}

			if (result) {
				this.focusedItem(result);

				if (focused && ((bShiftKey && isArrow) || ' ' === sEventKey)) {
					focused.checked(!focused.checked());
				}

				if (' ' !== sEventKey && (this.autoSelect() || bForceSelect) && !this.isListChecked()) {
					this.selectedItem(result);
				}

				this.scrollToFocused();
			} else if (focused) {
				if ((bShiftKey && isArrow) || ' ' === sEventKey) {
					focused.checked(!focused.checked());
				}

				this.focusedItem(focused);
			}
		}

		/**
		 * @returns {boolean}
		 */
		scrollToFocused() {
			const scrollable = this.oContentScrollable;
			if (scrollable) {
				let block, focused = scrollable.querySelector(this.sItemFocusedSelector);
				if (focused) {
					const fRect = focused.getBoundingClientRect(),
						sRect = scrollable.getBoundingClientRect();
					if (fRect.top < sRect.top) {
						block = 'start';
					} else if (fRect.bottom > sRect.bottom) {
						block = 'end';
					}
					block && focused.scrollIntoView(block === 'start');
				} else {
					scrollable.scrollTop = 0;
				}
			}
		}

		/**
		 * @returns {boolean}
		 */
		scrollToTop() {
			this.oContentScrollable && (this.oContentScrollable.scrollTop = 0);
		}

		eventClickFunction(item, event) {
			let index = 0,
				length = 0,
				changeRange = false,
				isInRange = false,
				list = [],
				checked = false,
				listItem = null,
				lineUid = '';

			const uid = this.getItemUid(item);
			if (event && event.shiftKey) {
				if (uid && this.sLastUid && uid !== this.sLastUid) {
					list = this.list();
					checked = item.checked();

					for (index = 0, length = list.length; index < length; index++) {
						listItem = list[index];
						lineUid = this.getItemUid(listItem);

						changeRange = false;
						if (lineUid === this.sLastUid || lineUid === uid) {
							changeRange = true;
						}

						if (changeRange) {
							isInRange = !isInRange;
						}

						if (isInRange || changeRange) {
							listItem.checked(checked);
						}
					}
				}
			}

			this.sLastUid = uid || '';
		}

		/**
		 * @param {Object} item
		 * @param {Object=} event
		 */
		actionClick(item, event = null) {
			if (item) {
				let click = true;
				if (event) {
					if (event.shiftKey && !(event.ctrlKey || event.metaKey) && !event.altKey) {
						click = false;
						if (!this.sLastUid) {
							this.sLastUid = this.getItemUid(item);
						}

						item.checked(!item.checked());
						this.eventClickFunction(item, event);

						this.focusedItem(item);
					} else if ((event.ctrlKey || event.metaKey) && !event.shiftKey && !event.altKey) {
						click = false;
						this.focusedItem(item);

						if (this.selectedItem() && item !== this.selectedItem()) {
							this.selectedItem().checked(true);
						}

						item.checked(!item.checked());
					}
				}

				if (click) {
					this.selectMessageItem(item);
				}
			}
		}

		on(eventName, callback) {
			this.oCallbacks[eventName] = callback;
		}

		selectMessageItem(messageItem) {
			this.focusedItem(messageItem);
			this.selectedItem(messageItem);
			this.scrollToFocused();
		}
	}

	const trim = text => null == text ? "" : (text + "").trim();

	/**
	 * @enum {number}
	 */
	const ContactPropertyType = {
		Unknown: 0,

		FullName: 10,

		FirstName: 15,
		LastName: 16,
		MiddleName: 17,
		Nick: 18,

		NamePrefix: 20,
		NameSuffix: 21,

		Email: 30,
		Phone: 31,
		Web: 32,

		Birthday: 40,

		Facebook: 90,
		Skype: 91,
		GitHub: 92,

		Note: 110,

		Custom: 250
	};

	class ContactPropertyModel extends AbstractModel {
		/**
		 * @param {number=} type = Enums.ContactPropertyType.Unknown
		 * @param {string=} typeStr = ''
		 * @param {string=} value = ''
		 * @param {boolean=} focused = false
		 * @param {string=} placeholder = ''
		 */
		constructor(type = ContactPropertyType.Unknown, typeStr = '', value = '', focused = false, placeholder = '') {
			super();

			this.addObservables({
				type: pInt(type),
				typeStr: pString(typeStr),
				focused: !!focused,
				value: pString(value),

				placeholder: placeholder
			});

			this.addComputables({
				placeholderValue: () => {
					const v = this.placeholder();
					return v ? i18n(v) : '';
				},

				largeValue: () => ContactPropertyType.Note === this.type()
			});
		}

		isType(type) {
			return this.type && type === this.type();
		}

		isValid() {
			return this.value && !!trim(this.value());
		}

		toJSON() {
			return {
				type: this.type(),
				typeStr: this.typeStr(),
				value: this.value()
			};
		}

	//	static reviveFromJson(json) {}
	}

	class ContactModel extends AbstractModel {
		constructor() {
			super();

			this.id = 0;
			this.display = '';
			this.properties = [];
			this.readOnly = false;

			this.addObservables({
				focused: false,
				selected: false,
				checked: false,
				deleted: false
			});
		}

		/**
		 * @returns {Array|null}
		 */
		getNameAndEmailHelper() {
			let name = '',
				email = '';

			if (arrayLength(this.properties)) {
				this.properties.forEach(property => {
					if (property) {
						if (ContactPropertyType.FirstName === property.type()) {
							name = (property.value() + ' ' + name).trim();
						} else if (ContactPropertyType.LastName === property.type()) {
							name = (name + ' ' + property.value()).trim();
						} else if (!email && ContactPropertyType.Email === property.type()) {
							email = property.value();
						}
					}
				});
			}

			return email ? [email, name] : null;
		}

		/**
		 * @static
		 * @param {FetchJsonContact} json
		 * @returns {?ContactModel}
		 */
		static reviveFromJson(json) {
			const contact = super.reviveFromJson(json);
			if (contact) {
				let list = [];
				if (arrayLength(json.properties)) {
					json.properties.forEach(property => {
						property = ContactPropertyModel.reviveFromJson(property);
						property && list.push(property);
					});
				}
				contact.properties = list;
				contact.initDefaultProperties();
			}
			return contact;
		}

		initDefaultProperties() {
			let list = this.properties;
			list.sort((p1,p2) =>{
				if (p2.type() == ContactPropertyType.FirstName) {
					return 1;
				}
				if (p1.type() == ContactPropertyType.FirstName || p1.type() == ContactPropertyType.LastName) {
					return -1;
				}
				if (p2.type() == ContactPropertyType.LastName) {
					return 1;
				}
				return 0;
			});
			let found = list.find(prop => prop.type() == ContactPropertyType.LastName);
			if (!found) {
				found = new ContactPropertyModel(ContactPropertyType.LastName);
				list.unshift(found);
			}
			found.placeholder('CONTACTS/PLACEHOLDER_ENTER_LAST_NAME');
			found = list.find(prop => prop.type() == ContactPropertyType.FirstName);
			if (!found) {
				found = new ContactPropertyModel(ContactPropertyType.FirstName);
				list.unshift(found);
			}
			found.placeholder('CONTACTS/PLACEHOLDER_ENTER_FIRST_NAME');
			this.properties = list;
		}

		/**
		 * @returns {string}
		 */
		generateUid() {
			return ''+this.id;
		}

		/**
		 * @return string
		 */
		lineAsCss() {
			const result = [];
			if (this.deleted()) {
				result.push('deleted');
			}
			if (this.selected()) {
				result.push('selected');
			}
			if (this.checked()) {
				result.push('checked');
			}
			if (this.focused()) {
				result.push('focused');
			}

			return result.join(' ');
		}
	}

	const CONTACTS_PER_PAGE = 50,
		propertyIsMail = prop => prop.isType(ContactPropertyType.Email),
		propertyIsName = prop => prop.isType(ContactPropertyType.FirstName) || prop.isType(ContactPropertyType.LastName);

	class ContactsPopupView extends AbstractViewPopup {
		constructor() {
			super('Contacts');

			this.bBackToCompose = false;
			this.sLastComposeFocusedField = '';

			this.allowContactsSync = ContactUserStore.allowSync;
			this.enableContactsSync = ContactUserStore.enableSync;

			this.addObservables({
				search: '',
				contactsCount: 0,

				currentContact: null,

				importUploaderButton: null,

				contactsPage: 1,

				emptySelection: true,
				viewClearSearch: false,

				viewID: '',
				viewReadOnly: false,

				viewSaveTrigger: SaveSettingsStep.Idle,

				viewSaving: false,

				watchDirty: false,
				watchHash: false
			});

			this.contacts = ContactUserStore;

			this.viewProperties = ko.observableArray();

			this.useCheckboxesInList = SettingsUserStore.useCheckboxesInList;

			this.selector = new Selector(
				ContactUserStore,
				this.currentContact,
				null,
				'.e-contact-item .actionHandle',
				'.e-contact-item .checkboxItem',
				'.e-contact-item.focused'
			);

			this.selector.on('ItemSelect', contact => {
				this.populateViewContact(contact);
				if (!contact) {
					this.emptySelection(true);
				}
			});

			this.selector.on('ItemGetUid', contact => contact ? contact.generateUid() : '');

			this.bDropPageAfterDelete = false;

			// this.saveCommandDebounce = this.saveCommand.bind(this).debounce(1000);

			const
	//			propertyFocused = property => !property.isValid() && !property.focused(),
				pagecount = () => Math.max(1, Math.ceil(this.contactsCount() / CONTACTS_PER_PAGE));

			this.addComputables({
				contactsPageCount: pagecount,

				contactsPaginator: computedPaginatorHelper(this.contactsPage, pagecount),

				viewPropertiesNames: () => this.viewProperties.filter(propertyIsName),

				viewPropertiesEmails: () => this.viewProperties.filter(propertyIsMail),

				viewPropertiesOther: () => this.viewProperties.filter(property => property.isType(ContactPropertyType.Nick)),

				viewPropertiesWeb: () => this.viewProperties.filter(property => property.isType(ContactPropertyType.Web)),

				viewPropertiesPhones: () => this.viewProperties.filter(property => property.isType(ContactPropertyType.Phone)),

				contactHasValidName: () => !!this.viewProperties.find(prop => propertyIsName(prop) && prop.isValid()),

				contactsCheckedOrSelected: () => {
					const checked = ContactUserStore.filter(item => item.checked && item.checked()),
						selected = this.currentContact();

					return selected
						? [...checked, selected].unique()
						: checked;
				},

				contactsCheckedOrSelectedUids: () => this.contactsCheckedOrSelected().map(contact => contact.id),

				viewHash: () => '' + this.viewProperties.map(property => property.value && property.value()).join('')
			});

			this.search.subscribe(() => this.reloadContactList());

			this.viewHash.subscribe(() => {
				if (this.watchHash() && !this.viewReadOnly() && !this.watchDirty()) {
					this.watchDirty(true);
				}
			});

			decorateKoCommands(this, {
				newCommand: 1,
				deleteCommand: self => 0 < self.contactsCheckedOrSelected().length,
				newMessageCommand: self => 0 < self.contactsCheckedOrSelected().length,
				clearCommand: 1,
				saveCommand: self => !self.viewSaving() && !self.viewReadOnly()
					&& (self.contactHasValidName() || self.viewProperties.find(prop => propertyIsMail(prop) && prop.isValid())),
				syncCommand: self => !self.contacts.syncing() && !self.contacts.importing()
			});
		}

		newCommand() {
			this.populateViewContact(null);
			this.currentContact(null);
		}

		deleteCommand() {
			this.deleteSelectedContacts();
			this.emptySelection(true);
		}

		newMessageCommand() {
			let aE = [],
				toEmails = null,
				ccEmails = null,
				bccEmails = null;

			const aC = this.contactsCheckedOrSelected();
			if (arrayLength(aC)) {
				aE = aC.map(oItem => {
					if (oItem) {
						const data = oItem.getNameAndEmailHelper(),
							email = data ? new EmailModel(data[0], data[1]) : null;

						if (email && email.validate()) {
							return email;
						}
					}

					return null;
				});

				aE = aE.filter(value => !!value);
			}

			if (arrayLength(aE)) {
				this.bBackToCompose = false;

				this.closeCommand();

				switch (this.sLastComposeFocusedField) {
					case 'cc':
						ccEmails = aE;
						break;
					case 'bcc':
						bccEmails = aE;
						break;
					default:
						toEmails = aE;
						break;
				}

				this.sLastComposeFocusedField = '';

				setTimeout(() =>
					showMessageComposer([ComposeType.Empty, null, toEmails, ccEmails, bccEmails])
				, 200);
			}

			return true;
		}

		clearCommand() {
			this.search('');
		}

		saveCommand() {
			this.viewSaving(true);
			this.viewSaveTrigger(SaveSettingsStep.Animate);

			const requestUid = Jua.randomId();

			Remote.request('ContactSave',
				(iError, oData) => {
					let res = false;
					this.viewSaving(false);

					if (
						!iError &&
						oData.Result.RequestUid === requestUid &&
						0 < pInt(oData.Result.ResultID)
					) {
						if (!this.viewID()) {
							this.viewID(pInt(oData.Result.ResultID));
						}

						this.reloadContactList(); // TODO: remove when e-contact-foreach is dynamic
						res = true;
					}

					setTimeout(() =>
						this.viewSaveTrigger(res ? SaveSettingsStep.TrueResult : SaveSettingsStep.FalseResult)
					, 350);

					if (res) {
						this.watchDirty(false);
						setTimeout(() => this.viewSaveTrigger(SaveSettingsStep.Idle), 1000);
					}
				}, {
					RequestUid: requestUid,
					Uid: this.viewID(),
					Properties: this.viewProperties.map(oItem => oItem.toJSON())
				}
			);
		}

		syncCommand() {
			ContactUserStore.sync(iError => {
				iError && alert(getNotification(iError));

				this.reloadContactList(true);
			});
		}

		getPropertyPlaceholder(type) {
			let result = '';
			switch (type) {
				case ContactPropertyType.LastName:
					result = 'CONTACTS/PLACEHOLDER_ENTER_LAST_NAME';
					break;
				case ContactPropertyType.FirstName:
					result = 'CONTACTS/PLACEHOLDER_ENTER_FIRST_NAME';
					break;
				case ContactPropertyType.Nick:
					result = 'CONTACTS/PLACEHOLDER_ENTER_NICK_NAME';
					break;
				// no default
			}

			return result;
		}

		addNewProperty(type, typeStr) {
			this.viewProperties.push(
				new ContactPropertyModel(type, typeStr || '', '', true, this.getPropertyPlaceholder(type))
			);
		}

		addNewOrFocusProperty(type, typeStr) {
			const item = this.viewProperties.find(prop => prop.isType(type));
			if (item) {
				item.focused(true);
			} else {
				this.addNewProperty(type, typeStr);
			}
		}

		addNewEmail() {
			this.addNewProperty(ContactPropertyType.Email, 'Home');
		}

		addNewPhone() {
			this.addNewProperty(ContactPropertyType.Phone, 'Mobile');
		}

		addNewWeb() {
			this.addNewProperty(ContactPropertyType.Web);
		}

		addNewNickname() {
			this.addNewOrFocusProperty(ContactPropertyType.Nick);
		}

		addNewNotes() {
			this.addNewOrFocusProperty(ContactPropertyType.Note);
		}

		addNewBirthday() {
			this.addNewOrFocusProperty(ContactPropertyType.Birthday);
		}

		exportVcf() {
			download(serverRequestRaw('ContactsVcf'), 'contacts.vcf');
		}

		exportCsv() {
			download(serverRequestRaw('ContactsCsv'), 'contacts.csv');
		}

		removeCheckedOrSelectedContactsFromList() {
			const contacts = this.contactsCheckedOrSelected();

			let currentContact = this.currentContact(),
				count = ContactUserStore.length;

			if (contacts.length) {
				contacts.forEach(contact => {
					if (currentContact && currentContact.id === contact.id) {
						currentContact = null;
						this.currentContact(null);
					}

					contact.deleted(true);
					--count;
				});

				if (0 >= count) {
					this.bDropPageAfterDelete = true;
				}

				setTimeout(() => {
					contacts.forEach(contact => {
						ContactUserStore.remove(contact);
						delegateRunOnDestroy(contact);
					});
				}, 500);
			}
		}

		deleteSelectedContacts() {
			if (this.contactsCheckedOrSelected().length) {
				Remote.request('ContactsDelete',
					(iError, oData) => {
						if (500 < (!iError && oData && oData.Time ? pInt(oData.Time) : 0)) {
							this.reloadContactList(this.bDropPageAfterDelete);
						} else {
							setTimeout(() => this.reloadContactList(this.bDropPageAfterDelete), 500);
						}
					}, {
						Uids: this.contactsCheckedOrSelectedUids().join(',')
					}
				);
				this.removeCheckedOrSelectedContactsFromList();
			}
		}

		removeProperty(oProp) {
			this.viewProperties.remove(oProp);
			delegateRunOnDestroy(oProp);
		}

		/**
		 * @param {?ContactModel} contact
		 */
		populateViewContact(contact) {
			let id = '';

			this.watchHash(false);

			this.emptySelection(false);
			this.viewReadOnly(false);

			if (contact) {
				id = contact.id;
				this.viewReadOnly(!!contact.readOnly);
			} else {
				contact = new ContactModel;
				contact.initDefaultProperties();
			}

			this.viewID(id);

	//		delegateRunOnDestroy(this.viewProperties());
	//		this.viewProperties([]);
			this.viewProperties(contact.properties);

			this.watchDirty(false);
			this.watchHash(true);
		}

		/**
		 * @param {boolean=} dropPagePosition = false
		 */
		reloadContactList(dropPagePosition = false) {
			let offset = (this.contactsPage() - 1) * CONTACTS_PER_PAGE;

			this.bDropPageAfterDelete = false;

			if (dropPagePosition) {
				this.contactsPage(1);
				offset = 0;
			}

			ContactUserStore.loading(true);
			Remote.request('Contacts',
				(iError, data) => {
					let count = 0,
						list = [];

					if (!iError && arrayLength(data.Result.List)) {
						data.Result.List.forEach(item => {
							item = ContactModel.reviveFromJson(item);
							item && list.push(item);
						});

						count = pInt(data.Result.Count);
						count = 0 < count ? count : 0;
					}

					this.contactsCount(count);

					delegateRunOnDestroy(ContactUserStore());
					ContactUserStore(list);

					ContactUserStore.loading(false);
					this.viewClearSearch(!!this.search());
				},
				{
					Offset: offset,
					Limit: CONTACTS_PER_PAGE,
					Search: this.search()
				},
				null,
				'',
				['Contacts']
			);
		}

		onBuild(dom) {
			this.selector.init(dom.querySelector('.b-list-content'), Scope.Contacts);

			shortcuts.add('delete', '', Scope.Contacts, () => {
				this.deleteCommand();
				return false;
			});

			shortcuts.add('c,w', '', Scope.Contacts, () => {
				this.newMessageCommand();
				return false;
			});

			const self = this;

			dom.addEventListener('click', event => {
				let el = event.target.closestWithin('.e-paginator a', dom);
				if (el && ko.dataFor(el)) {
					self.contactsPage(pInt(ko.dataFor(el).value));
					self.reloadContactList();
				}
			});

			// initUploader

			if (this.importUploaderButton()) {
				const j = new Jua({
					action: serverRequest('UploadContacts'),
					limit: 1,
					disableDocumentDropPrevent: true,
					clickElement: this.importUploaderButton()
				});

				if (j) {
					j.on('onStart', () => {
						ContactUserStore.importing(true);
					}).on('onComplete', (id, result, data) => {
						ContactUserStore.importing(false);
						this.reloadContactList();
						if (!id || !result || !data || !data.Result) {
							alert(i18n('CONTACTS/ERROR_IMPORT_FILE'));
						}
					});
				}
			}
		}

		onShow(bBackToCompose, sLastComposeFocusedField) {
			this.bBackToCompose = !!bBackToCompose;
			this.sLastComposeFocusedField = sLastComposeFocusedField;

			rl.route.off();
			this.reloadContactList(true);
		}

		onHide() {
			rl.route.on();

			this.currentContact(null);
			this.emptySelection(true);
			this.search('');
			this.contactsCount(0);

			delegateRunOnDestroy(ContactUserStore());
			ContactUserStore([]);

			this.sLastComposeFocusedField = '';

			if (this.bBackToCompose) {
				this.bBackToCompose = false;

				showMessageComposer();
			}
		}
	}

	//import { clearCache } from 'Common/Cache';
	//import { koComputable } from 'External/ko';

	class SystemDropDownUserView extends AbstractViewRight {
		constructor() {
			super('SystemDropDown');

			this.allowAccounts = Settings.capa(Capa.AdditionalAccounts);

			this.accountEmail = AccountUserStore.email;

			this.accounts = AccountUserStore.accounts;
			this.accountsLoading = AccountUserStore.loading;
	/*
			this.accountsUnreadCount = : koComputable(() => 0);
			this.accountsUnreadCount = : koComputable(() => AccountUserStore.accounts().reduce((result, item) => result + item.count(), 0));
	*/

			this.addObservables({
				currentAudio: '',
				accountMenuDropdownTrigger: false
			});

			this.allowContacts = AppUserStore.allowContacts();

			addEventListener('audio.stop', () => this.currentAudio(''));
			addEventListener('audio.start', e => this.currentAudio(e.detail));
		}

		stopPlay() {
			fireEvent('audio.api.stop');
		}

		accountClick(account, event) {
			if (account && 0 === event.button) {
				AccountUserStore.loading(true);
				event.preventDefault();
				event.stopPropagation();
				Remote.request('AccountSwitch',
					(iError/*, oData*/) => {
						if (iError) {
							AccountUserStore.loading(false);
							alert(getNotification(iError).replace('%EMAIL%', account.email));
							if (account.isAdditional()) {
								showScreenPopup(AccountPopupView, [account]);
							}
						} else {
	/*						// Not working yet
							forEachObjectEntry(oData.Result, (key, value) => rl.settings.set(key, value));
							clearCache();
	//						MessageUserStore.setMessage();
	//						MessageUserStore.purgeMessageBodyCache();
	//						MessageUserStore.hideMessageBodies();
							MessageUserStore.list([]);
	//						FolderUserStore.folderList([]);
							Remote.foldersReload(value => {
								if (value) {
	//								4. Change to INBOX = reload MessageList
	//								MessageUserStore.setMessageList();
								}
							});
							AccountUserStore.loading(false);
	*/
	//						rl.route.reload();
							location.reload();
						}
					}, {Email:account.email}
				);
			}
			return true;
		}

		emailTitle() {
			return AccountUserStore.email();
		}

		settingsClick() {
			rl.route.setHash(settings());
		}

		settingsHelp() {
			showScreenPopup(KeyboardShortcutsHelpPopupView);
		}

		addAccountClick() {
			this.allowAccounts && showScreenPopup(AccountPopupView);
		}

		contactsClick() {
			this.allowContacts && showScreenPopup(ContactsPopupView);
		}

		toggleLayout()
		{
			const mobile = !ThemeStore.isMobile();
			doc.cookie = 'rllayout=' + (mobile ? 'mobile' : 'desktop') + '; samesite=strict';
			ThemeStore.isMobile(mobile);
			leftPanelDisabled(mobile);
		}

		logoutClick() {
			rl.app.logout();
		}

		onBuild() {
			shortcuts.add('m,contextmenu', '', [Scope.MessageList, Scope.MessageView, Scope.Settings], () => {
				if (!this.viewModelDom.hidden) {
					MessageUserStore.messageFullScreenMode(false);
					this.accountMenuDropdownTrigger(true);
					return false;
				}
			});

			// shortcuts help
			shortcuts.add('?,f1,help', '', [Scope.MessageList, Scope.MessageView, Scope.Settings], () => {
				if (!this.viewModelDom.hidden) {
					showScreenPopup(KeyboardShortcutsHelpPopupView);
					return false;
				}
			});
		}
	}

	class FolderCreatePopupView extends AbstractViewPopup {
		constructor() {
			super('FolderCreate');

			this.addObservables({
				folderName: '',
				folderSubscribe: SettingsUserStore.hideUnsubscribed(),

				selectedParentValue: UNUSED_OPTION_VALUE
			});

			this.parentFolderSelectList = koComputable(() =>
				folderListOptionsBuilder(
					[],
					[['', '']],
					oItem =>
						oItem ? (oItem.isSystemFolder() ? oItem.name() + ' ' + oItem.manageFolderSystemName() : oItem.name()) : '',
					FolderUserStore.namespace
						? item => FolderUserStore.namespace !== item.fullName.slice(0, FolderUserStore.namespace.length)
						: null,
					true
				)
			);

			this.defaultOptionsAfterRender = defaultOptionsAfterRender;

			decorateKoCommands(this, {
				createFolderCommand: self => self.simpleFolderNameValidation(self.folderName())
			});
		}

		createFolderCommand() {
			let parentFolderName = this.selectedParentValue();
			if (!parentFolderName && 1 < FolderUserStore.namespace.length) {
				parentFolderName = FolderUserStore.namespace.slice(0, FolderUserStore.namespace.length - 1);
			}

			Remote.abort('Folders').post('FolderCreate', FolderUserStore.foldersCreating, {
					Folder: this.folderName(),
					Parent: parentFolderName,
					Subscribe: this.folderSubscribe() ? 1 : 0
				})
				.then(
					data => {
						const folder = getFolderFromCacheList(parentFolderName),
							subFolder = FolderModel.reviveFromJson(data.Result),
							folders = (folder ? folder.subFolders : FolderUserStore.folderList);
						setFolder(subFolder);
						folders.push(subFolder);
						sortFolders(folders);
	/*
						var collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
						console.log((folder ? folder.subFolders : FolderUserStore.folderList).sort(collator.compare));
	*/
					},
					error => {
						FolderUserStore.folderListError(
							getNotification(error.code, '', Notification.CantCreateFolder)
							+ '.\n' + error.message);
					}
				);

			this.cancelCommand();
		}

		simpleFolderNameValidation(sName) {
			return /^[^\\/]+$/g.test(sName);
		}

		onShow() {
			this.folderName('');
			this.selectedParentValue('');
		}
	}

	/**
	 * @param {string} sFullName
	 * @param {boolean} bExpanded
	 */
	function setExpandedFolder(sFullName, bExpanded) {
		let aExpandedList = get(ClientSideKeyName.ExpandedFolders);
		if (!isArray(aExpandedList)) {
			aExpandedList = [];
		}

		if (bExpanded) {
			if (!aExpandedList.includes(sFullName))
				aExpandedList.push(sFullName);
		} else {
			aExpandedList = aExpandedList.filter(value => value !== sFullName);
		}

		set(ClientSideKeyName.ExpandedFolders, aExpandedList);
	}

	class MailFolderList extends AbstractViewLeft {
		constructor() {
			super('MailFolderList');

			this.oContentScrollable = null;

			this.composeInEdit = AppUserStore.composeInEdit;

			this.folderList = FolderUserStore.folderList;
			this.folderListSystem = FolderUserStore.folderListSystem;
			this.foldersChanging = FolderUserStore.foldersChanging;

			this.moveAction = moveAction;

			this.foldersListWithSingleInboxRootFolder = ko.observable(false);

			this.allowContacts = AppUserStore.allowContacts();

			addComputablesTo(this, {
				folderListVisible: () => {
					let multiple = false,
						inbox, visible,
						result = FolderUserStore.folderList().filter(folder => {
							if (folder.isInbox()) {
								inbox = folder;
							}
							visible = folder.visible();
							multiple |= visible && !folder.isInbox();
							return visible;
						});
					if (inbox && !multiple) {
						inbox.collapsed(false);
					}
					this.foldersListWithSingleInboxRootFolder(!multiple);
					return result;
				}
			});
		}

		onBuild(dom) {
			const qs = s => dom.querySelector(s),
				eqs = (ev, s) => ev.target.closestWithin(s, dom);

			this.oContentScrollable = qs('.b-content');

			dom.addEventListener('click', event => {
				let el = eqs(event, '.e-collapsed-sign');
				if (el) {
					const folder = ko.dataFor(el);
					if (folder) {
						const collapsed = folder.collapsed();
						setExpandedFolder(folder.fullName, collapsed);

						folder.collapsed(!collapsed);
						event.preventDefault();
						event.stopPropagation();
						return;
					}
				}

				el = eqs(event, 'a');
				if (el && el.matches('.selectable')) {
					event.preventDefault();
					const folder = ko.dataFor(el);
					if (folder) {
						if (moveAction()) {
							moveAction(false);
							rl.app.moveMessagesToFolder(
								FolderUserStore.currentFolderFullName(),
								MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
								folder.fullName,
								event.ctrlKey
							);
						} else {
							if (!SettingsUserStore.usePreviewPane()) {
								MessageUserStore.message(null);
							}

							if (folder.fullName === FolderUserStore.currentFolderFullName()) {
								setFolderHash(folder.fullName, '');
							}

							rl.route.setHash(
								mailBox(folder.fullNameHash, 1,
									(event.target.matches('.flag-icon') && !folder.isFlagged()) ? 'flagged' : ''
								)
							);
						}

						AppUserStore.focusedState(Scope.MessageList);
					}
				}
			});

			shortcuts.add('arrowup,arrowdown', '', Scope.FolderList, event => {
				let items = [], index = 0;
				dom.querySelectorAll('li a:not(.hidden)').forEach(node => {
					if (node.offsetHeight || node.getClientRects().length) {
						items.push(node);
						if (node.matches('.focused')) {
							node.classList.remove('focused');
							index = items.length - 1;
						}
					}
				});
				if (items.length) {
					if ('ArrowUp' === event.key) {
						index && --index;
					} else if (index < items.length - 1) {
						++index;
					}
					items[index].classList.add('focused');
					this.scrollToFocused();
				}

				return false;
			});

			shortcuts.add('enter,open', '', Scope.FolderList, () => {
				const item = qs('li a:not(.hidden).focused');
				if (item) {
					AppUserStore.focusedState(Scope.MessageList);
					item.click();
				}

				return false;
			});

			shortcuts.add('space', '', Scope.FolderList, () => {
				const item = qs('li a:not(.hidden).focused'),
					folder = item && ko.dataFor(item);
				if (folder) {
					const collapsed = folder.collapsed();
					setExpandedFolder(folder.fullName, collapsed);
					folder.collapsed(!collapsed);
				}

				return false;
			});

	//		shortcuts.add('tab', 'shift', Scope.FolderList, () => {
			shortcuts.add('escape,tab,arrowright', '', Scope.FolderList, () => {
				AppUserStore.focusedState(Scope.MessageList);
				moveAction(false);
				return false;
			});

			AppUserStore.focusedState.subscribe(value => {
				let el = qs('li a.focused');
				el && el.classList.remove('focused');
				if (Scope.FolderList === value) {
					el = qs('li a.selected');
					el && el.classList.add('focused');
				}
			});
		}

		scrollToFocused() {
			const scrollable = this.oContentScrollable;
			if (scrollable) {
				let block, focused = scrollable.querySelector('li a.focused');
				if (focused) {
					const fRect = focused.getBoundingClientRect(),
						sRect = scrollable.getBoundingClientRect();
					if (fRect.top < sRect.top) {
						block = 'start';
					} else if (fRect.bottom > sRect.bottom) {
						block = 'end';
					}
					block && focused.scrollIntoView(block === 'start');
				}
			}
		}

		composeClick() {
			showMessageComposer();
		}

		createFolder() {
			showScreenPopup(FolderCreatePopupView);
		}

		configureFolders() {
			rl.route.setHash(settings('folders'));
		}

		contactsClick() {
			if (this.allowContacts) {
				showScreenPopup(ContactsPopupView);
			}
		}
	}

	class FolderClearPopupView extends AbstractViewPopup {
		constructor() {
			super('FolderClear');

			this.addObservables({
				selectedFolder: null,
				clearingProcess: false,
				clearingError: ''
			});

			this.addComputables({
				folderFullNameForClear: () => {
					const folder = this.selectedFolder();
					return folder ? folder.printableFullName() : '';
				},

				folderNameForClear: () => {
					const folder = this.selectedFolder();
					return folder ? folder.localName() : '';
				},

				dangerDescHtml: () => i18n('POPUPS_CLEAR_FOLDER/DANGER_DESC_HTML_1', { FOLDER: this.folderNameForClear() })
			});

			decorateKoCommands(this, {
				clearCommand: self => {
						const folder = self.selectedFolder();
						return !self.clearingProcess() && null !== folder;
					}
			});
		}

		clearCommand() {
			const folderToClear = this.selectedFolder();
			if (folderToClear) {
				MessageUserStore.message(null);
				MessageUserStore.list([]);

				this.clearingProcess(true);

				folderToClear.messageCountAll(0);
				folderToClear.messageCountUnread(0);

				setFolderHash(folderToClear.fullName, '');

				Remote.request('FolderClear', iError => {
					this.clearingProcess(false);
					if (iError) {
						this.clearingError(getNotification(iError));
					} else {
						rl.app.reloadMessageList(true);
						this.cancelCommand();
					}
				}, {
					Folder: folderToClear.fullName
				});
			}
		}

		onShow(folder) {
			this.clearingProcess(false);
			this.selectedFolder(folder || null);
		}
	}

	class AdvancedSearchPopupView extends AbstractViewPopup {
		constructor() {
			super('AdvancedSearch');

			this.addObservables({
				from: '',
				to: '',
				subject: '',
				text: '',
				selectedDateValue: -1,
				selectedTreeValue: '',

				hasAttachment: false,
				starred: false,
				unseen: false
			});

			this.showMultisearch = koComputable(() => FolderUserStore.hasCapability('MULTISEARCH'));

			this.selectedDates = koComputable(() => {
				trigger();
				let prefix = 'SEARCH/LABEL_ADV_DATE_';
				return [
					{ id: -1, name: i18n(prefix + 'ALL') },
					{ id: 3, name: i18n(prefix + '3_DAYS') },
					{ id: 7, name: i18n(prefix + '7_DAYS') },
					{ id: 30, name: i18n(prefix + 'MONTH') },
					{ id: 90, name: i18n(prefix + '3_MONTHS') },
					{ id: 180, name: i18n(prefix + '6_MONTHS') },
					{ id: 365, name: i18n(prefix + 'YEAR') }
				];
			});

			this.selectedTree = koComputable(() => {
				trigger();
				let prefix = 'SEARCH/LABEL_ADV_SUBFOLDERS_';
				return [
					{ id: '', name: i18n(prefix + 'NONE') },
					{ id: 'subtree-one', name: i18n(prefix + 'SUBTREE_ONE') },
					{ id: 'subtree', name: i18n(prefix + 'SUBTREE') }
				];
			});

			decorateKoCommands(this, {
				searchCommand: 1
			});
		}

		searchCommand() {
			const search = this.buildSearchString();
			if (search) {
				MessageUserStore.mainMessageListSearch(search);
			}

			this.cancelCommand();
		}

		parseSearchStringValue(search) {
			const parts = (search || '').split(/[\s]+/g);
			parts.forEach(part => {
				switch (part) {
					case 'has:attachment':
						this.hasAttachment(true);
						break;
					case 'is:unseen,flagged':
						this.starred(true);
					/* falls through */
					case 'is:unseen':
						this.unseen(true);
						break;
					// no default
				}
			});
		}

		buildSearchString() {
			const
				data = new FormData(),
				append = (key, value) => value.length && data.append(key, value);

			append('from', this.from().trim());
			append('to', this.to().trim());
			append('subject', this.subject().trim());
			append('text', this.text().trim());
			append('in', this.selectedTreeValue());
			if (-1 < this.selectedDateValue()) {
				let d = new Date();
				d.setDate(d.getDate() - this.selectedDateValue());
				append('date', d.format('Y.m.d') + '/');
			}

			let result = new URLSearchParams(data).toString();

			if (this.hasAttachment()) {
				result += '&attachment';
			}
			if (this.unseen()) {
				result += '&unseen';
			}
			if (this.starred()) {
				result += '&flagged';
			}

			return result.replace(/^&+/, '');
		}

		onShow(search) {
			this.from('');
			this.to('');
			this.subject('');
			this.text('');

			this.selectedDateValue(-1);
			this.hasAttachment(false);
			this.starred(false);
			this.unseen(false);

			this.parseSearchStringValue(search);
		}
	}

	const
		canBeMovedHelper = () => MessageUserStore.hasCheckedOrSelected();

	class MailMessageList extends AbstractViewRight {
		constructor() {
			super('MailMessageList');

			this.bPrefetch = false;
			this.emptySubjectValue = '';

			this.iGoToUpOrDownTimeout = 0;

			this.newMoveToFolder = !!SettingsGet('NewMoveToFolder');

			this.allowSearch = SettingsCapa(Capa.Search);
			this.allowSearchAdv = SettingsCapa(Capa.SearchAdv);
			this.allowDangerousActions = SettingsCapa(Capa.DangerousActions);

			this.messageList = MessageUserStore.list;

			this.composeInEdit = AppUserStore.composeInEdit;

			this.isMobile = ThemeStore.isMobile;
			this.leftPanelDisabled = leftPanelDisabled;

			this.messageListSearch = MessageUserStore.listSearch;
			this.messageListError = MessageUserStore.listError;

			this.popupVisibility = arePopupsVisible;

			this.useCheckboxesInList = SettingsUserStore.useCheckboxesInList;

			this.messageListThreadUid = MessageUserStore.listEndThreadUid;

			this.messageListIsLoading = MessageUserStore.listIsLoading;

			initOnStartOrLangChange(() => this.emptySubjectValue = i18n('MESSAGE_LIST/EMPTY_SUBJECT_TEXT'));

			this.userUsageProc = FolderUserStore.quotaPercentage;

			this.addObservables({
				moveDropdownTrigger: false,
				moreDropdownTrigger: false,
				sortDropdownTrigger: false,

				dragOverArea: null,
				dragOverBodyArea: null,

				inputMessageListSearchFocus: false
			});

			// append drag and drop
			this.dragOver = ko.observable(false).extend({ throttle: 1 });
			this.dragOverEnter = ko.observable(false).extend({ throttle: 1 });

			this.sLastSearchValue = '';

			this.addComputables({

				sortSupported: () =>
					FolderUserStore.hasCapability('SORT') | FolderUserStore.hasCapability('ESORT'),

				folderMenuForMove: () =>
					folderListOptionsBuilder(
						[FolderUserStore.currentFolderFullName()],
						[],
						item => item ? item.localName() : ''
					),

				messageListSearchDesc: () => {
					const value = MessageUserStore.list().Search;
					return value ? i18n('MESSAGE_LIST/SEARCH_RESULT_FOR', { SEARCH: value }) : ''
				},

				messageListPaginator: computedPaginatorHelper(MessageUserStore.listPage,
					MessageUserStore.listPageCount),

				checkAll: {
					read: () => 0 < MessageUserStore.listChecked().length,
					write: (value) => {
						value = !!value;
						MessageUserStore.list.forEach(message => message.checked(value));
					}
				},

				inputProxyMessageListSearch: {
					read: MessageUserStore.mainMessageListSearch,
					write: value => this.sLastSearchValue = value
				},

				isIncompleteChecked: () => {
					const c = MessageUserStore.listChecked().length;
					return c && MessageUserStore.list.length > c;
				},

				hasMessages: () => 0 < MessageUserStore.list().length,

				isSpamFolder: () => (FolderUserStore.spamFolder() || 0) === MessageUserStore.list().Folder,

				isSpamDisabled: () => UNUSED_OPTION_VALUE === FolderUserStore.spamFolder(),

				isTrashFolder: () => (FolderUserStore.trashFolder() || 0) === MessageUserStore.list().Folder,

				isDraftFolder: () => (FolderUserStore.draftsFolder() || 0) === MessageUserStore.list().Folder,

				isSentFolder: () => (FolderUserStore.sentFolder() || 0) === MessageUserStore.list().Folder,

				isArchiveFolder: () => (FolderUserStore.archiveFolder() || 0) === MessageUserStore.list().Folder,

				isArchiveDisabled: () => UNUSED_OPTION_VALUE === FolderUserStore.archiveFolder(),

				isArchiveVisible: () => !this.isArchiveFolder() && !this.isArchiveDisabled() && !this.isDraftFolder(),

				isSpamVisible: () =>
					!this.isSpamFolder() && !this.isSpamDisabled() && !this.isDraftFolder() && !this.isSentFolder(),

				isUnSpamVisible: () =>
					this.isSpamFolder() && !this.isSpamDisabled() && !this.isDraftFolder() && !this.isSentFolder(),

				mobileCheckedStateShow: () => ThemeStore.isMobile() ? 0 < MessageUserStore.listChecked().length : true,

				mobileCheckedStateHide: () => ThemeStore.isMobile() ? !MessageUserStore.listChecked().length : true,

				sortText: () => {
					let mode = FolderUserStore.sortMode(),
						desc = '' === mode || mode.includes('REVERSE');
					mode = mode.split(/\s+/);
					if (mode.includes('FROM')) {
						 return '@' + (desc ? '⬆' : '⬇');
					}
					if (mode.includes('SUBJECT')) {
						 return '𝐒' + (desc ? '⬆' : '⬇');
					}
					return (mode.includes('SIZE') ? '✉' : '📅') + (desc ? '⬇' : '⬆');
				}
			});

			this.hasCheckedOrSelectedLines = MessageUserStore.hasCheckedOrSelected,

			this.selector = new Selector(
				MessageUserStore.list,
				MessageUserStore.selectorMessageSelected,
				MessageUserStore.selectorMessageFocused,
				'.messageListItem .actionHandle',
				'.messageListItem .checkboxMessage',
				'.messageListItem.focused'
			);

			this.selector.on('ItemSelect', message => MessageUserStore.selectMessage(message));

			this.selector.on('MiddleClick', message => MessageUserStore.populateMessageBody(message, true));

			this.selector.on('ItemGetUid', message => (message ? message.generateUid() : ''));

			this.selector.on('AutoSelect', () => this.useAutoSelect());

			this.selector.on('UpOrDown', v => this.goToUpOrDown(v));

			addEventListener('mailbox.message-list.selector.go-down',
				e => this.selector.newSelectPosition('ArrowDown', false, e.detail)
			);

			addEventListener('mailbox.message-list.selector.go-up',
				e => this.selector.newSelectPosition('ArrowUp', false, e.detail)
			);

			addEventListener('mailbox.message.show', e => {
				const sFolder = e.detail.Folder, iUid = e.detail.Uid;

				const message = MessageUserStore.list.find(
					item => item && sFolder === item.folder && iUid == item.uid
				);

				if ('INBOX' === sFolder) {
					rl.route.setHash(mailBox(sFolder));
				}

				if (message) {
					this.selector.selectMessageItem(message);
				} else {
					if ('INBOX' !== sFolder) {
						rl.route.setHash(mailBox(sFolder));
					}

					MessageUserStore.selectMessageByFolderAndUid(sFolder, iUid);
				}
			});

			MessageUserStore.listEndHash.subscribe((() =>
				this.selector.scrollToFocused()
			).throttle(50));

			decorateKoCommands(this, {
				clearCommand: 1,
				reloadCommand: 1,
				multyForwardCommand: canBeMovedHelper,
				deleteWithoutMoveCommand: canBeMovedHelper,
				deleteCommand: canBeMovedHelper,
				archiveCommand: canBeMovedHelper,
				spamCommand: canBeMovedHelper,
				notSpamCommand: canBeMovedHelper,
				moveCommand: canBeMovedHelper,
				moveNewCommand: canBeMovedHelper,
			});
		}

		changeSort(self, event) {
			FolderUserStore.sortMode(event.target.closest('li').dataset.sort);
			this.reloadCommand();
		}

		clearCommand() {
			if (SettingsCapa(Capa.DangerousActions)) {
				showScreenPopup(FolderClearPopupView, [FolderUserStore.currentFolder()]);
			}
		}

		reloadCommand() {
			if (!MessageUserStore.listIsLoading()) {
				rl.app.reloadMessageList(false, true);
			}
		}

		multyForwardCommand() {
			showMessageComposer([
				ComposeType.ForwardAsAttachment,
				MessageUserStore.listCheckedOrSelected()
			]);
		}

		deleteWithoutMoveCommand() {
			if (SettingsCapa(Capa.DangerousActions)) {
				rl.app.deleteMessagesFromFolder(
					FolderType.Trash,
					FolderUserStore.currentFolderFullName(),
					MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
					false
				);
			}
		}

		deleteCommand() {
			rl.app.deleteMessagesFromFolder(
				FolderType.Trash,
				FolderUserStore.currentFolderFullName(),
				MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
				true
			);
		}

		archiveCommand() {
			rl.app.deleteMessagesFromFolder(
				FolderType.Archive,
				FolderUserStore.currentFolderFullName(),
				MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
				true
			);
		}

		spamCommand() {
			rl.app.deleteMessagesFromFolder(
				FolderType.Spam,
				FolderUserStore.currentFolderFullName(),
				MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
				true
			);
		}

		notSpamCommand() {
			rl.app.deleteMessagesFromFolder(
				FolderType.NotSpam,
				FolderUserStore.currentFolderFullName(),
				MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
				true
			);
		}

		moveCommand() {}

		moveNewCommand(vm, event) {
			if (this.newMoveToFolder && this.mobileCheckedStateShow()) {
				if (vm && event && event.preventDefault) {
					event.preventDefault();
					event.stopPropagation();
				}

				let b = moveAction();
				AppUserStore.focusedState(b ? Scope.MessageList : Scope.FolderList);
				moveAction(!b);
			}
		}

		composeClick() {
			showMessageComposer();
		}

		goToUpOrDown(up) {
			if (MessageUserStore.listChecked().length) {
				return false;
			}

			clearTimeout(this.iGoToUpOrDownTimeout);
			this.iGoToUpOrDownTimeout = setTimeout(() => {
				let prev, next, temp, current;

				this.messageListPaginator().find(item => {
					if (item) {
						if (current) {
							next = item;
						}

						if (item.current) {
							current = item;
							prev = temp;
						}

						if (next) {
							return true;
						}

						temp = item;
					}

					return false;
				});

				if (!SettingsUserStore.usePreviewPane() && !MessageUserStore.message()) {
					this.selector.iFocusedNextHelper = up ? -1 : 1;
				} else {
					this.selector.iSelectNextHelper = up ? -1 : 1;
				}

				if (up ? prev : next) {
					this.selector.unselect();
					this.gotoPage(up ? prev : next);
				}
			}, 350);

			return true;
		}

		useAutoSelect() {
			return !MessageUserStore.listDisableAutoSelect()
				&& !/is:unseen/.test(MessageUserStore.mainMessageListSearch())
				&& SettingsUserStore.usePreviewPane();
		}

		searchEnterAction() {
			MessageUserStore.mainMessageListSearch(this.sLastSearchValue);
			this.inputMessageListSearchFocus(false);
		}

		cancelSearch() {
			MessageUserStore.mainMessageListSearch('');
			this.inputMessageListSearchFocus(false);
		}

		cancelThreadUid() {
			rl.route.setHash(
				mailBox(
					FolderUserStore.currentFolderFullNameHash(),
					MessageUserStore.listPageBeforeThread(),
					MessageUserStore.listSearch()
				)
			);
		}

		/**
		 * @param {string} sToFolderFullName
		 * @param {boolean} bCopy
		 * @returns {boolean}
		 */
		moveSelectedMessagesToFolder(sToFolderFullName, bCopy) {
			if (MessageUserStore.hasCheckedOrSelected()) {
				rl.app.moveMessagesToFolder(
					FolderUserStore.currentFolderFullName(),
					MessageUserStore.listCheckedOrSelectedUidsWithSubMails(),
					sToFolderFullName,
					bCopy
				);
			}

			return false;
		}

		getDragData(event) {
			const item = ko.dataFor(doc.elementFromPoint(event.clientX, event.clientY));
			item && item.checked && item.checked(true);
			const uids = MessageUserStore.listCheckedOrSelectedUidsWithSubMails();
			item && !uids.includes(item.uid) && uids.push(item.uid);
			return uids.length ? {
				copy: event.ctrlKey,
				folder: FolderUserStore.currentFolderFullName(),
				uids: uids
			} : null;
		}

		/**
		 * @param {string} sFolderFullName
		 * @param {number} iSetAction
		 * @param {Array=} aMessages = null
		 * @returns {void}
		 */
		setAction(sFolderFullName, iSetAction, aMessages) {
			rl.app.messageListAction(sFolderFullName, iSetAction, aMessages);
		}

		listSetSeen() {
			this.setAction(
				FolderUserStore.currentFolderFullName(),
				MessageSetAction.SetSeen,
				MessageUserStore.listCheckedOrSelected()
			);
		}

		listSetAllSeen() {
			let sFolderFullName = FolderUserStore.currentFolderFullName(),
				iThreadUid = MessageUserStore.listEndThreadUid();
			if (sFolderFullName) {
				let cnt = 0;
				const uids = [];

				let folder = getFolderFromCacheList(sFolderFullName);
				if (folder) {
					MessageUserStore.list.forEach(message => {
						if (message.isUnseen()) {
							++cnt;
						}

						message.flags.push('\\seen');
	//					message.flags.valueHasMutated();
						uids.push(message.uid);
					});

					if (iThreadUid) {
						folder.messageCountUnread(Math.max(0, folder.messageCountUnread() - cnt));
					} else {
						folder.messageCountUnread(0);
					}

					MessageFlagsCache.clearFolder(sFolderFullName);

					Remote.messageSetSeenToAll(sFolderFullName, true, iThreadUid ? uids : null);

					MessageUserStore.reloadFlagsAndCachedMessage();
				}
			}
		}

		listUnsetSeen() {
			this.setAction(
				FolderUserStore.currentFolderFullName(),
				MessageSetAction.UnsetSeen,
				MessageUserStore.listCheckedOrSelected()
			);
		}

		listSetFlags() {
			this.setAction(
				FolderUserStore.currentFolderFullName(),
				MessageSetAction.SetFlag,
				MessageUserStore.listCheckedOrSelected()
			);
		}

		listUnsetFlags() {
			this.setAction(
				FolderUserStore.currentFolderFullName(),
				MessageSetAction.UnsetFlag,
				MessageUserStore.listCheckedOrSelected()
			);
		}

		flagMessages(currentMessage) {
			const checked = MessageUserStore.listCheckedOrSelected();
			if (currentMessage) {
				const checkedUids = checked.map(message => message.uid);
				if (checkedUids.includes(currentMessage.uid)) {
					this.setAction(
						currentMessage.folder,
						currentMessage.isFlagged() ? MessageSetAction.UnsetFlag : MessageSetAction.SetFlag,
						checked
					);
				} else {
					this.setAction(
						currentMessage.folder,
						currentMessage.isFlagged() ? MessageSetAction.UnsetFlag : MessageSetAction.SetFlag,
						[currentMessage]
					);
				}
			}
		}

		flagMessagesFast(bFlag) {
			const checked = MessageUserStore.listCheckedOrSelected();
			if (checked.length) {
				if (undefined === bFlag) {
					const flagged = checked.filter(message => message.isFlagged());
					this.setAction(
						checked[0].folder,
						checked.length === flagged.length ? MessageSetAction.UnsetFlag : MessageSetAction.SetFlag,
						checked
					);
				} else {
					this.setAction(
						checked[0].folder,
						!bFlag ? MessageSetAction.UnsetFlag : MessageSetAction.SetFlag,
						checked
					);
				}
			}
		}

		seenMessagesFast(seen) {
			const checked = MessageUserStore.listCheckedOrSelected();
			if (checked.length) {
				if (undefined === seen) {
					const unseen = checked.filter(message => message.isUnseen());
					this.setAction(
						checked[0].folder,
						unseen.length ? MessageSetAction.SetSeen : MessageSetAction.UnsetSeen,
						checked
					);
				} else {
					this.setAction(
						checked[0].folder,
						seen ? MessageSetAction.SetSeen : MessageSetAction.UnsetSeen,
						checked
					);
				}
			}
		}

		gotoPage(page) {
			page && rl.route.setHash(
				mailBox(
					FolderUserStore.currentFolderFullNameHash(),
					page.value,
					MessageUserStore.listSearch(),
					MessageUserStore.listThreadUid()
				)
			);
		}

		gotoThread(message) {
			if (message && 0 < message.threadsLen()) {
				MessageUserStore.listPageBeforeThread(MessageUserStore.listPage());

				rl.route.setHash(
					mailBox(FolderUserStore.currentFolderFullNameHash(), 1, MessageUserStore.listSearch(), message.uid)
				);
			}
		}

		listEmptyMessage() {
			if (!this.dragOver()
			 && !MessageUserStore.list().length
			 && !MessageUserStore.listIsLoading()
			 && !MessageUserStore.listError()) {
				 return i18n('MESSAGE_LIST/EMPTY_' + (MessageUserStore.listSearch() ? 'SEARCH_' : '') + 'LIST');
			}
			return '';
		}

		clearListIsVisible() {
			return (
				!this.messageListSearchDesc() &&
				!MessageUserStore.listError() &&
				!MessageUserStore.listEndThreadUid() &&
				MessageUserStore.list().length &&
				(this.isSpamFolder() || this.isTrashFolder())
			);
		}

		onBuild(dom) {
			const eqs = (ev, s) => ev.target.closestWithin(s, dom);

			this.selector.init(dom.querySelector('.b-content'), Scope.MessageList);

			dom.addEventListener('click', event => {
				ThemeStore.isMobile() && !eqs(event, '.toggleLeft') && leftPanelDisabled(true);

				if (eqs(event, '.messageList') && Scope.MessageView === AppUserStore.focusedState()) {
					AppUserStore.focusedState(Scope.MessageList);
				}

				let el = eqs(event, '.e-paginator a');
				el && this.gotoPage(ko.dataFor(el));

				eqs(event, '.checkboxCheckAll') && this.checkAll(!this.checkAll());

				el = eqs(event, '.flagParent');
				el && this.flagMessages(ko.dataFor(el));

				el = eqs(event, '.threads-len');
				el && this.gotoThread(ko.dataFor(el));
			});

			dom.addEventListener('dblclick', event => {
				let  el = eqs(event, '.actionHandle');
				el && this.gotoThread(ko.dataFor(el));
			});

			// initUploaderForAppend

			if (Settings.app('allowAppendMessage') && this.dragOverArea()) {
				const oJua = new Jua({
					action: serverRequest('Append'),
					name: 'AppendFile',
					limit: 1,
					hidden: {
						Folder: () => FolderUserStore.currentFolderFullName()
					},
					dragAndDropElement: this.dragOverArea(),
					dragAndDropBodyElement: this.dragOverBodyArea()
				});

				this.dragOver.subscribe(value => value && this.selector.scrollToTop());

				oJua
					.on('onDragEnter', () => this.dragOverEnter(true))
					.on('onDragLeave', () => this.dragOverEnter(false))
					.on('onBodyDragEnter', () => this.dragOver(true))
					.on('onBodyDragLeave', () => this.dragOver(false))
					.on('onSelect', (sUid, oData) => {
						if (sUid && oData && 'message/rfc822' === oData.Type) {
							MessageUserStore.listLoading(true);
							return true;
						}

						return false;
					})
					.on('onComplete', () => rl.app.reloadMessageList(true, true));
			}

			// initShortcuts

			shortcuts.add('enter,open', '', Scope.MessageList, () => {
				if (MessageUserStore.message() && this.useAutoSelect()) {
					fireEvent('mailbox.message-view.toggle-full-screen');
					return false;
				}
			});

			// archive (zip)
			shortcuts.add('z', '', [Scope.MessageList, Scope.MessageView], () => {
				this.archiveCommand();
				return false;
			});

			// delete
			shortcuts.add('delete', 'shift', Scope.MessageList, () => {
				MessageUserStore.listCheckedOrSelected().length && this.deleteWithoutMoveCommand();
				return false;
			});
	//		shortcuts.add('3', 'shift', Scope.MessageList, () => {
			shortcuts.add('delete', '', Scope.MessageList, () => {
				MessageUserStore.listCheckedOrSelected().length && this.deleteCommand();
				return false;
			});

			// check mail
			shortcuts.add('r', 'meta', [Scope.FolderList, Scope.MessageList, Scope.MessageView], () => {
				this.reloadCommand();
				return false;
			});

			// check all
			shortcuts.add('a', 'meta', Scope.MessageList, () => {
				this.checkAll(!(this.checkAll() && !this.isIncompleteChecked()));
				return false;
			});

			// write/compose (open compose popup)
			shortcuts.add('w,c,new', '', [Scope.MessageList, Scope.MessageView], () => {
				showMessageComposer();
				return false;
			});

			// important - star/flag messages
			shortcuts.add('i', '', [Scope.MessageList, Scope.MessageView], () => {
				this.flagMessagesFast();
				return false;
			});

			shortcuts.add('t', '', [Scope.MessageList], () => {
				let message = MessageUserStore.selectorMessageSelected();
				if (!message) {
					message = MessageUserStore.selectorMessageFocused();
				}

				if (message && 0 < message.threadsLen()) {
					this.gotoThread(message);
				}

				return false;
			});

			// move
			shortcuts.add('insert', '', Scope.MessageList, () => {
				if (this.newMoveToFolder) {
					this.moveNewCommand();
				} else {
					this.moveDropdownTrigger(true);
				}

				return false;
			});

			// read
			shortcuts.add('q', '', [Scope.MessageList, Scope.MessageView], () => {
				this.seenMessagesFast(true);
				return false;
			});

			// unread
			shortcuts.add('u', '', [Scope.MessageList, Scope.MessageView], () => {
				this.seenMessagesFast(false);
				return false;
			});

			shortcuts.add('f,mailforward', 'shift', [Scope.MessageList, Scope.MessageView], () => {
				this.multyForwardCommand();
				return false;
			});

			if (SettingsCapa(Capa.Search)) {
				// search input focus
				shortcuts.add('/', '', [Scope.MessageList, Scope.MessageView], () => {
					this.inputMessageListSearchFocus(true);
					return false;
				});
			}

			// cancel search
			shortcuts.add('escape', '', Scope.MessageList, () => {
				if (this.messageListSearchDesc()) {
					this.cancelSearch();
					return false;
				} else if (MessageUserStore.listEndThreadUid()) {
					this.cancelThreadUid();
					return false;
				}
			});

			// change focused state
			shortcuts.add('tab', 'shift', Scope.MessageList, () => {
				AppUserStore.focusedState(Scope.FolderList);
				return false;
			});
			shortcuts.add('arrowleft', '', Scope.MessageList, () => {
				AppUserStore.focusedState(Scope.FolderList);
				return false;
			});
			shortcuts.add('tab,arrowright', '', Scope.MessageList, () => {
				if (MessageUserStore.message()){
					AppUserStore.focusedState(Scope.MessageView);
					return false;
				}
			});

			shortcuts.add('arrowleft', 'meta', Scope.MessageView, ()=>false);
			shortcuts.add('arrowright', 'meta', Scope.MessageView, ()=>false);

			if (!ThemeStore.isMobile() && SettingsCapa(Capa.Prefetch)) {
				ifvisible.idle(this.prefetchNextTick.bind(this));
			}
		}

		prefetchNextTick() {
			if (!this.bPrefetch && !ifvisible.now() && !this.viewModelDom.hidden) {
				const message = MessageUserStore.list.find(
					item => item && !hasRequestedMessage(item.folder, item.uid)
				);
				if (message) {
					this.bPrefetch = true;

					addRequestedMessage(message.folder, message.uid);

					Remote.message(
						iError => {
							const next = !iError;
							setTimeout(() => {
								this.bPrefetch = false;
								next && this.prefetchNextTick();
							}, 1000);
						},
						message.folder,
						message.uid
					);
				}
			}
		}

		advancedSearchClick() {
			SettingsCapa(Capa.SearchAdv)
				&& showScreenPopup(AdvancedSearchPopupView, [MessageUserStore.mainMessageListSearch()]);
		}

		quotaTooltip() {
			return i18n('MESSAGE_LIST/QUOTA_SIZE', {
				SIZE: FileInfo.friendlySize(FolderUserStore.quotaUsage()),
				PROC: FolderUserStore.quotaPercentage(),
				LIMIT: FileInfo.friendlySize(FolderUserStore.quotaLimit())
			}).replace(/<[^>]+>/g, '');
		}
	}

	const
		// RFC2045
		QPDecodeIn = /=([0-9A-F]{2})/g,
		QPDecodeOut = (...args) => String.fromCharCode(parseInt(args[1], 16));

	function ParseMime(text)
	{
		class MimePart
		{
			header(name) {
				return this.headers && this.headers[name];
			}

			headerValue(name) {
				return (this.header(name) || {value:null}).value;
			}

			get raw() {
				return text.slice(this.start, this.end);
			}

			get bodyRaw() {
				return text.slice(this.bodyStart, this.bodyEnd);
			}

			get body() {
				let body = this.bodyRaw,
					encoding = this.headerValue('content-transfer-encoding');
				if ('quoted-printable' == encoding) {
					body = body.replace(/=\r?\n/g, '').replace(QPDecodeIn, QPDecodeOut);
				} else if ('base64' == encoding) {
					body = atob(body.replace(/\r?\n/g, ''));
				}
				return body;
			}

			get dataUrl() {
				let body = this.bodyRaw,
					encoding = this.headerValue('content-transfer-encoding');
				if ('base64' == encoding) {
					body = body.replace(/\r?\n/g, '');
				} else {
					if ('quoted-printable' == encoding) {
						body = body.replace(/=\r?\n/g, '').replace(QPDecodeIn, QPDecodeOut);
					}
					body = btoa(body);
				}
				return 'data:' + this.headerValue('content-type') + ';base64,' + body;
			}

			forEach(fn) {
				fn(this);
				if (this.parts) {
					this.parts.forEach(part => part.forEach(fn));
				}
			}

			getByContentType(type) {
				if (type == this.headerValue('content-type')) {
					return this;
				}
				if (this.parts) {
					let i = 0, p = this.parts, part;
					for (i; i < p.length; ++i) {
						if ((part = p[i].getByContentType(type))) {
							return part;
						}
					}
				}
			}
		}

		const ParsePart = (mimePart, start_pos = 0, id = '') =>
		{
			let part = new MimePart;
			if (id) {
				part.id = id;
				part.start = start_pos;
				part.end = start_pos + mimePart.length;
			}

			// get headers
			let head = mimePart.match(/^[\s\S]+?\r?\n\r?\n/);
			if (head) {
				head = head[0];
				let headers = {};
				head.replace(/\r?\n\s+/g, ' ').split(/\r?\n/).forEach(header => {
					let match = header.match(/^([^:]+):\s*([^;]+)/),
						params = {};
					[...header.matchAll(/;\s*([^;=]+)=\s*"?([^;"]+)"?/g)].forEach(param =>
						params[param[1].trim().toLowerCase()] = param[2].trim()
					);
					headers[match[1].trim().toLowerCase()] = {
						value: match[2].trim(),
						params: params
					};
				});
				part.headers = headers;

				// get body
				part.bodyStart = start_pos + head.length;
				part.bodyEnd = start_pos + mimePart.length;

				// get child parts
				let boundary = headers['content-type'].params.boundary;
				if (boundary) {
					part.boundary = boundary;
					part.parts = [];
					let regex = new RegExp('(?:^|\r?\n)--' + boundary + '(?:--)?(?:\r?\n|$)', 'g'),
						body = mimePart.slice(head.length),
						bodies = body.split(regex),
						pos = part.bodyStart;
					[...body.matchAll(regex)].forEach(([boundary], index) => {
						if (!index) {
							// Mostly something like: "This is a multi-part message in MIME format."
							part.bodyText = bodies[0];
						}
						// Not the end?
						if ('--' != boundary.trim().slice(-2)) {
							pos += bodies[index].length + boundary.length;
							part.parts.push(ParsePart(bodies[1+index], pos, ((id ? id + '.' : '') + (1+index))));
						}
					});
				}
			}

			return part;
		};

		return ParsePart(text);
	}

	/**
	 * @param string data
	 * @param MessageModel message
	 */
	function MimeToMessage(data, message)
	{
		let signed;
		const struct = ParseMime(data);
		if (struct.headers) {
			let html = struct.getByContentType('text/html');
			html = html ? html.body : '';

			struct.forEach(part => {
				let cd = part.header('content-disposition'),
					cid = part.header('content-id'),
					type = part.header('content-type');
				if (cid || cd) {
					// if (cd && 'attachment' === cd.value) {
					let attachment = new AttachmentModel;
					attachment.mimeType = type.value;
					attachment.fileName = type.name || (cd && cd.params.filename) || '';
					attachment.fileNameExt = attachment.fileName.replace(/^.+(\.[a-z]+)$/, '$1');
					attachment.fileType = FileInfo.getType('', type.value);
					attachment.url = part.dataUrl;
					attachment.friendlySize = FileInfo.friendlySize(part.body.length);
	/*
					attachment.isThumbnail = false;
					attachment.contentLocation = '';
					attachment.download = '';
					attachment.folder = '';
					attachment.uid = '';
					attachment.mimeIndex = part.id;
					attachment.framed = false;
	*/
					attachment.cid = cid ? cid.value : '';
					if (cid && html) {
						let cid = 'cid:' + attachment.contentId(),
							found = html.includes(cid);
						attachment.isInline(found);
						attachment.isLinked(found);
						found && (html = html
							.replace('src="' + cid + '"', 'src="' + attachment.url + '"')
							.replace("src='" + cid + "'", "src='" + attachment.url + "'")
						);
					} else {
						message.attachments.push(attachment);
					}
				} else if ('multipart/signed' === type.value && 'application/pgp-signature' === type.params.protocol) {
					signed = {
						MicAlg: type.micalg,
						BodyPart: part.parts[0],
						SigPart: part.parts[1]
					};
				}
			});

			const text = struct.getByContentType('text/plain');
			message.plain(text ? text.body : '');
			message.html(html);
		} else {
			message.plain(data);
		}

		if (!signed && message.plain().includes(BEGIN_PGP_MESSAGE)) {
			signed = true;
		}
		message.pgpSigned(signed);

		// TODO: Verify instantly?
	}

	const
		oMessageScrollerDom = () => elementById('messageItem') || {},

		currentMessage = () => MessageUserStore.message();

	class MailMessageView extends AbstractViewRight {
		constructor() {
			super('MailMessageView');

			const
				createCommandReplyHelper = type =>
					createCommand(() => {
						this.lastReplyAction(type);
						this.replyOrforward(type);
					}, this.canBeRepliedOrForwarded),

				createCommandActionHelper = (folderType, useFolder) =>
					createCommand(() => {
						const message = currentMessage();
						if (message) {
							MessageUserStore.message(null);
							rl.app.deleteMessagesFromFolder(folderType, message.folder, [message.uid], useFolder);
						}
					}, this.messageVisibility);

			this.addObservables({
				showAttachmentControls: false,
				downloadAsZipLoading: false,
				lastReplyAction_: '',
				showFullInfo: '1' === get(ClientSideKeyName.MessageHeaderFullInfo),
				moreDropdownTrigger: false,

				// viewer
				viewFromShort: '',
				viewFromDkimData: ['none', ''],
				viewToShort: ''
			});

			this.moveAction = moveAction;

			this.allowMessageActions = SettingsCapa(Capa.MessageActions);

			const attachmentsActions = Settings.app('attachmentsActions');
			this.attachmentsActions = ko.observableArray(arrayLength(attachmentsActions) ? attachmentsActions : []);

			this.message = MessageUserStore.message;
			this.hasCheckedMessages = MessageUserStore.hasCheckedMessages;
			this.messageLoadingThrottle = MessageUserStore.messageLoading;
			this.messagesBodiesDom = MessageUserStore.messagesBodiesDom;
			this.messageActiveDom = MessageUserStore.messageActiveDom;
			this.messageError = MessageUserStore.messageError;

			this.fullScreenMode = MessageUserStore.messageFullScreenMode;

			this.messageListOfThreadsLoading = ko.observable(false).extend({ rateLimit: 1 });
			this.highlightUnselectedAttachments = ko.observable(false).extend({ falseTimeout: 2000 });

			this.showAttachmentControlsState = v => set(ClientSideKeyName.MessageAttachmentControls, !!v);

			this.downloadAsZipError = ko.observable(false).extend({ falseTimeout: 7000 });

			this.messageDomFocused = ko.observable(false).extend({ rateLimit: 0 });

			// commands
			this.replyCommand = createCommandReplyHelper(ComposeType.Reply);
			this.replyAllCommand = createCommandReplyHelper(ComposeType.ReplyAll);
			this.forwardCommand = createCommandReplyHelper(ComposeType.Forward);
			this.forwardAsAttachmentCommand = createCommandReplyHelper(ComposeType.ForwardAsAttachment);
			this.editAsNewCommand = createCommandReplyHelper(ComposeType.EditAsNew);

			this.deleteCommand = createCommandActionHelper(FolderType.Trash, true);
			this.deleteWithoutMoveCommand = createCommandActionHelper(FolderType.Trash, false);
			this.archiveCommand = createCommandActionHelper(FolderType.Archive, true);
			this.spamCommand = createCommandActionHelper(FolderType.Spam, true);
			this.notSpamCommand = createCommandActionHelper(FolderType.NotSpam, true);

			// viewer
			this.viewHash = '';

			this.addComputables({
				allowAttachmentControls: () => this.attachmentsActions.length && SettingsCapa(Capa.AttachmentsActions),

				downloadAsZipAllowed: () => this.attachmentsActions.includes('zip') && this.allowAttachmentControls(),

				lastReplyAction: {
					read: this.lastReplyAction_,
					write: value => this.lastReplyAction_(
						[ComposeType.Reply, ComposeType.ReplyAll, ComposeType.Forward].includes(value)
							? ComposeType.Reply
							: value
					)
				},

				messageVisibility: () => !MessageUserStore.messageLoading() && !!currentMessage(),

				canBeRepliedOrForwarded: () => !this.isDraftFolder() && this.messageVisibility(),

				viewFromDkimVisibility: () => 'none' !== this.viewFromDkimData()[0],

				viewFromDkimStatusIconClass:() => {
					switch (this.viewFromDkimData()[0]) {
						case 'none':
							return '';
						case 'pass':
							return 'icon-ok iconcolor-green';
						default:
							return 'icon-warning-alt iconcolor-red';
					}
				},

				viewFromDkimStatusTitle:() => {
					const status = this.viewFromDkimData();
					if (arrayLength(status) && status[0]) {
						return status[1] || 'DKIM: ' + status[0];
					}

					return '';
				},

				pgpSupported: () => currentMessage() && PgpUserStore.isSupported(),

				messageListOrViewLoading:
					() => MessageUserStore.listIsLoading() | MessageUserStore.messageLoading()
			});

			this.addSubscribables({
				showAttachmentControls: v => currentMessage()
					&& currentMessage().attachments.forEach(item => item && item.checked(!!v)),

				lastReplyAction_: value => set(ClientSideKeyName.LastReplyAction, value),

				message: message => {
					this.messageActiveDom(null);

					if (message) {
						this.showAttachmentControls(false);
						if (get(ClientSideKeyName.MessageAttachmentControls)) {
							setTimeout(() => {
								this.showAttachmentControls(true);
							}, 50);
						}

						if (this.viewHash !== message.hash) {
							this.scrollMessageToTop();
						}

						this.viewHash = message.hash;
						this.viewFromShort(message.fromToLine(true, true));
						this.viewFromDkimData(message.fromDkimData());
						this.viewToShort(message.toToLine(true, true));
					} else {
						MessageUserStore.selectorMessageSelected(null);

						this.viewHash = '';

						this.scrollMessageToTop();
					}
				},

				fullScreenMode: value => {
					if (this.oContent) {
						value ? this.oContent.requestFullscreen() : exitFullscreen();
					} else {
						$htmlCL.toggle('rl-message-fullscreen', value);
					}
				}
			});

			this.lastReplyAction(get(ClientSideKeyName.LastReplyAction) || ComposeType.Reply);

			addEventListener('mailbox.message-view.toggle-full-screen', () => this.toggleFullScreen());

			decorateKoCommands(this, {
				closeMessageCommand: 1,
				messageEditCommand: self => self.messageVisibility(),
				goUpCommand: self => !self.messageListOrViewLoading(),
				goDownCommand: self => !self.messageListOrViewLoading()
			});
		}

		closeMessageCommand() {
			MessageUserStore.message(null);
		}

		messageEditCommand() {
			if (currentMessage()) {
				showMessageComposer([ComposeType.Draft, currentMessage()]);
			}
		}

		goUpCommand() {
			fireEvent('mailbox.message-list.selector.go-up',
				SettingsUserStore.usePreviewPane() || !!currentMessage() // bForceSelect
			);
		}

		goDownCommand() {
			fireEvent('mailbox.message-list.selector.go-down',
				SettingsUserStore.usePreviewPane() || !!currentMessage() // bForceSelect
			);
		}

		toggleFullScreen() {
			try {
				getSelection().removeAllRanges();
			} catch (e) {} // eslint-disable-line no-empty

			this.fullScreenMode(!this.fullScreenMode());
		}

		/**
		 * @param {string} sType
		 * @returns {void}
		 */
		replyOrforward(sType) {
			showMessageComposer([sType, currentMessage()]);
		}

		onBuild(dom) {
			this.fullScreenMode.subscribe(value =>
				value && currentMessage() && AppUserStore.focusedState(Scope.MessageView));

			this.showFullInfo.subscribe(value => set(ClientSideKeyName.MessageHeaderFullInfo, value ? '1' : '0'));

			const el = dom.querySelector('.b-content');
			this.oContent = initFullscreen(el, () => this.fullScreenMode(getFullscreenElement() === el));

			const eqs = (ev, s) => ev.target.closestWithin(s, dom);
			dom.addEventListener('click', event => {
				ThemeStore.isMobile() && leftPanelDisabled(true);

				let el = eqs(event, 'a');
				if (el) {
					return !(
						0 === event.button &&
						mailToHelper(el.href)
					);
				}

				if (eqs(event, '.attachmentsPlace .attachmentIconParent')) {
					event.stopPropagation();
				}

				el = eqs(event, '.attachmentsPlace .showPreplay');
				if (el) {
					event.stopPropagation();
					const attachment = ko.dataFor(el);
					if (attachment && SMAudio.supported) {
						switch (true) {
							case SMAudio.supportedMp3 && attachment.isMp3():
								SMAudio.playMp3(attachment.linkDownload(), attachment.fileName);
								break;
							case SMAudio.supportedOgg && attachment.isOgg():
								SMAudio.playOgg(attachment.linkDownload(), attachment.fileName);
								break;
							case SMAudio.supportedWav && attachment.isWav():
								SMAudio.playWav(attachment.linkDownload(), attachment.fileName);
								break;
							// no default
						}
					}
				}

				el = eqs(event, '.attachmentsPlace .attachmentItem .attachmentNameParent');
				if (el) {
					const attachment = ko.dataFor(el);
					attachment && attachment.linkDownload() && download(attachment.linkDownload(), attachment.fileName);
				}

				if (eqs(event, '.messageItemHeader .subjectParent .flagParent')) {
					const message = currentMessage();
					message && rl.app.messageListAction(
						message.folder,
						message.isFlagged() ? MessageSetAction.UnsetFlag : MessageSetAction.SetFlag,
						[message]
					);
				}
			});

			AppUserStore.focusedState.subscribe((value) => {
				if (Scope.MessageView !== value) {
					this.scrollMessageToTop();
					this.scrollMessageToLeft();
				}
			});

			keyScopeReal.subscribe(value => this.messageDomFocused(Scope.MessageView === value && !inFocus()));

			// initShortcuts

			// exit fullscreen, back
			shortcuts.add('escape,backspace', '', Scope.MessageView, () => {
				if (!this.viewModelDom.hidden && currentMessage()) {
					const preview = SettingsUserStore.usePreviewPane();
					if (this.fullScreenMode()) {
						this.fullScreenMode(false);

						if (preview) {
							AppUserStore.focusedState(Scope.MessageList);
						}
					} else if (!preview) {
						MessageUserStore.message(null);
					} else {
						AppUserStore.focusedState(Scope.MessageList);
					}

					return false;
				}
			});

			// fullscreen
			shortcuts.add('enter,open', '', Scope.MessageView, () => {
				this.toggleFullScreen();
				return false;
			});

			// reply
			shortcuts.add('r,mailreply', '', [Scope.MessageList, Scope.MessageView], () => {
				if (currentMessage()) {
					this.replyCommand();
					return false;
				}
				return true;
			});

			// replyAll
			shortcuts.add('a', '', [Scope.MessageList, Scope.MessageView], () => {
				if (currentMessage()) {
					this.replyAllCommand();
					return false;
				}
			});
			shortcuts.add('mailreply', 'shift', [Scope.MessageList, Scope.MessageView], () => {
				if (currentMessage()) {
					this.replyAllCommand();
					return false;
				}
			});

			// forward
			shortcuts.add('f,mailforward', '', [Scope.MessageList, Scope.MessageView], () => {
				if (currentMessage()) {
					this.forwardCommand();
					return false;
				}
			});

			// message information
			shortcuts.add('i', 'meta', [Scope.MessageList, Scope.MessageView], () => {
				if (currentMessage()) {
					this.showFullInfo(!this.showFullInfo());
				}
				return false;
			});

			// toggle message blockquotes
			shortcuts.add('b', '', [Scope.MessageList, Scope.MessageView], () => {
				const message = currentMessage();
				if (message && message.body) {
					message.body.querySelectorAll('.rlBlockquoteSwitcher').forEach(node => node.click());
					return false;
				}
			});

			shortcuts.add('arrowup,arrowleft', 'meta', [Scope.MessageList, Scope.MessageView], () => {
				this.goUpCommand();
				return false;
			});

			shortcuts.add('arrowdown,arrowright', 'meta', [Scope.MessageList, Scope.MessageView], () => {
				this.goDownCommand();
				return false;
			});

			// print
			shortcuts.add('p,printscreen', 'meta', [Scope.MessageView, Scope.MessageList], () => {
				currentMessage() && currentMessage().printMessage();
				return false;
			});

			// delete
			shortcuts.add('delete', '', Scope.MessageView, () => {
				this.deleteCommand();
				return false;
			});
			shortcuts.add('delete', 'shift', Scope.MessageView, () => {
				this.deleteWithoutMoveCommand();
				return false;
			});

			// change focused state
			shortcuts.add('arrowleft', '', Scope.MessageView, () => {
				if (!this.fullScreenMode() && currentMessage() && SettingsUserStore.usePreviewPane()
				 && !oMessageScrollerDom().scrollLeft) {
					AppUserStore.focusedState(Scope.MessageList);
					return false;
				}
			});
			shortcuts.add('tab', 'shift', Scope.MessageView, () => {
				if (!this.fullScreenMode() && currentMessage() && SettingsUserStore.usePreviewPane()) {
					AppUserStore.focusedState(Scope.MessageList);
				}
				return false;
			});
		}

		/**
		 * @returns {boolean}
		 */
		isDraftFolder() {
			return currentMessage() && FolderUserStore.draftsFolder() === currentMessage().folder;
		}

		/**
		 * @returns {boolean}
		 */
		isSentFolder() {
			return currentMessage() && FolderUserStore.sentFolder() === currentMessage().folder;
		}

		/**
		 * @returns {boolean}
		 */
		isSpamFolder() {
			return currentMessage() && FolderUserStore.spamFolder() === currentMessage().folder;
		}

		/**
		 * @returns {boolean}
		 */
		isSpamDisabled() {
			return currentMessage() && FolderUserStore.spamFolder() === UNUSED_OPTION_VALUE;
		}

		/**
		 * @returns {boolean}
		 */
		isArchiveFolder() {
			return currentMessage() && FolderUserStore.archiveFolder() === currentMessage().folder;
		}

		/**
		 * @returns {boolean}
		 */
		isArchiveDisabled() {
			return currentMessage() && FolderUserStore.archiveFolder() === UNUSED_OPTION_VALUE;
		}

		/**
		 * @returns {boolean}
		 */
		isDraftOrSentFolder() {
			return this.isDraftFolder() || this.isSentFolder();
		}

		composeClick() {
			showMessageComposer();
		}

		scrollMessageToTop() {
			oMessageScrollerDom().scrollTop = (50 < oMessageScrollerDom().scrollTop) ? 50 : 0;
		}

		scrollMessageToLeft() {
			oMessageScrollerDom().scrollLeft = 0;
		}

		downloadAsZip() {
			const hashes = (currentMessage() ? currentMessage().attachments : [])
				.map(item => (item && !item.isLinked() && item.checked() ? item.download : ''))
				.filter(v => v);
			if (hashes.length) {
				Remote.attachmentsActions('Zip', hashes, this.downloadAsZipLoading)
					.then(result => {
						let hash = result && result.Result && result.Result.FileHash;
						if (hash) {
							download(attachmentDownload(hash), hash+'.zip');
						} else {
							this.downloadAsZipError(true);
						}
					})
					.catch(() => this.downloadAsZipError(true));
			} else {
				this.highlightUnselectedAttachments(true);
			}
		}

		/**
		 * @param {MessageModel} oMessage
		 * @returns {void}
		 */
		showImages() {
			currentMessage().showExternalImages();
		}

		/**
		 * @returns {string}
		 */
		printableCheckedMessageCount() {
			const cnt = MessageUserStore.listCheckedOrSelectedUidsWithSubMails().length;
			return 0 < cnt ? (100 > cnt ? cnt : '99+') : '';
		}

		/**
		 * @param {MessageModel} oMessage
		 * @returns {void}
		 */
		readReceipt() {
			let oMessage = currentMessage();
			if (oMessage.readReceipt()) {
				Remote.request('SendReadReceiptMessage', null, {
					MessageFolder: oMessage.folder,
					MessageUid: oMessage.uid,
					ReadReceipt: oMessage.readReceipt(),
					Subject: i18n('READ_RECEIPT/SUBJECT', { SUBJECT: oMessage.subject() }),
					Text: i18n('READ_RECEIPT/BODY', { 'READ-RECEIPT': AccountUserStore.email() })
				});

				oMessage.flags.push('$mdnsent');
	//			oMessage.flags.valueHasMutated();

				MessageFlagsCache.store(oMessage);

				MessageUserStore.reloadFlagsAndCachedMessage();
			}
		}

		pgpDecrypt() {
			const oMessage = currentMessage();
			PgpUserStore.decrypt(oMessage).then(result => {
				if (result && result.data) {
					MimeToMessage(result.data, oMessage);
					oMessage.html() ? oMessage.viewHtml() : oMessage.viewPlain();
					if (result.signatures && result.signatures.length) {
						oMessage.pgpSigned(true);
						oMessage.pgpVerified({
							signatures: result.signatures,
							success: !!result.signatures.length
						});
					}
				}
			});
		}

		pgpVerify(/*self, event*/) {
			const oMessage = currentMessage()/*, ctrl = event.target.closest('.openpgp-control')*/;
			PgpUserStore.verify(oMessage).then(result => {
				if (result) {
					oMessage.pgpVerified(result);
				}
	/*
				if (result && result.success) {
					i18n('OPENPGP/GOOD_SIGNATURE', {
						USER: validKey.user + ' (' + validKey.id + ')'
					});
					message.getText()
				} else {
					const keyIds = arrayLength(signingKeyIds) ? signingKeyIds : null,
						additional = keyIds
							? keyIds.map(item => (item && item.toHex ? item.toHex() : null)).filter(v => v).join(', ')
							: '';

					i18n('OPENPGP/ERROR', {
						ERROR: 'message'
					}) + (additional ? ' (' + additional + ')' : '');
				}
	*/
			});
		}

	}

	class MailBoxUserScreen extends AbstractScreen {
		constructor() {
			super('mailbox', [
				SystemDropDownUserView,
				MailFolderList,
				MailMessageList,
				MailMessageView
			]);
		}

		/**
		 * @returns {void}
		 */
		updateWindowTitle() {
			const count = Settings.app('listPermanentFiltered') ? 0 : FolderUserStore.foldersInboxUnreadCount(),
				email = AccountUserStore.email();

			rl.setWindowTitle(
				(email
					? '' + (0 < count ? '(' + count + ') ' : ' ') + email + ' - '
					: ''
				) + i18n('TITLES/MAILBOX')
			);
		}

		/**
		 * @returns {void}
		 */
		onShow() {
			this.updateWindowTitle();

			AppUserStore.focusedState(Scope.None);
			AppUserStore.focusedState(Scope.MessageList);

			ThemeStore.isMobile() && leftPanelDisabled(true);
		}

		/**
		 * @param {string} folderHash
		 * @param {number} page
		 * @param {string} search
		 * @returns {void}
		 */
		onRoute(folderHash, page, search) {
			const folder = getFolderFromCacheList(getFolderFullName(folderHash.replace(/~([\d]+)$/, '')));
			if (folder) {
				let threadUid = folderHash.replace(/^.+~(\d+)$/, '$1');

				FolderUserStore.currentFolder(folder);

				MessageUserStore.listPage(1 > page ? 1 : page);
				MessageUserStore.listSearch(search);
				MessageUserStore.listThreadUid((folderHash === threadUid) ? 0 : pInt(threadUid));

				rl.app.reloadMessageList();
			}
		}

		/**
		 * @returns {void}
		 */
		onStart() {
			if (!this.__started) {
				super.onStart();

				addEventListener('mailbox.inbox-unread-count', e => {
					FolderUserStore.foldersInboxUnreadCount(e.detail);
	/*				// Disabled in SystemDropDown.html
					const email = AccountUserStore.email();
					AccountUserStore.accounts.forEach(item =>
						item && email === item.email && item.count(e.detail)
					);
	*/
					this.updateWindowTitle();
				});
			}
		}

		/**
		 * @returns {void}
		 */
		onBuild() {
			setTimeout(() => {
				// initMailboxLayoutResizer
				const top = elementById('V-MailMessageList'),
					bottom = elementById('V-MailMessageView'),
					fToggle = () => {
						let layout = SettingsUserStore.layout();
						setLayoutResizer(top, bottom, ClientSideKeyName.MessageListSize,
							(ThemeStore.isMobile() || Layout.NoPreview === layout)
								? 0
								: (Layout.SidePreview === layout ? 'Width' : 'Height')
						);
					};
				if (top && bottom) {
					fToggle();
					addEventListener('rl-layout', fToggle);
				}
			}, 1);

			doc.addEventListener('click', event =>
				event.target.closest('#rl-right') && moveAction(false)
			);
		}

		/**
		 * Parse link as generated by mailBox()
		 * @returns {Array}
		 */
		routes() {
			const
				folder = (request, vals) => request ? pString(vals[0]) : getFolderInboxName(),
				fNormS = (request, vals) => [folder(request, vals), request ? pInt(vals[1]) : 1, decodeURI(pString(vals[2]))];

			return [
				// Folder: INBOX | INBOX.sub | Sent | fullNameHash
				[/^([^/]*)$/, { normalize_: fNormS }],
				// Search: {folder}/{string}
				[/^([a-zA-Z0-9.~_-]+)\/(.+)\/?$/, { normalize_: (request, vals) =>
					[folder(request, vals), 1, decodeURI(pString(vals[1]))]
				}],
				// Page: {folder}/p{int}(/{search})?
				[/^([a-zA-Z0-9.~_-]+)\/p([1-9][0-9]*)(?:\/(.+))?$/, { normalize_: fNormS }]
			];
		}
	}

	const VIEW_MODELS = [];

	class AbstractSettingsScreen extends AbstractScreen {
		/**
		 * @param {Array} viewModels
		 */
		constructor(viewModels) {
			super('settings', viewModels);

			this.menu = ko.observableArray();

			this.oCurrentSubScreen = null;
		}

		onRoute(subName) {
			let settingsScreen = null,
				viewModelDom = null,
				RoutedSettingsViewModel = VIEW_MODELS.find(
					SettingsViewModel => subName === SettingsViewModel.__rlSettingsData.route
				);

			if (RoutedSettingsViewModel) {
				if (RoutedSettingsViewModel.__builded && RoutedSettingsViewModel.__vm) {
					settingsScreen = RoutedSettingsViewModel.__vm;
				} else {
					const vmPlace = elementById('rl-settings-subscreen');
					if (vmPlace) {
						viewModelDom = createElement('div',{
							id: 'V-Settings-' + RoutedSettingsViewModel.name.replace(/(User|Admin)Settings/,''),
							hidden: ''
						});
						vmPlace.append(viewModelDom);

						settingsScreen = new RoutedSettingsViewModel();
						settingsScreen.viewModelDom = viewModelDom;

						RoutedSettingsViewModel.__dom = viewModelDom;
						RoutedSettingsViewModel.__builded = true;
						RoutedSettingsViewModel.__vm = settingsScreen;

						ko.applyBindingAccessorsToNode(
							viewModelDom,
							{
								i18nInit: true,
								template: () => ({ name: RoutedSettingsViewModel.__rlSettingsData.template })
							},
							settingsScreen
						);

						settingsScreen.onBuild && settingsScreen.onBuild(viewModelDom);
					} else {
						console.log('Cannot find sub settings view model position: SettingsSubScreen');
					}
				}

				if (settingsScreen) {
					setTimeout(() => {
						// hide
						this.onHide();
						// --

						this.oCurrentSubScreen = settingsScreen;

						// show
						settingsScreen.onBeforeShow && settingsScreen.onBeforeShow();
						settingsScreen.viewModelDom.hidden = false;
						settingsScreen.onShow && settingsScreen.onShow();

						this.menu.forEach(item => {
							item.selected(
								item.route === RoutedSettingsViewModel.__rlSettingsData.route
							);
						});

						elementById('rl-settings-subscreen').scrollTop = 0;
						// --
					}, 1);
				}
			} else {
				rl.route.setHash(settings(), false, true);
			}
		}

		onHide() {
			let subScreen = this.oCurrentSubScreen;
			if (subScreen) {
				subScreen.onHide && subScreen.onHide();
				subScreen.viewModelDom.hidden = true;
			}
		}

		onBuild() {
			VIEW_MODELS.forEach(SettingsViewModel => this.menu.push(SettingsViewModel.__rlSettingsData));
		}

		routes() {
			const DefaultViewModel = VIEW_MODELS.find(
					SettingsViewModel => SettingsViewModel.__rlSettingsData.isDefault
				),
				defaultRoute =
					DefaultViewModel ? DefaultViewModel.__rlSettingsData.route : 'general',
				rules = {
					subname: /^(.*)$/,
					normalize_: (rquest, vals) => {
						vals.subname = null == vals.subname ? defaultRoute : pString(vals.subname);
						return [vals.subname];
					}
				};

			return [
				['{subname}/', rules],
				['{subname}', rules],
				['', rules]
			];
		}
	}

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} template
	 * @param {string} labelName
	 * @param {string} route
	 * @param {boolean=} isDefault = false
	 * @returns {void}
	 */
	function settingsAddViewModel(SettingsViewModelClass, template, labelName, route, isDefault = false) {
		SettingsViewModelClass.__rlSettingsData = {
			label: labelName,
			route: route,
			selected: ko.observable(false),
			template: template,
			isDefault: !!isDefault
		};

		VIEW_MODELS.push(SettingsViewModelClass);
	}

	const USER_VIEW_MODELS_HOOKS = [],
		ADMIN_VIEW_MODELS_HOOKS = [];

	/**
	 * @param {Function} callback
	 * @param {string} action
	 * @param {Object=} parameters
	 * @param {?number=} timeout
	 */
	rl.pluginRemoteRequest = (callback, action, parameters, timeout) => {
		rl.app && rl.app.Remote.request('Plugin' + action, callback, parameters, timeout);
	};

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} labelName
	 * @param {string} template
	 * @param {string} route
	 */
	rl.addSettingsViewModel = (SettingsViewModelClass, template, labelName, route) => {
		USER_VIEW_MODELS_HOOKS.push([SettingsViewModelClass, template, labelName, route]);
	};

	/**
	 * @param {Function} SettingsViewModelClass
	 * @param {string} labelName
	 * @param {string} template
	 * @param {string} route
	 */
	rl.addSettingsViewModelForAdmin = (SettingsViewModelClass, template, labelName, route) => {
		ADMIN_VIEW_MODELS_HOOKS.push([SettingsViewModelClass, template, labelName, route]);
	};

	/**
	 * @param {boolean} admin
	 */
	function runSettingsViewModelHooks(admin) {
		(admin ? ADMIN_VIEW_MODELS_HOOKS : USER_VIEW_MODELS_HOOKS).forEach(view => {
			settingsAddViewModel(view[0], view[1], view[2], view[3]);
		});
	}

	/**
	 * @param {string} pluginSection
	 * @param {string} name
	 * @returns {?}
	 */
	rl.pluginSettingsGet = (pluginSection, name) => {
		let plugins = SettingsGet('Plugins');
		plugins = plugins && null != plugins[pluginSection] ? plugins[pluginSection] : null;
		return plugins ? (null == plugins[name] ? null : plugins[name]) : null;
	};

	rl.pluginPopupView = AbstractViewPopup;

	const reEmail = /^[^@\s]+@[^@\s]+$/;

	class IdentityPopupView extends AbstractViewPopup {
		constructor() {
			super('Identity');

			this.id = '';
			this.addObservables({
				edit: false,
				owner: false,

				email: '',
				emailFocused: false,
				emailHasError: false,

				name: '',

				replyTo: '',
				replyToFocused: false,
				replyToHasError: false,

				bcc: '',
				bccFocused: false,
				bccHasError: false,

				signature: '',
				signatureInsertBefore: false,

				showBcc: false,
				showReplyTo: false,

				submitRequest: false,
				submitError: ''
			});

			this.addSubscribables({
				email: value => this.emailHasError(value && !reEmail.test(value)),
				replyTo: value => {
					this.replyToHasError(value && !reEmail.test(value));
					if (false === this.showReplyTo() && value.length) {
						this.showReplyTo(true);
					}
				},
				bcc: value => {
					this.bccHasError(value && !reEmail.test(value));
					if (false === this.showBcc() && value.length) {
						this.showBcc(true);
					}
				}
			});
	/*
			this.email.valueHasMutated();
			this.replyTo.valueHasMutated();
			this.bcc.valueHasMutated();
	*/
			decorateKoCommands(this, {
				addOrEditIdentityCommand: self => !self.submitRequest()
			});
		}

		addOrEditIdentityCommand() {
			if (this.signature && this.signature.__fetchEditorValue) {
				this.signature.__fetchEditorValue();
			}

			if (!this.emailHasError()) {
				this.emailHasError(!this.email().trim());
			}

			if (this.emailHasError()) {
				if (!this.owner()) {
					this.emailFocused(true);
				}

				return false;
			}

			if (this.replyToHasError()) {
				this.replyToFocused(true);
				return false;
			}

			if (this.bccHasError()) {
				this.bccFocused(true);
				return false;
			}

			this.submitRequest(true);

			Remote.request('IdentityUpdate', iError => {
					this.submitRequest(false);
					if (iError) {
						this.submitError(getNotification(iError));
					} else {
						rl.app.accountsAndIdentities();
						this.cancelCommand();
					}
				}, {
					Id: this.id,
					Email: this.email(),
					Name: this.name(),
					ReplyTo: this.replyTo(),
					Bcc: this.bcc(),
					Signature: this.signature(),
					SignatureInsertBefore: this.signatureInsertBefore() ? 1 : 0
				}
			);

			return true;
		}

		clearPopup() {
			this.id = '';
			this.edit(false);
			this.owner(false);

			this.name('');
			this.email('');
			this.replyTo('');
			this.bcc('');
			this.signature('');
			this.signatureInsertBefore(false);

			this.emailHasError(false);
			this.replyToHasError(false);
			this.bccHasError(false);

			this.showBcc(false);
			this.showReplyTo(false);

			this.submitRequest(false);
			this.submitError('');
		}

		/**
		 * @param {?IdentityModel} oIdentity
		 */
		onShow(identity) {
			this.clearPopup();

			if (identity) {
				this.edit(true);

				this.id = identity.id() || '';
				this.name(identity.name());
				this.email(identity.email());
				this.replyTo(identity.replyTo());
				this.bcc(identity.bcc());
				this.signature(identity.signature());
				this.signatureInsertBefore(identity.signatureInsertBefore());

				this.owner(!this.id);
			} else {
				this.id = Jua.randomId();
			}
		}

		onShowWithDelay() {
			this.owner() || this.emailFocused(true);
		}

		onHideWithDelay() {
			this.clearPopup();
		}
	}

	class GeneralUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.language = LanguageStore.language;
			this.languages = LanguageStore.languages;
			this.messageReadDelay = SettingsUserStore.messageReadDelay;
			this.messagesPerPage = SettingsUserStore.messagesPerPage;

			this.editorDefaultType = SettingsUserStore.editorDefaultType;
			this.layout = SettingsUserStore.layout;

			this.enableSoundNotification = NotificationUserStore.enableSoundNotification;
			this.notificationSound = ko.observable(SettingsGet('NotificationSound'));
			this.notificationSounds = ko.observableArray(SettingsGet('NewMailSounds'));

			this.enableDesktopNotification = NotificationUserStore.enableDesktopNotification;
			this.isDesktopNotificationAllowed = NotificationUserStore.isDesktopNotificationAllowed;

			this.viewHTML = SettingsUserStore.viewHTML;
			this.showImages = SettingsUserStore.showImages;
			this.removeColors = SettingsUserStore.removeColors;
			this.useCheckboxesInList = SettingsUserStore.useCheckboxesInList;
			this.threadsAllowed = AppUserStore.threadsAllowed;
			this.useThreads = SettingsUserStore.useThreads;
			this.replySameFolder = SettingsUserStore.replySameFolder;
			this.allowLanguagesOnSettings = !!SettingsGet('AllowLanguagesOnSettings');

			this.languageTrigger = ko.observable(SaveSettingsStep.Idle).extend({ debounce: 100 });

			addObservablesTo(this, {
				mppTrigger: SaveSettingsStep.Idle,
				messageReadDelayTrigger: SaveSettingsStep.Idle,
				editorDefaultTypeTrigger: SaveSettingsStep.Idle,
				layoutTrigger: SaveSettingsStep.Idle
			});

			this.identities = IdentityUserStore;

			addComputablesTo(this, {
				languageFullName: () => convertLangName(this.language()),

				identityMain: () => {
					const list = this.identities();
					return isArray(list) ? list.find(item => item && !item.id()) : null;
				},

				identityMainDesc: () => {
					const identity = this.identityMain();
					return identity ? identity.formattedName() : '---';
				},

				editorDefaultTypes: () => {
					trigger();
					return [
						{ id: EditorDefaultType.Html, name: i18n('SETTINGS_GENERAL/LABEL_EDITOR_HTML') },
						{ id: EditorDefaultType.Plain, name: i18n('SETTINGS_GENERAL/LABEL_EDITOR_PLAIN') },
						{ id: EditorDefaultType.HtmlForced, name: i18n('SETTINGS_GENERAL/LABEL_EDITOR_HTML_FORCED') },
						{ id: EditorDefaultType.PlainForced, name: i18n('SETTINGS_GENERAL/LABEL_EDITOR_PLAIN_FORCED') }
					];
				},

				layoutTypes: () => {
					trigger();
					return [
						{ id: Layout.NoPreview, name: i18n('SETTINGS_GENERAL/LABEL_LAYOUT_NO_SPLIT') },
						{ id: Layout.SidePreview, name: i18n('SETTINGS_GENERAL/LABEL_LAYOUT_VERTICAL_SPLIT') },
						{ id: Layout.BottomPreview, name: i18n('SETTINGS_GENERAL/LABEL_LAYOUT_HORIZONTAL_SPLIT') }
					];
				}
			});

			const fReloadLanguageHelper = (saveSettingsStep) => () => {
					this.languageTrigger(saveSettingsStep);
					setTimeout(() => this.languageTrigger(SaveSettingsStep.Idle), 1000);
				};
			addSubscribablesTo(this, {
				language: value => {
					this.languageTrigger(SaveSettingsStep.Animate);
					translatorReload(false, value)
						.then(fReloadLanguageHelper(SaveSettingsStep.TrueResult),
							fReloadLanguageHelper(SaveSettingsStep.FalseResult))
						.then(() => Remote.saveSetting('Language', value));
				},

				editorDefaultType: value => Remote.saveSetting('EditorDefaultType', value,
					settingsSaveHelperSimpleFunction(this.editorDefaultTypeTrigger, this)),

				messageReadDelay: value => Remote.saveSetting('MessageReadDelay', value,
					settingsSaveHelperSimpleFunction(this.messageReadDelayTrigger, this)),

				messagesPerPage: value => Remote.saveSetting('MPP', value,
					settingsSaveHelperSimpleFunction(this.mppTrigger, this)),

				viewHTML: value => Remote.saveSetting('ViewHTML', value ? 1 : 0),
				showImages: value => Remote.saveSetting('ShowImages', value ? 1 : 0),

				removeColors: value => {
					let dom = MessageUserStore.messagesBodiesDom();
					if (dom) {
						dom.innerHTML = '';
					}
					Remote.saveSetting('RemoveColors', value ? 1 : 0);
				},

				useCheckboxesInList: value => Remote.saveSetting('UseCheckboxesInList', value ? 1 : 0),

				enableDesktopNotification: value => Remote.saveSetting('DesktopNotifications', value ? 1 : 0),

				enableSoundNotification: value => Remote.saveSetting('SoundNotification', value ? 1 : 0),
				notificationSound: value => {
					Remote.saveSetting('NotificationSound', value);
					Settings.set('NotificationSound', value);
				},

				replySameFolder: value => Remote.saveSetting('ReplySameFolder', value ? 1 : 0),

				useThreads: value => {
					MessageUserStore.list([]);
					Remote.saveSetting('UseThreads', value ? 1 : 0);
				},

				layout: value => {
					MessageUserStore.list([]);
					Remote.saveSetting('Layout', value, settingsSaveHelperSimpleFunction(this.layoutTrigger, this));
				}
			});
		}

		editMainIdentity() {
			const identity = this.identityMain();
			identity && showScreenPopup(IdentityPopupView, [identity]);
		}

		testSoundNotification() {
			NotificationUserStore.playSoundNotification(true);
		}

		testSystemNotification() {
			NotificationUserStore.displayDesktopNotification('SnappyMail', 'Test notification');
		}

		selectLanguage() {
			showScreenPopup(LanguagesPopupView, [this.language, this.languages(), LanguageStore.userLanguage()]);
		}
	}

	class ContactsUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.contactsAutosave = ko.observable(!!SettingsGet('ContactsAutosave'));

			this.allowContactsSync = ContactUserStore.allowSync;
			this.enableContactsSync = ContactUserStore.enableSync;
			this.contactsSyncUrl = ContactUserStore.syncUrl;
			this.contactsSyncUser = ContactUserStore.syncUser;
			this.contactsSyncPass = ContactUserStore.syncPass;

			this.saveTrigger = koComputable(() =>
					[
						ContactUserStore.enableSync() ? '1' : '0',
						ContactUserStore.syncUrl(),
						ContactUserStore.syncUser(),
						ContactUserStore.syncPass()
					].join('|')
				)
				.extend({ debounce: 500 });

			this.contactsAutosave.subscribe(value =>
				Remote.saveSettings(null, {
					ContactsAutosave: value ? 1 : 0
				})
			);

			this.saveTrigger.subscribe(() =>
				Remote.request('SaveContactsSyncData', null, {
					Enable: ContactUserStore.enableSync() ? 1 : 0,
					Url: ContactUserStore.syncUrl(),
					User: ContactUserStore.syncUser(),
					Password: ContactUserStore.syncPass()
				})
			);
		}
	}

	class AccountsUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.allowAdditionalAccount = SettingsCapa(Capa.AdditionalAccounts);
			this.allowIdentities = SettingsCapa(Capa.Identities);

			this.accounts = AccountUserStore.accounts;
			this.loading = AccountUserStore.loading;
			this.identities = IdentityUserStore;
			this.mainEmail = SettingsGet('MainEmail');

			this.accountForDeletion = ko.observable(null).askDeleteHelper();
			this.identityForDeletion = ko.observable(null).askDeleteHelper();
		}

		addNewAccount() {
			showScreenPopup(AccountPopupView);
		}

		editAccount(account) {
			if (account && account.isAdditional()) {
				showScreenPopup(AccountPopupView, [account]);
			}
		}

		addNewIdentity() {
			showScreenPopup(IdentityPopupView);
		}

		editIdentity(identity) {
			showScreenPopup(IdentityPopupView, [identity]);
		}

		/**
		 * @param {AccountModel} accountToRemove
		 * @returns {void}
		 */
		deleteAccount(accountToRemove) {
			if (accountToRemove && accountToRemove.askDelete()) {
				this.accountForDeletion(null);
				if (accountToRemove) {
					this.accounts.remove((account) => accountToRemove === account);

					Remote.request('AccountDelete', (iError, data) => {
						if (!iError && data.Reload) {
							rl.route.root();
							setTimeout(() => location.reload(), 1);
						} else {
							rl.app.accountsAndIdentities();
						}
					}, {
						EmailToDelete: accountToRemove.email
					});
				}
			}
		}

		/**
		 * @param {IdentityModel} identityToRemove
		 * @returns {void}
		 */
		deleteIdentity(identityToRemove) {
			if (identityToRemove && identityToRemove.askDelete()) {
				this.identityForDeletion(null);

				if (identityToRemove) {
					IdentityUserStore.remove(oIdentity => identityToRemove === oIdentity);
					Remote.request('IdentityDelete', () => rl.app.accountsAndIdentities(), {
						IdToDelete: identityToRemove.id()
					});
				}
			}
		}

		accountsAndIdentitiesAfterMove() {
			Remote.request('AccountsAndIdentitiesSortOrder', null, {
				Accounts: AccountUserStore.getEmailAddresses().filter(v => v != SettingsGet('MainEmail')),
				Identities: IdentityUserStore.getIDS()
			});
		}

		onBuild(oDom) {
			oDom.addEventListener('click', event => {
				let el = event.target.closestWithin('.accounts-list .e-action', oDom);
				el && ko.dataFor(el) && this.editAccount(ko.dataFor(el));

				el = event.target.closestWithin('.identities-list .e-action', oDom);
				el && ko.dataFor(el) && this.editIdentity(ko.dataFor(el));
			});
		}
	}

	const SieveUserStore = {
		// capabilities
		capa: ko.observableArray(),
		// Sieve scripts SieveScriptModel
		scripts: ko.observableArray()
	};

	/**
	 * @enum {string}
	 */
	const FilterConditionField = {
		From: 'From',
		Recipient: 'Recipient',
		Subject: 'Subject',
		Header: 'Header',
		Body: 'Body',
		Size: 'Size'
	};

	/**
	 * @enum {string}
	 */
	const FilterConditionType = {
		Contains: 'Contains',
		NotContains: 'NotContains',
		EqualTo: 'EqualTo',
		NotEqualTo: 'NotEqualTo',
		Regex: 'Regex',
		Over: 'Over',
		Under: 'Under',
		Text: 'Text',
		Raw: 'Raw'
	};

	class FilterConditionModel extends AbstractModel {
		constructor() {
			super();

			this.addObservables({
				field: FilterConditionField.From,
				type: FilterConditionType.Contains,
				value: '',
				valueError: false,

				valueSecond: '',
				valueSecondError: false
			});

			this.template = koComputable(() => {
				const template = 'SettingsFiltersCondition';
				switch (this.field()) {
					case FilterConditionField.Body:
						return template + 'Body';
					case FilterConditionField.Size:
						return template + 'Size';
					case FilterConditionField.Header:
						return template + 'More';
					default:
						return template + 'Default';
				}
			});

			this.addSubscribables({
				field: () => {
					this.value('');
					this.valueSecond('');
				}
			});
		}

		verify() {
			if (!this.value()) {
				this.valueError(true);
				return false;
			}

			if (FilterConditionField.Header === this.field() && !this.valueSecond()) {
				this.valueSecondError(true);
				return false;
			}

			return true;
		}

	//	static reviveFromJson(json) {}

		toJson() {
			return {
	//			'@Object': 'Object/FilterCondition',
				Field: this.field(),
				Type: this.type(),
				Value: this.value(),
				ValueSecond: this.valueSecond()
			};
		}

		cloneSelf() {
			const filterCond = new FilterConditionModel();

			filterCond.field(this.field());
			filterCond.type(this.type());
			filterCond.value(this.value());
			filterCond.valueSecond(this.valueSecond());

			return filterCond;
		}
	}

	/**
	 * @enum {string}
	 */
	const FilterAction = {
		None: 'None',
		MoveTo: 'MoveTo',
		Discard: 'Discard',
		Vacation: 'Vacation',
		Reject: 'Reject',
		Forward: 'Forward'
	};

	/**
	 * @enum {string}
	 */
	const FilterRulesType = {
		All: 'All',
		Any: 'Any'
	};

	class FilterModel extends AbstractModel {
		constructor() {
			super();

			this.id = '';

			this.addObservables({
				enabled: true,
				askDelete: false,
				canBeDeleted: true,

				name: '',
				nameError: false,
				nameFocused: false,

				conditionsType: FilterRulesType.Any,

				// Actions
				actionValue: '',
				actionValueError: false,

				actionValueSecond: '',
				actionValueThird: '',

				actionValueFourth: '',
				actionValueFourthError: false,

				actionMarkAsRead: false,

				actionKeep: true,
				actionNoStop: false,

				actionType: FilterAction.MoveTo
			});

			this.conditions = ko.observableArray();

			const fGetRealFolderName = (folderFullName) => {
				const folder = getFolderFromCacheList(folderFullName);
				return folder ? folder.fullName.replace('.' === folder.delimiter ? /\./ : /[\\/]+/, ' / ') : folderFullName;
			};

			this.addComputables({
				nameSub: () => {
					let result = '';
					const actionValue = this.actionValue(), root = 'SETTINGS_FILTERS/SUBNAME_';

					switch (this.actionType()) {
						case FilterAction.MoveTo:
							result = i18n(root + 'MOVE_TO', {
								FOLDER: fGetRealFolderName(actionValue)
							});
							break;
						case FilterAction.Forward:
							result = i18n(root + 'FORWARD_TO', {
								EMAIL: actionValue
							});
							break;
						case FilterAction.Vacation:
							result = i18n(root + 'VACATION_MESSAGE');
							break;
						case FilterAction.Reject:
							result = i18n(root + 'REJECT');
							break;
						case FilterAction.Discard:
							result = i18n(root + 'DISCARD');
							break;
						// no default
					}

					return result ? '(' + result + ')' : '';
				},

				actionTemplate: () => {
					const result = 'SettingsFiltersAction';
					switch (this.actionType()) {
						case FilterAction.Forward:
							return result + 'Forward';
						case FilterAction.Vacation:
							return result + 'Vacation';
						case FilterAction.Reject:
							return result + 'Reject';
						case FilterAction.None:
							return result + 'None';
						case FilterAction.Discard:
							return result + 'Discard';
						case FilterAction.MoveTo:
						default:
							return result + 'MoveToFolder';
					}
				}
			});

			this.addSubscribables({
				name: sValue => this.nameError(!sValue),
				actionValue: sValue => this.actionValueError(!sValue),
				actionType: () => {
					this.actionValue('');
					this.actionValueError(false);
					this.actionValueSecond('');
					this.actionValueThird('');
					this.actionValueFourth('');
					this.actionValueFourthError(false);
				}
			});
		}

		generateID() {
			this.id = Jua.randomId();
		}

		verify() {
			if (!this.name()) {
				this.nameError(true);
				return false;
			}

			if (this.conditions.length && this.conditions.find(cond => cond && !cond.verify())) {
				return false;
			}

			if (!this.actionValue()) {
				if ([
						FilterAction.MoveTo,
						FilterAction.Forward,
						FilterAction.Reject,
						FilterAction.Vacation
					].includes(this.actionType())
				) {
					this.actionValueError(true);
					return false;
				}
			}

			if (FilterAction.Forward === this.actionType() && !this.actionValue().includes('@')) {
				this.actionValueError(true);
				return false;
			}

			if (
				FilterAction.Vacation === this.actionType() &&
				this.actionValueFourth() &&
				!this.actionValueFourth().includes('@')
			) {
				this.actionValueFourthError(true);
				return false;
			}

			this.nameError(false);
			this.actionValueError(false);

			return true;
		}

		toJson() {
			return {
	//			'@Object': 'Object/Filter',
				ID: this.id,
				Enabled: this.enabled() ? 1 : 0,
				Name: this.name(),
				Conditions: this.conditions.map(item => item.toJson()),
				ConditionsType: this.conditionsType(),

				ActionType: this.actionType(),
				ActionValue: this.actionValue(),
				ActionValueSecond: this.actionValueSecond(),
				ActionValueThird: this.actionValueThird(),
				ActionValueFourth: this.actionValueFourth(),

				Keep: this.actionKeep() ? 1 : 0,
				Stop: this.actionNoStop() ? 0 : 1,
				MarkAsRead: this.actionMarkAsRead() ? 1 : 0
			};
		}

		addCondition() {
			this.conditions.push(new FilterConditionModel());
		}

		removeCondition(oConditionToDelete) {
			this.conditions.remove(oConditionToDelete);
			delegateRunOnDestroy(oConditionToDelete);
		}

		setRecipients() {
			this.actionValueFourth(AccountUserStore.getEmailAddresses().join(', '));
		}

		/**
		 * @static
		 * @param {FetchJsonFilter} json
		 * @returns {?FilterModel}
		 */
		static reviveFromJson(json) {
			const filter = super.reviveFromJson(json);
			if (filter) {
				filter.id = pString(json.ID);

				filter.conditions([]);

				if (arrayLength(json.Conditions)) {
					filter.conditions(
						json.Conditions.map(aData => FilterConditionModel.reviveFromJson(aData)).filter(v => v)
					);
				}

				filter.actionKeep(0 != json.Keep);
				filter.actionNoStop(0 == json.Stop);
				filter.actionMarkAsRead(1 == json.MarkAsRead);
			}
			return filter;
		}

		cloneSelf() {
			const filter = new FilterModel();

			filter.id = this.id;

			filter.enabled(this.enabled());

			filter.name(this.name());
			filter.nameError(this.nameError());

			filter.conditionsType(this.conditionsType());

			filter.actionMarkAsRead(this.actionMarkAsRead());

			filter.actionType(this.actionType());

			filter.actionValue(this.actionValue());
			filter.actionValueError(this.actionValueError());

			filter.actionValueSecond(this.actionValueSecond());
			filter.actionValueThird(this.actionValueThird());
			filter.actionValueFourth(this.actionValueFourth());

			filter.actionKeep(this.actionKeep());
			filter.actionNoStop(this.actionNoStop());

			filter.conditions(this.conditions.map(item => item.cloneSelf()));

			return filter;
		}
	}

	const SIEVE_FILE_NAME = 'rainloop.user';

	// collectionToFileString
	function filtersToSieveScript(filters)
	{
		let eol = '\r\n',
			split = /.{0,74}/g,
			require = {},
			parts = [
				'# This is SnappyMail sieve script.',
				'# Please don\'t change anything here.',
				'# RAINLOOP:SIEVE',
				''
			];

		const quote = string => '"' + string.trim().replace(/(\\|")/, '\\\\$1') + '"';
		const StripSpaces = string => string.replace(/\s+/, ' ').trim();

		// conditionToSieveScript
		const conditionToString = (condition, require) =>
		{
			let result = '',
				type = condition.type(),
				field = condition.field(),
				value = condition.value().trim(),
				valueSecond = condition.valueSecond().trim();

			if (value.length && ('Header' !== field || valueSecond.length)) {
				switch (type)
				{
					case 'NotEqualTo':
						result += 'not ';
						type = ':is';
						break;
					case 'EqualTo':
						type = ':is';
						break;
					case 'NotContains':
						result += 'not ';
						type = ':contains';
						break;
					case 'Text':
					case 'Raw':
					case 'Over':
					case 'Under':
					case 'Contains':
						type = ':' + type.toLowerCase();
						break;
					case 'Regex':
						type = ':regex';
						require.regex = 1;
						break;
					default:
						return '/* @Error: unknown type value ' + type + '*/ false';
				}

				switch (field)
				{
					case 'From':
						result += 'header ' + type + ' ["From"]';
						break;
					case 'Recipient':
						result += 'header ' + type + ' ["To", "CC"]';
						break;
					case 'Subject':
						result += 'header ' + type + ' ["Subject"]';
						break;
					case 'Header':
						result += 'header ' + type + ' [' + quote(valueSecond) + ']';
						break;
					case 'Body':
						// :text :raw :content
						result += 'body ' + type + ' :contains';
						require.body = 1;
						break;
					case 'Size':
						result += 'size ' + type;
						break;
					default:
						return '/* @Error: unknown field value ' + field + ' */ false';
				}

				if (('From' === field || 'Recipient' === field) && value.includes(',')) {
					result += ' [' + value.split(',').map(value => quote(value)).join(', ').trim() + ']';
				} else if ('Size' === field) {
					result += ' ' + value;
				} else {
					result += ' ' + quote(value);
				}

				return StripSpaces(result);
			}

			return '/* @Error: empty condition value */ false';
		};

		// filterToSieveScript
		const filterToString = (filter, require) =>
		{
			let sTab = '    ',
				block = true,
				result = [],
				conditions = filter.conditions();

			const errorAction = type => result.push(sTab + '# @Error (' + type + '): empty action value');

			// Conditions
			if (1 < conditions.length) {
				result.push('Any' === filter.conditionsType()
					? 'if anyof('
					: 'if allof('
				);
				result.push(conditions.map(condition => sTab + conditionToString(condition, require)).join(',' + eol));
				result.push(')');
			} else if (conditions.length) {
				result.push('if ' + conditionToString(conditions[0], require));
			} else {
				block = false;
			}

			// actions
			block ? result.push('{') : (sTab = '');

			if (filter.actionMarkAsRead() && ['None','MoveTo','Forward'].includes(filter.actionType())) {
				require.imap4flags = 1;
				result.push(sTab + 'addflag "\\\\Seen";');
			}

			let value = filter.actionValue().trim();
			value = value.length ? quote(value) : 0;
			switch (filter.actionType())
			{
				case 'None':
					break;
				case 'Discard':
					result.push(sTab + 'discard;');
					break;
				case 'Vacation':
					if (value) {
						require.vacation = 1;

						let days = 1,
							subject = '',
							addresses = '',
							paramValue = filter.actionValueSecond().trim();

						if (paramValue.length) {
							subject = ':subject ' + quote(StripSpaces(paramValue)) + ' ';
						}

						paramValue = pString(filter.actionValueThird()).trim();
						if (paramValue.length) {
							days = Math.max(1, parseInt(paramValue, 10));
						}

						paramValue = pString(filter.actionValueFourth()).trim();
						if (paramValue.length) {
							paramValue = paramValue.split(',').map(email =>
								email.trim().length ? quote(email) : ''
							).filter(email => email.length);
							if (paramValue.length) {
								addresses = ':addresses [' + paramValue.join(', ') + '] ';
							}
						}

						result.push(sTab + 'vacation :days ' + days + ' ' + addresses + subject + value + ';');
					} else {
						errorAction('vacation');
					}
					break;
				case 'Reject': {
					if (value) {
						require.reject = 1;
						result.push(sTab + 'reject ' + value + ';');
					} else {
						errorAction('reject');
					}
					break; }
				case 'Forward':
					if (value) {
						if (filter.actionKeep()) {
							require.fileinto = 1;
							result.push(sTab + 'fileinto "INBOX";');
						}
						result.push(sTab + 'redirect ' + value + ';');
					} else {
						errorAction('redirect');
					}
					break;
				case 'MoveTo':
					if (value) {
						require.fileinto = 1;
						result.push(sTab + 'fileinto ' + value + ';');
					} else {
						errorAction('fileinto');
					}
					break;
			}

			filter.actionNoStop() || result.push(sTab + 'stop;');

			block && result.push('}');

			return result.join(eol);
		};

		filters.forEach(filter => {
			parts.push([
				'/*',
				'BEGIN:FILTER:' + filter.id,
				'BEGIN:HEADER',
				b64EncodeJSON(filter.toJson()).match(split).join(eol) + 'END:HEADER',
				'*/',
				filter.enabled() ? '' : '/* @Filter is disabled ',
				filterToString(filter, require),
				filter.enabled() ? '' : '*/',
				'/* END:FILTER */',
				''
			].join(eol));
		});

		require = Object.keys(require);
		return (require.length ? 'require ' + JSON.stringify(require) + ';' + eol : '') + eol + parts.join(eol);
	}

	// fileStringToCollection
	function sieveScriptToFilters(script)
	{
		let regex = /BEGIN:HEADER([\s\S]+?)END:HEADER/gm,
			filters = [],
			json,
			filter;
		if (script.length && script.includes('RAINLOOP:SIEVE')) {
			while ((json = regex.exec(script))) {
				json = decodeURIComponent(escape(atob(json[1].replace(/\s+/g, ''))));
				if (json && json.length && (json = JSON.parse(json))) {
					json['@Object'] = 'Object/Filter';
					json.Conditions.forEach(condition => condition['@Object'] = 'Object/FilterCondition');
					filter = FilterModel.reviveFromJson(json);
					filter && filters.push(filter);
				}
			}
		}
	/*
		// TODO: branch sieveparser
		// https://github.com/the-djmaze/snappymail/tree/sieveparser/dev/Sieve
		else if (script.length && window.Sieve) {
			let tree = window.Sieve.parseScript(script);
		}
	*/
		return filters;
	}

	class SieveScriptModel extends AbstractModel
	{
		constructor() {
			super();

			this.addObservables({
				name: '',
				active: false,
				body: '',

				exists: false,
				nameError: false,
				bodyError: false,
				askDelete: false,
				canBeDeleted: true,
				hasChanges: false
			});

			this.filters = ko.observableArray();
	//		this.saving = ko.observable(false).extend({ debounce: 200 });

			this.addSubscribables({
				name: () => this.hasChanges(true),
				filters: () => this.hasChanges(true),
				body: () => this.hasChanges(true)
			});
		}

		filtersToRaw() {
			return filtersToSieveScript(this.filters);
	//		this.body(filtersToSieveScript(this.filters));
		}

		rawToFilters() {
			return sieveScriptToFilters(this.body());
	//		this.filters(sieveScriptToFilters(this.body()));
		}

		verify() {
			this.nameError(!this.name().trim());
			this.bodyError(this.allowFilters() ? !this.filters.length : !this.body().trim());
			return !this.nameError() && !this.bodyError();
		}

		toJson() {
			return {
				name: this.name(),
				active: this.active() ? 1 : 0,
				body: this.body(),
				filters: this.filters.map(item => item.toJson())
			};
		}

		/**
		 * Only 'rainloop.user' script supports filters
		 */
		allowFilters() {
			return SIEVE_FILE_NAME === this.name();
		}

		/**
		 * @static
		 * @param {FetchJsonScript} json
		 * @returns {?SieveScriptModel}
		 */
		static reviveFromJson(json) {
			const script = super.reviveFromJson(json);
			if (script) {
				if (script.allowFilters()) {
					script.filters(
						arrayLength(json.filters)
							? json.filters.map(aData => FilterModel.reviveFromJson(aData)).filter(v => v)
							: sieveScriptToFilters(script.body())
					);
				} else {
					script.filters([]);
				}
				script.canBeDeleted(SIEVE_FILE_NAME !== json.name);
				script.exists(true);
				script.hasChanges(false);
			}
			return script;
		}

	}

	class FilterPopupView extends AbstractViewPopup {
		constructor() {
			super('Filter');

			this.addObservables({
				isNew: true,
				filter: null,
				allowMarkAsRead: false,
				selectedFolderValue: ''
			});

			this.fTrueCallback = null;

			this.defaultOptionsAfterRender = defaultOptionsAfterRender;
			this.folderSelectList = koComputable(() =>
				folderListOptionsBuilder(
					[SettingsGet('SieveAllowFileintoInbox') ? '' : 'INBOX'],
					[['', '']],
					item => item ? item.localName() : ''
				)
			);

			this.selectedFolderValue.subscribe(() => this.filter() && this.filter().actionValueError(false));

			['actionTypeOptions','fieldOptions','typeOptions','typeOptionsSize','typeOptionsBody'].forEach(
				key => this[key] = ko.observableArray()
			);

			initOnStartOrLangChange(this.populateOptions.bind(this));

			SieveUserStore.capa.subscribe(this.populateOptions, this);

			decorateKoCommands(this, {
				saveFilterCommand: 1
			});
		}

		saveFilterCommand() {
			if (this.filter()) {
				if (FilterAction.MoveTo === this.filter().actionType()) {
					this.filter().actionValue(this.selectedFolderValue());
				}

				if (!this.filter().verify()) {
					return false;
				}

				this.fTrueCallback && this.fTrueCallback(this.filter());

				this.modalVisibility() && this.closeCommand();
			}

			return true;
		}

		populateOptions() {
			this.actionTypeOptions([]);

			let i18nFilter = key => i18n('POPUPS_FILTER/SELECT_' + key);

			this.fieldOptions([
				{ id: FilterConditionField.From, name: i18n('GLOBAL/FROM') },
				{ id: FilterConditionField.Recipient, name: i18nFilter('FIELD_RECIPIENTS') },
				{ id: FilterConditionField.Subject, name: i18n('GLOBAL/SUBJECT') },
				{ id: FilterConditionField.Size, name: i18nFilter('FIELD_SIZE') },
				{ id: FilterConditionField.Header, name: i18nFilter('FIELD_HEADER') }
			]);

			this.typeOptions([
				{ id: FilterConditionType.Contains, name: i18nFilter('TYPE_CONTAINS') },
				{ id: FilterConditionType.NotContains, name: i18nFilter('TYPE_NOT_CONTAINS') },
				{ id: FilterConditionType.EqualTo, name: i18nFilter('TYPE_EQUAL_TO') },
				{ id: FilterConditionType.NotEqualTo, name: i18nFilter('TYPE_NOT_EQUAL_TO') }
			]);

			// this.actionTypeOptions.push({id: FilterAction.None,
			// name: i18n('GLOBAL/NONE')});
			const modules = SieveUserStore.capa;
			if (modules) {
				if (modules.includes('imap4flags')) {
					this.allowMarkAsRead(true);
				}

				if (modules.includes('fileinto')) {
					this.actionTypeOptions.push({
						id: FilterAction.MoveTo,
						name: i18nFilter('ACTION_MOVE_TO')
					});
					this.actionTypeOptions.push({
						id: FilterAction.Forward,
						name: i18nFilter('ACTION_FORWARD_TO')
					});
				}

				if (modules.includes('reject')) {
					this.actionTypeOptions.push({ id: FilterAction.Reject, name: i18nFilter('ACTION_REJECT') });
				}

				if (modules.includes('vacation')) {
					this.actionTypeOptions.push({
						id: FilterAction.Vacation,
						name: i18nFilter('ACTION_VACATION_MESSAGE')
					});
				}

				if (modules.includes('body')) {
					this.fieldOptions.push({ id: FilterConditionField.Body, name: i18nFilter('FIELD_BODY') });
				}

				if (modules.includes('regex')) {
					this.typeOptions.push({ id: FilterConditionType.Regex, name: 'Regex' });
				}
			}

			this.actionTypeOptions.push({ id: FilterAction.Discard, name: i18nFilter('ACTION_DISCARD') });

			this.typeOptionsSize([
				{ id: FilterConditionType.Over, name: i18nFilter('TYPE_OVER') },
				{ id: FilterConditionType.Under, name: i18nFilter('TYPE_UNDER') }
			]);

			this.typeOptionsBody([
				{ id: FilterConditionType.Text, name: i18nFilter('TYPE_TEXT') },
				{ id: FilterConditionType.Raw, name: i18nFilter('TYPE_RAW') }
			]);
		}

		removeCondition(oConditionToDelete) {
			if (this.filter()) {
				this.filter().removeCondition(oConditionToDelete);
			}
		}

		onShow(oFilter, fTrueCallback, bEdit) {
			this.isNew(!bEdit);

			this.fTrueCallback = fTrueCallback;
			this.filter(oFilter);

			if (oFilter) {
				this.selectedFolderValue(oFilter.actionValue());
			}

			if (!bEdit && oFilter) {
				oFilter.nameFocused(true);
			}
		}

		onShowWithDelay() {
			this.isNew() && this.filter() && this.filter().nameFocused(true);
		}
	}

	class SieveScriptPopupView extends AbstractViewPopup {
		constructor() {
			super('SieveScript');

			addObservablesTo(this, {
				saveError: false,
				saveErrorText: '',
				rawActive: false,
				allowToggle: false,
				script: null
			});

			this.sieveCapabilities = SieveUserStore.capa.join(' ');
			this.saving = false;

			this.filterForDeletion = ko.observable(null).askDeleteHelper();
	/*
			decorateKoCommands(this, {
				saveScriptCommand: 1
			});
	*/
		}

		saveScriptCommand() {
			let self = this,
				script = self.script();
			if (!self.saving/* && script.hasChanges()*/) {
				if (!script.verify()) {
					return false;
				}

				if (!script.exists() && SieveUserStore.scripts.find(item => item.name() === script.name())) {
					script.nameError(true);
					return false;
				}

				self.saving = true;
				self.saveError(false);

				if (self.allowToggle()) {
					script.body(script.filtersToRaw());
				}

				Remote.request('FiltersScriptSave',
					(iError, data) => {
						self.saving = false;

						if (iError) {
							self.saveError(true);
							self.saveErrorText((data && data.ErrorMessageAdditional) || getNotification(iError));
						} else {
							script.exists() || SieveUserStore.scripts.push(script);
							script.exists(true);
							script.hasChanges(false);
						}
					},
					script.toJson()
				);
			}

			return true;
		}

		deleteFilter(filter) {
			this.script().filters.remove(filter);
			delegateRunOnDestroy(filter);
		}

		addFilter() {
			/* this = SieveScriptModel */
			const filter = new FilterModel();
			filter.generateID();
			showScreenPopup(FilterPopupView, [
				filter,
				() => {
					this.filters.push(filter);
				},
				false
			]);
		}

		editFilter(filter) {
			const clonedFilter = filter.cloneSelf();
			showScreenPopup(FilterPopupView, [
				clonedFilter,
				() => {
					const script = this.script(),
						filters = script.filters(),
						index = filters.indexOf(filter);
					if (-1 < index) {
						delegateRunOnDestroy(filters[index]);
						filters[index] = clonedFilter;
						script.filters(filters);
					}
				},
				true
			]);
		}

		toggleFiltersRaw() {
			if (!this.rawActive()) {
				let script = this.script(),
					changed = script.hasChanges();
				script.body(script.filtersToRaw());
				script.hasChanges(changed);
			}
			this.rawActive(!this.rawActive());
		}

		onBuild(oDom) {
			oDom.addEventListener('click', event => {
				const el = event.target.closestWithin('td.e-action', oDom),
					filter = el && ko.dataFor(el);
				filter && this.editFilter(filter);
			});
		}

		onShow(oScript) {
			this.script(oScript);
			this.rawActive(!oScript.allowFilters());
			this.allowToggle(oScript.allowFilters());
			this.saveError(false);
		}

		onShowWithDelay() {
			// Sometimes not everything is translated, try again
			i18nToNodes(this.viewModelDom);
		}
	}

	class FiltersUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.scripts = SieveUserStore.scripts;
			this.loading = ko.observable(false).extend({ debounce: 200 });

			addObservablesTo(this, {
				serverError: false,
				serverErrorDesc: ''
			});

			this.scriptForDeletion = ko.observable(null).askDeleteHelper();
		}

		setError(text) {
			this.serverError(true);
			this.serverErrorDesc(text);
		}

		updateList() {
			if (!this.loading()) {
				this.loading(true);
				this.serverError(false);

				Remote.request('Filters', (iError, data) => {
					this.loading(false);
					this.scripts([]);

					if (iError) {
						SieveUserStore.capa([]);
						this.setError(getNotification(iError));
					} else {
						SieveUserStore.capa(data.Result.Capa);
	/*
						this.scripts(
							data.Result.Scripts.map(aItem => SieveScriptModel.reviveFromJson(aItem)).filter(v => v)
						);
	*/
						forEachObjectValue(data.Result.Scripts, value => {
							value = SieveScriptModel.reviveFromJson(value);
							value && this.scripts.push(value);
						});
					}
				});
			}
		}

		addScript() {
			showScreenPopup(SieveScriptPopupView, [new SieveScriptModel()]);
		}

		editScript(script) {
			showScreenPopup(SieveScriptPopupView, [script]);
		}

		deleteScript(script) {
			this.serverError(false);
			Remote.request('FiltersScriptDelete',
				(iError, data) => {
					if (iError) {
						this.setError((data && data.ErrorMessageAdditional) || getNotification(iError));
					} else {
						this.scripts.remove(script);
						delegateRunOnDestroy(script);
					}
				},
				{name:script.name()}
			);
		}

		toggleScript(script) {
			let name = script.active() ? '' : script.name();
			this.serverError(false);
			Remote.request('FiltersScriptActivate',
				(iError, data) => {
					if (iError) {
						this.setError((data && data.ErrorMessageAdditional) || iError);
					} else {
						this.scripts.forEach(script => script.active(script.name() === name));
					}
				},
				{name:name}
			);
		}

		onBuild(oDom) {
			oDom.addEventListener('click', event => {
				const el = event.target.closestWithin('.script-item .e-action', oDom),
					script = el && ko.dataFor(el);
				script && this.editScript(script);
			});
		}

		onShow() {
			this.updateList();
		}
	}

	class SecurityUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.capaAutoLogout = Settings.capa(Capa.AutoLogout);

			this.autoLogout = SettingsUserStore.autoLogout;
			this.autoLogoutTrigger = ko.observable(SaveSettingsStep.Idle);

			let i18nLogout = (key, params) => i18n('SETTINGS_SECURITY/AUTOLOGIN_' + key, params);
			this.autoLogoutOptions = koComputable(() => {
				trigger();
				return [
					{ id: 0, name: i18nLogout('NEVER_OPTION_NAME') },
					{ id: 5, name: i18nLogout('MINUTES_OPTION_NAME', { MINUTES: 5 }) },
					{ id: 10, name: i18nLogout('MINUTES_OPTION_NAME', { MINUTES: 10 }) },
					{ id: 30, name: i18nLogout('MINUTES_OPTION_NAME', { MINUTES: 30 }) },
					{ id: 60, name: i18nLogout('MINUTES_OPTION_NAME', { MINUTES: 60 }) },
					{ id: 60 * 2, name: i18nLogout('HOURS_OPTION_NAME', { HOURS: 2 }) },
					{ id: 60 * 5, name: i18nLogout('HOURS_OPTION_NAME', { HOURS: 5 }) },
					{ id: 60 * 10, name: i18nLogout('HOURS_OPTION_NAME', { HOURS: 10 }) }
				];
			});

			if (this.capaAutoLogout) {
				this.autoLogout.subscribe(value => Remote.saveSetting(
					'AutoLogout', pInt(value),
					settingsSaveHelperSimpleFunction(this.autoLogoutTrigger, this)
				));
			}
		}
	}

	class FolderSystemPopupView extends AbstractViewPopup {
		constructor() {
			super('FolderSystem');

			this.sChooseOnText = '';
			this.sUnuseText = '';

			initOnStartOrLangChange(() => {
				this.sChooseOnText = i18n('POPUPS_SYSTEM_FOLDERS/SELECT_CHOOSE_ONE');
				this.sUnuseText = i18n('POPUPS_SYSTEM_FOLDERS/SELECT_UNUSE_NAME');
			});

			this.notification = ko.observable('');

			this.folderSelectList = koComputable(() =>
				folderListOptionsBuilder(
					FolderUserStore.folderListSystemNames(),
					[
						['', this.sChooseOnText],
						[UNUSED_OPTION_VALUE, this.sUnuseText]
					]
				)
			);

			this.sentFolder = FolderUserStore.sentFolder;
			this.draftsFolder = FolderUserStore.draftsFolder;
			this.spamFolder = FolderUserStore.spamFolder;
			this.trashFolder = FolderUserStore.trashFolder;
			this.archiveFolder = FolderUserStore.archiveFolder;

			const fSaveSystemFolders = (()=>FolderUserStore.saveSystemFolders()).debounce(1000);

			addSubscribablesTo(FolderUserStore, {
				sentFolder: fSaveSystemFolders,
				draftsFolder: fSaveSystemFolders,
				spamFolder: fSaveSystemFolders,
				trashFolder: fSaveSystemFolders,
				archiveFolder: fSaveSystemFolders
			});

			this.defaultOptionsAfterRender = defaultOptionsAfterRender;
		}

		/**
		 * @param {number=} notificationType = SetSystemFoldersNotification.None
		 */
		onShow(notificationType = SetSystemFoldersNotification.None) {
			let notification = '', prefix = 'POPUPS_SYSTEM_FOLDERS/NOTIFICATION_';
			switch (notificationType) {
				case SetSystemFoldersNotification.Sent:
					notification = i18n(prefix + 'SENT');
					break;
				case SetSystemFoldersNotification.Draft:
					notification = i18n(prefix + 'DRAFTS');
					break;
				case SetSystemFoldersNotification.Spam:
					notification = i18n(prefix + 'SPAM');
					break;
				case SetSystemFoldersNotification.Trash:
					notification = i18n(prefix + 'TRASH');
					break;
				case SetSystemFoldersNotification.Archive:
					notification = i18n(prefix + 'ARCHIVE');
					break;
				// no default
			}

			this.notification(notification);
		}
	}

	const folderForDeletion = ko.observable(null).askDeleteHelper();

	class FoldersUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.showKolab = koComputable(() => FolderUserStore.hasCapability('METADATA') && Settings.capa(Capa.Kolab));
			this.defaultOptionsAfterRender = defaultOptionsAfterRender;
			this.kolabTypeOptions = ko.observableArray();
			let i18nFilter = key => i18n('SETTINGS_FOLDERS/TYPE_' + key);
			initOnStartOrLangChange(()=>{
				this.kolabTypeOptions([
					{ id: '', name: '' },
					{ id: 'event', name: i18nFilter('CALENDAR') },
					{ id: 'contact', name: i18nFilter('CONTACTS') },
					{ id: 'task', name: i18nFilter('TASKS') },
					{ id: 'note', name: i18nFilter('NOTES') },
					{ id: 'file', name: i18nFilter('FILES') },
					{ id: 'journal', name: i18nFilter('JOURNAL') },
					{ id: 'configuration', name: i18nFilter('CONFIGURATION') }
				]);
			});

			this.displaySpecSetting = FolderUserStore.displaySpecSetting;
			this.folderList = FolderUserStore.folderList;
			this.folderListOptimized = FolderUserStore.folderListOptimized;
			this.folderListError = FolderUserStore.folderListError;
			this.hideUnsubscribed = SettingsUserStore.hideUnsubscribed;

			this.loading = koComputable(() => {
				const loading = FolderUserStore.foldersLoading(),
					creating = FolderUserStore.foldersCreating(),
					deleting = FolderUserStore.foldersDeleting(),
					renaming = FolderUserStore.foldersRenaming();

				return loading || creating || deleting || renaming;
			});

			this.folderForDeletion = folderForDeletion;

			this.folderForEdit = ko.observable(null).extend({ toggleSubscribeProperty: [this, 'edited'] });

			SettingsUserStore.hideUnsubscribed.subscribe(value => Remote.saveSetting('HideUnsubscribed', value ? 1 : 0));
		}

		folderEditOnEnter(folder) {
			const nameToEdit = folder ? folder.nameForEdit().trim() : '';

			if (nameToEdit && folder.name() !== nameToEdit) {
				Remote.abort('Folders').post('FolderRename', FolderUserStore.foldersRenaming, {
						Folder: folder.fullName,
						NewFolderName: nameToEdit,
						Subscribe: folder.subscribed() ? 1 : 0
					})
					.then(data => {
						folder.name(nameToEdit/*data.Name*/);
						if (folder.subFolders.length) {
							Remote.setTrigger(FolderUserStore.foldersLoading, true);
	//						clearTimeout(Remote.foldersTimeout);
	//						Remote.foldersTimeout = setTimeout(() => Remote.foldersReload(), 500);
							setTimeout(() => Remote.foldersReload(), 500);
							// TODO: rename all subfolders with folder.delimiter to prevent reload?
						} else {
							removeFolderFromCacheList(folder.fullName);
							folder.fullName = data.Result.FullName;
							setFolder(folder);
							const parent = getFolderFromCacheList(folder.parentName);
							sortFolders(parent ? parent.subFolders : FolderUserStore.folderList);
						}
					})
					.catch(error => {
						FolderUserStore.folderListError(
							getNotification(error.code, '', Notification.CantRenameFolder)
							+ '.\n' + error.message);
					});
			}

			folder.edited(false);
		}

		folderEditOnEsc(folder) {
			if (folder) {
				folder.edited(false);
			}
		}

		onShow() {
			FolderUserStore.folderListError('');
		}
	/*
		onBuild(oDom) {
		}
	*/
		createFolder() {
			showScreenPopup(FolderCreatePopupView);
		}

		systemFolder() {
			showScreenPopup(FolderSystemPopupView);
		}

		deleteFolder(folderToRemove) {
			if (folderToRemove
			 && folderToRemove.canBeDeleted()
			 && folderToRemove.askDelete()
			) {
				if (0 < folderToRemove.privateMessageCountAll()) {
	//				FolderUserStore.folderListError(getNotification(Notification.CantDeleteNonEmptyFolder));
					folderToRemove.errorMsg(getNotification(Notification.CantDeleteNonEmptyFolder));
				} else {
					folderForDeletion(null);

					if (folderToRemove) {
						Remote.abort('Folders').post('FolderDelete', FolderUserStore.foldersDeleting, {
								Folder: folderToRemove.fullName
							}).then(
								() => {
	//								folderToRemove.flags.push('\\nonexistent');
									folderToRemove.selectable(false);
	//								folderToRemove.subscribed(false);
	//								folderToRemove.checkable(false);
									if (!folderToRemove.subFolders.length) {
										removeFolderFromCacheList(folderToRemove.fullName);
										const folder = getFolderFromCacheList(folderToRemove.parentName);
										(folder ? folder.subFolders : FolderUserStore.folderList).remove(folderToRemove);
									}
								},
								error => {
									FolderUserStore.folderListError(
										getNotification(error.code, '', Notification.CantDeleteFolder)
										+ '.\n' + error.message
									);
								}
							);
					}
				}
			}
		}

		toggleFolderKolabType(folder, event) {
			let type = event.target.value;
			// TODO: append '.default' ?
			Remote.folderSetMetadata(null, folder.fullName, FolderMetadataKeys.KolabFolderType, type);
			folder.kolabType(type);
		}

		toggleFolderSubscription(folder) {
			let subscribe = !folder.subscribed();
			Remote.request('FolderSubscribe', null, {
				Folder: folder.fullName,
				Subscribe: subscribe ? 1 : 0
			});
			folder.subscribed(subscribe);
		}

		toggleFolderCheckable(folder) {
			let checkable = !folder.checkable();
			Remote.request('FolderCheckable', null, {
				Folder: folder.fullName,
				Checkable: checkable ? 1 : 0
			});
			folder.checkable(checkable);
		}
	}

	class ThemesUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.theme = ThemeStore.theme;
			this.themes = ThemeStore.themes;
			this.themesObjects = ko.observableArray();

			this.background = {};
			this.background.name = ThemeStore.userBackgroundName;
			this.background.hash = ThemeStore.userBackgroundHash;
			this.background.uploaderButton = ko.observable(null);
			this.background.loading = ko.observable(false);
			this.background.error = ko.observable('');

			this.capaUserBackground = ko.observable(Settings.capa(Capa.UserBackground));

			this.themeTrigger = ko.observable(SaveSettingsStep.Idle).extend({ debounce: 100 });

			this.theme.subscribe((value) => {
				this.themesObjects.forEach(theme => {
					theme.selected(value === theme.name);
				});

				changeTheme(value, this.themeTrigger);

				Remote.saveSettings(null, {
					Theme: value
				});
			});
		}

		onBuild() {
			const currentTheme = this.theme();

			this.themesObjects(
				this.themes.map(theme => ({
					name: theme,
					nameDisplay: convertThemeName(theme),
					selected: ko.observable(theme === currentTheme),
					themePreviewSrc: themePreviewLink(theme)
				}))
			);

			// initUploader

			if (this.background.uploaderButton() && this.capaUserBackground()) {
				const oJua = new Jua({
					action: serverRequest('UploadBackground'),
					limit: 1,
					clickElement: this.background.uploaderButton()
				});

				oJua
					.on('onStart', () => {
						this.background.loading(true);
						this.background.error('');
						return true;
					})
					.on('onComplete', (id, result, data) => {
						this.background.loading(false);

						if (result && id && data && data.Result && data.Result.Name && data.Result.Hash) {
							this.background.name(data.Result.Name);
							this.background.hash(data.Result.Hash);
						} else {
							this.background.name('');
							this.background.hash('');

							let errorMsg = '';
							if (data.ErrorCode) {
								switch (data.ErrorCode) {
									case UploadErrorCode.FileIsTooBig:
										errorMsg = i18n('SETTINGS_THEMES/ERROR_FILE_IS_TOO_BIG');
										break;
									case UploadErrorCode.FileType:
										errorMsg = i18n('SETTINGS_THEMES/ERROR_FILE_TYPE_ERROR');
										break;
									// no default
								}
							}

							if (!errorMsg && data.ErrorMessage) {
								errorMsg = data.ErrorMessage;
							}

							this.background.error(errorMsg || i18n('SETTINGS_THEMES/ERROR_UNKNOWN'));
						}

						return true;
					});
			}
		}

		onShow() {
			this.background.error('');
		}

		clearBackground() {
			if (this.capaUserBackground()) {
				Remote.request('ClearUserBackground', () => {
					this.background.name('');
					this.background.hash('');
				});
			}
		}
	}

	class OpenPgpImportPopupView extends AbstractViewPopup {
		constructor() {
			super('OpenPgpImport');

			this.addObservables({
				key: '',
				keyError: false,
				keyErrorMessage: '',

				saveGnuPG: true,
				saveServer: false
			});

			this.canGnuPG = GnuPGUserStore.isSupported();

			this.key.subscribe(() => {
				this.keyError(false);
				this.keyErrorMessage('');
			});
		}

		submitForm() {
			let keyTrimmed = this.key().trim();

			if (/\n/.test(keyTrimmed)) {
				keyTrimmed = keyTrimmed.replace(/\r+/g, '').replace(/\n{2,}/g, '\n\n');
			}

			this.keyError(!keyTrimmed);
			this.keyErrorMessage('');

			if (!keyTrimmed) {
				return;
			}

			let match = null,
				count = 30,
				done = false;
			// eslint-disable-next-line max-len
			const reg = /[-]{3,6}BEGIN[\s]PGP[\s](PRIVATE|PUBLIC)[\s]KEY[\s]BLOCK[-]{3,6}[\s\S]+?[-]{3,6}END[\s]PGP[\s](PRIVATE|PUBLIC)[\s]KEY[\s]BLOCK[-]{3,6}/gi;

			do {
				match = reg.exec(keyTrimmed);
				if (match && 0 < count) {
					if (match[0] && match[1] && match[2] && match[1] === match[2]) {
						this.saveGnuPG() && GnuPGUserStore.isSupported() && GnuPGUserStore.importKey(this.key());
						OpenPGPUserStore.isSupported() && OpenPGPUserStore.importKey(this.key());
					}

					--count;
					done = false;
				} else {
					done = true;
				}
			} while (!done);

			if (this.keyError()) {
				return;
			}

			this.cancelCommand();
		}

		onShow() {
			this.key('');
			this.keyError(false);
			this.keyErrorMessage('');
		}
	}

	//import { pInt } from 'Common/Utils';

	class OpenPgpGeneratePopupView extends AbstractViewPopup {
		constructor() {
			super('OpenPgpGenerate');

			this.identities = IdentityUserStore;

			this.addObservables({
				email: '',
				emailError: false,

				name: '',
				password: '',
				keyType: 'ECC',

				submitRequest: false,
				submitError: '',

				backupPublicKey: true,
				backupPrivateKey: false,

				saveGnuPGPublic: true,
				saveGnuPGPrivate: false
			});

			this.canGnuPG = Settings.capa(Capa.GnuPG);

			this.email.subscribe(() => this.emailError(false));
		}

		submitForm() {
			const type = this.keyType().toLowerCase(),
				userId = {
					name: this.name(),
					email: this.email()
				},
				cfg = {
					type: type,
					userIDs: [userId],
					passphrase: this.password().trim()
	//				format: 'armored' // output key format, defaults to 'armored' (other options: 'binary' or 'object')
				};
	/*
			if ('ecc' === type) {
				cfg.curve = 'curve25519';
			} else {
				cfg.rsaBits = pInt(this.keyBitLength());
			}
	*/
			this.emailError(!this.email().trim());
			if (this.emailError()) {
				return;
			}

			this.submitRequest(true);
			this.submitError('');

			openpgp.generateKey(cfg).then(keyPair => {
				if (keyPair) {
					const fn = () => {
						this.submitRequest(false);
						this.cancelCommand();
					};

					OpenPGPUserStore.storeKeyPair(keyPair);

					keyPair.onServer = (this.backupPublicKey() ? 1 : 0) + (this.backupPrivateKey() ? 2 : 0);
					keyPair.inGnuPG = (this.saveGnuPGPublic() ? 1 : 0) + (this.saveGnuPGPrivate() ? 2 : 0);
					if (keyPair.onServer || keyPair.inGnuPG) {
						if (!this.backupPrivateKey() && !this.saveGnuPGPrivate()) {
							delete keyPair.privateKey;
						}
						GnuPGUserStore.storeKeyPair(keyPair, fn);
					} else {
						fn();
					}
				}
			})
			.catch((e) => {
				this.submitRequest(false);
				this.showError(e);
			});
		}

		showError(e) {
			console.log(e);
			if (e && e.message) {
				this.submitError(e.message);
			}
		}

		onShow() {
			this.name(''/*IdentityUserStore()[0].name()*/);
			this.password('');
			this.email(''/*IdentityUserStore()[0].email()*/);
			this.emailError(false);
			this.submitError('');
		}
	}

	class OpenPgpUserSettings /*extends AbstractViewSettings*/ {
		constructor() {
			this.gnupgPublicKeys = GnuPGUserStore.publicKeys;
			this.gnupgPrivateKeys = GnuPGUserStore.privateKeys;

			this.openpgpkeysPublic = OpenPGPUserStore.publicKeys;
			this.openpgpkeysPrivate = OpenPGPUserStore.privateKeys;

			this.canOpenPGP = SettingsCapa(Capa.OpenPGP);
			this.canGnuPG = GnuPGUserStore.isSupported();
			this.canMailvelope = !!window.mailvelope;

			this.allowDraftAutosave = SettingsUserStore.allowDraftAutosave;

			this.allowDraftAutosave.subscribe(value => Remote.saveSetting('AllowDraftAutosave', value ? 1 : 0));
		}

		addOpenPgpKey() {
			showScreenPopup(OpenPgpImportPopupView);
		}

		generateOpenPgpKey() {
			showScreenPopup(OpenPgpGeneratePopupView);
		}

		onBuild() {
			/**
			 * Create an iframe to display the Mailvelope keyring settings.
			 * The iframe will be injected into the container identified by selector.
			 */
			window.mailvelope && mailvelope.createSettingsContainer('#mailvelope-settings'/*[, keyring], options*/);
		}
	}

	class MenuSettingsUserView extends AbstractViewLeft {
		/**
		 * @param {Object} screen
		 */
		constructor(screen) {
			super('SettingsMenu');

			this.menu = screen.menu;
		}

		link(route) {
			return settings(route);
		}

		backToMailBoxClick() {
			rl.route.setHash(mailbox(getFolderInboxName()));
		}
	}

	class PaneSettingsUserView extends AbstractViewRight {
		constructor() {
			super('SettingsPane');

			this.isMobile = ThemeStore.isMobile;
			this.leftPanelDisabled = leftPanelDisabled;
		}

		onShow() {
			MessageUserStore.message(null);
		}

		onBuild(dom) {
			dom.addEventListener('click', () =>
				ThemeStore.isMobile() && !event.target.closestWithin('.toggleLeft', dom) && leftPanelDisabled(true)
			);
		}

		backToMailBoxClick() {
			rl.route.setHash(mailbox(getFolderInboxName()));
		}
	}

	class SettingsUserScreen extends AbstractSettingsScreen {
		constructor() {
			super([SystemDropDownUserView, MenuSettingsUserView, PaneSettingsUserView]);

			settingsAddViewModel(GeneralUserSettings, 'SettingsGeneral', 'SETTINGS_LABELS/LABEL_GENERAL_NAME', 'general', true);

			if (AppUserStore.allowContacts()) {
				settingsAddViewModel(ContactsUserSettings, 'SettingsContacts', 'SETTINGS_LABELS/LABEL_CONTACTS_NAME', 'contacts');
			}

			if (SettingsCapa(Capa.AdditionalAccounts) || SettingsCapa(Capa.Identities)) {
				settingsAddViewModel(
					AccountsUserSettings,
					'SettingsAccounts',
					SettingsCapa(Capa.AdditionalAccounts)
						? 'SETTINGS_LABELS/LABEL_ACCOUNTS_NAME'
						: 'SETTINGS_LABELS/LABEL_IDENTITIES_NAME',
					'accounts'
				);
			}

			if (SettingsCapa(Capa.Sieve)) {
				settingsAddViewModel(FiltersUserSettings, 'SettingsFilters', 'SETTINGS_LABELS/LABEL_FILTERS_NAME', 'filters');
			}

			if (SettingsCapa(Capa.AutoLogout)) {
				settingsAddViewModel(SecurityUserSettings, 'SettingsSecurity', 'SETTINGS_LABELS/LABEL_SECURITY_NAME', 'security');
			}

			settingsAddViewModel(FoldersUserSettings, 'SettingsFolders', 'SETTINGS_LABELS/LABEL_FOLDERS_NAME', 'folders');

			if (SettingsCapa(Capa.Themes)) {
				settingsAddViewModel(ThemesUserSettings, 'SettingsThemes', 'SETTINGS_LABELS/LABEL_THEMES_NAME', 'themes');
			}

			if (SettingsCapa(Capa.OpenPGP) || SettingsCapa(Capa.GnuPG)) {
				settingsAddViewModel(OpenPgpUserSettings, 'SettingsOpenPGP', 'OpenPGP', 'openpgp');
			}

			runSettingsViewModelHooks(false);

			initOnStartOrLangChange(
				() => this.sSettingsTitle = i18n('TITLES/SETTINGS'),
				() => this.setSettingsTitle()
			);
		}

		onShow() {
			this.setSettingsTitle();
			keyScope(Scope.Settings);
			ThemeStore.isMobile() && leftPanelDisabled(true);
		}

		setSettingsTitle() {
			const sEmail = AccountUserStore.email();
			rl.setWindowTitle((sEmail ? sEmail + ' - ' :  '') + this.sSettingsTitle);
		}
	}

	class AbstractComponent {
		constructor() {
			this.disposable = [];
		}

		dispose() {
			this.disposable.forEach((funcToDispose) => {
				if (funcToDispose && funcToDispose.dispose) {
					funcToDispose.dispose();
				}
			});
		}
	}

	class AbstractInput extends AbstractComponent {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super();

			this.value = params.value || '';
			this.label = params.label || '';
			this.enable = null == params.enable ? true : params.enable;
			this.trigger = params.trigger && params.trigger.subscribe ? params.trigger : null;
			this.placeholder = params.placeholder || '';

			this.labeled = null != params.label;

			let size = 0 < params.size ? 'span' + params.size : '';
			if (this.trigger) {
				const
					classForTrigger = ko.observable(''),
					setTriggerState = value => {
						switch (pInt(value)) {
							case SaveSettingsStep.TrueResult:
								classForTrigger('success');
								break;
							case SaveSettingsStep.FalseResult:
								classForTrigger('error');
								break;
							default:
								classForTrigger('');
								break;
						}
					};

				setTriggerState(this.trigger());

				this.className = koComputable(() =>
					(size + ' settings-saved-trigger-input ' + classForTrigger()).trim()
				);

				this.disposable.push(this.trigger.subscribe(setTriggerState, this));
			} else {
				this.className = size;
			}

			this.disposable.push(this.className);
		}
	}

	class InputComponent extends AbstractInput {}

	class SelectComponent extends AbstractInput {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super(params);

			this.options = params.options || '';

			this.optionsText = params.optionsText || null;
			this.optionsValue = params.optionsValue || null;
			this.optionsCaption = i18n(params.optionsCaption || null);

			this.defaultOptionsAfterRender = defaultOptionsAfterRender;
		}
	}

	class TextAreaComponent extends AbstractInput {
		/**
		 * @param {Object} params
		 */
		constructor(params) {
			super(params);

			this.rows = params.rows || 5;
			this.spellcheck = !!params.spellcheck;
		}
	}

	class AbstractCheckbox extends AbstractComponent {
		/**
		 * @param {Object} params = {}
		 */
		constructor(params = {}) {
			super();

			this.value = ko.isObservable(params.value) ? params.value
				: ko.observable(!!params.value);

			this.enable = ko.isObservable(params.enable) ? params.enable
				: ko.observable(undefined === params.enable || !!params.enable);

			this.label = params.label || '';
			this.inline = !!params.inline;

			this.labeled = undefined !== params.label;
		}

		click() {
			this.enable() && this.value(!this.value());
		}
	}

	class CheckboxMaterialDesignComponent extends AbstractCheckbox {}

	class CheckboxComponent extends AbstractCheckbox {}

	class AbstractApp {
		/**
		 * @param {RemoteStorage|AdminRemoteStorage} Remote
		 */
		constructor(Remote) {
			this.Remote = Remote;
		}

		logoutReload() {
			const url = logoutLink();

			if (location.href !== url) {
				setTimeout(() => (Settings.app('inIframe') ? parent : window).location.href = url, 100);
			} else {
				rl.route.reload();
			}
		}

		refresh() {
	//		rl.adminArea() || !translatorReload(false, );
			rl.adminArea() || (
				LanguageStore.language(SettingsGet('Language'))
				& ThemeStore.populate()
				& changeTheme(SettingsGet('Theme'))
			);

			this.start();
		}

		bootstart() {
			const register = (key, ClassObject, templateID) => ko.components.register(key, {
					template: { element: templateID || (key + 'Component') },
					viewModel: {
						createViewModel: (params, componentInfo) => {
							params = params || {};

							if (componentInfo && componentInfo.element) {

								i18nToNodes(componentInfo.element);

								if (params.inline) {
									componentInfo.element.style.display = 'inline-block';
								}
							}

							return new ClassObject(params);
						}
					}
				});

			register('Input', InputComponent);
			register('Select', SelectComponent);
			register('TextArea', TextAreaComponent);
			register('Checkbox', CheckboxMaterialDesignComponent, 'CheckboxMaterialDesignComponent');
			register('CheckboxSimple', CheckboxComponent, 'CheckboxComponent');

			initOnStartOrLangChange();

			LanguageStore.populate();
			ThemeStore.populate();

			this.start();
		}
	}

	class ComposeAttachmentModel extends AbstractModel {
		/**
		 * @param {string} id
		 * @param {string} fileName
		 * @param {?number=} size = null
		 * @param {boolean=} isInline = false
		 * @param {boolean=} isLinked = false
		 * @param {string=} CID = ''
		 * @param {string=} contentLocation = ''
		 */
		constructor(id, fileName, size = null, isInline = false, isLinked = false, CID = '', contentLocation = '') {
			super();

			this.id = id;
			this.isInline = !!isInline;
			this.isLinked = !!isLinked;
			this.CID = CID;
			this.contentLocation = contentLocation;
			this.fromMessage = false;

			this.addObservables({
				fileName: fileName,
				size: size,
				tempName: '',

				progress: 0,
				error: '',
				waiting: true,
				uploading: false,
				enabled: true,
				complete: false
			});

			this.addComputables({
				progressText: () => {
					const p = this.progress();
					return 0 === p ? '' : '' + (98 < p ? 100 : p) + '%';
				},

				progressStyle: () => {
					const p = this.progress();
					return 0 === p ? '' : 'width:' + (98 < p ? 100 : p) + '%';
				},

				title: () => this.error() || this.fileName(),

				friendlySize: () => {
					const localSize = this.size();
					return null === localSize ? '' : FileInfo.friendlySize(localSize);
				},

				mimeType: () => FileInfo.getContentType(this.fileName()),
				fileExt: () => FileInfo.getExtension(this.fileName())
			});
		}

		static fromAttachment(item)
		{
			const attachment = new ComposeAttachmentModel(
				item.download,
				item.fileName,
				item.estimatedSize,
				item.isInline(),
				item.isLinked(),
				item.cid,
				item.contentLocation
			);
			attachment.fromMessage = true;
			return attachment;
		}

		/**
		 * @param {FetchJsonComposeAttachment} json
		 * @returns {boolean}
		 */
		initByUploadJson(json) {
			let bResult = false;
			if (json) {
				this.fileName(json.Name);
				this.size(undefined === json.Size ? 0 : pInt(json.Size));
				this.tempName(undefined === json.TempName ? '' : json.TempName);
				this.isInline = false;

				bResult = true;
			}

			return bResult;
		}

		/**
		 * @returns {string}
		 */
		iconClass() {
			return FileInfo.getIconClass(this.fileExt(), this.mimeType());
		}
	}

	const
		base64_encode = text => btoa(text).match(/.{1,76}/g).join('\r\n'),

		email = new EmailModel(),
		getEmail = value => {
			email.clear();
			email.parse(value.trim());
			return email.email || false;
		},

		/**
		 * @param {string} prefix
		 * @param {string} subject
		 * @returns {string}
		 */
		replySubjectAdd = (prefix, subject) => {
			prefix = prefix.toUpperCase().trim();
			subject = subject.replace(/\s+/g, ' ').trim();

			let drop = false,
				re = 'RE' === prefix,
				fwd = 'FWD' === prefix;

			const parts = [],
				prefixIsRe = !fwd;

			if (subject) {
				subject.split(':').forEach(part => {
					const trimmedPart = part.trim();
					if (!drop && (/^(RE|FWD)$/i.test(trimmedPart) || /^(RE|FWD)[[(][\d]+[\])]$/i.test(trimmedPart))) {
						if (!re) {
							re = !!/^RE/i.test(trimmedPart);
						}

						if (!fwd) {
							fwd = !!/^FWD/i.test(trimmedPart);
						}
					} else {
						parts.push(part);
						drop = true;
					}
				});
			}

			if (prefixIsRe) {
				re = false;
			} else {
				fwd = false;
			}

			return ((prefixIsRe ? 'Re: ' : 'Fwd: ') + (re ? 'Re: ' : '')
				+ (fwd ? 'Fwd: ' : '') + parts.join(':').trim()).trim();
		};

	ko.extenders.toggleSubscribe = (target, options) => {
		target.subscribe(options[1], options[0], 'beforeChange');
		target.subscribe(options[2], options[0]);
		return target;
	};

	class MimePart {
		constructor() {
			this.headers = {};
			this.body = '';
			this.boundary = '';
			this.children = [];
		}

		toString() {
			const hasSub = this.children.length,
				boundary = this.boundary || (this.boundary = 'part' + Jua.randomId()),
				headers = this.headers;
			if (hasSub) {
				headers['Content-Type'] += `; boundary="${boundary}"`;
			}
			let result = Object.entries(headers).map(([key, value]) => `${key}: ${value}`).join('\r\n') + '\r\n';
			if (this.body) {
				result += '\r\n' + this.body.replace(/\r?\n/g, '\r\n');
			}
			if (hasSub) {
				this.children.forEach(part => result += '\r\n--' + boundary + '\r\n' + part);
				result += '\r\n--' + boundary + '--\r\n';
			}
			return result;
		}
	}

	class ComposePopupView extends AbstractViewPopup {
		constructor() {
			super('Compose');

			const fEmailOutInHelper = (context, identity, name, isIn) => {
				if (identity && context && identity[name]() && (isIn ? true : context[name]())) {
					const identityEmail = identity[name]();
					let list = context[name]().trim().split(',');

					list = list.filter(email => {
						email = email.trim();
						return email && identityEmail.trim() !== email;
					});

					if (isIn) {
						list.push(identityEmail);
					}

					context[name](list.join(','));
				}
			};

			this.oLastMessage = null;
			this.oEditor = null;
			this.aDraftInfo = null;
			this.sInReplyTo = '';
			this.bFromDraft = false;
			this.sReferences = '';

			this.sLastFocusedField = 'to';

			this.allowContacts = AppUserStore.allowContacts();

			this.bSkipNextHide = false;

			this.addObservables({
				identitiesDropdownTrigger: false,

				from: '',
				to: '',
				cc: '',
				bcc: '',
				replyTo: '',

				subject: '',

				isHtml: false,

				requestDsn: false,
				requestReadReceipt: false,
				markAsImportant: false,

				sendError: false,
				sendSuccessButSaveError: false,
				savedError: false,

				sendErrorDesc: '',
				savedErrorDesc: '',

				savedTime: 0,

				emptyToError: false,

				attachmentsInProcessError: false,
				attachmentsInErrorError: false,

				showCc: false,
				showBcc: false,
				showReplyTo: false,

				pgpSign: false,
				canPgpSign: false,
				pgpEncrypt: false,
				canPgpEncrypt: false,
				canMailvelope: false,

				draftsFolder: '',
				draftUid: 0,
				sending: false,
				saving: false,

				viewArea: 'body',

				attacheMultipleAllowed: false,
				addAttachmentEnabled: false,

				editorArea: null, // initDom

				currentIdentity: IdentityUserStore()[0]
			});

			this.from(IdentityUserStore()[0].formattedName());

			// this.to.subscribe((v) => console.log(v));

			// Used by ko.bindingHandlers.emailsTags
			this.to.focused = ko.observable(false);
			this.to.focused.subscribe(value => value && (this.sLastFocusedField = 'to'));
			this.cc.focused = ko.observable(false);
			this.cc.focused.subscribe(value => value && (this.sLastFocusedField = 'cc'));
			this.bcc.focused = ko.observable(false);
			this.bcc.focused.subscribe(value => value && (this.sLastFocusedField = 'bcc'));

			this.attachments = ko.observableArray();

			this.dragAndDropOver = ko.observable(false).extend({ debounce: 1 });
			this.dragAndDropVisible = ko.observable(false).extend({ debounce: 1 });

			this.currentIdentity.extend({
				toggleSubscribe: [
					this,
					(identity) => {
						fEmailOutInHelper(this, identity, 'bcc');
						fEmailOutInHelper(this, identity, 'replyTo');
					},
					(identity) => {
						fEmailOutInHelper(this, identity, 'bcc', true);
						fEmailOutInHelper(this, identity, 'replyTo', true);
					}
				]
			});

			this.bDisabeCloseOnEsc = true;

			this.tryToClosePopup = this.tryToClosePopup.debounce(200);

			this.iTimer = 0;

			this.addComputables({
				sendButtonSuccess: () => !this.sendError() && !this.sendSuccessButSaveError(),

				savedTimeText: () =>
					this.savedTime() ? i18n('COMPOSE/SAVED_TIME', { TIME: this.savedTime().format('LT') }) : '',

				emptyToErrorTooltip: () => (this.emptyToError() ? i18n('COMPOSE/EMPTY_TO_ERROR_DESC') : ''),

				attachmentsErrorTooltip: () => {
					let result = '';
					switch (true) {
						case this.attachmentsInProcessError():
							result = i18n('COMPOSE/ATTACHMENTS_UPLOAD_ERROR_DESC');
							break;
						case this.attachmentsInErrorError():
							result = i18n('COMPOSE/ATTACHMENTS_ERROR_DESC');
							break;
						// no default
					}
					return result;
				},

				attachmentsInProcess: () => this.attachments.filter(item => item && !item.complete()),
				attachmentsInError: () => this.attachments.filter(item => item && item.error()),

				attachmentsCount: () => this.attachments.length,
				attachmentsInErrorCount: () => this.attachmentsInError.length,
				attachmentsInProcessCount: () => this.attachmentsInProcess.length,
				isDraftFolderMessage: () => this.draftsFolder() && this.draftUid(),

				identitiesOptions: () =>
					IdentityUserStore.map(item => ({
						item: item,
						optValue: item.id(),
						optText: item.formattedName()
					})),

				canBeSentOrSaved: () => !this.sending() && !this.saving()
			});

			this.addSubscribables({
				sendError: value => !value && this.sendErrorDesc(''),

				savedError: value => !value && this.savedErrorDesc(''),

				sendSuccessButSaveError: value => !value && this.savedErrorDesc(''),

				currentIdentity: value => value && this.from(value.formattedName()),

				from: value => {
					this.canPgpSign(false);
					value = getEmail(value);
					value && PgpUserStore.getKeyForSigning(value).then(result => {
						console.log({
							email: value,
							canPgpSign:result
						});
						this.canPgpSign(result);
					});
				},

				cc: value => {
					if (false === this.showCc() && value.length) {
						this.showCc(true);
					}
					this.initPgpEncrypt();
				},

				bcc: value => {
					if (false === this.showBcc() && value.length) {
						this.showBcc(true);
					}
					this.initPgpEncrypt();
				},

				replyTo: value => {
					if (false === this.showReplyTo() && value.length) {
						this.showReplyTo(true);
					}
				},

				attachmentsInErrorCount: value => {
					if (0 === value) {
						this.attachmentsInErrorError(false);
					}
				},

				to: value => {
					if (this.emptyToError() && value.length) {
						this.emptyToError(false);
					}
					this.initPgpEncrypt();
				},

				attachmentsInProcess: value => {
					if (this.attachmentsInProcessError() && arrayLength(value)) {
						this.attachmentsInProcessError(false);
					}
				}
			});

			decorateKoCommands(this, {
				sendCommand: self => self.canBeSentOrSaved(),
					saveCommand: self => self.canBeSentOrSaved(),
					deleteCommand: self => self.isDraftFolderMessage(),
					skipCommand: self => self.canBeSentOrSaved(),
					contactsCommand: self => self.allowContacts
			});
		}

		async getMessageRequestParams(sSaveFolder, draft)
		{
			const
				identity = this.currentIdentity(),
				params = {
					IdentityID: identity.id(),
					MessageFolder: this.draftsFolder(),
					MessageUid: this.draftUid(),
					SaveFolder: sSaveFolder,
					From: this.from(),
					To: this.to(),
					Cc: this.cc(),
					Bcc: this.bcc(),
					ReplyTo: this.replyTo(),
					Subject: this.subject(),
					DraftInfo: this.aDraftInfo,
					InReplyTo: this.sInReplyTo,
					References: this.sReferences,
					MarkAsImportant: this.markAsImportant() ? 1 : 0,
					Attachments: this.prepareAttachmentsForSendOrSave(),
					// Only used at send, not at save:
					Dsn: this.requestDsn() ? 1 : 0,
					ReadReceiptRequest: this.requestReadReceipt() ? 1 : 0
				},
				recipients = draft ? [identity.email()] : this.allRecipients(),
				sign = !draft && this.pgpSign() && this.canPgpSign(),
				encrypt = this.pgpEncrypt() && this.canPgpEncrypt(),
				TextIsHtml = this.oEditor.isHtml();

			let Text = this.oEditor.getData();
			if (TextIsHtml) {
				let l;
				do {
					l = Text.length;
					Text = Text
						// Remove Microsoft Office styling
						.replace(/(<[^>]+[;"'])\s*mso-[a-z-]+\s*:[^;"']+/gi, '$1')
						// Remove hubspot data-hs- attributes
						.replace(/(<[^>]+)\s+data-hs-[a-z-]+=("[^"]+"|'[^']+')/gi, '$1');
				} while (l != Text.length)
				params.Html = Text;
			} else {
				params.Text = Text;
			}

			if (this.mailvelope && 'mailvelope' === this.viewArea()) {
				params.Encrypted = draft
					? await this.mailvelope.createDraft()
					: await this.mailvelope.encrypt(recipients);
			} else if (sign || encrypt) {
				let data = new MimePart;
				data.headers['Content-Type'] = 'text/'+(TextIsHtml?'html':'plain')+'; charset="utf-8"';
				data.headers['Content-Transfer-Encoding'] = 'base64';
				data.body = base64_encode(Text);
				if (TextIsHtml) {
					let alternative = new MimePart;
					alternative.headers['Content-Type'] = 'multipart/alternative';
					alternative.children.push(data);
					data = new MimePart;
					data.headers['Content-Type'] = 'text/plain; charset="utf-8"';
					data.headers['Content-Transfer-Encoding'] = 'base64';
					data.body = base64_encode(htmlToPlain(Text));
					alternative.children.push(data);
					data = alternative;
				}
				if (sign && !draft && sign[1]) {
					if ('openpgp' == sign[0]) {
						// Doesn't sign attachments
						params.Html = params.Text = '';
						let signed = new MimePart;
						signed.headers['Content-Type'] =
							'multipart/signed; micalg="pgp-sha256"; protocol="application/pgp-signature"';
						signed.headers['Content-Transfer-Encoding'] = '7Bit';
						signed.children.push(data);
						let signature = new MimePart;
						signature.headers['Content-Type'] = 'application/pgp-signature; name="signature.asc"';
						signature.headers['Content-Transfer-Encoding'] = '7Bit';
						signature.body = await OpenPGPUserStore.sign(data.toString(), sign[1], 1);
						signed.children.push(signature);
						params.Signed = signed.toString();
						params.Boundary = signed.boundary;
						data = signed;
					} else if ('gnupg' == sign[0]) {
						// TODO: sign in PHP fails
	//					params.SignData = data.toString();
						params.SignFingerprint = sign[1].fingerprint;
						params.SignPassphrase = await GnuPGUserStore.sign(sign[1]);
					} else {
						throw 'Signing with ' + sign[0] + ' not yet implemented';
					}
				}
				if (encrypt) {
					if ('openpgp' == encrypt) {
						// Doesn't encrypt attachments
						params.Encrypted = await OpenPGPUserStore.encrypt(data.toString(), recipients);
						params.Signed = '';
					} else if ('gnupg' == encrypt) {
						// Does encrypt attachments
						params.EncryptFingerprints = JSON.stringify(GnuPGUserStore.getPublicKeyFingerprints(recipients));
					} else {
						throw 'Encryption with ' + encrypt + ' not yet implemented';
					}
				}
			}
			return params;
		}

		sendCommand() {
			let sSentFolder = FolderUserStore.sentFolder();

			this.attachmentsInProcessError(false);
			this.attachmentsInErrorError(false);
			this.emptyToError(false);

			if (this.attachmentsInProcess().length) {
				this.attachmentsInProcessError(true);
				this.attachmentsArea();
			} else if (this.attachmentsInError().length) {
				this.attachmentsInErrorError(true);
				this.attachmentsArea();
			}

			if (!this.to().trim() && !this.cc().trim() && !this.bcc().trim()) {
				this.emptyToError(true);
			}

			if (!this.emptyToError() && !this.attachmentsInErrorError() && !this.attachmentsInProcessError()) {
				if (SettingsUserStore.replySameFolder()) {
					if (
						3 === arrayLength(this.aDraftInfo) &&
						null != this.aDraftInfo[2] &&
						this.aDraftInfo[2].length
					) {
						sSentFolder = this.aDraftInfo[2];
					}
				}

				if (!sSentFolder) {
					showScreenPopup(FolderSystemPopupView, [SetSystemFoldersNotification.Sent]);
				} else try {
					this.sendError(false);
					this.sending(true);

					if (3 === arrayLength(this.aDraftInfo)) {
						const flagsCache = MessageFlagsCache.getFor(this.aDraftInfo[2], this.aDraftInfo[1]);
						if (isArray(flagsCache)) {
							flagsCache.push(('forward' === this.aDraftInfo[0]) ? '$forwarded' : '\\answered');
							MessageFlagsCache.setFor(this.aDraftInfo[2], this.aDraftInfo[1], flagsCache);
							MessageUserStore.reloadFlagsAndCachedMessage();
							setFolderHash(this.aDraftInfo[2], '');
						}
					}

					sSentFolder = UNUSED_OPTION_VALUE === sSentFolder ? '' : sSentFolder;

					this.getMessageRequestParams(sSentFolder).then(params => {
						Remote.request('SendMessage',
							(iError, data) => {
								this.sending(false);
								if (this.modalVisibility()) {
									if (iError) {
										if (Notification.CantSaveMessage === iError) {
											this.sendSuccessButSaveError(true);
											this.savedErrorDesc(i18n('COMPOSE/SAVED_ERROR_ON_SEND').trim());
										} else {
											this.sendError(true);
											this.sendErrorDesc(getNotification(iError, data && data.ErrorMessage)
												|| getNotification(Notification.CantSendMessage));
										}
									} else {
										this.closeCommand();
									}
								}
								setFolderHash(this.draftsFolder(), '');
								setFolderHash(sSentFolder, '');
								this.reloadDraftFolder();
							},
							params,
							30000
						);
					}).catch(e => {
						console.error(e);
						this.sendError(true);
						this.sendErrorDesc(e);
						this.sending(false);
					});
				} catch (e) {
					console.error(e);
					this.sendError(true);
					this.sendErrorDesc(e);
					this.sending(false);
				}
			}
		}

		saveCommand() {
			if (FolderUserStore.draftsFolderNotEnabled()) {
				showScreenPopup(FolderSystemPopupView, [SetSystemFoldersNotification.Draft]);
			} else {
				this.savedError(false);
				this.saving(true);
				this.autosaveStart();
				this.getMessageRequestParams(FolderUserStore.draftsFolder(), 1).then(params => {
					Remote.request('SaveMessage',
						(iError, oData) => {
							let result = false;

							this.saving(false);

							if (!iError) {
								if (oData.Result.NewFolder && oData.Result.NewUid) {
									result = true;

									if (this.bFromDraft) {
										const message = MessageUserStore.message();
										if (message && this.draftsFolder() === message.folder && this.draftUid() == message.uid) {
											MessageUserStore.message(null);
										}
									}

									this.draftsFolder(oData.Result.NewFolder);
									this.draftUid(oData.Result.NewUid);

									this.savedTime(new Date);

									if (this.bFromDraft) {
										setFolderHash(this.draftsFolder(), '');
									}
									setFolderHash(FolderUserStore.draftsFolder(), '');
								}
							}

							if (!result) {
								this.savedError(true);
								this.savedErrorDesc(getNotification(Notification.CantSaveMessage));
							}

							this.reloadDraftFolder();
						},
						params,
						200000
					);
				}).catch(e => {
					this.saving(false);
					this.savedError(true);
					this.savedErrorDesc(getNotification(Notification.CantSaveMessage) + ': ' + e);
				});
			}
		}

		deleteCommand() {
			if (AskPopupView.hidden() && this.modalVisibility()) {
				showScreenPopup(AskPopupView, [
					i18n('POPUPS_ASK/DESC_WANT_DELETE_MESSAGES'),
					() => {
						if (this.modalVisibility()) {
							const
								sFromFolderFullName = this.draftsFolder(),
								aUidForRemove = [this.draftUid()];
							rl.app.messagesDeleteHelper(sFromFolderFullName, aUidForRemove);
							MessageUserStore.removeMessagesFromList(sFromFolderFullName, aUidForRemove);
							this.closeCommand();
						}
					}
				]);
			}
		}

		skipCommand() {
			this.bSkipNextHide = true;

			if (
				this.modalVisibility() &&
				!this.saving() &&
				!this.sending() &&
				!FolderUserStore.draftsFolderNotEnabled() &&
				SettingsUserStore.allowDraftAutosave()
			) {
				this.saveCommand();
			}

			this.tryToClosePopup();
		}

		contactsCommand() {
			if (this.allowContacts) {
				this.skipCommand();
				setTimeout(() => {
					showScreenPopup(ContactsPopupView, [true, this.sLastFocusedField]);
				}, 200);
			}
		}

		autosaveStart() {
			clearTimeout(this.iTimer);
			this.iTimer = setTimeout(()=>{
				if (this.modalVisibility()
					&& !FolderUserStore.draftsFolderNotEnabled()
					&& SettingsUserStore.allowDraftAutosave()
					&& !this.isEmptyForm(false)
					&& !this.saving()
					&& !this.sending()
					&& !this.savedError()
				) {
					this.saveCommand();
				}

				this.autosaveStart();
			}, 60000);
		}

		emailsSource(oData, fResponse) {
			rl.app.getAutocomplete(oData.term, aData => fResponse(aData.map(oEmailItem => oEmailItem.toLine(false))));
		}

		reloadDraftFolder() {
			const draftsFolder = FolderUserStore.draftsFolder();
			if (draftsFolder && UNUSED_OPTION_VALUE !== draftsFolder) {
				setFolderHash(draftsFolder, '');
				if (FolderUserStore.currentFolderFullName() === draftsFolder) {
					rl.app.reloadMessageList(true);
				} else {
					rl.app.folderInformation(draftsFolder);
				}
			}
		}

		findIdentityByMessage(composeType, message) {
			let resultIndex = 1000,
				resultIdentity = null;
			const identities = IdentityUserStore(),
				identitiesCache = {},
				fEachHelper = (item) => {
					if (item && item.email && identitiesCache[item.email]) {
						if (!resultIdentity || resultIndex > identitiesCache[item.email][1]) {
							resultIdentity = identitiesCache[item.email][0];
							resultIndex = identitiesCache[item.email][1];
						}
					}
				};

			identities.forEach((item, index) => identitiesCache[item.email()] = [item, index]);

			if (message) {
				switch (composeType) {
					case ComposeType.Empty:
						break;
					case ComposeType.Reply:
					case ComposeType.ReplyAll:
					case ComposeType.Forward:
					case ComposeType.ForwardAsAttachment:
						message.to.concat(message.cc, message.bcc).forEach(fEachHelper);
						if (!resultIdentity) {
							message.deliveredTo.forEach(fEachHelper);
						}
						break;
					case ComposeType.Draft:
						message.from.concat(message.replyTo).forEach(fEachHelper);
						break;
					// no default
				}
			}

			return resultIdentity || identities[0] || null;
		}

		selectIdentity(identity) {
			identity = identity && identity.item;
			if (identity) {
				this.currentIdentity(identity);
				this.setSignatureFromIdentity(identity);
			}
		}

		onHide() {
			// Stop autosave
			clearTimeout(this.iTimer);

			AppUserStore.composeInEdit(this.bSkipNextHide);

			this.bSkipNextHide || this.reset();

			this.bSkipNextHide = false;

			this.to.focused(false);

			rl.route.on();

			(getFullscreenElement() === this.oContent) && exitFullscreen();
		}

		dropMailvelope() {
			if (this.mailvelope) {
				elementById('mailvelope-editor').textContent = '';
				this.mailvelope = null;
			}
		}

		editor(fOnInit) {
			if (fOnInit && this.editorArea()) {
				if (this.oEditor) {
					fOnInit(this.oEditor);
				} else {
					// setTimeout(() => {
					this.oEditor = new HtmlEditor(
						this.editorArea(),
						null,
						() => fOnInit(this.oEditor),
						bHtml => this.isHtml(!!bHtml)
					);
					// }, 1000);
				}
			}
		}

		setSignatureFromIdentity(identity) {
			if (identity) {
				this.editor(editor => {
					let signature = identity.signature() || '',
						isHtml = ':HTML:' === signature.slice(0, 6),
						fromLine = this.oLastMessage ? this.emailArrayToStringLineHelper(this.oLastMessage.from, true) : '';
					if (fromLine) {
						signature = signature.replace(/{{FROM-FULL}}/g, fromLine);
						if (!fromLine.includes(' ') && 0 < fromLine.indexOf('@')) {
							fromLine = fromLine.replace(/@\S+/, '');
						}
						signature = signature.replace(/{{FROM}}/g, fromLine);
					}
					signature = (isHtml ? signature.slice(6) : signature)
						.replace(/\r/g, '')
						.replace(/\s{1,2}?{{FROM}}/g, '')
						.replace(/\s{1,2}?{{FROM-FULL}}/g, '')
						.replace(/{{DATE}}/g, new Date().format('LLLL'))
						.replace(/{{TIME}}/g, new Date().format('LT'))
						.replace(/{{MOMENT:[^}]+}}/g, '');
					editor.setSignature(signature, isHtml, !!identity.signatureInsertBefore());
				});
			}
		}

		/**
		 * @param {string=} type = ComposeType.Empty
		 * @param {?MessageModel|Array=} oMessageOrArray = null
		 * @param {Array=} aToEmails = null
		 * @param {Array=} aCcEmails = null
		 * @param {Array=} aBccEmails = null
		 * @param {string=} sCustomSubject = null
		 * @param {string=} sCustomPlainText = null
		 */
		onShow(type, oMessageOrArray, aToEmails, aCcEmails, aBccEmails, sCustomSubject, sCustomPlainText) {
			rl.route.off();

			this.autosaveStart();

			this.viewModelDom.dataset.wysiwyg = SettingsUserStore.editorDefaultType();

			if (AppUserStore.composeInEdit()) {
				type = type || ComposeType.Empty;
				if (ComposeType.Empty !== type) {
					showScreenPopup(AskPopupView, [
						i18n('COMPOSE/DISCARD_UNSAVED_DATA'),
						() => {
							this.initOnShow(type, oMessageOrArray, aToEmails, aCcEmails, aBccEmails, sCustomSubject, sCustomPlainText);
						},
						null,
						false
					]);
				} else {
					this.addEmailsTo(this.to, aToEmails);
					this.addEmailsTo(this.cc, aCcEmails);
					this.addEmailsTo(this.bcc, aBccEmails);

					if (sCustomSubject && !this.subject()) {
						this.subject(sCustomSubject);
					}
				}
			} else {
				this.initOnShow(type, oMessageOrArray, aToEmails, aCcEmails, aBccEmails, sCustomSubject, sCustomPlainText);
			}

	//		(navigator.standalone || matchMedia('(display-mode: standalone)').matches || matchMedia('(display-mode: fullscreen)').matches) &&
			ThemeStore.isMobile() && this.oContent.requestFullscreen && this.oContent.requestFullscreen();
		}

		/**
		 * @param {Function} fKoValue
		 * @param {Array} emails
		 */
		addEmailsTo(fKoValue, emails) {
			if (arrayLength(emails)) {
				const value = fKoValue().trim(),
					values = emails.map(item => item ? item.toLine(false) : null)
						.validUnique();

				fKoValue(value + (value ? ', ' :  '') + values.join(', ').trim());
			}
		}

		/**
		 *
		 * @param {Array} aList
		 * @param {boolean} bFriendly
		 * @returns {string}
		 */
		emailArrayToStringLineHelper(aList, bFriendly) {
			bFriendly = !!bFriendly;
			return aList.map(item => item.toLine(bFriendly)).join(', ');
		}

		isPlainEditor() {
			let type = SettingsUserStore.editorDefaultType();
			return EditorDefaultType.Html !== type && EditorDefaultType.HtmlForced !== type;
		}

		/**
		 * @param {string=} sType = ComposeType.Empty
		 * @param {?MessageModel|Array=} oMessageOrArray = null
		 * @param {Array=} aToEmails = null
		 * @param {Array=} aCcEmails = null
		 * @param {Array=} aBccEmails = null
		 * @param {string=} sCustomSubject = null
		 * @param {string=} sCustomPlainText = null
		 */
		initOnShow(sType, oMessageOrArray, aToEmails, aCcEmails, aBccEmails, sCustomSubject, sCustomPlainText) {
			let sFrom = '',
				sTo = '',
				sCc = '',
				sDate = '',
				sSubject = '',
				sText = '',
				identity = null,
				aDraftInfo = null,
				message = null;

			const excludeEmail = {},
				mEmail = AccountUserStore.email(),
				lineComposeType = sType || ComposeType.Empty;

			oMessageOrArray = oMessageOrArray || null;
			if (oMessageOrArray) {
				message =
					1 === arrayLength(oMessageOrArray)
						? oMessageOrArray[0]
						: isArray(oMessageOrArray)
						? null
						: oMessageOrArray;
			}

			this.oLastMessage = message;

			if (null !== mEmail) {
				excludeEmail[mEmail] = true;
			}

			this.reset();

			identity = this.findIdentityByMessage(lineComposeType, message);
			if (identity) {
				excludeEmail[identity.email()] = true;
			}

			if (arrayLength(aToEmails)) {
				this.to(this.emailArrayToStringLineHelper(aToEmails));
			}

			if (arrayLength(aCcEmails)) {
				this.cc(this.emailArrayToStringLineHelper(aCcEmails));
			}

			if (arrayLength(aBccEmails)) {
				this.bcc(this.emailArrayToStringLineHelper(aBccEmails));
			}

			if (lineComposeType && message) {
				sDate = timestampToString(message.dateTimeStampInUTC(), 'FULL');
				sSubject = message.subject();
				aDraftInfo = message.aDraftInfo;

				let resplyAllParts = null;
				switch (lineComposeType) {
					case ComposeType.Empty:
						break;

					case ComposeType.Reply:
						this.to(this.emailArrayToStringLineHelper(message.replyEmails(excludeEmail)));
						this.subject(replySubjectAdd('Re', sSubject));
						this.prepareMessageAttachments(message, lineComposeType);
						this.aDraftInfo = ['reply', message.uid, message.folder];
						this.sInReplyTo = message.sMessageId;
						this.sReferences = (this.sInReplyTo + ' ' + message.sReferences).trim();
						break;

					case ComposeType.ReplyAll:
						resplyAllParts = message.replyAllEmails(excludeEmail);
						this.to(this.emailArrayToStringLineHelper(resplyAllParts[0]));
						this.cc(this.emailArrayToStringLineHelper(resplyAllParts[1]));
						this.subject(replySubjectAdd('Re', sSubject));
						this.prepareMessageAttachments(message, lineComposeType);
						this.aDraftInfo = ['reply', message.uid, message.folder];
						this.sInReplyTo = message.sMessageId;
						this.sReferences = (this.sInReplyTo + ' ' + message.references).trim();
						break;

					case ComposeType.Forward:
						this.subject(replySubjectAdd('Fwd', sSubject));
						this.prepareMessageAttachments(message, lineComposeType);
						this.aDraftInfo = ['forward', message.uid, message.folder];
						this.sInReplyTo = message.sMessageId;
						this.sReferences = (this.sInReplyTo + ' ' + message.sReferences).trim();
						break;

					case ComposeType.ForwardAsAttachment:
						this.subject(replySubjectAdd('Fwd', sSubject));
						this.prepareMessageAttachments(message, lineComposeType);
						this.aDraftInfo = ['forward', message.uid, message.folder];
						this.sInReplyTo = message.sMessageId;
						this.sReferences = (this.sInReplyTo + ' ' + message.sReferences).trim();
						break;

					case ComposeType.Draft:
						this.to(this.emailArrayToStringLineHelper(message.to));
						this.cc(this.emailArrayToStringLineHelper(message.cc));
						this.bcc(this.emailArrayToStringLineHelper(message.bcc));
						this.replyTo(this.emailArrayToStringLineHelper(message.replyTo));

						this.bFromDraft = true;

						this.draftsFolder(message.folder);
						this.draftUid(message.uid);

						this.subject(sSubject);
						this.prepareMessageAttachments(message, lineComposeType);

						this.aDraftInfo = 3 === arrayLength(aDraftInfo) ? aDraftInfo : null;
						this.sInReplyTo = message.sInReplyTo;
						this.sReferences = message.sReferences;
						break;

					case ComposeType.EditAsNew:
						this.to(this.emailArrayToStringLineHelper(message.to));
						this.cc(this.emailArrayToStringLineHelper(message.cc));
						this.bcc(this.emailArrayToStringLineHelper(message.bcc));
						this.replyTo(this.emailArrayToStringLineHelper(message.replyTo));

						this.subject(sSubject);
						this.prepareMessageAttachments(message, lineComposeType);

						this.aDraftInfo = 3 === arrayLength(aDraftInfo) ? aDraftInfo : null;
						this.sInReplyTo = message.sInReplyTo;
						this.sReferences = message.sReferences;
						break;
					// no default
				}

				sText = message.bodyAsHTML();
				let encrypted;

				switch (lineComposeType) {
					case ComposeType.Reply:
					case ComposeType.ReplyAll:
						sFrom = message.fromToLine(false, true);
						sText = '<div><p>' + i18n('COMPOSE/REPLY_MESSAGE_TITLE', { DATETIME: sDate, EMAIL: sFrom })
							+ ':</p><blockquote>'
							+ sText.replace(/<img[^>]+>/g, '').replace(/<a\s[^>]+><\/a>/g, '').trim()
							+ '</blockquote></div>';
						break;

					case ComposeType.Forward:
						sFrom = message.fromToLine(false, true);
						sTo = message.toToLine(false, true);
						sCc = message.ccToLine(false, true);
						sText = '<div><p>' + i18n('COMPOSE/FORWARD_MESSAGE_TOP_TITLE') + '</p>'
							+ i18n('GLOBAL/FROM') + ': ' + sFrom
							+ '<br>'
							+ i18n('GLOBAL/TO') + ': ' + sTo
							+ (sCc.length ? '<br>' + i18n('GLOBAL/CC') + ': ' + sCc : '')
							+ '<br>'
							+ i18n('COMPOSE/FORWARD_MESSAGE_TOP_SENT')
							+ ': '
							+ encodeHtml(sDate)
							+ '<br>'
							+ i18n('GLOBAL/SUBJECT')
							+ ': '
							+ encodeHtml(sSubject)
							+ '<br><br>'
							+ sText.trim()
							+ '</div>';
						break;

					case ComposeType.ForwardAsAttachment:
						sText = '';
						break;
					default:
						encrypted = PgpUserStore.isEncrypted(sText);
						if (encrypted) {
							sText = message.plain();
						}
				}

				this.editor(editor => {
					encrypted || editor.setHtml(sText);

					if (encrypted
						|| EditorDefaultType.PlainForced === SettingsUserStore.editorDefaultType()
						|| (!message.isHtml() && EditorDefaultType.HtmlForced !== SettingsUserStore.editorDefaultType())
					) {
						editor.modePlain();
					}

					!encrypted || editor.setPlain(sText);

					if (identity && ComposeType.Draft !== lineComposeType && ComposeType.EditAsNew !== lineComposeType) {
						this.setSignatureFromIdentity(identity);
					}

					this.setFocusInPopup();
				});
			} else if (ComposeType.Empty === lineComposeType) {
				this.subject(null != sCustomSubject ? '' + sCustomSubject : '');

				sText = null != sCustomPlainText ? '' + sCustomPlainText : '';

				this.editor(editor => {
					editor.setHtml(sText);

					if (this.isPlainEditor()) {
						editor.modePlain();
					}

					if (identity) {
						this.setSignatureFromIdentity(identity);
					}

					this.setFocusInPopup();
				});
			} else if (arrayLength(oMessageOrArray)) {
				oMessageOrArray.forEach(item => this.addMessageAsAttachment(item));

				this.editor(editor => {
					if (this.isPlainEditor()) {
						editor.setPlain('');
					} else {
						editor.setHtml('');
					}

					if (identity && ComposeType.Draft !== lineComposeType && ComposeType.EditAsNew !== lineComposeType) {
						this.setSignatureFromIdentity(identity);
					}

					this.setFocusInPopup();
				});
			} else {
				this.setFocusInPopup();
			}

			const downloads = this.getAttachmentsDownloadsForUpload();
			if (arrayLength(downloads)) {
				Remote.request('MessageUploadAttachments',
					(iError, oData) => {
						if (!iError) {
							forEachObjectEntry(oData.Result, (tempName, id) => {
								const attachment = this.getAttachmentById(id);
								if (attachment) {
									attachment.tempName(tempName);
									attachment
										.waiting(false)
										.uploading(false)
										.complete(true);
								}
							});
						} else {
							this.attachments.forEach(attachment => {
								if (attachment && attachment.fromMessage) {
									attachment
										.waiting(false)
										.uploading(false)
										.complete(true)
										.error(getUploadErrorDescByCode(UploadErrorCode.NoFileUploaded));
								}
							});
						}
					},
					{
						Attachments: downloads
					},
					999000
				);
			}

			if (identity) {
				this.currentIdentity(identity);
			}
		}

		setFocusInPopup() {
			setTimeout(() => {
				if (!this.to()) {
					this.to.focused(true);
				} else if (!this.to.focused()) {
					this.oEditor && this.oEditor.focus();
				}
			}, 100);
		}

		tryToClosePopup() {
			if (AskPopupView.hidden() && this.modalVisibility()) {
				if (this.bSkipNextHide || (this.isEmptyForm() && !this.draftUid())) {
					this.closeCommand();
				} else {
					showScreenPopup(AskPopupView, [
						i18n('POPUPS_ASK/DESC_WANT_CLOSE_THIS_WINDOW'),
						() => this.closeCommand()
					]);
				}
			}
		}

		popupMenu(event) {
			if (event.ctrlKey || event.metaKey || 'ContextMenu' == event.key
			 || (this.oEditor && !this.oEditor.hasFocus() && !inFocus())) {
				this.identitiesDropdownTrigger(true);
				return false;
			}
		}

		onBuild(dom) {
			// initUploader
			const oJua = new Jua({
					action: serverRequest('Upload'),
					clickElement: dom.querySelector('#composeUploadButton'),
					dragAndDropElement: dom.querySelector('.b-attachment-place')
				}),
				uploadCache = {},
				attachmentSizeLimit = pInt(SettingsGet('AttachmentLimit'));

			oJua
				// .on('onLimitReached', (limit) => {
				// 	alert(limit);
				// })
				.on('onDragEnter', () => {
					this.dragAndDropOver(true);
				})
				.on('onDragLeave', () => {
					this.dragAndDropOver(false);
				})
				.on('onBodyDragEnter', () => {
					this.attachmentsArea();
					this.dragAndDropVisible(true);
				})
				.on('onBodyDragLeave', () => {
					this.dragAndDropVisible(false);
				})
				.on('onProgress', (id, loaded, total) => {
					let item = uploadCache[id];
					if (!item) {
						item = this.getAttachmentById(id);
						if (item) {
							uploadCache[id] = item;
						}
					}

					if (item) {
						item.progress(Math.floor((loaded / total) * 100));
					}
				})
				.on('onSelect', (sId, oData) => {
					this.dragAndDropOver(false);

					const fileName = undefined === oData.FileName ? '' : oData.FileName.toString(),
						size = pInt(oData.Size, null),
						attachment = new ComposeAttachmentModel(sId, fileName, size);

					attachment.cancel = this.cancelAttachmentHelper(sId, oJua);

					this.attachments.push(attachment);

					this.attachmentsArea();

					if (0 < size && 0 < attachmentSizeLimit && attachmentSizeLimit < size) {
						attachment
							.waiting(false)
							.uploading(true)
							.complete(true)
							.error(i18n('UPLOAD/ERROR_FILE_IS_TOO_BIG'));

						return false;
					}

					return true;
				})
				.on('onStart', (id) => {
					let item = uploadCache[id];
					if (!item) {
						item = this.getAttachmentById(id);
						if (item) {
							uploadCache[id] = item;
						}
					}

					if (item) {
						item
							.waiting(false)
							.uploading(true)
							.complete(false);
					}
				})
				.on('onComplete', (id, result, data) => {
					const attachment = this.getAttachmentById(id),
						response = (data && data.Result) || {},
						errorCode = response.ErrorCode,
						attachmentJson = result && response.Attachment;

					let error = '';
					if (null != errorCode) {
						error = getUploadErrorDescByCode(errorCode);
					} else if (!attachmentJson) {
						error = i18n('UPLOAD/ERROR_UNKNOWN');
					}

					if (attachment) {
						if (error) {
							attachment
								.waiting(false)
								.uploading(false)
								.complete(true)
								.error(error + '\n' + response.ErrorMessage);
						} else if (attachmentJson) {
							attachment
								.waiting(false)
								.uploading(false)
								.complete(true);

							attachment.initByUploadJson(attachmentJson);
						}

						if (undefined === uploadCache[id]) {
							delete uploadCache[id];
						}
					}
				});

			this.addAttachmentEnabled(true);

			shortcuts.add('q', 'meta', Scope.Compose, ()=>false);
			shortcuts.add('w', 'meta', Scope.Compose, ()=>false);

			shortcuts.add('contextmenu', '', Scope.Compose, e => this.popupMenu(e));
			shortcuts.add('m', 'meta', Scope.Compose, e => this.popupMenu(e));

			shortcuts.add('escape,close', '', Scope.Compose, () => {
				this.skipCommand();
				return false;
			});
			shortcuts.add('arrowdown', 'meta', Scope.Compose, () => {
				this.skipCommand();
				return false;
			});

			shortcuts.add('s', 'meta', Scope.Compose, () => {
				this.saveCommand();
				return false;
			});
			shortcuts.add('save', '', Scope.Compose, () => {
				this.saveCommand();
				return false;
			});

			if (Settings.app('allowCtrlEnterOnCompose')) {
				shortcuts.add('enter', 'meta', Scope.Compose, () => {
					this.sendCommand();
					return false;
				});
			}
			shortcuts.add('mailsend', '', Scope.Compose, () => {
				this.sendCommand();
				return false;
			});

			shortcuts.add('escape,close', 'shift', Scope.Compose, () => {
				this.modalVisibility() && this.tryToClosePopup();
				return false;
			});

			this.editor(editor => editor[this.isPlainEditor()?'modePlain':'modeWysiwyg']());

			// Fullscreen must be on app, else other popups fail
			const el = doc.getElementById('rl-app');
			this.oContent = initFullscreen(el, () =>
				ThemeStore.isMobile()
				&& this.modalVisibility()
				&& (getFullscreenElement() !== el)
				&& this.skipCommand()
			);
		}

		/**
		 * @param {string} id
		 * @returns {?Object}
		 */
		getAttachmentById(id) {
			return this.attachments.find(item => item && id === item.id);
		}

		cancelAttachmentHelper(id, oJua) {
			return () => {
				const attachment = this.getAttachmentById(id);
				if (attachment) {
					this.attachments.remove(attachment);
					delegateRunOnDestroy(attachment);
					oJua && oJua.cancel(id);
				}
			};
		}

		/**
		 * @returns {Object}
		 */
		prepareAttachmentsForSendOrSave() {
			const result = {};
			this.attachments.forEach(item => {
				if (item && item.complete() && item.tempName() && item.enabled()) {
					result[item.tempName()] = [item.fileName(), item.isInline ? '1' : '0', item.CID, item.contentLocation];
				}
			});

			return result;
		}

		/**
		 * @param {MessageModel} message
		 */
		addMessageAsAttachment(message) {
			if (message) {
				let temp = message.subject();
				temp = '.eml' === temp.slice(-4).toLowerCase() ? temp : temp + '.eml';

				const attachment = new ComposeAttachmentModel(message.requestHash, temp, message.size());

				attachment.fromMessage = true;
				attachment.cancel = this.cancelAttachmentHelper(message.requestHash);
				attachment
					.waiting(false)
					.uploading(true)
					.complete(true);

				this.attachments.push(attachment);
			}
		}

		/**
		 * @param {string} url
		 * @param {string} name
		 * @param {number} size
		 * @returns {ComposeAttachmentModel}
		 */
		addAttachmentHelper(url, name, size) {
			const attachment = new ComposeAttachmentModel(url, name, size);

			attachment.fromMessage = false;
			attachment.cancel = this.cancelAttachmentHelper(url);
			attachment
				.waiting(false)
				.uploading(true)
				.complete(false);

			this.attachments.push(attachment);

			this.attachmentsArea();

			return attachment;
		}

		/**
		 * @param {MessageModel} message
		 * @param {string} type
		 */
		prepareMessageAttachments(message, type) {
			if (message) {
				if (ComposeType.ForwardAsAttachment === type) {
					this.addMessageAsAttachment(message);
				} else {
					message.attachments.forEach(item => {
						let add = false;
						switch (type) {
							case ComposeType.Reply:
							case ComposeType.ReplyAll:
								break;

							case ComposeType.Forward:
							case ComposeType.Draft:
							case ComposeType.EditAsNew:
								add = true;
								break;
							// no default
						}

						if (add) {
							const attachment = ComposeAttachmentModel.fromAttachment(item);
							attachment.cancel = this.cancelAttachmentHelper(item.download);
							attachment
								.waiting(false)
								.uploading(true)
								.complete(false);

							this.attachments.push(attachment);
						}
					});
				}
			}
		}

		/**
		 * @param {boolean=} includeAttachmentInProgress = true
		 * @returns {boolean}
		 */
		isEmptyForm(includeAttachmentInProgress = true) {
			const withoutAttachment = includeAttachmentInProgress
				? !this.attachments.length
				: !this.attachments.some(item => item && item.complete());

			return (
				!this.to.length &&
				!this.cc.length &&
				!this.bcc.length &&
				!this.replyTo.length &&
				!this.subject.length &&
				withoutAttachment &&
				(!this.oEditor || !this.oEditor.getData())
			);
		}

		reset() {
			this.to('');
			this.cc('');
			this.bcc('');
			this.replyTo('');
			this.subject('');

			this.requestDsn(false);
			this.requestReadReceipt(false);
			this.markAsImportant(false);

			this.bodyArea();

			this.aDraftInfo = null;
			this.sInReplyTo = '';
			this.bFromDraft = false;
			this.sReferences = '';

			this.sendError(false);
			this.sendSuccessButSaveError(false);
			this.savedError(false);
			this.savedTime(0);
			this.emptyToError(false);
			this.attachmentsInProcessError(false);

			this.showCc(false);
			this.showBcc(false);
			this.showReplyTo(false);

			this.pgpSign(false);
			this.pgpEncrypt(false);

			delegateRunOnDestroy(this.attachments());
			this.attachments([]);

			this.dragAndDropOver(false);
			this.dragAndDropVisible(false);

			this.draftsFolder('');
			this.draftUid(0);

			this.sending(false);
			this.saving(false);

			this.oEditor && this.oEditor.clear();

			this.dropMailvelope();
		}

		/**
		 * @returns {Array}
		 */
		getAttachmentsDownloadsForUpload() {
			return this.attachments.filter(item => item && !item.tempName()).map(
				item => item.id
			);
		}

		mailvelopeArea() {
			if (!this.mailvelope) {
				/**
				 * Creates an iframe with an editor for a new encrypted mail.
				 * The iframe will be injected into the container identified by selector.
				 * https://mailvelope.github.io/mailvelope/Editor.html
				 */
				let text = this.oEditor.getData(),
					encrypted = PgpUserStore.isEncrypted(text),
					size = SettingsGet('PhpUploadSizes')['post_max_size'],
					quota = pInt(size);
				switch (size.slice(-1)) {
					case 'G': quota *= 1024; // fallthrough
					case 'M': quota *= 1024; // fallthrough
					case 'K': quota *= 1024;
				}
				// Issue: can't select signing key
	//			this.pgpSign(this.pgpSign() || confirm('Sign this message?'));
				mailvelope.createEditorContainer('#mailvelope-editor', PgpUserStore.mailvelopeKeyring, {
					// https://mailvelope.github.io/mailvelope/global.html#EditorContainerOptions
					quota: Math.max(2048, (quota / 1024)) - 48, // (text + attachments) limit in kilobytes
					armoredDraft: encrypted ? text : '', // Ascii Armored PGP Text Block
					predefinedText: encrypted ? '' : (this.oEditor.isHtml() ? htmlToPlain(text) : text),
	/*
					quotedMail: '', // Ascii Armored PGP Text Block mail that should be quoted
					quotedMailIndent: true, // if true the quoted mail will be indented (default: true)
					quotedMailHeader: '', // header to be added before the quoted mail
					keepAttachments: false, // add attachments of quotedMail to editor (default: false)
					// Issue: can't select signing key
					signMsg: this.pgpSign()
	*/
				}).then(editor => this.mailvelope = editor);
			}
			this.viewArea('mailvelope');
		}
		attachmentsArea() {
			this.viewArea('attachments');
		}
		bodyArea() {
			this.viewArea('body');
		}

		allRecipients() {
			return [
					// From/sender is also recipient (Sent mailbox)
	//				this.currentIdentity().email(),
					this.from(),
					this.to(),
					this.cc(),
					this.bcc()
				].join(',').split(',').map(value => getEmail(value.trim())).validUnique();
		}

		initPgpEncrypt() {
			const recipients = this.allRecipients();
			PgpUserStore.hasPublicKeyForEmails(recipients).then(result => {
				console.log({canPgpEncrypt:result});
				this.canPgpEncrypt(result);
			});
			PgpUserStore.mailvelopeHasPublicKeyForEmails(recipients).then(result => {
				console.log({canMailvelope:result});
				this.canMailvelope(result);
				if (!result) {
					'mailvelope' === this.viewArea() && this.bodyArea();
	//				this.dropMailvelope();
				}
			});
		}

		togglePgpSign() {
			this.pgpSign(!this.pgpSign()/* && this.canPgpSign()*/);
		}

		togglePgpEncrypt() {
			this.pgpEncrypt(!this.pgpEncrypt()/* && this.canPgpEncrypt()*/);
		}
	}

	// Every 5 minutes
	const refreshFolders = 300000;

	let moveCache = {};

	class AppUser extends AbstractApp {
		constructor() {
			super(Remote);

			this.moveOrDeleteResponseHelper = this.moveOrDeleteResponseHelper.bind(this);

			this.messagesMoveTrigger = this.messagesMoveTrigger.debounce(500);

			// wakeUp
			const interval = 3600000; // 60m
			var lastTime = Date.now();
			setInterval(() => {
				const currentTime = Date.now();
				if (currentTime > (lastTime + interval + 1000)) {
					Remote.request('Version',
						iError => (100 < iError) && this.reload(),
						{ Version: Settings.app('version') }
					);
				}
				lastTime = currentTime;
			}, interval);

			const fn = (ev=>$htmlCL.toggle('rl-ctrl-key-pressed', ev.ctrlKey)).debounce(500);
			['keydown','keyup'].forEach(t => doc.addEventListener(t, fn));

			shortcuts.add('escape,enter', '', Scope.All, () => rl.Dropdowns.detectVisibility());
		}

		reload() {
			(Settings.app('inIframe') ? parent : window).location.reload();
		}

		/**
		 * @param {boolean=} bDropPagePosition = false
		 * @param {boolean=} bDropCurrenFolderCache = false
		 */
		reloadMessageList(bDropPagePosition = false, bDropCurrenFolderCache = false) {
			let iOffset = (MessageUserStore.listPage() - 1) * SettingsUserStore.messagesPerPage();

			if (bDropCurrenFolderCache) {
				setFolderHash(FolderUserStore.currentFolderFullName(), '');
			}

			if (bDropPagePosition) {
				MessageUserStore.listPage(1);
				MessageUserStore.listPageBeforeThread(1);
				iOffset = 0;

				rl.route.setHash(
					mailBox(
						FolderUserStore.currentFolderFullNameHash(),
						MessageUserStore.listPage(),
						MessageUserStore.listSearch(),
						MessageUserStore.listThreadUid()
					),
					true,
					true
				);
			}

			MessageUserStore.listLoading(true);
			MessageUserStore.listError('');
			Remote.messageList(
				(iError, oData, bCached) => {
					if (iError) {
						if (Notification.RequestAborted !== iError) {
							MessageUserStore.list([]);
							MessageUserStore.listError(getNotification(iError));
						}
					} else {
						MessageUserStore.setMessageList(oData, bCached);
					}
					MessageUserStore.listLoading(false);
				},
				{
					Folder: FolderUserStore.currentFolderFullName(),
					Offset: iOffset,
					Limit: SettingsUserStore.messagesPerPage(),
					Search: MessageUserStore.listSearch(),
					ThreadUid: MessageUserStore.listThreadUid()
				}
			);
		}

		messagesMoveTrigger() {
			const sTrashFolder = FolderUserStore.trashFolder(),
				sSpamFolder = FolderUserStore.spamFolder();

			forEachObjectValue(moveCache, item => {
				const isSpam = sSpamFolder === item.To,
					isTrash = sTrashFolder === item.To,
					isHam = !isSpam && sSpamFolder === item.From && getFolderInboxName() === item.To;

				Remote.request('MessageMove',
					this.moveOrDeleteResponseHelper,
					{
						FromFolder: item.From,
						ToFolder: item.To,
						Uids: item.Uid.join(','),
						MarkAsRead: (isSpam || isTrash) ? 1 : 0,
						Learning: isSpam ? 'SPAM' : isHam ? 'HAM' : ''
					},
					null,
					'',
					['MessageList']
				);
			});

			moveCache = {};
		}

		messagesMoveHelper(fromFolderFullName, toFolderFullName, uidsForMove) {
			const hash = '$$' + fromFolderFullName + '$$' + toFolderFullName + '$$';
			if (!moveCache[hash]) {
				moveCache[hash] = {
					From: fromFolderFullName,
					To: toFolderFullName,
					Uid: []
				};
			}

			moveCache[hash].Uid = moveCache[hash].Uid.concat(uidsForMove).unique();
			this.messagesMoveTrigger();
		}

		messagesDeleteHelper(sFromFolderFullName, aUidForRemove) {
			Remote.request('MessageDelete',
				this.moveOrDeleteResponseHelper,
				{
					Folder: sFromFolderFullName,
					Uids: aUidForRemove.join(',')
				},
				null,
				'',
				['MessageList']
			);
		}

		moveOrDeleteResponseHelper(iError, oData) {
			if (iError) {
				setFolderHash(FolderUserStore.currentFolderFullName(), '');
				alert(getNotification(iError));
			} else if (FolderUserStore.currentFolder()) {
				if (2 === arrayLength(oData.Result)) {
					setFolderHash(oData.Result[0], oData.Result[1]);
				} else {
					setFolderHash(FolderUserStore.currentFolderFullName(), '');
				}
				this.reloadMessageList(!MessageUserStore.list.length);
			}
		}

		/**
		 * @param {number} iDeleteType
		 * @param {string} sFromFolderFullName
		 * @param {Array} aUidForRemove
		 * @param {boolean=} bUseFolder = true
		 */
		deleteMessagesFromFolder(iDeleteType, sFromFolderFullName, aUidForRemove, bUseFolder) {
			let oMoveFolder = null,
				nSetSystemFoldersNotification = null;

			switch (iDeleteType) {
				case FolderType.Spam:
					oMoveFolder = getFolderFromCacheList(FolderUserStore.spamFolder());
					nSetSystemFoldersNotification = SetSystemFoldersNotification.Spam;
					break;
				case FolderType.NotSpam:
					oMoveFolder = getFolderFromCacheList(getFolderInboxName());
					break;
				case FolderType.Trash:
					oMoveFolder = getFolderFromCacheList(FolderUserStore.trashFolder());
					nSetSystemFoldersNotification = SetSystemFoldersNotification.Trash;
					break;
				case FolderType.Archive:
					oMoveFolder = getFolderFromCacheList(FolderUserStore.archiveFolder());
					nSetSystemFoldersNotification = SetSystemFoldersNotification.Archive;
					break;
				// no default
			}

			bUseFolder = undefined === bUseFolder ? true : !!bUseFolder;
			if (bUseFolder) {
				if (
					(FolderType.Spam === iDeleteType && UNUSED_OPTION_VALUE === FolderUserStore.spamFolder()) ||
					(FolderType.Trash === iDeleteType && UNUSED_OPTION_VALUE === FolderUserStore.trashFolder()) ||
					(FolderType.Archive === iDeleteType && UNUSED_OPTION_VALUE === FolderUserStore.archiveFolder())
				) {
					bUseFolder = false;
				}
			}

			if (!oMoveFolder && bUseFolder) {
				showScreenPopup(FolderSystemPopupView, [nSetSystemFoldersNotification]);
			} else if (
				!bUseFolder ||
				(FolderType.Trash === iDeleteType &&
					(sFromFolderFullName === FolderUserStore.spamFolder()
					 || sFromFolderFullName === FolderUserStore.trashFolder()))
			) {
				showScreenPopup(AskPopupView, [
					i18n('POPUPS_ASK/DESC_WANT_DELETE_MESSAGES'),
					() => {
						this.messagesDeleteHelper(sFromFolderFullName, aUidForRemove);
						MessageUserStore.removeMessagesFromList(sFromFolderFullName, aUidForRemove);
					}
				]);
			} else if (oMoveFolder) {
				this.messagesMoveHelper(sFromFolderFullName, oMoveFolder.fullName, aUidForRemove);
				MessageUserStore.removeMessagesFromList(sFromFolderFullName, aUidForRemove, oMoveFolder.fullName);
			}
		}

		/**
		 * @param {string} sFromFolderFullName
		 * @param {Array} aUidForMove
		 * @param {string} sToFolderFullName
		 * @param {boolean=} bCopy = false
		 */
		moveMessagesToFolder(sFromFolderFullName, aUidForMove, sToFolderFullName, bCopy) {
			if (sFromFolderFullName !== sToFolderFullName && arrayLength(aUidForMove)) {
				const oFromFolder = getFolderFromCacheList(sFromFolderFullName),
					oToFolder = getFolderFromCacheList(sToFolderFullName);

				if (oFromFolder && oToFolder) {
					if (undefined === bCopy ? false : !!bCopy) {
						Remote.request('MessageCopy', null, {
							FromFolder: oFromFolder.fullName,
							ToFolder: oToFolder.fullName,
							Uids: aUidForMove.join(',')
						});
					} else {
						this.messagesMoveHelper(oFromFolder.fullName, oToFolder.fullName, aUidForMove);
					}

					MessageUserStore.removeMessagesFromList(oFromFolder.fullName, aUidForMove, oToFolder.fullName, bCopy);
					return true;
				}
			}

			return false;
		}

		accountsAndIdentities() {
			AccountUserStore.loading(true);
			IdentityUserStore.loading(true);

			Remote.request('AccountsAndIdentities', (iError, oData) => {
				AccountUserStore.loading(false);
				IdentityUserStore.loading(false);

				if (!iError) {
					const
	//					counts = {},
						accounts = oData.Result.Accounts,
						mainEmail = SettingsGet('MainEmail');

					if (isArray(accounts)) {
	//					AccountUserStore.accounts.forEach(oAccount => counts[oAccount.email] = oAccount.count());

						delegateRunOnDestroy(AccountUserStore.accounts());

						AccountUserStore.accounts(
							accounts.map(
								sValue => new AccountModel(sValue/*, counts[sValue]*/)
							)
						);
	//					accounts.length &&
						AccountUserStore.accounts.unshift(new AccountModel(mainEmail/*, counts[mainEmail]*/, false));
					}

					if (isArray(oData.Result.Identities)) {
						delegateRunOnDestroy(IdentityUserStore());

						IdentityUserStore(
							oData.Result.Identities.map(identityData => {
								const identity = new IdentityModel(
									pString(identityData.Id),
									pString(identityData.Email)
								);

								identity.name(pString(identityData.Name));
								identity.replyTo(pString(identityData.ReplyTo));
								identity.bcc(pString(identityData.Bcc));
								identity.signature(pString(identityData.Signature));
								identity.signatureInsertBefore(!!identityData.SignatureInsertBefore);

								return identity;
							})
						);
					}
				}
			});
		}

		/**
		 * @param {string} folder
		 * @param {Array=} list = []
		 */
		folderInformation(folder, list) {
			if (folder && folder.trim()) {
				Remote.folderInformation(
					(iError, data) => {
						if (!iError && data.Result) {
							const result = data.Result,
								hash = getFolderHash(result.Folder),
								folderFromCache = getFolderFromCacheList(result.Folder);
							if (folderFromCache) {
								folderFromCache.expires = Date.now();

								setFolderHash(result.Folder, result.Hash);

								folderFromCache.messageCountAll(result.MessageCount);

								let unreadCountChange = (folderFromCache.messageCountUnread() !== result.MessageUnseenCount);

								folderFromCache.messageCountUnread(result.MessageUnseenCount);

								if (unreadCountChange) {
									MessageFlagsCache.clearFolder(folderFromCache.fullName);
								}

								if (result.Flags.length) {
									result.Flags.forEach(message =>
										MessageFlagsCache.setFor(folderFromCache.fullName, message.Uid.toString(), message.Flags)
									);

									MessageUserStore.reloadFlagsAndCachedMessage();
								}

								MessageUserStore.initUidNextAndNewMessages(
									folderFromCache.fullName,
									result.UidNext,
									result.NewMessages
								);

								if (!hash || unreadCountChange || result.Hash !== hash) {
									if (folderFromCache.fullName === FolderUserStore.currentFolderFullName()) {
										this.reloadMessageList();
									} else if (getFolderInboxName() === folderFromCache.fullName) {
										Remote.messageList(null, {Folder: getFolderInboxName()}, true);
									}
								}
							}
						}
					},
					folder,
					list
				);
			}
		}

		/**
		 * @param {boolean=} boot = false
		 */
		folderInformationMultiply(boot = false) {
			const folders = FolderUserStore.getNextFolderNames(refreshFolders);
			if (arrayLength(folders)) {
				Remote.request('FolderInformationMultiply', (iError, oData) => {
					if (!iError && arrayLength(oData.Result)) {
						const utc = Date.now();
						oData.Result.forEach(item => {
							const hash = getFolderHash(item.Folder),
								folder = getFolderFromCacheList(item.Folder);

							if (folder) {
								folder.expires = utc;

								setFolderHash(item.Folder, item.Hash);

								folder.messageCountAll(item.MessageCount);

								let unreadCountChange = folder.messageCountUnread() !== item.MessageUnseenCount;

								folder.messageCountUnread(item.MessageUnseenCount);

								if (unreadCountChange) {
									MessageFlagsCache.clearFolder(folder.fullName);
								}

								if (!hash || item.Hash !== hash) {
									if (folder.fullName === FolderUserStore.currentFolderFullName()) {
										this.reloadMessageList();
									}
								} else if (unreadCountChange
								 && folder.fullName === FolderUserStore.currentFolderFullName()
								 && MessageUserStore.list.length) {
									this.folderInformation(folder.fullName, MessageUserStore.list());
								}
							}
						});

						if (boot) {
							setTimeout(() => this.folderInformationMultiply(true), 2000);
						}
					}
				}, {
					Folders: folders
				});
			}
		}

		/**
		 * @param {string} sFolderFullName
		 * @param {number} iSetAction
		 * @param {Array=} messages = null
		 */
		messageListAction(sFolderFullName, iSetAction, messages) {
			messages = messages || MessageUserStore.listChecked();

			let folder = null,
				alreadyUnread = 0,
				rootUids = messages.map(oMessage => oMessage && oMessage.uid ? oMessage.uid : null)
					.validUnique(),
				length = rootUids.length;

			if (sFolderFullName && length) {
				switch (iSetAction) {
					case MessageSetAction.SetSeen:
						length = 0;
						// fallthrough is intentionally
					case MessageSetAction.UnsetSeen:
						rootUids.forEach(sSubUid =>
							alreadyUnread += MessageFlagsCache.storeBySetAction(sFolderFullName, sSubUid, iSetAction)
						);

						folder = getFolderFromCacheList(sFolderFullName);
						if (folder) {
							folder.messageCountUnread(folder.messageCountUnread() - alreadyUnread + length);
						}

						Remote.request('MessageSetSeen', null, {
							Folder: sFolderFullName,
							Uids: rootUids.join(','),
							SetAction: iSetAction == MessageSetAction.SetSeen ? 1 : 0
						});
						break;

					case MessageSetAction.SetFlag:
					case MessageSetAction.UnsetFlag:
						rootUids.forEach(sSubUid =>
							MessageFlagsCache.storeBySetAction(sFolderFullName, sSubUid, iSetAction)
						);
						Remote.request('MessageSetFlagged', null, {
							Folder: sFolderFullName,
							Uids: rootUids.join(','),
							SetAction: iSetAction == MessageSetAction.SetFlag ? 1 : 0
						});
						break;
					// no default
				}

				MessageUserStore.reloadFlagsAndCachedMessage();
			}
		}

		/**
		 * @param {string} query
		 * @param {Function} autocompleteCallback
		 */
		getAutocomplete(query, autocompleteCallback) {
			Remote.suggestions((iError, data) => {
				if (!iError && isArray(data.Result)) {
					autocompleteCallback(
						data.Result.map(item => (item && item[0] ? new EmailModel(item[0], item[1]) : null)).filter(v => v)
					);
				} else if (Notification.RequestAborted !== iError) {
					autocompleteCallback([]);
				}
			}, query);
		}

		logout() {
			Remote.request('Logout', () => rl.logoutReload());
		}

		bootstart() {
			super.bootstart();

			addEventListener('resize', () => leftPanelDisabled(ThemeStore.isMobile() || 1000 > innerWidth));
			addEventListener('beforeunload', event => {
				if (arePopupsVisible()) {
					event.preventDefault();
					return event.returnValue = "Are you sure you want to exit?";
				}
			}, {capture: true});
		}

		start() {
			if (SettingsGet('Auth')) {
				rl.setWindowTitle(i18n('GLOBAL/LOADING'));

				NotificationUserStore.enableSoundNotification(!!SettingsGet('SoundNotification'));
				NotificationUserStore.enableDesktopNotification(!!SettingsGet('DesktopNotifications'));

				AccountUserStore.email(SettingsGet('Email'));

				SettingsUserStore.init();
				ContactUserStore.init();

				Remote.foldersReload(value => {
					try {
						if (value) {
							startScreens([
								MailBoxUserScreen,
								SettingsUserScreen
							]);

							setInterval(() => {
								const cF = FolderUserStore.currentFolderFullName(),
									iF = getFolderInboxName();
								this.folderInformation(iF);
								if (iF !== cF) {
									this.folderInformation(cF);
								}
								this.folderInformationMultiply();
							}, refreshFolders);

							ContactUserStore.init();

							this.accountsAndIdentities();

							setTimeout(() => {
								const cF = FolderUserStore.currentFolderFullName();
								if (getFolderInboxName() !== cF) {
									this.folderInformation(cF);
								}
								FolderUserStore.hasCapability('LIST-STATUS') || this.folderInformationMultiply(true);
							}, 1000);

							setTimeout(() => Remote.request('AppDelayStart'), 35000);

							// When auto-login is active
							if (
								SettingsGet('AccountSignMe') &&
								navigator.registerProtocolHandler
							) {
								setTimeout(() => {
									try {
										navigator.registerProtocolHandler(
											'mailto',
											location.protocol + '//' + location.host + location.pathname + '?mailto&to=%s',
											(SettingsGet('Title') || 'SnappyMail')
										);
									} catch (e) {} // eslint-disable-line no-empty

									value = SettingsGet('MailToEmail');
									value && mailToHelper(value);
								}, 500);
							}

							// add pointermove ?
							['touchstart','mousemove','keydown'].forEach(
								t => doc.addEventListener(t, SettingsUserStore.delayLogout, {passive:true})
							);
							SettingsUserStore.delayLogout();

							// initLeftSideLayoutResizer
							setTimeout(() => {
								const left = elementById('rl-left'),
									right = elementById('rl-right'),
									fToggle = () =>
										setLayoutResizer(left, right, ClientSideKeyName.FolderListSize,
											(ThemeStore.isMobile() || leftPanelDisabled()) ? 0 : 'Width');
								if (left && right) {
									fToggle();
									leftPanelDisabled.subscribe(fToggle);
								}
							}, 1);

							setInterval(this.reloadTime(), 60000);

							PgpUserStore.init();
						} else {
							this.logout();
						}
					} catch (e) {
						console.error(e);
					}
				});

			} else {
				startScreens([LoginUserScreen]);
			}
		}

		reloadTime()
		{
			setTimeout(() =>
				doc.querySelectorAll('time').forEach(element => timeToNode(element))
				, 1);
		}

		showMessageComposer(params = [])
		{
			showScreenPopup(ComposePopupView, params);
		}
	}

	var App = new AppUser();

	bootstrap(App);

}());
