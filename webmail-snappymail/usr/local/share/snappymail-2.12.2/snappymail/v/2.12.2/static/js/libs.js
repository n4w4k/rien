
(doc=>{
	Array.prototype.unique = function() { return this.filter((v, i, a) => a.indexOf(v) === i); };
	Array.prototype.validUnique = function(fn) {
		return this.filter((v, i, a) => (fn ? fn(v) : v) && a.indexOf(v) === i);
	};

	Date.defineRelativeTimeFormat = config => relativeTime = config;

	let formats = {
		LT   : {hour: 'numeric', minute: 'numeric'},
		L    : {},
		LL   : {year: 'numeric', month: 'short', day: 'numeric'},
		LLL  : {year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'},
		LLLL : {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'},
		'd M': {day: '2-digit', month: 'short'}
	},
	phpFormats = {
		// Day
		d: {day: '2-digit'},
		D: {weekday: 'short'},
		j: {day: 'numeric'},
		l: {weekday: 'long'},
		z: {},
		// Month
		F: {month: 'long'},
		m: {month: '2-digit'},
		M: {month: 'short'},
		n: {month: 'numeric'},
		Y: {year: 'numeric'},
		y: {year: '2-digit'},
		// Time
		A: {hour12: true},
		G: {hour: 'numeric'},
		H: {hour: '2-digit'},
		i: {minute: '2-digit'},
		s: {second: '2-digit'},
		u: {fractionalSecondDigits: 3},
		Z: {timeZone: 'UTC'}
	},
	relativeTime = {
		// see /snappymail/v/0.0.0/app/localization/relativetimeformat/
	},
	pad2 = v => 10 > v ? '0' + v : v;

	// Format momentjs/PHP date formats to Intl.DateTimeFormat
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat
	Date.prototype.format = function (options, UTC) {
		if (typeof options == 'string') {
			if ('Y-m-d\\TH:i:s' == options) {
				return this.getFullYear() + '-' + pad2(1 + this.getMonth()) + '-' + pad2(this.getDate())
					+ 'T' + pad2(this.getHours()) + ':' + pad2(this.getMinutes()) + ':' + pad2(this.getSeconds());
			}
			if (formats[options]) {
				options = formats[options];
			} else {
				let o, s = options + (UTC?'Z':''), i = s.length;
				console.log('Date.format('+s+')');
				options = {};
				while (i--) {
					o = phpFormats[s[i]] || phpFormats[s[i].toUpperCase()];
					o && Object.entries(o).forEach(([k,v])=>options[k]=v);
				}
				formats[s] = options;
			}
		}
		return new Intl.DateTimeFormat(doc.documentElement.lang, options).format(this);
	};

	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/RelativeTimeFormat
	Date.prototype.fromNow = function() {
		let unit = 'second',
			value = Math.round((this.getTime() - Date.now()) / 1000),
			t = [[60,'minute'],[3600,'hour'],[86400,'day'],[2628000,'month'],[31536000,'year']],
			i = 5,
			abs = Math.abs(value);
		while (i--) {
			if (t[i][0] <= abs) {
				value = Math.round(value / t[i][0]);
				unit = t[i][1];
				break;
			}
		}
		if (Intl.RelativeTimeFormat) {
			let rtf = new Intl.RelativeTimeFormat(doc.documentElement.lang);
			return rtf.format(value, unit);
		}
		abs = Math.abs(value);
		let rtf = relativeTime.long[unit][0 > value ? 'past' : 'future'],
			plural = relativeTime.plural(abs);
		return (rtf[plural] || rtf).replace('{0}', abs);
	};

	Element.prototype.closestWithin = function(selector, parent) {
		const el = this.closest(selector);
		return (el && el !== parent && parent.contains(el)) ? el : null;
	};

	Element.fromHTML = string => {
		const template = doc.createElement('template');
		template.innerHTML = string.trim();
		return template.content.firstChild;
	};

	/**
	 * Every time the function is executed,
	 * it will delay the execution with the given amount of milliseconds.
	 */
	if (!Function.prototype.debounce) {
		Function.prototype.debounce = function(ms) {
			let func = this, timer;
			return function(...args) {
				timer && clearTimeout(timer);
				timer = setTimeout(()=>{
					func.apply(this, args)
					timer = 0;
				}, ms);
			};
		};
	}

	/**
	 * No matter how many times the event is executed,
	 * the function will be executed only once, after the given amount of milliseconds.
	 */
	if (!Function.prototype.throttle) {
		Function.prototype.throttle = function(ms) {
			let func = this, timer;
			return function(...args) {
				if (!timer) {
					timer = setTimeout(()=>{
						func.apply(this, args)
						timer = 0;
					}, ms);
				}
			};
		};
	}

})(document);

(doc => {
	let idle = 'idle',
		visible = 'visible',
		status = visible,
		timer = 0,
		wakeUp = () => {
			clearTimeout(timer);
			status = visible;
			timer = setTimeout(() => {
				if (status === visible) {
					status = idle;
					dispatchEvent(new CustomEvent(idle));
				}
			}, 10000);
		},
		init = () => {
			init = () => 0;
			// Safari
			addEventListener('pagehide', () => status = 'hidden');
			// Else
			doc.addEventListener('visibilitychange', () => {
				status = doc.visibilityState;
				doc.hidden || wakeUp();
			});
			wakeUp();
			['mousemove','keyup','touchstart'].forEach(t => doc.addEventListener(t, wakeUp));
			['scroll','pageshow'].forEach(t => addEventListener(t, wakeUp));
		};

	this.ifvisible = {
		idle: callback => {
			init();
			addEventListener(idle, callback);
		},
		now: () => {
			init();
			return status === visible;
		}
	};
})(document);

/**
 * Modified version of https://github.com/Bernardo-Castilho/dragdroptouch
 * This is to only support Firefox Mobile.
 * Because touchstart must call preventDefault() to prevent scrolling
 * but then it doesn't work native in Chrome on Android
 */

(doc => {
	let ua = navigator.userAgent.toLowerCase();
	// Chrome on mobile supports drag & drop
	if (ua.includes('mobile') && ua.includes('gecko/')) {

		let opt = { passive: false, capture: false },

			dropEffect = 'move',
			effectAllowed = 'all',
			data = {},

			dataTransfer,
			dragSource,
			isDragging,
			allowDrop,
			lastTarget,
			lastTouch,
			holdInterval,

			img;

/*
		class DataTransferItem
		{
			get kind() { return 'string'; }
		}
*/
		/** https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer */
		class DataTransfer
		{
			get dropEffect() { return dropEffect; }
			set dropEffect(value) { dropEffect = value; }

			get effectAllowed() { return effectAllowed; }
			set effectAllowed(value) { effectAllowed = value; }

			get files() { return []; }
			get items() { return []; } // DataTransferItemList
			get types() { return Object.keys(data); }

			clearData(type) {
				if (type != null) {
					delete data[type];
				} else {
					data = {};
				}
			}

			getData(type) {
				return data[type] || '';
			}

			setData(type, value) {
				data[type] = value;
			}

			constructor() {
				this.setDragImage = setDragImage;
			}
		}

		const
		htmlDrag = b => doc.documentElement.classList.toggle('firefox-drag', b),

		setDragImage = (src, xOffset, yOffset) => {
			img && img.remove();
			if (src) {
				// create drag image from custom element or drag source
				img = src.cloneNode(true);
				copyStyle(src, img);
				img._x = xOffset == null ? src.clientWidth / 2 : xOffset;
				img._y = yOffset == null ? src.clientHeight / 2 : yOffset;
			}
		},

		// clear all members
		reset = () => {
			if (dragSource) {
				clearInterval(holdInterval);
				// dispose of drag image element
				img && img.remove();
				isDragging && dispatchEvent(lastTouch, 'dragend', dragSource);
				img = dragSource = lastTouch = lastTarget = dataTransfer = holdInterval = null;
				isDragging = allowDrop = false;
				htmlDrag(false);
			}
		},

		// get point for a touch event
		getPoint = e => {
			e = e.touches ? e.touches[0] : e;
			return { x: e.clientX, y: e.clientY };
		},

		touchend = e => {
			if (dragSource) {
				// finish dragging
				allowDrop && 'touchcancel' !== e.type && dispatchEvent(lastTouch, 'drop', lastTarget);
				reset();
			}
		},

		// get the element at a given touch event
		getTarget = pt => {
			let el = doc.elementFromPoint(pt.x, pt.y);
			while (el && getComputedStyle(el).pointerEvents == 'none') {
				el = el.parentElement;
			}
			return el;
		},

		// move the drag image element
		moveImage = pt => {
			requestAnimationFrame(() => {
				if (img) {
					img.style.left = Math.round(pt.x - img._x) + 'px';
					img.style.top = Math.round(pt.y - img._y) + 'px';
				}
			});
		},

		copyStyle = (src, dst) => {
			// remove potentially troublesome attributes
			['id','class','style','draggable'].forEach(att => dst.removeAttribute(att));
			// copy canvas content
			if (src instanceof HTMLCanvasElement) {
				let cSrc = src, cDst = dst;
				cDst.width = cSrc.width;
				cDst.height = cSrc.height;
				cDst.getContext('2d').drawImage(cSrc, 0, 0);
			}
			// copy style (without transitions)
			let cs = getComputedStyle(src);
			Object.entries(cs).forEach(([key, value]) => key.includes('transition') || (dst.style[key] = value));
			dst.style.pointerEvents = 'none';
			// and repeat for all children
			let i = src.children.length;
			while (i--) copyStyle(src.children[i], dst.children[i]);
		},

		// return false when cancelled
		dispatchEvent = (e, type, target) => {
			if (e && target) {
				let evt = new Event(type, {bubbles:true,cancelable:true});
				evt.button = 0;
				evt.buttons = 1;
				// copy event properties into new event
				['altKey','ctrlKey','metaKey','shiftKey'].forEach(k => evt[k] = e[k]);
				let src = e.touches ? e.touches[0] : e;
				['pageX','pageY','clientX','clientY','screenX','screenY','offsetX','offsetY'].forEach(k => evt[k] = src[k]);
				if (dragSource) {
					evt.dataTransfer = dataTransfer;
				}
				return target.dispatchEvent(evt);
			}
			return false;
		};

		doc.addEventListener('touchstart', e => {
			// clear all variables
			reset();
			// ignore events that have been handled or that involve more than one touch
			if (e && !e.defaultPrevented && e.touches && e.touches.length < 2) {
				// get nearest draggable element
				dragSource = e.target.closest('[draggable]');
				if (dragSource) {
					// get ready to start dragging
					lastTouch = e;
//					dragSource.style.userSelect = 'none';

					// 1000 ms to wait, chrome on android triggers dragstart in 600
					holdInterval = setTimeout(() => {
						// start dragging
						dataTransfer = new DataTransfer();
						if ((isDragging = dispatchEvent(e, 'dragstart', dragSource))) {
							htmlDrag(true);

							let pt = getPoint(e);

							// create drag image from custom element or drag source
							img || setDragImage(dragSource);
							let style = img.style;
							style.top = style.left = '-9999px';
							style.position = 'fixed';
							style.pointerEvents = 'none';
							style.zIndex = '999999999';
							// add image to document
							moveImage(pt);
							doc.body.append(img);

							dispatchEvent(e, 'dragenter', getTarget(pt));
						} else {
							reset();
						}
					}, 1000);
				}
			}
		}, opt);

		doc.addEventListener('touchmove', e => {
			if (isDragging) {
				// continue dragging
				let pt = getPoint(e),
					target = getTarget(pt);
				lastTouch = e;
				if (target != lastTarget) {
					dispatchEvent(e, 'dragleave', lastTarget);
					dispatchEvent(e, 'dragenter', target);
					lastTarget = target;
				}
				moveImage(pt);
				allowDrop = !dispatchEvent(e, 'dragover', target);
			} else {
				reset();
			}
		}, opt);

		doc.addEventListener('touchend', touchend);
		doc.addEventListener('touchcancel', touchend);
	}

})(document);


(win => {

let
	scope = {},
	_scope = 'all';

const
	doc = document,
	// On Mac we use ⌘ else the Ctrl key
	meta = /Mac OS X/.test(navigator.userAgent) ? 'meta' : 'ctrl',
	_scopes = {
		all: {}
	},
	toArray = v => Array.isArray(v) ? v : v.split(/\s*,\s*/),

	// ignore keydown in any element that supports keyboard input
	filter = node => !(!node.closest || node.closest('input,select,textarea,[contenteditable]')),

	shortcuts = {
		on: () => doc.addEventListener('keydown', keydown),
		off: () => doc.removeEventListener('keydown', keydown),
		add: (keys, modifiers, scopes, method) => {
			if (method === undefined) {
				method = scopes;
				scopes = 'all';
			}
			toArray(scopes).forEach(scope => {
				if (!_scopes[scope]) {
					_scopes[scope] = {};
				}
				toArray(keys).forEach(key => {
					key = key.toLowerCase();
					if (!_scopes[scope][key]) {
						_scopes[scope][key] = {};
					}
					modifiers = toArray(modifiers)
						.map(key => 'meta' == key ? meta : key)
						.unique().sort().join('+');
					if (!_scopes[scope][key][modifiers]) {
						_scopes[scope][key][modifiers] = [];
					}
					_scopes[scope][key][modifiers].push(method);
				});
			});
		},
		setScope: value => {
			_scope = value || 'all';
			scope = _scopes[_scope] || {};
		},
		getScope: () => _scope,
		getMetaKey: () => 'meta' === meta ? '⌘' : 'Ctrl'
	},

	keydown = event => {
		let key = (event.key || '').toLowerCase().replace(' ','space'),
			scopes = [];
		scope[key] && scopes.push(scope[key]);
		_scope !== 'all' && _scopes.all[key] && scopes.push(_scopes.all[key]);
		if (scopes.length && filter(event.target)) {
			let modifiers = ['alt','ctrl','meta','shift'].filter(v => event[v+'Key']).join('+');
			scopes.forEach(actions => {
				// for each potential shortcut
				actions[modifiers] && actions[modifiers].forEach(cmd => {
					try {
						// call the handler and stop the event if neccessary
						if (!event.defaultPrevented && cmd(event) === false) {
							event.preventDefault();
							event.stopPropagation();
						}
					} catch (e) {
						console.error(e);
					}
				});
			});
		}
	};

win.shortcuts = shortcuts;

shortcuts.on();

})(this);

/*!!
 * Hasher <http://github.com/millermedeiros/hasher>
 * @author Miller Medeiros
 * @version 1.1.2 (2012/10/31 03:19 PM)
 * Released under the MIT License
 */

(global => {

    //--------------------------------------------------------------------------------------
    // Private
    //--------------------------------------------------------------------------------------

    const
    _hashValRegexp = /#(.*)$/,
    _hashRegexp = /^#/,
    _hashTrim = /^\/|\$/g,
    _trimHash = hash => hash ? hash.replace(_hashTrim, '') : '',
    _getWindowHash = () => {
        //parsed full URL instead of getting window.location.hash because Firefox decode hash value (and all the other browsers don't)
        //also because of IE8 bug with hash query in local file [issue #6]
        var result = _hashValRegexp.exec( location.href );
        return (result && result[1]) ? decodeURIComponent(result[1]) : '';
    },
    _registerChange = newHash => {
        if (_hash !== newHash) {
            var oldHash = _hash;
            _hash = newHash; //should come before event dispatch to make sure user can get proper value inside event handler
            hasher.dispatch(_trimHash(newHash), _trimHash(oldHash));
        }
    },
    _checkHistory = () => {
        var windowHash = _getWindowHash();
        if (windowHash !== _hash) {
            _registerChange(windowHash);
        }
    },
    _setHash = (path, replace) => {
        path = path.join('/');
        path = path ? '/' + path.replace(_hashRegexp, '') : path;
        if (path !== _hash){
            // we should store raw value
            _registerChange(path);
            if (path === _hash) {
                path = '#' + encodeURI(path)
                // we check if path is still === _hash to avoid error in
                // case of multiple consecutive redirects [issue #39]
                replace
                    ? location.replace(path)
                    : (location.hash = path);
            }
        }
    },

    //--------------------------------------------------------------------------------------
    // Public (API)
    //--------------------------------------------------------------------------------------

    hasher = /** @lends hasher */ {
        clear : () => {
            _bindings = [];
            hasher.active = true;
        },

        /**
         * Signal dispatched when hash value changes.
         * - pass current hash as 1st parameter to listeners and previous hash value as 2nd parameter.
         * @type signals.Signal
         */
        active : true,
        add : callback => _bindings.push(callback),
        dispatch : (...args) => hasher.active && _bindings.forEach(callback => callback(...args)),

        /**
         * Start listening/dispatching changes in the hash/history.
         * <ul>
         *   <li>hasher won't dispatch CHANGE events by manually typing a new value or pressing the back/forward buttons before calling this method.</li>
         * </ul>
         */
        init : () => hasher.dispatch(_trimHash(_hash)),

        /**
         * Set Hash value, generating a new history record.
         * @param {...string} path    Hash value without '#'.
         * @example hasher.setHash('lorem', 'ipsum', 'dolor') -> '#/lorem/ipsum/dolor'
         */
        setHash : (...path) => _setHash(path),

        /**
         * Set Hash value without keeping previous hash on the history record.
         * Similar to calling `window.location.replace("#/hash")` but will also work on IE6-7.
         * @param {...string} path    Hash value without '#'.
         * @example hasher.replaceHash('lorem', 'ipsum', 'dolor') -> '#/lorem/ipsum/dolor'
         */
        replaceHash : (...path) => _setHash(path, true)
    };

    var _hash = _getWindowHash(),
        _bindings = [];

    addEventListener('hashchange', _checkHistory);

    global.hasher = hasher;
})(this);

/** @license
 * Crossroads.js <http://millermedeiros.github.com/crossroads.js>
 * Released under the MIT license
 * Author: Miller Medeiros
 * Version: 0.7.1 - Build: 93 (2012/02/02 09:29 AM)
 */

(global => {

    const isFunction = obj => typeof obj === 'function';

    // Crossroads --------
    //====================

    global.Crossroads = class Crossroads {

        constructor() {
            this._routes = [];
        }

        addRoute(pattern, callback) {
            var route = new Route(pattern, callback, this);
            this._routes.push(route);
            return route;
        }

        parse(request) {
            request = request || '';
            var i = 0,
                routes = this._routes,
                n = routes.length,
                route;
            //should be decrement loop since higher priorities are added at the end of array
            while (n--) {
                route = routes[n];
                if ((!i || route.greedy) && route.match(request)) {
                    route.callback && route.callback(...route._getParamsArray(request));
                    ++i;
                }
            }
        }
    }

    // Route --------------
    //=====================

    class Route {

        constructor(pattern, callback, router) {
            var isRegexPattern = pattern instanceof RegExp;
            Object.assign(this, {
                greedy: false,
                rules: {},
                _router: router,
                _pattern: pattern,
                _paramsIds: isRegexPattern ? null : captureVals(PARAMS_REGEXP, pattern),
                _optionalParamsIds: isRegexPattern ? null : captureVals(OPTIONAL_PARAMS_REGEXP, pattern),
                _matchRegexp: isRegexPattern ? pattern : compilePattern(pattern),
                callback: isFunction(callback) ? callback : null
            });
        }

        match(request) {
            // validate params even if regexp.
            var values = this._getParamsObject(request);
            return this._matchRegexp.test(request)
             && 0 == Object.entries(this.rules).filter(([key, validationRule]) => {
                var val = values[key],
                    isValid = false;
                if (key === 'normalize_'
                 || (val == null && this._optionalParamsIds && this._optionalParamsIds.indexOf(key) !== -1)) {
                    isValid = true;
                }
                else if (validationRule instanceof RegExp) {
                    isValid = validationRule.test(val);
                }
                else if (Array.isArray(validationRule)) {
                    isValid = validationRule.indexOf(val) !== -1;
                }
                else if (isFunction(validationRule)) {
                    isValid = validationRule(val, request, values);
                }
                // fail silently if validationRule is from an unsupported type
                return !isValid;
            }).length;
        }

        _getParamsObject(request) {
            var values = getParamValues(request, this._matchRegexp) || [],
                n = values.length;
            if (this._paramsIds) {
                while (n--) {
                    values[this._paramsIds[n]] = values[n];
                }
            }
            return values;
        }

        _getParamsArray(request) {
            var norm = this.rules.normalize_;
            return isFunction(norm)
                ? norm(request, this._getParamsObject(request))
                : getParamValues(request, this._matchRegexp);
        }

    }



    // Pattern Lexer ------
    //=====================

    const
        ESCAPE_CHARS_REGEXP = /[\\.+*?^$[\](){}/'#]/g, //match chars that should be escaped on string regexp
        UNNECESSARY_SLASHES_REGEXP = /\/$/g, //trailing slash
        OPTIONAL_SLASHES_REGEXP = /([:}]|\w(?=\/))\/?(:)/g, //slash between `::` or `}:` or `\w:`. $1 = before, $2 = after
        REQUIRED_SLASHES_REGEXP = /([:}])\/?(\{)/g, //used to insert slash between `:{` and `}{`

        REQUIRED_PARAMS_REGEXP = /\{([^}]+)\}/g, //match everything between `{ }`
        OPTIONAL_PARAMS_REGEXP = /:([^:]+):/g, //match everything between `: :`
        PARAMS_REGEXP = /(?:\{|:)([^}:]+)(?:\}|:)/g, //capture everything between `{ }` or `: :`

        //used to save params during compile (avoid escaping things that
        //shouldn't be escaped).
        SAVE_REQUIRED_PARAMS = '__CR_RP__',
        SAVE_OPTIONAL_PARAMS = '__CR_OP__',
        SAVE_REQUIRED_SLASHES = '__CR_RS__',
        SAVE_OPTIONAL_SLASHES = '__CR_OS__',
        SAVED_REQUIRED_REGEXP = new RegExp(SAVE_REQUIRED_PARAMS, 'g'),
        SAVED_OPTIONAL_REGEXP = new RegExp(SAVE_OPTIONAL_PARAMS, 'g'),
        SAVED_OPTIONAL_SLASHES_REGEXP = new RegExp(SAVE_OPTIONAL_SLASHES, 'g'),
        SAVED_REQUIRED_SLASHES_REGEXP = new RegExp(SAVE_REQUIRED_SLASHES, 'g'),

        captureVals = (regex, pattern) => {
            var vals = [], match;
            while (match = regex.exec(pattern)) {
                vals.push(match[1]);
            }
            return vals;
        },

        getParamValues = (request, regexp) => {
            var vals = regexp.exec(request);
            if (vals) {
                vals.shift();
            }
            return vals;
        },
        compilePattern = pattern => {
            return new RegExp('^' + (pattern
                ? pattern
                    // tokenize, save chars that shouldn't be escaped
                    .replace(UNNECESSARY_SLASHES_REGEXP, '')
                    .replace(OPTIONAL_SLASHES_REGEXP, '$1'+ SAVE_OPTIONAL_SLASHES +'$2')
                    .replace(REQUIRED_SLASHES_REGEXP, '$1'+ SAVE_REQUIRED_SLASHES +'$2')
                    .replace(OPTIONAL_PARAMS_REGEXP, SAVE_OPTIONAL_PARAMS)
                    .replace(REQUIRED_PARAMS_REGEXP, SAVE_REQUIRED_PARAMS)
                    .replace(ESCAPE_CHARS_REGEXP, '\\$&')
                    // untokenize
                    .replace(SAVED_OPTIONAL_SLASHES_REGEXP, '\\/?')
                    .replace(SAVED_REQUIRED_SLASHES_REGEXP, '\\/')
                    .replace(SAVED_OPTIONAL_REGEXP, '([^\\/]+)?/?')
                    .replace(SAVED_REQUIRED_REGEXP, '([^\\/]+)')
                : ''
            ) + '/?$'); //trailing slash is optional
        };

})(this);

/* RainLoop Webmail (c) RainLoop Team | MIT */
(doc => {
	const
		defined = v => undefined !== v,
		/**
		 * @param {*} aItems
		 * @param {Function} fFileCallback
		 * @param {number=} iLimit = 20
		 * @param {Function=} fLimitCallback
		 */
		getDataFromFiles = (aItems, fFileCallback, iLimit, fLimitCallback) =>
		{
			if (aItems && aItems.length)
			{
				let
					oFile,
					iInputLimit = iLimit,
					bUseLimit = 0 < iLimit,
					bCallLimit = !fLimitCallback
				;

				[...aItems].forEach(oItem => {
					if (oItem)
					{
						if (!bUseLimit || 0 <= --iLimit)
						{
							oFile = getDataFromFile(oItem);
							oFile && fFileCallback(oFile);
						}
						else if (bUseLimit && !bCallLimit)
						{
							bCallLimit = true;
							fLimitCallback(iInputLimit);
						}
					}
				});
			}
		},

		addEventListeners = (element, obj) =>
			Object.entries(obj).forEach(([key, value]) => element.addEventListener(key, value)),

		/**
		 * @param {*} oFile
		 * @return {Object}
		 */
		getDataFromFile = oFile =>
		{
			let
				iSize = oFile.size || 0,
				sType = oFile.type || ''
			;

			return (sType && iSize)
			? {
				FileName: (oFile.name || '').replace(/^.*\/([^/]*)$/, '$1'),
				Size: iSize,
				Type: sType,
				Folder: '',
				File : oFile
			}
			: null; // Folder
		},

		eventContainsFiles = oEvent =>
		{
			try {
				return oEvent.dataTransfer.types.includes('Files');
			} catch (e) {
				return false;
			}
		};

	class Queue extends Array
	{
		constructor(limit) {
			super();
			this.limit = parseInt(limit || 0, 10);
		}
		push(fn, ...args) {
			if (this.limit > this.length) {
				super.push([fn, args]);
				this.call();
			}
		}
		call() {
			if (!this.running) {
				this.running = true;
				let f;
				while ((f = this.shift())) f[0](...f[1]);
				this.running = false;
			}
		}
	}

	/**
	 * @constructor
	 * @param {Object=} options
	 */
	class Jua
	{
		constructor(options)
		{
			let timer,
				el = options.clickElement;

			const self = this,
				timerStart = fn => {
					timerStop();
					timer = setTimeout(fn, 200);
				},
				timerStop = () => {
					timer && clearTimeout(timer);
					timer = 0;
				};

			self.oEvents = {
				onSelect: null,
				onStart: null,
				onComplete: null,
				onProgress: null,
				onDragEnter: null,
				onDragLeave: null,
				onBodyDragEnter: null,
				onBodyDragLeave: null,
				onLimitReached: null
			};

			self.oXhrs = {};
			self.oUids = {};
			self.options = Object.assign({
					action: '',
					name: 'uploader',
					hidden: {},
					limit: 0
				}, options || {});
			self.oQueue = new Queue(1 == options.limit ? 1 : 2);

			if (el) {
				el.style.position = 'relative';
				el.style.overflow = 'hidden';
				if ('inline' === el.style.display) {
					el.style.display = 'inline-block';
				}

				self.generateNewInput(el);
			}

			el = options.dragAndDropElement;
			if (el)
			{
				let oBigDropZone = options.dragAndDropBodyElement || doc;

				if (!options.disableDocumentDropPrevent)
				{
					doc.addEventListener('dragover', oEvent => {
						if (eventContainsFiles(oEvent))
						{
							try
							{
								oEvent.dataTransfer.dropEffect = 'none';
								oEvent.preventDefault();
							}
							catch (oExc) {
								console.error(oExc);
							}
						}
					});
				}

				if (oBigDropZone)
				{
					addEventListeners(oBigDropZone, {
						dragover: () => timerStop(),
						dragenter: oEvent => {
							if (eventContainsFiles(oEvent))
							{
								timerStop();
								oEvent.preventDefault();

								self.runEvent('onBodyDragEnter', oEvent);
							}
						},
						dragleave: oEvent =>
							oEvent.dataTransfer && timerStart(() => self.runEvent('onBodyDragLeave', oEvent)),
						drop: oEvent => {
							if (oEvent.dataTransfer) {
								let bFiles = eventContainsFiles(oEvent);
								bFiles && oEvent.preventDefault();

								self.runEvent('onBodyDragLeave', oEvent);

								return !bFiles;
							}

							return false;
						}
					});
				}

				addEventListeners(el, {
					dragenter: oEvent => {
						if (eventContainsFiles(oEvent)) {
							timerStop();

							oEvent.preventDefault();
							self.runEvent('onDragEnter', el, oEvent);
						}
					},
					dragover: oEvent => {
						if (eventContainsFiles(oEvent)) {
							try
							{
								let sEffect = oEvent.dataTransfer.effectAllowed;

								timerStop();

								oEvent.dataTransfer.dropEffect = (sEffect === 'move' || sEffect === 'linkMove') ? 'move' : 'copy';

								oEvent.stopPropagation();
								oEvent.preventDefault();
							}
							catch (oExc) {
								console.error(oExc);
							}
						}
					},
					dragleave: oEvent => {
						if (oEvent.dataTransfer) {
							let oRelatedTarget = doc.elementFromPoint(oEvent.clientX, oEvent.clientY);
							if (!oRelatedTarget || !el.contains(oRelatedTarget)) {
								timerStop();
								self.runEvent('onDragLeave', el, oEvent);
							}
						}
					},
					drop: oEvent => {
						if (eventContainsFiles(oEvent)) {
							oEvent.preventDefault();

							getDataFromFiles(
								oEvent.files || oEvent.dataTransfer.files,
								oFile => {
									if (oFile) {
										self.addNewFile(oFile);
										timerStop();
									}
								},
								self.options.limit,
								self.getEvent('onLimitReached')
							);
						}

						self.runEvent('onDragLeave', oEvent);
					}
				});
			}
		}

		/**
		 * @param {string} sName
		 * @param {Function} fFunc
		 */
		on(sName, fFunc)
		{
			this.oEvents[sName] = fFunc;
			return this;
		}

		/**
		 * @param {string} sName
		 */
		runEvent(sName, ...aArgs)
		{
			this.oEvents[sName] && this.oEvents[sName].apply(null, aArgs);
		}

		/**
		 * @param {string} sName
		 */
		getEvent(sName)
		{
			return this.oEvents[sName] || null;
		}

		/**
		 * @param {Object} oFileInfo
		 */
		addNewFile(oFileInfo)
		{
			this.addFile('jua-uid-' + Jua.randomId(16) + '-' + (Date.now().toString()), oFileInfo);
		}

		/**
		 * @param {string} sUid
		 * @param {Object} oFileInfo
		 */
		addFile(sUid, oFileInfo)
		{
			const fOnSelect = this.getEvent('onSelect');
			if (oFileInfo && (!fOnSelect || (false !== fOnSelect(sUid, oFileInfo))))
			{
				this.oUids[sUid] = true;
				this.oQueue.push((...args) => this.uploadTask(...args), sUid, oFileInfo);
			}
			else
			{
				this.cancel(sUid);
			}
		}

		/**
		 * @param {string} sUid
		 * @param {?} oFileInfo
		 */
		uploadTask(sUid, oFileInfo)
		{
			if (false === this.oUids[sUid] || !oFileInfo || !oFileInfo['File'])
			{
				return false;
			}

			try
			{
				const
					self = this,
					oXhr = new XMLHttpRequest(),
					oFormData = new FormData(),
					sAction = this.options.action,
					aHidden = this.options.hidden,
					fStartFunction = this.getEvent('onStart'),
					fProgressFunction = this.getEvent('onProgress')
				;

				oXhr.open('POST', sAction, true);

				if (fProgressFunction && oXhr.upload)
				{
					oXhr.upload.onprogress = oEvent => {
						if (oEvent && oEvent.lengthComputable && defined(oEvent.loaded) && defined(oEvent.total))
						{
							fProgressFunction(sUid, oEvent.loaded, oEvent.total);
						}
					};
				}

				oXhr.onreadystatechange = () => {
					if (4 === oXhr.readyState)
					{
						delete self.oXhrs[sUid];
						let bResult = false,
							oResult = null;
						if (200 === oXhr.status)
						{
							try
							{
								oResult = JSON.parse(oXhr.responseText);
								bResult = true;
							}
							catch (e)
							{
								console.error(e);
							}
						}
						this.getEvent('onComplete')(sUid, bResult, oResult);
					}
				};

				fStartFunction && fStartFunction(sUid);

				oFormData.append(this.options.name, oFileInfo['File']);
				Object.entries(aHidden).forEach(([key, value]) =>
					oFormData.append(key, (typeof value === "function" ? value(oFileInfo) : value).toString())
				);

				oXhr.send(oFormData);

				this.oXhrs[sUid] = oXhr;
				return true;
			}
			catch (oError)
			{
				console.error(oError)
			}

			return false;
		}

		generateNewInput(oClickElement)
		{
			if (oClickElement)
			{
				const self = this,
					limit = self.options.limit,
					oInput = doc.createElement('input'),
					onClick = ()=>oInput.click();

				oInput.type = 'file';
				oInput.tabIndex = -1;
				oInput.style.display = 'none';
				oInput.multiple = 1 < limit;

				oClickElement.addEventListener('click', onClick);

				oInput.addEventListener('input', () => {
					const fFileCallback = oFile => {
						self.addNewFile(oFile);
						setTimeout(() => {
							oInput.remove();
							oClickElement.removeEventListener('click', onClick);
							self.generateNewInput(oClickElement);
						}, 10);
					};
					if (oInput.files && oInput.files.length) {
						getDataFromFiles(oInput.files, fFileCallback,
							limit,
							self.getEvent('onLimitReached')
						);
					} else {
						fFileCallback({
							FileName: oInput.value.split(/\\\//).pop(),
							Size: null,
							Type: null,
							Folder: '',
							File : null
						});
					}
				});
			}
		}

		/**
		 * @param {string} sUid
		 */
		cancel(sUid)
		{
			this.oUids[sUid] = false;
			if (this.oXhrs[sUid])
			{
				try
				{
					this.oXhrs[sUid].abort && this.oXhrs[sUid].abort();
				}
				catch (oError)
				{
					console.error(oError);
				}

				delete this.oXhrs[sUid];
			}
		}
	}

	Jua.randomId = len => {
		let arr = new Uint8Array((len || 32) / 2);
		crypto.getRandomValues(arr);
		return arr.map(dec => dec.toString(16).padStart(2,'0')).join('');
	}

	this.Jua = Jua;

})(document);

/*!
	* Native JavaScript for Bootstrap v3.0.10 (https://thednp.github.io/bootstrap.native/)
	* Copyright 2015-2020 © dnp_theme
	* Licensed under MIT (https://github.com/thednp/bootstrap.native/blob/master/LICENSE)
	*/

(doc => {
	const setFocus = element => element.focus ? element.focus() : element.setActive();

	this.BSN = {
		Dropdown: function(toggleBtn) {
			let menu, menuItems = [];
			const self = this,
				parent = toggleBtn.parentNode,
				preventEmptyAnchor = e => {
					const t = e.target, href = t.href || (t.parentNode && t.parentNode.href);
					(href && href.slice(-1) === '#') && e.preventDefault();
				},
				open = bool => {
					menu && menu.classList.toggle('show', bool);
					parent.classList.toggle('show', bool);
					toggleBtn.setAttribute('aria-expanded', bool);
					toggleBtn.open = bool;
					if (bool) {
						toggleBtn.removeEventListener('click',clickHandler);
					} else {
						setTimeout(() => toggleBtn.addEventListener('click',clickHandler), 1);
					}
				},
				toggleEvents = () => {
					let action = (toggleBtn.open ? 'add' : 'remove') + 'EventListener';
					doc[action]('click',dismissHandler);
					doc[action]('keydown',preventScroll);
					doc[action]('keyup',keyHandler);
					doc[action]('focus',dismissHandler);
				},
				dismissHandler = e => {
					let eventTarget = e.target;
					if ((!menu.contains(eventTarget) && !toggleBtn.contains(eventTarget)) || e.type !== 'focus') {
						self.hide();
						preventEmptyAnchor(e);
					}
				},
				clickHandler = e => {
					self.show();
					preventEmptyAnchor(e);
				},
				preventScroll = e => (e.key === 'ArrowUp' || e.key === 'ArrowDown') && e.preventDefault(),
				keyHandler = e => {
					if (e.key === 'Escape') {
						self.toggle();
					} else if (e.key === 'ArrowUp' || e.key === 'ArrowDown') {
						let activeItem = doc.activeElement,
							isMenuButton = activeItem === toggleBtn,
							idx = isMenuButton ? 0 : menuItems.indexOf(activeItem);
						if (parent.contains(activeItem)) {
							if (!isMenuButton) {
								idx = e.key === 'ArrowUp' ? (idx > 1 ? idx-1 : 0)
									: e.key === 'ArrowDown' ? (idx < menuItems.length-1 ? idx+1 : idx) : idx;
							}
							menuItems[idx] && setFocus(menuItems[idx]);
						} else {
							console.log('activeElement not in menu');
						}
					}
				};
			self.show = () => {
				menu = parent.querySelector('.dropdown-menu');
				menuItems = [...menu.querySelectorAll('A')].filter(item => 'none' != item.parentNode.style.display);
				!('tabindex' in menu) && menu.setAttribute('tabindex', '0');
				open(true);
				setTimeout(() => {
					setFocus( menu.getElementsByTagName('INPUT')[0] || toggleBtn );
					toggleEvents();
				},1);
			};
			self.hide = () => {
				open(false);
				toggleEvents();
				setFocus(toggleBtn);
			};
			self.toggle = () => toggleBtn.open ? self.hide() : self.show();
			open(false);
			toggleBtn.Dropdown = self;
		}
	};

})(document);

/*!
 * Knockout JavaScript library v3.5.1-sm
 * (c) The Knockout.js team - http://knockoutjs.com/
 * License: MIT (http://www.opensource.org/licenses/mit-license.php)
 */

(T=>{function O(a,c){return null===a||U[typeof a]?a===c:!1}function V(a,c){var d;return()=>{d||(d=setTimeout(()=>{d=0;a()},c))}}function W(a,c){var d;return()=>{clearTimeout(d);d=setTimeout(a,c)}}function X(a,c){null!==c&&c.o&&c.o()}function Y(a,c){var d=this.Zb,g=d[A];g.X||(this.Ta&&this.za[c]?(d.sb(c,a,this.za[c]),this.za[c]=null,--this.Ta):g.u[c]||d.sb(c,a,g.v?{T:a}:d.Qb(a)),a.ja&&a.Vb())}var L=T.document,P={},b="undefined"!==typeof P?P:{};b.m=(a,c)=>{a=a.split(".");for(var d=b,g=
0,k=a.length-1;g<k;g++)d=d[a[g]];d[a[k]]=c};b.fa=(a,c,d)=>{a[c]=d};b.m("version","3.5.1-sm");b.a={extend:(a,c)=>c?Object.assign(a,c):a,L:(a,c)=>a&&Object.entries(a).forEach(d=>c(d[0],d[1])),Xa:a=>[...a.childNodes].forEach(c=>b.removeNode(c)),Jb:a=>{a=[...a];var c=(a[0]&&a[0].ownerDocument||L).createElement("div");a.forEach(d=>c.append(b.ca(d)));return c},ya:(a,c)=>Array.prototype.map.call(a,c?d=>b.ca(d.cloneNode(!0)):d=>d.cloneNode(!0)),ua:(a,c)=>{b.a.Xa(a);c&&a.append(...c)},Aa:(a,c)=>{if(a.length){for(c=
8===c.nodeType&&c.parentNode||c;a.length&&a[0].parentNode!==c;)a.splice(0,1);for(;1<a.length&&a[a.length-1].parentNode!==c;)--a.length;if(1<a.length){c=a[0];var d=a[a.length-1];for(a.length=0;c!==d;)a.push(c),c=c.nextSibling;a.push(d)}}return a},Pb:a=>null==a?"":a.trim?a.trim():a.toString().replace(/^[\s\xa0]+|[\s\xa0]+$/g,""),Wa:a=>a.ownerDocument.documentElement.contains(1!==a.nodeType?a.parentNode:a),Sb:(a,c)=>{if(!a||!a.nodeType)throw Error("element must be a DOM node when calling triggerEvent");
a.dispatchEvent(new Event(c))},f:a=>b.O(a)?a():a,gb:(a,c)=>a.textContent=b.a.f(c)||""};b.m("utils",b.a);b.m("unwrap",b.a.f);(()=>{let a=0,c="__ko__"+Date.now(),d=new WeakMap;b.a.c={get:(g,k)=>(d.get(g)||{})[k],set:(g,k,t)=>{if(d.has(g))d.get(g)[k]=t;else{let e={};e[k]=t;d.set(g,e)}return t},Za:function(g,k,t){return this.get(g,k)||this.set(g,k,t)},clear:g=>d.delete(g),V:()=>a++ +c}})();b.a.I=(()=>{var a=b.a.c.V(),c={1:1,8:1,9:1},d={1:1,9:1};const g=(e,f)=>{var h=b.a.c.get(e,a);f&&!h&&(h=new Set,b.a.c.set(e,
a,h));return h},k=e=>{var f=g(e);f&&(new Set(f)).forEach(h=>h(e));b.a.c.clear(e);d[e.nodeType]&&t(e.childNodes,!0)},t=(e,f)=>{for(var h=[],r,p=0;p<e.length;p++)if(!f||8===e[p].nodeType)if(k(h[h.length]=r=e[p]),e[p]!==r)for(;p--&&!h.includes(e[p]););};return{ma:(e,f)=>{if("function"!=typeof f)throw Error("Callback must be a function");g(e,1).add(f)},fb:(e,f)=>{var h=g(e);h&&(h.delete(f),h.size||b.a.c.set(e,a,null))},ca:e=>{b.l.N(()=>{c[e.nodeType]&&(k(e),d[e.nodeType]&&t(e.getElementsByTagName("*")))});
return e},removeNode:e=>{b.ca(e);e.parentNode&&e.parentNode.removeChild(e)}}})();b.ca=b.a.I.ca;b.removeNode=b.a.I.removeNode;b.m("utils.domNodeDisposal",b.a.I);b.m("utils.domNodeDisposal.addDisposeCallback",b.a.I.ma);b.Rb=(()=>{function a(){if(g)for(var f=g,h=0,r;t<g;)if(r=d[t++]){if(t>f){if(5E3<=++h){t=g;setTimeout(()=>{throw Error(`'Too much recursion' after processing ${h} task groups.`);},0);break}f=g}try{r()}catch(p){setTimeout(()=>{throw p;},0)}}}function c(){a();t=g=d.length=0}var d=[],g=0,
k=1,t=0,e=(f=>{var h=L.createElement("div");(new MutationObserver(f)).observe(h,{attributes:!0});return()=>h.classList.toggle("foo")})(c);return{Nb:f=>{g||e(c);d[g++]=f;return k++},cancel:f=>{f-=k-g;f>=t&&f<g&&(d[f]=null)}}})();b.Ya={debounce:(a,c)=>a.Ha(d=>W(d,c)),rateLimit:(a,c)=>{if("number"==typeof c)var d=c;else{d=c.timeout;var g=c.method}var k="function"==typeof g?g:V;a.Ha(t=>k(t,d,c))},notify:(a,c)=>{a.ra="always"==c?null:O}};var U={undefined:1,"boolean":1,number:1,string:1};b.m("extenders",
b.Ya);class Z{constructor(a,c,d){this.T=a;this.mb=c;this.oa=d;this.Na=!1;this.C=this.W=null;b.fa(this,"dispose",this.o)}o(){this.Na||(this.C&&b.a.I.fb(this.W,this.C),this.Na=!0,this.oa(),this.T=this.mb=this.oa=this.W=this.C=null)}i(a){this.W=a;b.a.I.ma(a,this.C=this.o.bind(this))}}b.R=function(){Object.setPrototypeOf(this,H);H.Ea(this)};var H={Ea:a=>{a.S=new Map;a.S.set("change",new Set);a.rb=1},subscribe:function(a,c,d){var g=this;d=d||"change";var k=new Z(g,c?a.bind(c):a,()=>{g.S.get(d).delete(k);
g.wa&&g.wa(d)});g.na&&g.na(d);g.S.has(d)||g.S.set(d,new Set);g.S.get(d).add(k);return k},s:function(a,c){c=c||"change";"change"===c&&this.Ka();if(this.sa(c)){c="change"===c&&this.Tb||new Set(this.S.get(c));try{b.l.wb(),c.forEach(d=>{d.Na||d.mb(a)})}finally{b.l.end()}}},Ca:function(){return this.rb},ec:function(a){return this.Ca()!==a},Ka:function(){++this.rb},Ha:function(a){var c=this,d=b.O(c),g,k,t,e,f;c.va||(c.va=c.s,c.s=function(r,p){p&&"change"!==p?"beforeChange"===p?this.ob(r):this.va(r,p):this.pb(r)});
var h=a(()=>{c.ja=!1;d&&e===c&&(e=c.nb?c.nb():c());var r=k||f&&c.Ga(t,e);f=k=g=!1;r&&c.va(t=e)});c.pb=(r,p)=>{p&&c.ja||(f=!p);c.Tb=new Set(c.S.get("change"));c.ja=g=!0;e=r;h()};c.ob=r=>{g||(t=r,c.va(r,"beforeChange"))};c.qb=()=>{f=!0};c.Vb=()=>{c.Ga(t,c.G(!0))&&(k=!0)}},sa:function(a){return(this.S.get(a)||[]).size},Ga:function(a,c){return!this.ra||!this.ra(a,c)},toString:()=>"[object Object]",extend:function(a){var c=this;a&&b.a.L(a,(d,g)=>{d=b.Ya[d];"function"==typeof d&&(c=d(c,g)||c)});return c}};
b.fa(H,"init",H.Ea);b.fa(H,"subscribe",H.subscribe);b.fa(H,"extend",H.extend);Object.setPrototypeOf(H,Function.prototype);b.R.fn=H;b.ic=a=>null!=a&&"function"==typeof a.subscribe&&"function"==typeof a.s;(()=>{var a=[],c,d=0;b.l={wb:g=>{a.push(c);c=g},end:()=>c=a.pop(),Mb:g=>{if(c){if(!b.ic(g))throw Error("Only subscribable things can act as dependencies");c.Xb.call(c.Yb,g,g.Ub||(g.Ub=++d))}},N:(g,k,t)=>{try{return a.push(c),c=void 0,g.apply(k,t||[])}finally{c=a.pop()}},Ba:()=>c&&c.j.Ba(),ab:()=>c&&
c.ab,j:()=>c&&c.j}})();const G=Symbol("_latestValue");b.Z=a=>{function c(){if(0<arguments.length)return c.Ga(c[G],arguments[0])&&(c.kb(),c[G]=arguments[0],c.La()),this;b.l.Mb(c);return c[G]}c[G]=a;Object.defineProperty(c,"length",{get:()=>null==c[G]?void 0:c[G].length});b.R.fn.Ea(c);Object.setPrototypeOf(c,J);return c};var J={toJSON:function(){let a=this[G];return a&&a.toJSON?a.toJSON():a},ra:O,G:function(){return this[G]},La:function(){this.s(this[G],"spectate");this.s(this[G])},kb:function(){this.s(this[G],
"beforeChange")}};Object.setPrototypeOf(J,b.R.fn);var K=b.Z.C="__ko_proto__";J[K]=b.Z;b.O=a=>{if((a="function"==typeof a&&a[K])&&a!==J[K]&&a!==b.j.fn[K])throw Error("Invalid object that looks like an observable; possibly from another Knockout instance");return!!a};b.jc=a=>"function"==typeof a&&(a[K]===J[K]||a[K]===b.j.fn[K]&&a.fc);b.m("observable",b.Z);b.m("isObservable",b.O);b.m("observable.fn",J);b.fa(J,"valueHasMutated",J.La);b.ha=a=>{a=a||[];if("object"!=typeof a||!("length"in a))throw Error("The argument passed when initializing an observable array must be an array, or null, or undefined.");
a=b.Z(a);Object.setPrototypeOf(a,b.ha.fn);return a.extend({trackArrayChanges:!0})};b.ha.fn={remove:function(a){for(var c=this.G(),d=!1,g="function"!=typeof a||b.O(a)?e=>e===a:a,k=c.length;k--;){var t=c[k];if(g(t)){if(c[k]!==t)throw Error("Array modified during remove; cannot remove item");d||this.kb();d=!0;c.splice(k,1)}}d&&this.La()}};Object.setPrototypeOf(b.ha.fn,b.Z.fn);Object.getOwnPropertyNames(Array.prototype).forEach(a=>{"function"===typeof Array.prototype[a]&&"constructor"!=a&&("copyWithin fill pop push reverse shift sort splice unshift".split(" ").includes(a)?
b.ha.fn[a]=function(...c){var d=this.G();this.kb();this.yb(d,a,c);c=d[a](...c);this.La();return c===d?this:c}:b.ha.fn[a]=function(...c){return this()[a](...c)})});b.Hb=a=>b.O(a)&&"function"==typeof a.remove&&"function"==typeof a.push;b.m("observableArray",b.ha);b.m("isObservableArray",b.Hb);b.Ya.trackArrayChanges=(a,c)=>{function d(){function q(){if(f){var l=[].concat(a.G()||[]);if(a.sa("arrayChange")){if(!k||1<f)k=b.a.zb(h,l,a.Qa);var n=k}h=l;k=null;f=0;n&&n.length&&a.s(n,"arrayChange")}}g?q():(g=
!0,e=a.subscribe(()=>++f,null,"spectate"),h=[].concat(a.G()||[]),k=null,t=a.subscribe(q))}a.Qa={};c&&"object"==typeof c&&b.a.extend(a.Qa,c);a.Qa.sparse=!0;if(!a.yb){var g=!1,k=null,t,e,f=0,h,r=a.na,p=a.wa;a.na=q=>{r&&r.call(a,q);"arrayChange"===q&&d()};a.wa=q=>{p&&p.call(a,q);"arrayChange"!==q||a.sa("arrayChange")||(t&&t.o(),e&&e.o(),e=t=null,g=!1,h=void 0)};a.yb=(q,l,n)=>{function m(E,C,z){return u[u.length]={status:E,value:C,index:z}}if(g&&!f){var u=[],w=q.length,v=n.length,y=0;switch(l){case "push":y=
w;case "unshift":for(q=0;q<v;q++)m("added",n[q],y+q);break;case "pop":y=w-1;case "shift":w&&m("deleted",q[y],y);break;case "splice":y=Math.min(Math.max(0,0>n[0]?w+n[0]:n[0]),w);w=1===v?w:Math.min(y+(n[1]||0),w);v=y+v-2;l=Math.max(w,v);var x=[],B=[];for(let E=y,C=2;E<l;++E,++C)E<w&&B.push(m("deleted",q[E],E)),E<v&&x.push(m("added",n[C],E));b.a.Eb(B,x);break;default:return}k=u}}}};var A=Symbol("_state");b.j=(a,c)=>{function d(){if(0<arguments.length){if("function"!==typeof g)throw Error("Cannot write a value to a ko.computed unless you specify a 'write' option. If you wish to read the current value, don't pass any parameters.");
g(...arguments);return this}k.X||b.l.Mb(d);(k.U||k.v&&d.ta())&&d.P();return k.J}"object"===typeof a?c=a:(c=c||{},a&&(c.read=a));if("function"!=typeof c.read)throw Error("Pass a function that returns the value of the ko.computed");var g=c.write,k={J:void 0,Y:!0,U:!0,Fa:!1,ib:!1,X:!1,eb:!1,v:!1,Lb:c.read,i:c.disposeWhenNodeIsRemoved||c.i||null,pa:c.disposeWhen||c.pa,Va:null,u:{},H:0,Db:null};d[A]=k;d.fc="function"===typeof g;b.R.fn.Ea(d);Object.setPrototypeOf(d,M);c.pure?(k.eb=!0,k.v=!0,b.a.extend(d,
aa)):c.deferEvaluation&&b.a.extend(d,ba);k.i&&(k.ib=!0,k.i.nodeType||(k.i=null));k.v||c.deferEvaluation||d.P();k.i&&d.ga()&&b.a.I.ma(k.i,k.Va=()=>{d.o()});return d};var M={ra:O,Ba:function(){return this[A].H},cc:function(){var a=[];b.a.L(this[A].u,(c,d)=>a[d.ka]=d.T);return a},$a:function(a){if(!this[A].H)return!1;var c=this.cc();return c.includes(a)||!!c.find(d=>d.$a&&d.$a(a))},sb:function(a,c,d){if(this[A].eb&&c===this)throw Error("A 'pure' computed must not be called recursively");this[A].u[a]=
d;d.ka=this[A].H++;d.la=c.Ca()},ta:function(){var a,c=this[A].u;for(a in c)if(Object.prototype.hasOwnProperty.call(c,a)){var d=c[a];if(this.ia&&d.T.ja||d.T.ec(d.la))return!0}},uc:function(){this.ia&&!this[A].Fa&&this.ia(!1)},ga:function(){var a=this[A];return a.U||0<a.H},vc:function(){this.ja?this[A].U&&(this[A].Y=!0):this.Cb()},Qb:function(a){return a.subscribe(this.Cb,this)},Cb:function(){var a=this,c=a.throttleEvaluation;c&&0<=c?(clearTimeout(this[A].Db),this[A].Db=setTimeout(()=>a.P(!0),c)):a.ia?
a.ia(!0):a.P(!0)},P:function(a){var c=this[A],d=c.pa,g=!1;if(!c.Fa&&!c.X){if(c.i&&!b.a.Wa(c.i)||d&&d()){if(!c.ib){this.o();return}}else c.ib=!1;c.Fa=!0;try{g=this.ac(a)}finally{c.Fa=!1}return g}},ac:function(a){var c=this[A],d=c.eb?void 0:!c.H;var g={Zb:this,za:c.u,Ta:c.H};b.l.wb({Yb:g,Xb:Y,j:this,ab:d});c.u={};c.H=0;a:{try{var k=c.Lb();break a}finally{b.l.end(),g.Ta&&!c.v&&b.a.L(g.za,X),c.Y=c.U=!1}k=void 0}c.H?g=this.Ga(c.J,k):(this.o(),g=!0);g&&(c.v?this.Ka():this.s(c.J,"beforeChange"),c.J=k,this.s(c.J,
"spectate"),!c.v&&a&&this.s(c.J),this.qb&&this.qb());d&&this.s(c.J,"awake");return g},G:function(a){var c=this[A];(c.U&&(a||!c.H)||c.v&&this.ta())&&this.P();return c.J},Ha:function(a){b.R.fn.Ha.call(this,a);this.nb=function(){this[A].v||(this[A].Y?this.P():this[A].U=!1);return this[A].J};this.ia=function(c){this.ob(this[A].J);this[A].U=!0;c&&(this[A].Y=!0);this.pb(this,!c)}},o:function(){var a=this[A];!a.v&&a.u&&b.a.L(a.u,(c,d)=>d.o&&d.o());a.i&&a.Va&&b.a.I.fb(a.i,a.Va);a.u=void 0;a.H=0;a.X=!0;a.Y=
!1;a.U=!1;a.v=!1;a.i=void 0;a.pa=void 0;a.Lb=void 0}},aa={na:function(a){var c=this,d=c[A];if(!d.X&&d.v&&"change"==a){d.v=!1;if(d.Y||c.ta())d.u=null,d.H=0,c.P()&&c.Ka();else{var g=[];b.a.L(d.u,(k,t)=>g[t.ka]=k);g.forEach((k,t)=>{var e=d.u[k],f=c.Qb(e.T);f.ka=t;f.la=e.la;d.u[k]=f});c.ta()&&c.P()&&c.Ka()}d.X||c.s(d.J,"awake")}},wa:function(a){var c=this[A];c.X||"change"!=a||this.sa("change")||(b.a.L(c.u,(d,g)=>{g.o&&(c.u[d]={T:g.T,ka:g.ka,la:g.la},g.o())}),c.v=!0,this.s(void 0,"asleep"))},Ca:function(){var a=
this[A];a.v&&(a.Y||this.ta())&&this.P();return b.R.fn.Ca.call(this)}},ba={na:function(a){"change"!=a&&"beforeChange"!=a||this.G()}};Object.setPrototypeOf(M,b.R.fn);var Q=b.Z.C;M[Q]=b.j;b.m("computed",b.j);b.m("isComputed",a=>"function"==typeof a&&a[Q]===M[Q]);b.m("computed.fn",M);b.fa(M,"dispose",M.o);b.oc=a=>{if("function"===typeof a)return b.j(a,{pure:!0});a={...a,pure:!0};return b.j(a)};(()=>{b.A={M:a=>{switch(a.nodeName){case "OPTION":return!0===a.__ko__hasDomDataOptionValue__?b.a.c.get(a,b.b.options.cb):
a.value;case "SELECT":return 0<=a.selectedIndex?b.A.M(a.options[a.selectedIndex]):void 0;default:return a.value}},Ma:(a,c,d)=>{switch(a.nodeName){case "OPTION":"string"===typeof c?(b.a.c.set(a,b.b.options.cb,void 0),delete a.__ko__hasDomDataOptionValue__,a.value=c):(b.a.c.set(a,b.b.options.cb,c),a.__ko__hasDomDataOptionValue__=!0,a.value="number"===typeof c?c:"");break;case "SELECT":for(var g=-1,k=""===c||null==c,t=0,e=a.options.length,f;t<e;++t)if(f=b.A.M(a.options[t]),f==c||""===f&&k){g=t;break}if(d||
0<=g||k&&1<a.size)a.selectedIndex=g;break;default:a.value=null==c?"":c}}}})();b.F=(()=>{function a(f){f=b.a.Pb(f);123===f.charCodeAt(0)&&(f=f.slice(1,-1));f+="\n,";var h=[],r=f.match(g),p=[],q=0;if(1<r.length){for(var l=0,n;n=r[l++];){var m=n.charCodeAt(0);if(44===m){if(0>=q){h.push(u&&p.length?{key:u,value:p.join("")}:{unknown:u||p.join("")});var u=q=0;p=[];continue}}else if(58===m){if(!q&&!u&&1===p.length){u=p.pop();continue}}else if(47===m&&1<n.length&&(47===n.charCodeAt(1)||42===n.charCodeAt(1)))continue;
else 47===m&&l&&1<n.length?(m=r[l-1].match(k))&&!t[m[0]]&&(f=f.slice(f.indexOf(n)+1),r=f.match(g),l=-1,n="/"):40===m||123===m||91===m?++q:41===m||125===m||93===m?--q:u||p.length||34!==m&&39!==m||(n=n.slice(1,-1));p.push(n)}if(0<q)throw Error("Unbalanced parentheses, braces, or brackets");}return h}var c=["true","false","null","undefined"],d=/^(?:[$_a-z][$\w]*|(.+)(\.\s*[$_a-z][$\w]*|\[.+\]))$/i,g=/"(?:\\.|[^"])*"|'(?:\\.|[^'])*'|`(?:\\.|[^`])*`|\/\*(?:[^*]|\*+[^*/])*\*+\/|\/\/.*\n|\/(?:\\.|[^/])+\/w*|[^\s:,/][^,"'`{}()/:[\]]*[^\s,"'`{}()/:[\]]|[^\s]/g,
k=/[\])"'A-Za-z0-9_$]+$/,t={"in":1,"return":1,"typeof":1},e=new Set;return{Pa:[],jb:e,mc:a,nc:function(f,h){function r(m,u){if(!n){var w=b.b[m];if(w&&w.preprocess&&!(u=w.preprocess(u,m,r)))return;if(w=e.has(m)){var v=u;c.includes(v)?v=!1:(w=v.match(d),v=null===w?!1:w[1]?"Object("+w[1]+")"+w[2]:v);w=v}w&&q.push("'"+m+"':function(_z){"+v+"=_z}")}l&&(u="function(){return "+u+" }");p.push("'"+m+"':"+u)}h=h||{};var p=[],q=[],l=h.valueAccessors,n=h.bindingParams;("string"===typeof f?a(f):f).forEach(m=>
r(m.key||m.unknown,m.value));q.length&&r("_ko_property_writers","{"+q.join(",")+" }");return p.join(",")},kc:(f,h)=>-1<f.findIndex(r=>r.key==h),lb:(f,h,r,p,q)=>{if(f&&b.O(f))!b.jc(f)||q&&f.G()===p||f(p);else if((f=h.get("_ko_property_writers"))&&f[r])f[r](p)}}})();(()=>{function a(e){return 8==e.nodeType&&g.test(e.nodeValue)}function c(e){return 8==e.nodeType&&k.test(e.nodeValue)}function d(e,f){for(var h=e,r=1,p=[];h=h.nextSibling;){if(c(h)&&(b.a.c.set(h,t,!0),!--r))return p;p.push(h);a(h)&&++r}if(!f)throw Error("Cannot find closing comment tag to match: "+
e.nodeValue);return null}var g=/^\s*ko(?:\s+([\s\S]+))?\s*$/,k=/^\s*\/ko\s*$/,t="__ko_matchedEndComment__";b.h={$:{},childNodes:e=>a(e)?d(e):e.childNodes,qa:e=>{a(e)?(e=d(e))&&[...e].forEach(f=>b.removeNode(f)):b.a.Xa(e)},ua:(e,f)=>{a(e)?(b.h.qa(e),e.after(...f)):b.a.ua(e,f)},prepend:(e,f)=>{a(e)?e.nextSibling.before(f):e.prepend(f)},Gb:(e,f,h)=>{h?h.after(f):b.h.prepend(e,f)},firstChild:e=>{if(a(e))return e=e.nextSibling,!e||c(e)?null:e;let f=e.firstChild;if(f&&c(f))throw Error("Found invalid end comment, as the first child of "+
e);return f},nextSibling:e=>{if(a(e)){var f=d(e,void 0);e=f?(f.length?f[f.length-1]:e).nextSibling:null}if((f=e.nextSibling)&&c(f)){if(c(f)&&!b.a.c.get(f,t))throw Error("Found end comment without a matching opening comment, as child of "+e);return null}return f},dc:a,sc:e=>(e=e.nodeValue.match(g))?e[1]:null}})();(()=>{const a=new Map;b.xb=new class{lc(c){switch(c.nodeType){case 1:return null!=c.getAttribute("data-bind");case 8:return b.h.dc(c);default:return!1}}bc(c,d){a:{switch(c.nodeType){case 1:var g=
c.getAttribute("data-bind");break a;case 8:g=b.h.sc(c);break a}g=null}if(g)try{let t={valueAccessors:!0},e=a.get(g);if(!e){var k="with($context){with($data||{}){return{"+b.F.nc(g,t)+"}}}";e=new Function("$context","$element",k);a.set(g,e)}return e(d,c)}catch(t){throw t.message="Unable to parse bindings.\nBindings value: "+g+"\nMessage: "+t.message,t;}return null}}})();(()=>{function a(l){var n=(l=b.a.c.get(l,p))&&l.D;n&&(l.D=null,n.Kb())}function c(l,n){for(var m,u=b.h.firstChild(n);m=u;)u=b.h.nextSibling(m),
d(l,m);b.g.notify(n,b.g.B)}function d(l,n){var m=l;if(1===n.nodeType||b.xb.lc(n))m=k(n,null,l).bindingContextForDescendants;m&&n.matches&&!n.matches("SCRIPT,TEXTAREA,TEMPLATE")&&c(m,n)}function g(l){var n=[],m={},u=[];b.a.L(l,function y(v){if(!m[v]){var x=b.b[v];x&&(x.after&&(u.push(v),x.after.forEach(B=>{if(l[B]){if(u.includes(B))throw Error("Cannot combine the following bindings, because they have a cyclic dependency: "+u.join(", "));y(B)}}),u.length--),n.push({key:v,Fb:x}));m[v]=!0}});return n}
function k(l,n,m){var u=b.a.c.Za(l,p,{}),w=u.Wb;if(!n){if(w)throw Error("You cannot apply bindings multiple times to the same element.");u.Wb=!0}w||(u.context=m);u.bb||(u.bb={});if(n&&"function"!==typeof n)var v=n;else{var y=b.j(()=>{if(v=n?n(m,l):b.xb.bc(l,m)){if(m[e])m[e]();if(m[h])m[h]()}return v},{i:l});v&&y.ga()||(y=null)}var x=m,B;if(v){var E=y?z=>()=>y()[z]():z=>v[z],C={get:z=>v[z]&&E(z)(),has:z=>z in v};b.g.B in v&&b.g.subscribe(l,b.g.B,()=>{var z=v[b.g.B]();if(z){var D=b.h.childNodes(l);
D.length&&z(D,b.Bb(D[0]))}});b.g.da in v&&(x=b.g.hb(l,m),b.g.subscribe(l,b.g.da,()=>{var z=v[b.g.da]();z&&b.h.firstChild(l)&&z(l)}));g(v).forEach(z=>{var D=z.Fb.init,F=z.Fb.update,I=z.key;if(8===l.nodeType&&!b.h.$[I])throw Error("The binding '"+I+"' cannot be used with virtual elements");try{"function"==typeof D&&b.l.N(()=>{var N=D(l,E(I),C,x.$data,x);if(N&&N.controlsDescendantBindings){if(void 0!==B)throw Error("Multiple bindings ("+B+" and "+I+") are trying to control descendant bindings of the same element. You cannot use these bindings together on the same element.");
B=I}}),"function"==typeof F&&b.j(()=>F(l,E(I),C,x.$data,x),{i:l})}catch(N){throw N.message='Unable to process binding "'+I+": "+v[I]+'"\nMessage: '+N.message,N;}})}u=void 0===B;return{shouldBindDescendants:u,bindingContextForDescendants:u&&x}}function t(l,n){return l&&l instanceof b.ba?l:new b.ba(l,void 0,void 0,n)}var e=Symbol("_subscribable"),f=Symbol("_ancestorBindingInfo"),h=Symbol("_dataDependency");b.b={};var r={};b.ba=class{constructor(l,n,m,u,w){function v(){var D=E?B():B,F=b.a.f(D);n?(b.a.extend(y,
n),f in n&&(y[f]=n[f])):(y.$parents=[],y.$root=F,y.ko=b);y[e]=z;x?F=y.$data:(y.$rawData=D,y.$data=F);m&&(y[m]=F);u&&u(y,n,F);if(n&&n[e]&&!b.l.j().$a(n[e]))n[e]();C&&(y[h]=C);return y.$data}var y=this,x=l===r,B=x?void 0:l,E="function"==typeof B&&!b.O(B),C=w&&w.dataDependency;if(w&&w.exportDependencies)v();else{var z=b.oc(v);z.G();z.ga()?z.ra=null:y[e]=void 0}}["createChildContext"](l,n,m,u){!u&&n&&"object"==typeof n&&(u=n,n=u.as,m=u.extend);return new b.ba(l,this,n,(w,v)=>{w.$parentContext=v;w.$parent=
v.$data;w.$parents=(v.$parents||[]).slice(0);w.$parents.unshift(w.$parent);m&&m(w)},u)}["extend"](l,n){return new b.ba(r,this,null,m=>b.a.extend(m,"function"==typeof l?l(m):l),n)}};var p=b.a.c.V();class q{constructor(l,n,m){this.C=l;this.oa=n;this.xa=new Set;this.B=!1;n.D||b.a.I.ma(l,a);m&&m.D&&(m.D.xa.add(l),this.W=m)}Kb(){this.W&&this.W.D&&this.W.D.$b(this.C)}$b(l){this.xa.delete(l);!this.xa.size&&this.B&&this.Ab()}Ab(){this.B=!0;this.oa.D&&!this.xa.size&&(this.oa.D=null,b.a.I.fb(this.C,a),b.g.notify(this.C,
b.g.da),this.Kb())}}b.g={B:"childrenComplete",da:"descendantsComplete",subscribe:(l,n,m,u,w)=>{var v=b.a.c.Za(l,p,{});v.ea||(v.ea=new b.R);w&&w.notifyImmediately&&v.bb[n]&&b.l.N(m,u,[l]);return v.ea.subscribe(m,u,n)},notify:(l,n)=>{var m=b.a.c.get(l,p);if(m&&(m.bb[n]=!0,m.ea&&m.ea.s(l,n),n==b.g.B))if(m.D)m.D.Ab();else if(void 0===m.D&&m.ea&&m.ea.sa(b.g.da))throw Error("descendantsComplete event not supported for bindings on this node");},hb:(l,n)=>{var m=b.a.c.Za(l,p,{});m.D||(m.D=new q(l,m,n[f]));
return n[f]==m?n:n.extend(u=>{u[f]=m})}};b.rc=l=>(l=b.a.c.get(l,p))&&l.context;b.tb=(l,n,m)=>k(l,n,t(m));b.vb=(l,n)=>{1!==n.nodeType&&8!==n.nodeType||c(t(l),n)};b.ub=function(l,n,m){if(2>arguments.length){if(n=L.body,!n)throw Error("ko.applyBindings: could not find document.body; has the document been loaded?");}else if(!n||1!==n.nodeType&&8!==n.nodeType)throw Error("ko.applyBindings: first parameter should be your view model; second parameter should be a DOM node");d(t(l,m),n)};b.Bb=l=>(l=l&&[1,
8].includes(l.nodeType)&&b.rc(l))?l.$data:void 0;b.m("bindingHandlers",b.b);b.m("applyBindings",b.ub);b.m("applyBindingAccessorsToNode",b.tb);b.m("dataFor",b.Bb)})();(()=>{function a(f,h){var r={},p=t[f]||{},q=p.template;p=p.viewModel;if(q){q.element||c(f,"Unknown template value: "+q);q=q.element;var l=L.getElementById(q);l||c(f,"Cannot find element with ID "+q);l.matches("TEMPLATE")||c(f,"Template Source Element not a <template>");r.template=b.a.ya(l.content.childNodes)}p&&("function"!==typeof p[e]&&
c(f,"Unknown viewModel value: "+p),r[e]=p[e]);r.template&&r[e]?h(r):h(null)}function c(f,h){throw Error(`Component '${f}': ${h}`);}function d(f,h){var r=!1;a(f,p=>{(r=null!=p)&&h(p)});r||h(null)}var g=Object.create(null),k=Object.create(null);b.Ra={get:(f,h)=>{var r=k[f];if(r)b.Rb.Nb(()=>h(r.definition));else{var p=g[f];if(p)p.subscribe(h);else{p=g[f]=new b.R;p.subscribe(h);d(f,l=>{k[f]={definition:l};delete g[f];q?p.s(l):b.Rb.Nb(()=>p.s(l))});var q=!0}}},tc:f=>delete k[f],register:(f,h)=>{if(!h)throw Error("Invalid configuration for "+
f);if(t[f])throw Error("Component "+f+" is already registered");t[f]=h}};var t=Object.create(null),e="createViewModel";b.m("components",b.Ra);b.m("components.register",b.Ra.register)})();(()=>{var a=0;b.b.component={init:(c,d,g,k,t)=>{var e,f,h,r=()=>{var q=e&&e.dispose;"function"===typeof q&&q.call(e);h&&h.o();f=e=h=null},p=[...b.h.childNodes(c)];b.h.qa(c);b.a.I.ma(c,r);b.j(()=>{var q=b.a.f(d());if("string"!==typeof q){var l=b.a.f(q.params);q=b.a.f(q.name)}if(!q)throw Error("No component name specified");
var n=b.g.hb(c,t),m=f=++a;b.Ra.get(q,u=>{if(f===m){r();if(!u)throw Error("Unknown component '"+q+"'");var w=u.template;if(!w)throw Error("Component '"+q+"' has no template");b.h.ua(c,b.a.ya(w));e=u.createViewModel(l,{element:c,templateNodes:p});b.vb(n.createChildContext(e,{extend:v=>{v.$component=e;v.$componentTemplateNodes=p}}),c)}})},{i:c});return{controlsDescendantBindings:!0}}};b.h.$.component=!0})();b.b.attr={update:(a,c)=>{c=b.a.f(c())||{};b.a.L(c,function(d,g){g=b.a.f(g);var k=d.indexOf(":");
k="lookupNamespaceURI"in a&&0<k&&a.lookupNamespaceURI(d.slice(0,k));var t=!1===g||null==g;t?k?a.removeAttributeNS(k,d):a.removeAttribute(d):g=g.toString();t||(k?a.setAttributeNS(k,d,g):a.setAttribute(d,g));"name"===d&&(a.name=t?"":g)})}};var R=(a,c,d)=>{c&&c.split(/\s+/).forEach(g=>a.classList.toggle(g,d))};b.b.css={update:(a,c)=>{c=b.a.f(c());null!==c&&"object"==typeof c?b.a.L(c,(d,g)=>{g=b.a.f(g);R(a,d,!!g)}):(c=b.a.Pb(c),R(a,a.__ko__cssValue,!1),a.__ko__cssValue=c,R(a,c,!0))}};b.b.enable={update:(a,
c)=>{(c=b.a.f(c()))&&a.disabled?a.removeAttribute("disabled"):c||a.disabled||(a.disabled=!0)}};b.b.disable={update:(a,c)=>b.b.enable.update(a,()=>!b.a.f(c()))};b.b.event={init:(a,c,d,g,k)=>{d=c()||{};b.a.L(d,t=>{"string"==typeof t&&a.addEventListener(t,function(e){var f=c()[t];if(f)try{g=k.$data;var h=f.apply(g,[g,...arguments])}finally{!0!==h&&e.preventDefault()}})})}};b.b.foreach={Ib:a=>()=>{var c=a(),d=b.O(c)?c.G():c;if(!d||"number"==typeof d.length)return{foreach:c};b.a.f(c);return{foreach:d.data,
as:d.as,beforeRemove:d.beforeRemove}},init:(a,c)=>b.b.template.init(a,b.b.foreach.Ib(c)),update:(a,c,d,g,k)=>b.b.template.update(a,b.b.foreach.Ib(c),d,g,k)};b.F.Pa.foreach=!1;b.h.$.foreach=!0;b.b.hasfocus={init:(a,c,d)=>{var g=t=>{a.__ko_hasfocusUpdating=!0;t=a.ownerDocument.activeElement===a;var e=c();b.F.lb(e,d,"hasfocus",t,!0);a.__ko_hasfocusLastValue=t;a.__ko_hasfocusUpdating=!1},k=g.bind(null,!0);g=g.bind(null,!1);a.addEventListener("focus",k);a.addEventListener("focusin",k);a.addEventListener("blur",
g);a.addEventListener("focusout",g);a.__ko_hasfocusLastValue=!1},update:(a,c)=>{c=!!b.a.f(c());a.__ko_hasfocusUpdating||a.__ko_hasfocusLastValue===c||(c?a.focus():a.blur())}};b.F.jb.add("hasfocus");b.b.html={init:()=>({controlsDescendantBindings:!0}),update:(a,c)=>{b.a.Xa(a);c=b.a.f(c());if(null!=c){const d=L.createElement("template");d.innerHTML="string"!=typeof c?c.toString():c;a.appendChild(d.content)}}};(function(){function a(c,d,g){b.b[c]={init:(k,t,e,f,h)=>{var r,p={};d&&(p={as:e.get("as"),
exportDependencies:!0});var q=e.has(b.g.da);b.j(()=>{var l=b.a.f(t()),n=!g!==!l,m=!r;q&&(h=b.g.hb(k,h));if(n){p.dataDependency=b.l.j();var u=d?h.createChildContext("function"==typeof l?l:t,p):b.l.Ba()?h.extend(null,p):h}m&&b.l.Ba()&&(r=b.a.ya(b.h.childNodes(k),!0));n?(m||b.h.ua(k,b.a.ya(r)),b.vb(u,k)):(b.h.qa(k),b.g.notify(k,b.g.B))},{i:k});return{controlsDescendantBindings:!0}}};b.F.Pa[c]=!1;b.h.$[c]=!0}a("if");a("ifnot",!1,!0);a("with",!0)})();var S={};b.b.options={init:a=>{if(!a.matches("SELECT"))throw Error("options binding applies only to SELECT elements");
for(;0<a.length;)a.remove(0);return{controlsDescendantBindings:!0}},update:(a,c,d)=>{function g(){return Array.from(a.options).filter(m=>m.selected)}function k(m,u,w){var v=typeof u;return"function"==v?u(m):"string"==v?m[u]:w}function t(m,u){l&&r?b.g.notify(a,b.g.B):p.length&&(m=p.includes(b.A.M(u[0])),u[0].selected=m,l&&!m&&b.l.N(b.a.Sb,null,[a,"change"]))}var e=a.multiple,f=0!=a.length&&e?a.scrollTop:null,h=b.a.f(c()),r=d.get("valueAllowUnset")&&d.has("value");c={};var p=[];r||(e?p=g().map(b.A.M):
0<=a.selectedIndex&&p.push(b.A.M(a.options[a.selectedIndex])));if(h){"undefined"==typeof h.length&&(h=[h]);var q=h.filter(m=>m||null==m);d.has("optionsCaption")&&(h=b.a.f(d.get("optionsCaption")),null!==h&&void 0!==h&&q.unshift(S))}var l=!1;c.beforeRemove=m=>a.removeChild(m);h=t;d.has("optionsAfterRender")&&"function"==typeof d.get("optionsAfterRender")&&(h=(m,u)=>{t(m,u);b.l.N(d.get("optionsAfterRender"),null,[u[0],m!==S?m:void 0])});b.a.Ob(a,q,function(m,u,w){w.length&&(p=!r&&w[0].selected?[b.A.M(w[0])]:
[],l=!0);u=a.ownerDocument.createElement("option");m===S?(b.a.gb(u,d.get("optionsCaption")),b.A.Ma(u,void 0)):(w=k(m,d.get("optionsValue"),m),b.A.Ma(u,b.a.f(w)),m=k(m,d.get("optionsText"),w),b.a.gb(u,m));return[u]},c,h);if(!r){var n;e?n=p.length&&g().length<p.length:n=p.length&&0<=a.selectedIndex?b.A.M(a.options[a.selectedIndex])!==p[0]:p.length||0<=a.selectedIndex;n&&b.l.N(b.a.Sb,null,[a,"change"])}(r||b.l.ab())&&b.g.notify(a,b.g.B);f&&20<Math.abs(f-a.scrollTop)&&(a.scrollTop=f)}};b.b.options.cb=
b.a.c.V();b.b.style={update:(a,c)=>{c=b.a.f(c()||{});b.a.L(c,(d,g)=>{g=b.a.f(g);if(null==g||!1===g)g="";if(/^--/.test(d))a.style.setProperty(d,g);else{d=d.replace(/-(\w)/g,(t,e)=>e.toUpperCase());var k=a.style[d];a.style[d]=g;g===k||a.style[d]!=k||isNaN(g)||(a.style[d]=g+"px")}})}};b.b.submit={init:(a,c,d,g,k)=>{if("function"!=typeof c())throw Error("The value for a submit binding must be a function");a.addEventListener("submit",t=>{var e=c();try{var f=e.call(k.$data,a)}finally{!0!==f&&(t.preventDefault?
t.preventDefault():t.returnValue=!1)}})}};b.b.text={init:()=>({controlsDescendantBindings:!0}),update:(a,c)=>{8===a.nodeType&&(a.text||a.after(a.text=L.createTextNode("")),a=a.text);b.a.gb(a,c())}};b.h.$.text=!0;b.b.textInput={init:(a,c,d)=>{var g=a.value,k,t,e=()=>{clearTimeout(k);t=k=void 0;var h=a.value;g!==h&&(g=h,b.F.lb(c(),d,"textInput",h))},f=()=>{var h=b.a.f(c());null==h&&(h="");void 0!==t&&h===t?setTimeout(f,4):a.value!==h&&(a.value=h,g=a.value)};a.addEventListener("input",e);a.addEventListener("change",
e);a.addEventListener("blur",e);b.j(f,{i:a})}};b.F.jb.add("textInput");b.b.textinput={preprocess:(a,c,d)=>d("textInput",a)};b.b.value={init:(a,c,d)=>{var g=a.matches("SELECT"),k=a.matches("INPUT");if(!k||"checkbox"!=a.type&&"radio"!=a.type){var t=new Set,e=d.get("valueUpdate"),f=null;e&&("string"==typeof e?t.add(e):e.forEach(q=>t.add(q)),t.delete("change"));var h=()=>{f=null;var q=c(),l=b.A.M(a);b.F.lb(q,d,"value",l)};t.forEach(q=>{var l=h;(q||"").startsWith("after")&&(l=()=>{f=b.A.M(a);setTimeout(h,
0)},q=q.slice(5));a.addEventListener(q,l)});var r=k&&"file"==a.type?()=>{var q=b.a.f(c());null==q||""===q?a.value="":b.l.N(h)}:()=>{var q=b.a.f(c()),l=b.A.M(a);if(null!==f&&q===f)setTimeout(r,0);else if(q!==l||void 0===l)g?(l=d.get("valueAllowUnset"),b.A.Ma(a,q,l),l||q===b.A.M(a)||b.l.N(h)):b.A.Ma(a,q)};if(g){var p;b.g.subscribe(a,b.g.B,()=>{p?d.get("valueAllowUnset")?r():h():(a.addEventListener("change",h),p=b.j(r,{i:a}))},null,{notifyImmediately:!0})}else a.addEventListener("change",h),b.j(r,{i:a})}else b.tb(a,
{checkedValue:c})},update:()=>{}};b.F.jb.add("value");b.b.visible={update:(a,c)=>{c=b.a.f(c());var d="none"!=a.style.display;c&&!d?a.style.display="":d&&!c&&(a.style.display="none")}};b.b.hidden={update:(a,c)=>a.hidden=!!b.a.f(c())};(function(a){b.b[a]={init:function(c,d,g,k,t){return b.b.event.init.call(this,c,()=>({[a]:d()}),g,k,t)}}})("click");(()=>{let a=b.a.c.V();class c{constructor(g){this.Ua=g}Ia(...g){let k=this.Ua;if(!g.length)return b.a.c.get(k,a)||(11===this.C?k.content:1===this.C?k:void 0);
b.a.c.set(k,a,g[0])}}class d extends c{constructor(g){super(g);g&&(this.C=g.matches("TEMPLATE")&&g.content?g.content.nodeType:1)}}b.Ja={Ua:d,Oa:c}})();(()=>{function a(e,f){if(e.length){var h=e[0],r=h.parentNode;g(h,e[e.length-1],p=>{1!==p.nodeType&&8!==p.nodeType||b.ub(f,p)});b.a.Aa(e,r)}}function c(e,f,h,r){var p=(e&&(e.nodeType?e:0<e.length?e[0]:null)||h||{}).ownerDocument;if("string"==typeof h){p=p||L;p=p.getElementById(h);if(!p)throw Error("Cannot find template with ID "+h);h=new b.Ja.Ua(p)}else if([1,
8].includes(h.nodeType))h=new b.Ja.Oa(h);else throw Error("Unknown template type: "+h);h=(h=h.Ia?h.Ia():null)?[...h.cloneNode(!0).childNodes]:null;if("number"!=typeof h.length||0<h.length&&"number"!=typeof h[0].nodeType)throw Error("Template engine must return an array of DOM nodes");p=!1;switch(f){case "replaceChildren":b.h.ua(e,h);p=!0;break;case "ignoreTargetNode":break;default:throw Error("Unknown renderMode: "+f);}p&&(a(h,r),"replaceChildren"==f&&b.g.notify(e,b.g.B));return h}function d(e,f,
h){return b.O(e)?e():"function"===typeof e?e(f,h):e}var g=(e,f,h)=>{var r;for(f=b.h.nextSibling(f);e&&(r=e)!==f;)e=b.h.nextSibling(r),h(r,e)};b.pc=function(e,f,h,r){h=h||{};var p=p||"replaceChildren";if(r){var q=r.nodeType?r:0<r.length?r[0]:null;return b.j(()=>{var l=f&&f instanceof b.ba?f:new b.ba(f,null,null,null,{exportDependencies:!0}),n=d(e,l.$data,l);c(r,p,n,l,h)},{pa:()=>!q||!b.a.Wa(q),i:q})}console.log("no targetNodeOrNodeArray")};b.qc=(e,f,h,r,p)=>{function q(v,y){b.l.N(b.a.Ob,null,[r,v,
m,h,u,y]);b.g.notify(r,b.g.B)}var l,n=h.as,m=(v,y)=>{l=p.createChildContext(v,{as:n,extend:x=>{x.$index=y;n&&(x[n+"Index"]=y)}});v=d(e,v,l);return c(r,"ignoreTargetNode",v,l,h)},u=(v,y)=>{a(y,l);l=null};if(!h.beforeRemove&&b.Hb(f)){q(f.G());var w=f.subscribe(v=>{q(f(),v)},null,"arrayChange");w.i(r);return w}return b.j(()=>{var v=b.a.f(f)||[];"undefined"==typeof v.length&&(v=[v]);q(v)},{i:r})};var k=b.a.c.V(),t=b.a.c.V();b.b.template={init:(e,f)=>{f=b.a.f(f());if("string"==typeof f||"name"in f)b.h.qa(e);
else if("nodes"in f){f=f.nodes||[];if(b.O(f))throw Error('The "nodes" option must be a plain, non-observable array.');let h=f[0]&&f[0].parentNode;h&&b.a.c.get(h,t)||(h=b.a.Jb(f),b.a.c.set(h,t,!0));(new b.Ja.Oa(e)).Ia(h)}else if(f=b.h.childNodes(e),0<f.length)f=b.a.Jb(f),(new b.Ja.Oa(e)).Ia(f);else throw Error("Anonymous template defined, but no template content was provided");return{controlsDescendantBindings:!0}},update:(e,f,h,r,p)=>{var q=f();f=b.a.f(q);h=!0;r=null;"string"==typeof f?f={}:(q="name"in
f?f.name:e,"if"in f&&(h=b.a.f(f["if"])),h&&"ifnot"in f&&(h=!b.a.f(f.ifnot)),h&&!q&&(h=!1));"foreach"in f?r=b.qc(q,h&&f.foreach||[],f,e,p):h?(h=p,"data"in f&&(h=p.createChildContext(f.data,{as:f.as,exportDependencies:!0})),r=b.pc(q,h,f,e)):b.h.qa(e);p=r;(f=b.a.c.get(e,k))&&"function"==typeof f.o&&f.o();b.a.c.set(e,k,!p||p.ga&&!p.ga()?void 0:p)}};b.F.Pa.template=e=>{e=b.F.mc(e);return 1==e.length&&e[0].unknown||b.F.kc(e,"name")?null:"This template engine does not support anonymous templates nested within its templates"};
b.h.$.template=!0})();b.a.Eb=(a,c,d)=>{if(a.length&&c.length){var g,k,t,e,f;for(g=k=0;(!d||g<d)&&(e=a[k]);++k){for(t=0;f=c[t];++t)if(e.value===f.value){e.moved=f.index;f.moved=e.index;c.splice(t,1);g=t=0;break}g+=t}}};b.a.zb=(()=>{function a(c,d,g,k,t){var e=Math.min,f=Math.max,h=[],r,p=c.length,q,l=d.length,n=l-p||1,m=p+l+1,u;for(r=0;r<=p;r++){var w=u;h.push(u=[]);var v=e(l,r+n);for(q=f(0,r-1);q<=v;q++)u[q]=q?r?c[r-1]===d[q-1]?w[q-1]:e(w[q]||m,u[q-1]||m)+1:q+1:r+1}e=[];f=[];n=[];r=p;for(q=l;r||q;)l=
h[r][q]-1,q&&l===h[r][q-1]?f.push(e[e.length]={status:g,value:d[--q],index:q}):r&&l===h[r-1][q]?n.push(e[e.length]={status:k,value:c[--r],index:r}):(--q,--r,t.sparse||e.push({status:"retained",value:d[q]}));b.a.Eb(n,f,!t.dontLimitMoves&&10*p);return e.reverse()}return function(c,d,g){g="boolean"===typeof g?{dontLimitMoves:g}:g||{};c=c||[];d=d||[];return c.length<d.length?a(c,d,"added","deleted",g):a(d,c,"deleted","added",g)}})();(()=>{function a(g,k,t,e,f){var h=[],r=b.j(()=>{var p=k(t,f,b.a.Aa(h,
g))||[];if(0<h.length){var q=h.nodeType?[h]:h;if(0<q.length){var l=q[0],n=l.parentNode,m;var u=0;for(m=p.length;u<m;u++)n.insertBefore(p[u],l);u=0;for(m=q.length;u<m;u++)b.removeNode(q[u])}e&&b.l.N(e,null,[t,p,f])}h.length=0;h.push(...p)},{i:g,pa:()=>!!h.find(b.a.Wa)});return{K:h,Sa:r.ga()?r:void 0}}var c=b.a.c.V(),d=b.a.c.V();b.a.Ob=(g,k,t,e,f,h)=>{function r(C){x={aa:C,Da:b.Z(m++)};l.push(x)}function p(C){x=q[C];x.Da(m++);b.a.Aa(x.K,g);l.push(x)}k=k||[];"undefined"==typeof k.length&&(k=[k]);e=e||
{};var q=b.a.c.get(g,c),l=[],n=0,m=0,u=[],w=[],v=[],y=0;if(q){if(!h||q&&q._countWaitingForRemove)h=Array.prototype.map.call(q,C=>C.aa),h=b.a.zb(h,k,{dontLimitMoves:e.dontLimitMoves,sparse:!0});for(let C=0,z,D,F;z=h[C];C++)switch(D=z.moved,F=z.index,z.status){case "deleted":for(;n<F;)p(n++);if(void 0===D){var x=q[n];x.Sa&&(x.Sa.o(),x.Sa=void 0);b.a.Aa(x.K,g).length&&(e.beforeRemove&&(l.push(x),y++,x.aa===d?x=null:v[x.Da.G()]=x),x&&u.push.apply(u,x.K))}n++;break;case "added":for(;m<F;)p(n++);void 0!==
D?(w.push(l.length),p(D)):r(z.value)}for(;m<k.length;)p(n++);l._countWaitingForRemove=y}else k.forEach(r);b.a.c.set(g,c,l);u.forEach(e.beforeRemove?b.ca:b.removeNode);var B,E;y=g.ownerDocument.activeElement;if(w.length)for(;void 0!=(k=w.shift());){x=l[k];for(B=void 0;k;)if((E=l[--k].K)&&E.length){B=E[E.length-1];break}for(n=0;u=x.K[n];B=u,n++)b.h.Gb(g,u,B)}for(k=0;x=l[k];k++){x.K||b.a.extend(x,a(g,t,x.aa,f,x.Da));for(n=0;u=x.K[n];B=u,n++)b.h.Gb(g,u,B);!x.hc&&f&&(f(x.aa,x.K,x.Da),x.hc=!0,B=x.K[x.K.length-
1])}y&&g.ownerDocument.activeElement!=y&&y.focus();(function(C,z){if(C)for(var D=0,F=z.length;D<F;D++)z[D]&&z[D].K.forEach(I=>C(I,D,z[D].aa))})(e.beforeRemove,v);for(k=0;k<v.length;++k)v[k]&&(v[k].aa=d)}})();T.ko=P})(this);

/* Copyright © 2011-2015 by Neil Jenkins. MIT Licensed. */
/* eslint max-len: 0 */

/**
	TODO: modifyBlocks function doesn't work very good.
	For example you have: UL > LI > [cursor here in text]
	Then create blockquote at cursor, the result is: BLOCKQUOTE > UL > LI
	not UL > LI > BLOCKQUOTE
*/

( doc => {

const
	DOCUMENT_POSITION_PRECEDING = 2, // Node.DOCUMENT_POSITION_PRECEDING
	ELEMENT_NODE = 1,                // Node.ELEMENT_NODE,
	TEXT_NODE = 3,                   // Node.TEXT_NODE,
	DOCUMENT_FRAGMENT_NODE = 11,     // Node.DOCUMENT_FRAGMENT_NODE,
	SHOW_ELEMENT = 1,                // NodeFilter.SHOW_ELEMENT,
	SHOW_TEXT = 4,                   // NodeFilter.SHOW_TEXT,
	SHOW_ELEMENT_OR_TEXT = 5,

	START_TO_START = 0, // Range.START_TO_START
	START_TO_END = 1,   // Range.START_TO_END
	END_TO_END = 2,     // Range.END_TO_END
	END_TO_START = 3,   // Range.END_TO_START

	ZWS = '\u200B',
	NBSP = '\u00A0',

	win = doc.defaultView,

	ua = navigator.userAgent,

	isMac = /Mac OS X/.test( ua ),
	isIOS = /iP(?:ad|hone|od)/.test( ua ) || ( isMac && !!navigator.maxTouchPoints ),

	isGecko = /Gecko\//.test( ua ),
	isWebKit = /WebKit\//.test( ua ),

	ctrlKey = isMac ? 'meta-' : 'ctrl-',
	osKey = isMac ? 'metaKey' : 'ctrlKey',

	// Use [^ \t\r\n] instead of \S so that nbsp does not count as white-space
	notWS = /[^ \t\r\n]/,

	indexOf = (array, value) => Array.prototype.indexOf.call(array, value),

	filterAccept = NodeFilter.FILTER_ACCEPT,

	typeToBitArray = {
		// ELEMENT_NODE
		1: 1,
		// ATTRIBUTE_NODE
		2: 2,
		// TEXT_NODE
		3: 4,
		// COMMENT_NODE
		8: 128,
		// DOCUMENT_NODE
		9: 256,
		// DOCUMENT_FRAGMENT_NODE
		11: 1024
	},

	inlineNodeNames = /^(?:#text|A|ABBR|ACRONYM|B|BR|BD[IO]|CITE|CODE|DATA|DEL|DFN|EM|FONT|HR|I|IMG|INPUT|INS|KBD|Q|RP|RT|RUBY|S|SAMP|SMALL|SPAN|STR(IKE|ONG)|SU[BP]|TIME|U|VAR|WBR)$/,
//	phrasingElements = 'ABBR,AUDIO,B,BDO,BR,BUTTON,CANVAS,CITE,CODE,COMMAND,DATA,DATALIST,DFN,EM,EMBED,I,IFRAME,IMG,INPUT,KBD,KEYGEN,LABEL,MARK,MATH,METER,NOSCRIPT,OBJECT,OUTPUT,PROGRESS,Q,RUBY,SAMP,SCRIPT,SELECT,SMALL,SPAN,STRONG,SUB,SUP,SVG,TEXTAREA,TIME,VAR,VIDEO,WBR',

	leafNodeNames = {
		BR: 1,
		HR: 1,
		IMG: 1
	},

	UNKNOWN = 0,
	INLINE = 1,
	BLOCK = 2,
	CONTAINER = 3,

	isLeaf = node => node.nodeType === ELEMENT_NODE && !!leafNodeNames[ node.nodeName ],

	getNodeCategory = node => {
		switch ( node.nodeType ) {
		case TEXT_NODE:
			return INLINE;
		case ELEMENT_NODE:
		case DOCUMENT_FRAGMENT_NODE:
			if ( nodeCategoryCache.has( node ) ) {
				return nodeCategoryCache.get( node );
			}
			break;
		default:
			return UNKNOWN;
		}

		let nodeCategory;
		if ( !Array.prototype.every.call( node.childNodes, isInline ) ) {
			// Malformed HTML can have block tags inside inline tags. Need to treat
			// these as containers rather than inline. See #239.
			nodeCategory = CONTAINER;
		} else if ( inlineNodeNames.test( node.nodeName ) ) {
			nodeCategory = INLINE;
		} else /*if ( blockElementNames.test( node.nodeName ) )*/ {
			nodeCategory = BLOCK;
		}
		nodeCategoryCache.set( node, nodeCategory );
		return nodeCategory;
	},
	isInline = node => getNodeCategory( node ) === INLINE,
	isBlock = node => getNodeCategory( node ) === BLOCK,
	isContainer = node => getNodeCategory( node ) === CONTAINER,
	createTreeWalker = (root, whatToShow, filter) => doc.createTreeWalker( root, whatToShow, filter ? {
			acceptNode: node => filter(node) ? filterAccept : NodeFilter.FILTER_SKIP
		} : null
	),
	getBlockWalker = ( node, root ) => {
		let walker = createTreeWalker( root, SHOW_ELEMENT, isBlock );
		walker.currentNode = node;
		return walker;
	},
	getPreviousBlock = ( node, root ) => {
//		node = getClosest( node, root, blockElementNames );
		node = getBlockWalker( node, root ).previousNode();
		return node !== root ? node : null;
	},
	getNextBlock = ( node, root ) => {
//		node = getClosest( node, root, blockElementNames );
		node = getBlockWalker( node, root ).nextNode();
		return node !== root ? node : null;
	},

	isEmptyBlock = block => !block.textContent && !block.querySelector( 'IMG' ),

	areAlike = ( node, node2 ) => {
		return !isLeaf( node ) && (
			node.nodeType === node2.nodeType &&
			node.nodeName === node2.nodeName &&
			node.nodeName !== 'A' &&
			node.className === node2.className &&
			( ( !node.style && !node2.style ) ||
			  node.style.cssText === node2.style.cssText )
		);
	},
	hasTagAttributes = ( node, tag, attributes ) => {
		return node.nodeName === tag && Object.entries(attributes).every(([k,v]) => node.getAttribute(k) === v);
	},
	getClosest = ( node, root, selector ) => {
		node = (!node || node.closest ? node : node.parentElement);
		node = node && node.closest(selector);
		return (node && root.contains(node)) ? node : null;
	},
	getNearest = ( node, root, tag, attributes ) => {
		while ( node && node !== root ) {
			if ( hasTagAttributes( node, tag, attributes ) ) {
				return node;
			}
			node = node.parentNode;
		}
		return null;
	},

	getPath = ( node, root, config ) => {
		let path = '', style;
		if ( node && node !== root ) {
			path = getPath( node.parentNode, root, config );
			if ( node.nodeType === ELEMENT_NODE ) {
				path += ( path ? '>' : '' ) + node.nodeName;
				if ( node.id ) {
					path += '#' + node.id;
				}
				if ( node.classList.length ) {
					path += '.' + [...node.classList].sort().join( '.' );
				}
				if ( node.dir ) {
					path += '[dir=' + node.dir + ']';
				}
				if ( style = node.style.cssText ) {
					path += '[style=' + style + ']';
				}
			}
		}
		return path;
	},

	getLength = node => {
		let nodeType = node.nodeType;
		return nodeType === ELEMENT_NODE || nodeType === DOCUMENT_FRAGMENT_NODE ?
			node.childNodes.length : node.length || 0;
	},

	detach = node => {
//		node.remove();
		let parent = node.parentNode;
		parent && parent.removeChild( node );
		return node;
	},

	empty = node => {
		let frag = doc.createDocumentFragment(),
			childNodes = node.childNodes;
		childNodes && frag.append( ...childNodes );
		return frag;
	},

	setStyle = ( node, style ) => {
		if (typeof style === 'object') {
			Object.entries(style).forEach(([k,v]) => node.style[k] = v);
		} else if ( style !== undefined ) {
			node.setAttribute( 'style', style );
		}
	},

	createElement = ( tag, props, children ) => {
		let el = doc.createElement( tag );
		if ( props instanceof Array ) {
			children = props;
			props = null;
		}
		props && Object.entries(props).forEach(([k,v]) => {
			if ('style' === k) {
				setStyle( el, v );
			} else if ( v !== undefined ) {
				el.setAttribute( k, v );
			}
		});
		children && el.append( ...children );
		return el;
	},

	fixCursor = ( node, root ) => {
		// In Webkit and Gecko, block level elements are collapsed and
		// unfocusable if they have no content (:empty). To remedy this, a <BR> must be
		// inserted. In Opera and IE, we just need a textnode in order for the
		// cursor to appear.
		let self = root.__squire__;
		let originalNode = node;
		let fixer, child;

		if ( node === root ) {
			if ( !( child = node.firstChild ) || child.nodeName === 'BR' ) {
				fixer = self.createDefaultBlock();
				if ( child ) {
					child.replaceWith( fixer );
				}
				else {
					node.append( fixer );
				}
				node = fixer;
				fixer = null;
			}
		}

		if ( node.nodeType === TEXT_NODE ) {
			return originalNode;
		}

		if ( isInline( node ) ) {
			child = node.firstChild;
			while ( isWebKit && child &&
					child.nodeType === TEXT_NODE && !child.data ) {
				child.remove(  );
				child = node.firstChild;
			}
			if ( !child ) {
				fixer = self._addZWS();
			}
//		} else if ( !node.querySelector( 'BR' ) ) {
//		} else if ( !node.innerText.trim().length ) {
		} else if ( node.matches( ':empty' ) ) {
			fixer = createElement( 'BR' );
			while ( ( child = node.lastElementChild ) && !isInline( child ) ) {
				node = child;
			}
		}
		if ( fixer ) {
			try {
				node.append( fixer );
			} catch ( error ) {
				self.didError({
					name: 'Squire: fixCursor – ' + error,
					message: 'Parent: ' + node.nodeName + '/' + node.innerHTML +
						' appendChild: ' + fixer.nodeName
				});
			}
		}

		return originalNode;
	},

	// Recursively examine container nodes and wrap any inline children.
	fixContainer = ( container, root ) => {
		let children = container.childNodes;
		let wrapper = null;
		let i = 0, l = children.length, child, isBR;

		for ( ; i < l; ++i ) {
			child = children[i];
			isBR = child.nodeName === 'BR';
			if ( !isBR && isInline( child )
//			 && (root.__squire__._config.blockTag !== 'DIV' || (child.matches && !child.matches(phrasingElements)))
			) {
				if ( !wrapper ) {
					 wrapper = createElement( 'div' );
				}
				wrapper.append( child );
				--i;
				--l;
			} else if ( isBR || wrapper ) {
				if ( !wrapper ) {
					wrapper = createElement( 'div' );
				}
				fixCursor( wrapper, root );
				if ( isBR ) {
					child.replaceWith( wrapper );
				} else {
					child.before( wrapper  );
					++i;
					++l;
				}
				wrapper = null;
			}
			if ( isContainer( child ) ) {
				fixContainer( child, root );
			}
		}
/*
		// Not live
		[...container.children].forEach(child => {
			isBR = child.nodeName === 'BR';
			if ( !isBR && isInline( child )
//			 && (root.__squire__._config.blockTag !== 'DIV' || (child.matches && !child.matches(phrasingElements)))
			) {
				if ( !wrapper ) {
					 wrapper = createElement( 'div' );
				}
				wrapper.append( child );
			} else if ( isBR || wrapper ) {
				if ( !wrapper ) {
					wrapper = createElement( 'div' );
				}
				fixCursor( wrapper, root );
				if ( isBR ) {
					child.replaceWith( wrapper );
				} else {
					child.before( wrapper  );
				}
				wrapper = null;
			}
			if ( isContainer( child ) ) {
				fixContainer( child, root );
			}
		});
*/
		if ( wrapper ) {
			container.append( fixCursor( wrapper, root ) );
		}
		return container;
	},

	split = ( node, offset, stopNode, root ) => {
		let nodeType = node.nodeType,
			parent, clone, next;
		if ( nodeType === TEXT_NODE && node !== stopNode ) {
			return split(
				node.parentNode, node.splitText( offset ), stopNode, root );
		}
		if ( nodeType === ELEMENT_NODE ) {
			if ( typeof( offset ) === 'number' ) {
				offset = offset < node.childNodes.length ?
					node.childNodes[ offset ] : null;
			}
			if ( node === stopNode ) {
				return offset;
			}

			// Clone node without children
			parent = node.parentNode;
			clone = node.cloneNode( false );

			// Add right-hand siblings to the clone
			while ( offset ) {
				next = offset.nextSibling;
				clone.append( offset );
				offset = next;
			}

			// Maintain li numbering if inside a quote.
			if ( node.nodeName === 'OL' &&
					getClosest( node, root, 'BLOCKQUOTE' ) ) {
				clone.start = ( +node.start || 1 ) + node.childNodes.length - 1;
			}

			// DO NOT NORMALISE. This may undo the fixCursor() call
			// of a node lower down the tree!

			// We need something in the element in order for the cursor to appear.
			fixCursor( node, root );
			fixCursor( clone, root );

			// Inject clone after original node
			node.after( clone );

			// Keep on splitting up the tree
			return split( parent, clone, stopNode, root );
		}
		return offset;
	},

	_mergeInlines = ( node, fakeRange ) => {
		let children = node.childNodes,
			l = children.length,
			frags = [],
			child, prev;
		while ( l-- ) {
			child = children[l];
			prev = l && children[ l - 1 ];
			if ( l && isInline( child ) && areAlike( child, prev ) &&
					!leafNodeNames[ child.nodeName ] ) {
				if ( fakeRange.startContainer === child ) {
					fakeRange.startContainer = prev;
					fakeRange.startOffset += getLength( prev );
				}
				if ( fakeRange.endContainer === child ) {
					fakeRange.endContainer = prev;
					fakeRange.endOffset += getLength( prev );
				}
				if ( fakeRange.startContainer === node ) {
					if ( fakeRange.startOffset > l ) {
						--fakeRange.startOffset;
					}
					else if ( fakeRange.startOffset === l ) {
						fakeRange.startContainer = prev;
						fakeRange.startOffset = getLength( prev );
					}
				}
				if ( fakeRange.endContainer === node ) {
					if ( fakeRange.endOffset > l ) {
						--fakeRange.endOffset;
					}
					else if ( fakeRange.endOffset === l ) {
						fakeRange.endContainer = prev;
						fakeRange.endOffset = getLength( prev );
					}
				}
				detach( child );
				if ( child.nodeType === TEXT_NODE ) {
					prev.appendData( child.data );
				}
				else {
					frags.push( empty( child ) );
				}
			}
			else if ( child.nodeType === ELEMENT_NODE ) {
				child.append(...frags.reverse());
				frags = [];
				_mergeInlines( child, fakeRange );
			}
		}
	},

	mergeInlines = ( node, range ) => {
		if ( node.nodeType === TEXT_NODE ) {
			node = node.parentNode;
		}
		if ( node.nodeType === ELEMENT_NODE ) {
			let fakeRange = {
				startContainer: range.startContainer,
				startOffset: range.startOffset,
				endContainer: range.endContainer,
				endOffset: range.endOffset
			};
			_mergeInlines( node, fakeRange );
			range.setStart( fakeRange.startContainer, fakeRange.startOffset );
			range.setEnd( fakeRange.endContainer, fakeRange.endOffset );
		}
	},

	mergeWithBlock = ( block, next, range, root ) => {
		let container = next;
		let parent, last, offset;
		while ( ( parent = container.parentNode ) &&
				parent !== root &&
				parent.nodeType === ELEMENT_NODE &&
				parent.childNodes.length === 1 ) {
			container = parent;
		}
		detach( container );

		offset = block.childNodes.length;

		// Remove extra <BR> fixer if present.
		last = block.lastChild;
		if ( last && last.nodeName === 'BR' ) {
			last.remove(  );
			--offset;
		}

		block.append( empty( next ) );

		range.setStart( block, offset );
		range.collapse( true );
		mergeInlines( block, range );
	},

	mergeContainers = ( node, root ) => {
		let prev = node.previousSibling,
			first = node.firstChild,
			isListItem = ( node.nodeName === 'LI' ),
			needsFix, block;

		// Do not merge LIs, unless it only contains a UL
		if ( isListItem && ( !first || !/^[OU]L$/.test( first.nodeName ) ) ) {
			return;
		}

		if ( prev && areAlike( prev, node ) ) {
			if ( !isContainer( prev ) ) {
				if ( !isListItem ) {
					return;
				}
				block = createElement( 'DIV' );
				block.append( empty( prev ) );
				prev.append( block );
			}
			detach( node );
			needsFix = !isContainer( node );
			prev.append( empty( node ) );
			if ( needsFix ) {
				fixContainer( prev, root );
			}
			if ( first ) {
				mergeContainers( first, root );
			}
		} else if ( isListItem ) {
			prev = createElement( 'DIV' );
			node.insertBefore( prev, first );
			fixCursor( prev, root );
		}
	},

	getNodeBefore = ( node, offset ) => {
		let children = node.childNodes;
		while ( offset && node.nodeType === ELEMENT_NODE ) {
			node = children[ offset - 1 ];
			children = node.childNodes;
			offset = children.length;
		}
		return node;
	},

	getNodeAfter = ( node, offset ) => {
		if ( node.nodeType === ELEMENT_NODE ) {
			let children = node.childNodes;
			if ( offset < children.length ) {
				node = children[ offset ];
			} else {
				while ( node && !node.nextSibling ) {
					node = node.parentNode;
				}
				if ( node ) { node = node.nextSibling; }
			}
		}
		return node;
	},

	insertNodeInRange = ( range, node ) => {
		// Insert at start.
		let startContainer = range.startContainer,
			startOffset = range.startOffset,
			endContainer = range.endContainer,
			endOffset = range.endOffset,
			parent, children, childCount, afterSplit;

		// If part way through a text node, split it.
		if ( startContainer.nodeType === TEXT_NODE ) {
			parent = startContainer.parentNode;
			children = parent.childNodes;
			if ( startOffset === startContainer.length ) {
				startOffset = indexOf( children, startContainer ) + 1;
				if ( range.collapsed ) {
					endContainer = parent;
					endOffset = startOffset;
				}
			} else {
				if ( startOffset ) {
					afterSplit = startContainer.splitText( startOffset );
					if ( endContainer === startContainer ) {
						endOffset -= startOffset;
						endContainer = afterSplit;
					}
					else if ( endContainer === parent ) {
						++endOffset;
					}
					startContainer = afterSplit;
				}
				startOffset = indexOf( children, startContainer );
			}
			startContainer = parent;
		} else {
			children = startContainer.childNodes;
		}

		childCount = children.length;

		if ( startOffset === childCount ) {
			startContainer.append( node );
		} else {
			startContainer.insertBefore( node, children[ startOffset ] );
		}

		if ( startContainer === endContainer ) {
			endOffset += children.length - childCount;
		}

		range.setStart( startContainer, startOffset );
		range.setEnd( endContainer, endOffset );
	},

	extractContentsOfRange = ( range, common, root ) => {
		let startContainer = range.startContainer,
			startOffset = range.startOffset,
			endContainer = range.endContainer,
			endOffset = range.endOffset;

		if ( !common ) {
			common = range.commonAncestorContainer;
		}

		if ( common.nodeType === TEXT_NODE ) {
			common = common.parentNode;
		}

		let endNode = split( endContainer, endOffset, common, root ),
			startNode = split( startContainer, startOffset, common, root ),
			frag = doc.createDocumentFragment(),
			next, before, after, beforeText, afterText;

		// End node will be null if at end of child nodes list.
		while ( startNode !== endNode ) {
			next = startNode.nextSibling;
			frag.append( startNode );
			startNode = next;
		}

		startContainer = common;
		startOffset = endNode ?
			indexOf( common.childNodes, endNode ) :
			common.childNodes.length;

		// Merge text nodes if adjacent. IE10 in particular will not focus
		// between two text nodes
		after = common.childNodes[ startOffset ];
		before = after && after.previousSibling;
		if ( before &&
				before.nodeType === TEXT_NODE &&
				after.nodeType === TEXT_NODE ) {
			startContainer = before;
			startOffset = before.length;
			beforeText = before.data;
			afterText = after.data;

			// If we now have two adjacent spaces, the second one needs to become
			// a nbsp, otherwise the browser will swallow it due to HTML whitespace
			// collapsing.
			if ( beforeText.charAt( beforeText.length - 1 ) === ' ' &&
					afterText.charAt( 0 ) === ' ' ) {
				afterText = NBSP + afterText.slice( 1 ); // nbsp
			}
			before.appendData( afterText );
			detach( after );
		}

		range.setStart( startContainer, startOffset );
		range.collapse( true );

		fixCursor( common, root );

		return frag;
	},

	deleteContentsOfRange = ( range, root ) => {
		let startBlock = getStartBlockOfRange( range, root );
		let endBlock = getEndBlockOfRange( range, root );
		let needsMerge = ( startBlock !== endBlock );
		let frag, child;

		// Move boundaries up as much as possible without exiting block,
		// to reduce need to split.
		moveRangeBoundariesDownTree( range );
		moveRangeBoundariesUpTree( range, startBlock, endBlock, root );

		// Remove selected range
		frag = extractContentsOfRange( range, null, root );

		// Move boundaries back down tree as far as possible.
		moveRangeBoundariesDownTree( range );

		// If we split into two different blocks, merge the blocks.
		if ( needsMerge ) {
			// endBlock will have been split, so need to refetch
			endBlock = getEndBlockOfRange( range, root );
			if ( startBlock && endBlock && startBlock !== endBlock ) {
				mergeWithBlock( startBlock, endBlock, range, root );
			}
		}

		// Ensure block has necessary children
		if ( startBlock ) {
			fixCursor( startBlock, root );
		}

		// Ensure root has a block-level element in it.
		child = root.firstChild;
		if ( child && child.nodeName !== 'BR' ) {
			range.collapse( true );
		} else {
			fixCursor( root, root );
			range.selectNodeContents( root.firstChild );
		}
		return frag;
	},

	// Contents of range will be deleted.
	// After method, range will be around inserted content
	insertTreeFragmentIntoRange = ( range, frag, root ) => {
		let firstInFragIsInline = frag.firstChild && isInline( frag.firstChild );
		let node, block, blockContentsAfterSplit, stopPoint, container, offset;
		let replaceBlock, firstBlockInFrag, nodeAfterSplit, nodeBeforeSplit;
		let tempRange;

		// Fixup content: ensure no top-level inline, and add cursor fix elements.
		fixContainer( frag, root );
		node = frag;
		while ( ( node = getNextBlock( node, root ) ) ) {
			fixCursor( node, root );
		}

		// Delete any selected content.
		if ( !range.collapsed ) {
			deleteContentsOfRange( range, root );
		}

		// Move range down into text nodes.
		moveRangeBoundariesDownTree( range );
		range.collapse( false ); // collapse to end

		// Where will we split up to? First blockquote parent, otherwise root.
		stopPoint = getClosest( range.endContainer, root, 'BLOCKQUOTE' ) || root;

		// Merge the contents of the first block in the frag with the focused block.
		// If there are contents in the block after the focus point, collect this
		// up to insert in the last block later. This preserves the style that was
		// present in this bit of the page.
		//
		// If the block being inserted into is empty though, replace it instead of
		// merging if the fragment had block contents.
		// e.g. <blockquote><p>Foo</p></blockquote>
		// This seems a reasonable approximation of user intent.

		block = getStartBlockOfRange( range, root );
		firstBlockInFrag = getNextBlock( frag, frag );
		replaceBlock = !firstInFragIsInline && !!block && isEmptyBlock( block );
		if ( block && firstBlockInFrag && !replaceBlock &&
				// Don't merge table cells or PRE elements into block
				!getClosest( firstBlockInFrag, frag, 'PRE,TABLE' ) ) {
			moveRangeBoundariesUpTree( range, block, block, root );
			range.collapse( true ); // collapse to start
			container = range.endContainer;
			offset = range.endOffset;
			// Remove trailing <br> – we don't want this considered content to be
			// inserted again later
			cleanupBRs( block, root, false );
			if ( isInline( container ) ) {
				// Split up to block parent.
				nodeAfterSplit = split(
					container, offset, getPreviousBlock( container, root ), root );
				container = nodeAfterSplit.parentNode;
				offset = indexOf( container.childNodes, nodeAfterSplit );
			}
			if ( /*isBlock( container ) && */offset !== getLength( container ) ) {
				// Collect any inline contents of the block after the range point
				blockContentsAfterSplit = doc.createDocumentFragment();
				while ( ( node = container.childNodes[ offset ] ) ) {
					blockContentsAfterSplit.append( node );
				}
			}
			// And merge the first block in.
			mergeWithBlock( container, firstBlockInFrag, range, root );

			// And where we will insert
			offset = indexOf( container.parentNode.childNodes, container ) + 1;
			container = container.parentNode;
			range.setEnd( container, offset );
		}

		// Is there still any content in the fragment?
		if ( getLength( frag ) ) {
			if ( replaceBlock ) {
				range.setEndBefore( block );
				range.collapse( false );
				detach( block );
			}
			moveRangeBoundariesUpTree( range, stopPoint, stopPoint, root );
			// Now split after block up to blockquote (if a parent) or root
			nodeAfterSplit = split(
				range.endContainer, range.endOffset, stopPoint, root );
			nodeBeforeSplit = nodeAfterSplit ?
				nodeAfterSplit.previousSibling :
				stopPoint.lastChild;
			stopPoint.insertBefore( frag, nodeAfterSplit );
			if ( nodeAfterSplit ) {
				range.setEndBefore( nodeAfterSplit );
			} else {
				range.setEnd( stopPoint, getLength( stopPoint ) );
			}
			block = getEndBlockOfRange( range, root );

			// Get a reference that won't be invalidated if we merge containers.
			moveRangeBoundariesDownTree( range );
			container = range.endContainer;
			offset = range.endOffset;

			// Merge inserted containers with edges of split
			if ( nodeAfterSplit && isContainer( nodeAfterSplit ) ) {
				mergeContainers( nodeAfterSplit, root );
			}
			nodeAfterSplit = nodeBeforeSplit && nodeBeforeSplit.nextSibling;
			if ( nodeAfterSplit && isContainer( nodeAfterSplit ) ) {
				mergeContainers( nodeAfterSplit, root );
			}
			range.setEnd( container, offset );
		}

		// Insert inline content saved from before.
		if ( blockContentsAfterSplit ) {
			tempRange = range.cloneRange();
			mergeWithBlock( block, blockContentsAfterSplit, tempRange, root );
			range.setEnd( tempRange.endContainer, tempRange.endOffset );
		}
		moveRangeBoundariesDownTree( range );
	},

	isNodeContainedInRange = ( range, node, partial = true ) => {
		let nodeRange = doc.createRange();

		nodeRange.selectNode( node );

		return partial
			// Node must not finish before range starts or start after range finishes.
			? range.compareBoundaryPoints( END_TO_START, nodeRange ) < 0
				&& range.compareBoundaryPoints( START_TO_END, nodeRange ) > 0
			// Node must start after range starts and finish before range finishes
			: range.compareBoundaryPoints( START_TO_START, nodeRange ) < 1
				&& range.compareBoundaryPoints( END_TO_END, nodeRange ) > -1;
	},

	moveRangeBoundariesDownTree = range => {
		let startContainer = range.startContainer,
			startOffset = range.startOffset,
			endContainer = range.endContainer,
			endOffset = range.endOffset,
			maySkipBR = true,
			child;

		while ( startContainer.nodeType !== TEXT_NODE ) {
			child = startContainer.childNodes[ startOffset ];
			if ( !child || isLeaf( child ) ) {
				break;
			}
			startContainer = child;
			startOffset = 0;
		}
		if ( endOffset ) {
			while ( endContainer.nodeType !== TEXT_NODE ) {
				child = endContainer.childNodes[ endOffset - 1 ];
				if ( !child || isLeaf( child ) ) {
					if ( maySkipBR && child && child.nodeName === 'BR' ) {
						--endOffset;
						maySkipBR = false;
						continue;
					}
					break;
				}
				endContainer = child;
				endOffset = getLength( endContainer );
			}
		} else {
			while ( endContainer.nodeType !== TEXT_NODE ) {
				child = endContainer.firstChild;
				if ( !child || isLeaf( child ) ) {
					break;
				}
				endContainer = child;
			}
		}

		// If collapsed, this algorithm finds the nearest text node positions
		// *outside* the range rather than inside, but also it flips which is
		// assigned to which.
		if ( range.collapsed ) {
			range.setStart( endContainer, endOffset );
			range.setEnd( startContainer, startOffset );
		} else {
			range.setStart( startContainer, startOffset );
			range.setEnd( endContainer, endOffset );
		}
	},

	moveRangeBoundariesUpTree = ( range, startMax, endMax, root ) => {
		let startContainer = range.startContainer;
		let startOffset = range.startOffset;
		let endContainer = range.endContainer;
		let endOffset = range.endOffset;
		let maySkipBR = true;
		let parent;

		if ( !startMax ) {
			startMax = range.commonAncestorContainer;
		}
		if ( !endMax ) {
			endMax = startMax;
		}

		while ( !startOffset &&
				startContainer !== startMax &&
				startContainer !== root ) {
			parent = startContainer.parentNode;
			startOffset = indexOf( parent.childNodes, startContainer );
			startContainer = parent;
		}

		while ( endContainer !== endMax && endContainer !== root ) {
			if ( maySkipBR &&
					endContainer.nodeType !== TEXT_NODE &&
					endContainer.childNodes[ endOffset ] &&
					endContainer.childNodes[ endOffset ].nodeName === 'BR' ) {
				++endOffset;
				maySkipBR = false;
			}
			if ( endOffset !== getLength( endContainer ) ) {
				break;
			}
			parent = endContainer.parentNode;
			endOffset = indexOf( parent.childNodes, endContainer ) + 1;
			endContainer = parent;
		}

		range.setStart( startContainer, startOffset );
		range.setEnd( endContainer, endOffset );
	},

	moveRangeBoundaryOutOf = ( range, nodeName, root ) => {
		let parent = getClosest( range.endContainer, root, 'A' );
		if ( parent ) {
			let clone = range.cloneRange();
			parent = parent.parentNode;
			moveRangeBoundariesUpTree( clone, parent, parent, root );
			if ( clone.endContainer === parent ) {
				range.setStart( clone.endContainer, clone.endOffset );
				range.setEnd( clone.endContainer, clone.endOffset );
			}
		}
		return range;
	},

	// Returns the first block at least partially contained by the range,
	// or null if no block is contained by the range.
	getStartBlockOfRange = ( range, root ) => {
		let container = range.startContainer,
			block;

		// If inline, get the containing block.
		if ( isInline( container ) ) {
			block = getPreviousBlock( container, root );
		} else if ( container !== root && isBlock( container ) ) {
			block = container;
		} else {
			block = getNextBlock( getNodeBefore( container, range.startOffset ), root );
		}
		// Check the block actually intersects the range
		return block && isNodeContainedInRange( range, block ) ? block : null;
	},

	// Returns the last block at least partially contained by the range,
	// or null if no block is contained by the range.
	getEndBlockOfRange = ( range, root ) => {
		let container = range.endContainer,
			block, child;

		// If inline, get the containing block.
		if ( isInline( container ) ) {
			block = getPreviousBlock( container, root );
		} else if ( container !== root && isBlock( container ) ) {
			block = container;
		} else {
			block = getNodeAfter( container, range.endOffset );
			if ( !block || !root.contains( block ) ) {
				block = root;
				while ( child = block.lastChild ) {
					block = child;
				}
			}
			block = getPreviousBlock( block, root );
		}
		// Check the block actually intersects the range
		return block && isNodeContainedInRange( range, block ) ? block : null;
	},

	newContentWalker = root => createTreeWalker( root,
		SHOW_ELEMENT_OR_TEXT,
		node => node.nodeType === TEXT_NODE ? notWS.test( node.data ) : node.nodeName === 'IMG'
	),

	rangeDoesStartAtBlockBoundary = ( range, root ) => {
		let startContainer = range.startContainer;
		let startOffset = range.startOffset;
		let nodeAfterCursor;

		// If in the middle or end of a text node, we're not at the boundary.
		if ( startContainer.nodeType === TEXT_NODE ) {
			if ( startOffset ) {
				return false;
			}
			nodeAfterCursor = startContainer;
		} else {
			nodeAfterCursor = getNodeAfter( startContainer, startOffset );
			if ( nodeAfterCursor && !root.contains( nodeAfterCursor ) ) {
				nodeAfterCursor = null;
			}
			// The cursor was right at the end of the document
			if ( !nodeAfterCursor ) {
				nodeAfterCursor = getNodeBefore( startContainer, startOffset );
				if ( nodeAfterCursor.nodeType === TEXT_NODE &&
						nodeAfterCursor.length ) {
					return false;
				}
			}
		}

		// Otherwise, look for any previous content in the same block.
		contentWalker = newContentWalker(getStartBlockOfRange( range, root ));
		contentWalker.currentNode = nodeAfterCursor;

		return !contentWalker.previousNode();
	},

	rangeDoesEndAtBlockBoundary = ( range, root ) => {
		let endContainer = range.endContainer,
			endOffset = range.endOffset,
			length;

		// Otherwise, look for any further content in the same block.
		contentWalker = newContentWalker(getStartBlockOfRange( range, root ));

		// If in a text node with content, and not at the end, we're not
		// at the boundary
		if ( endContainer.nodeType === TEXT_NODE ) {
			length = endContainer.data.length;
			if ( length && endOffset < length ) {
				return false;
			}
			contentWalker.currentNode = endContainer;
		} else {
			contentWalker.currentNode = getNodeBefore( endContainer, endOffset );
		}

		return !contentWalker.nextNode();
	},

	expandRangeToBlockBoundaries = ( range, root ) => {
		let start = getStartBlockOfRange( range, root ),
			end = getEndBlockOfRange( range, root );

		if ( start && end ) {
			range.setStart( start, 0 );
			range.setEnd( end, end.childNodes.length );
//			parent = start.parentNode;
//			range.setStart( parent, indexOf( parent.childNodes, start ) );
//			parent = end.parentNode;
//			range.setEnd( parent, indexOf( parent.childNodes, end ) + 1 );
		}
	},

	didError = error => console.error( error ),

	createRange = ( range, startOffset, endContainer, endOffset ) => {
		if ( range instanceof Range ) {
			return range.cloneRange();
		}
		let domRange = doc.createRange();
		domRange.setStart( range, startOffset );
		if ( endContainer ) {
			domRange.setEnd( endContainer, endOffset );
		} else {
			domRange.setEnd( range, startOffset );
		}
		return domRange;
	},

	mapKeyTo = method => ( self, event ) => {
		event.preventDefault();
		self[ method ]();
	},

	mapKeyToFormat = ( tag, remove ) => {
		return ( self, event ) => {
			event.preventDefault();
			self.toggleTag(tag, remove);
		};
	},

	// If you delete the content inside a span with a font styling, Webkit will
	// replace it with a <font> tag (!). If you delete all the text inside a
	// link in Opera, it won't delete the link. Let's make things consistent. If
	// you delete all text inside an inline tag, remove the inline tag.
	afterDelete = ( self, range ) => {
		try {
			if ( !range ) { range = self.getSelection(); }
			let node = range.startContainer,
				parent;
			// Climb the tree from the focus point while we are inside an empty
			// inline element
			if ( node.nodeType === TEXT_NODE ) {
				node = node.parentNode;
			}
			parent = node;
			while ( isInline( parent ) &&
					( !parent.textContent || parent.textContent === ZWS ) ) {
				node = parent;
				parent = node.parentNode;
			}
			// If focused in empty inline element
			if ( node !== parent ) {
				// Move focus to just before empty inline(s)
				range.setStart( parent,
					indexOf( parent.childNodes, node ) );
				range.collapse( true );
				// Remove empty inline(s)
				node.remove(  );
				// Fix cursor in block
				if ( !isBlock( parent ) ) {
					parent = getPreviousBlock( parent, self._root );
				}
				fixCursor( parent, self._root );
				// Move cursor into text node
				moveRangeBoundariesDownTree( range );
			}
			// If you delete the last character in the sole <div> in Chrome,
			// it removes the div and replaces it with just a <br> inside the
			// root. Detach the <br>; the _ensureBottomLine call will insert a new
			// block.
			if ( node === self._root &&
					( node = node.firstChild ) && node.nodeName === 'BR' ) {
				detach( node );
			}
			self._ensureBottomLine();
			self.setSelection( range );
			self._updatePath( range, true );
		} catch ( error ) {
			didError( error );
		}
	},

	detachUneditableNode = ( node, root ) => {
		let parent;
		while (( parent = node.parentNode )) {
			if ( parent === root || parent.isContentEditable ) {
				break;
			}
			node = parent;
		}
		detach( node );
	},

	handleEnter = ( self, shiftKey, range ) => {
		let root = self._root;
		let block, parent, node, offset, nodeAfterSplit;

		// Save undo checkpoint and add any links in the preceding section.
		// Remove any zws so we don't think there's content in an empty
		// block.
		self._recordUndoState( range, false );
		if ( self._config.addLinks ) {
			addLinks( range.startContainer, root, self );
		}
		self._removeZWS();
		self._getRangeAndRemoveBookmark( range );

		// Selected text is overwritten, therefore delete the contents
		// to collapse selection.
		if ( !range.collapsed ) {
			deleteContentsOfRange( range, root );
		}

		block = getStartBlockOfRange( range, root );

		// Inside a PRE, insert literal newline, unless on blank line.
		if ( block && ( parent = getClosest( block, root, 'PRE' ) ) ) {
			moveRangeBoundariesDownTree( range );
			node = range.startContainer;
			offset = range.startOffset;
			if ( node.nodeType !== TEXT_NODE ) {
				node = doc.createTextNode( '' );
				parent.insertBefore( node, parent.firstChild );
			}
			// If blank line: split and insert default block
			if ( !shiftKey &&
					( node.data.charAt( offset - 1 ) === '\n' ||
						rangeDoesStartAtBlockBoundary( range, root ) ) &&
					( node.data.charAt( offset ) === '\n' ||
						rangeDoesEndAtBlockBoundary( range, root ) ) ) {
				node.deleteData( offset && offset - 1, offset ? 2 : 1 );
				nodeAfterSplit =
					split( node, offset && offset - 1, root, root );
				node = nodeAfterSplit.previousSibling;
				if ( !node.textContent ) {
					detach( node );
				}
				node = self.createDefaultBlock();
				nodeAfterSplit.before( node );
				if ( !nodeAfterSplit.textContent ) {
					detach( nodeAfterSplit );
				}
				range.setStart( node, 0 );
			} else {
				node.insertData( offset, '\n' );
				fixCursor( parent, root );
				// Firefox bug: if you set the selection in the text node after
				// the new line, it draws the cursor before the line break still
				// but if you set the selection to the equivalent position
				// in the parent, it works.
				if ( node.length === offset + 1 ) {
					range.setStartAfter( node );
				} else {
					range.setStart( node, offset + 1 );
				}
			}
			range.collapse( true );
			self.setSelection( range );
			self._updatePath( range, true );
			self._docWasChanged();
			return;
		}

		// If this is a malformed bit of document or in a table;
		// just play it safe and insert a <br>.
		if ( !block || shiftKey || /^T[HD]$/.test( block.nodeName ) ) {
			// If inside an <a>, move focus out
			moveRangeBoundaryOutOf( range, 'A', root );
			insertNodeInRange( range, createElement( 'BR' ) );
			range.collapse( false );
			self.setSelection( range );
			self._updatePath( range, true );
			return;
		}

		// If in a list, we'll split the LI instead.
		block = getClosest( block, root, 'LI' ) || block;

		if ( isEmptyBlock( block ) && ( parent = getClosest( block, root, 'UL,OL,BLOCKQUOTE' ) ) ) {
			return 'BLOCKQUOTE' === parent.nodeName
				// Break blockquote
				? self.modifyBlocks( (/* frag */) => self.createDefaultBlock( createBookmarkNodes( self ) ), range )
				// Break list
				: self.decreaseListLevel( range );
		}

		// Otherwise, split at cursor point.
		nodeAfterSplit = splitBlock( self, block,
			range.startContainer, range.startOffset );

		// Clean up any empty inlines if we hit enter at the beginning of the block
		removeZWS( block );
		removeEmptyInlines( block );
		fixCursor( block, root );

		// Focus cursor
		// If there's a <b>/<i> etc. at the beginning of the split
		// make sure we focus inside it.
		while ( nodeAfterSplit.nodeType === ELEMENT_NODE ) {
			let child = nodeAfterSplit.firstChild,
				next;

			// Don't continue links over a block break; unlikely to be the
			// desired outcome.
			if ( nodeAfterSplit.nodeName === 'A' &&
					( !nodeAfterSplit.textContent ||
						nodeAfterSplit.textContent === ZWS ) ) {
				child = doc.createTextNode( '' );
				nodeAfterSplit.replaceWith( child );
				nodeAfterSplit = child;
				break;
			}

			while ( child && child.nodeType === TEXT_NODE && !child.data ) {
				next = child.nextSibling;
				if ( !next || next.nodeName === 'BR' ) {
					break;
				}
				detach( child );
				child = next;
			}

			// 'BR's essentially don't count; they're a browser hack.
			// If you try to select the contents of a 'BR', FF will not let
			// you type anything!
			if ( !child || child.nodeName === 'BR' ||
					child.nodeType === TEXT_NODE ) {
				break;
			}
			nodeAfterSplit = child;
		}
		range = createRange( nodeAfterSplit, 0 );
		self.setSelection( range );
		self._updatePath( range, true );
	},


	changeIndentationLevel = direction => ( self, event ) => {
		event.preventDefault();
		self.changeIndentationLevel(direction);
	},

	toggleList = ( type, methodIfNotInList ) => ( self, event ) => {
		event.preventDefault();
		let parent = self.getSelectionClosest('UL,OL');
		if (parent && type == parent.nodeName) {
			self.removeList();
		} else {
			self[ methodIfNotInList ]();
		}
	},

	fontSizes = {
		1: 'x-small',
		2: 'small',
		3: 'medium',
		4: 'large',
		5: 'x-large',
		6: 'xx-large',
		7: 'xxx-large',
		'-1': 'smaller',
		'+1': 'larger'
	},

	styleToSemantic = {
		fontWeight: {
			regexp: /^bold|^700/i,
			replace: () => createElement( 'B' )
		},
		fontStyle: {
			regexp: /^italic/i,
			replace: () => createElement( 'I' )
		},
		fontFamily: {
			regexp: notWS,
			replace: ( doc, family ) => createElement( 'SPAN', {
				style: 'font-family:' + family
			})
		},
		fontSize: {
			regexp: notWS,
			replace: ( doc, size ) => createElement( 'SPAN', {
				style: 'font-size:' + size
			})
		},
		textDecoration: {
			regexp: /^underline/i,
			replace: () => createElement( 'U' )
		}
	/*
		textDecoration: {
			regexp: /^line-through/i,
			replace: doc => createElement( 'S' )
		}
	*/
	},

	replaceWithTag = tag => node => {
		let el = createElement( tag );
		Array.prototype.forEach.call( node.attributes, attr => el.setAttribute( attr.name, attr.value ) );
		node.replaceWith( el );
		el.append( empty( node ) );
		return el;
	},

	replaceStyles = node => {
		let style = node.style;
		let css, newTreeBottom, newTreeTop, el;

		Object.entries(styleToSemantic).forEach(([attr,converter])=>{
			css = style[ attr ];
			if ( css && converter.regexp.test( css ) ) {
				el = converter.replace( doc, css );
				if ( el.nodeName === node.nodeName &&
						el.className === node.className ) {
					return;
				}
				if ( !newTreeTop ) {
					newTreeTop = el;
				}
				if ( newTreeBottom ) {
					newTreeBottom.append( el );
				}
				newTreeBottom = el;
				node.style[ attr ] = '';
			}
		});

		if ( newTreeTop ) {
			newTreeBottom.append( empty( node ) );
			node.append( newTreeTop );
		}

		return newTreeBottom || node;
	},

	stylesRewriters = {
		SPAN: replaceStyles,
		STRONG: replaceWithTag( 'B' ),
		EM: replaceWithTag( 'I' ),
		INS: replaceWithTag( 'U' ),
		STRIKE: replaceWithTag( 'S' ),
		FONT: node => {
			let face = node.face,
				size = node.size,
				color = node.color,
				newTag = createElement( 'SPAN' ),
				css = newTag.style;
			if ( face ) {
				css.fontFamily = face;
			}
			if ( size ) {
				css.fontSize = fontSizes[ size ];
			}
			if ( color && /^#?([\dA-F]{3}){1,2}$/i.test( color ) ) {
				if ( color.charAt( 0 ) !== '#' ) {
					color = '#' + color;
				}
				css.color = color;
			}
			node.replaceWith( newTag );
			newTag.append( empty( node ) );
			return newTag;
		},
	//	KBD:
	//	VAR:
	//	CODE:
	//	SAMP:
		TT: node => {
			let el = createElement( 'SPAN', {
				style: 'font-family:menlo,consolas,"courier new",monospace'
			});
			node.replaceWith( el );
			el.append( empty( node ) );
			return el;
		}
	},

	allowedBlock = /^(?:A(?:DDRESS|RTICLE|SIDE|UDIO)|BLOCKQUOTE|CAPTION|D(?:[DLT]|IV)|F(?:IGURE|IGCAPTION|OOTER)|H[1-6]|HEADER|L(?:ABEL|EGEND|I)|O(?:L|UTPUT)|P(?:RE)?|SECTION|T(?:ABLE|BODY|D|FOOT|H|HEAD|R)|COL(?:GROUP)?|UL)$/,

	blacklist = /^(?:HEAD|META|STYLE)/,

	/*
		Two purposes:

		1. Remove nodes we don't want, such as weird <o:p> tags, comment nodes
		   and whitespace nodes.
		2. Convert inline tags into our preferred format.
	*/
	cleanTree = ( node, config, preserveWS ) => {
		let children = node.childNodes,
			nonInlineParent, i, l, child, nodeName, nodeType, childLength,
			startsWithWS, endsWithWS, data, sibling;

		nonInlineParent = node;
		while ( isInline( nonInlineParent ) ) {
			nonInlineParent = nonInlineParent.parentNode;
		}
		let walker = createTreeWalker( nonInlineParent, SHOW_ELEMENT_OR_TEXT );

		for ( i = 0, l = children.length; i < l; ++i ) {
			child = children[i];
			nodeName = child.nodeName;
			nodeType = child.nodeType;
			if ( nodeType === ELEMENT_NODE ) {
				childLength = child.childNodes.length;
				if ( stylesRewriters[ nodeName ] ) {
					child = stylesRewriters[ nodeName ]( child );
				} else if ( blacklist.test( nodeName ) ) {
					child.remove(  );
					--i;
					--l;
					continue;
				} else if ( !allowedBlock.test( nodeName ) && !isInline( child ) ) {
					--i;
					l += childLength - 1;
					child.replaceWith( empty( child ) );
					continue;
				}
				if ( childLength ) {
					cleanTree( child, config,
						preserveWS || ( nodeName === 'PRE' ) );
				}
			} else {
				if ( nodeType === TEXT_NODE ) {
					data = child.data;
					startsWithWS = !notWS.test( data.charAt( 0 ) );
					endsWithWS = !notWS.test( data.charAt( data.length - 1 ) );
					if ( preserveWS || ( !startsWithWS && !endsWithWS ) ) {
						continue;
					}
					// Iterate through the nodes; if we hit some other content
					// before the start of a new block we don't trim
					if ( startsWithWS ) {
						walker.currentNode = child;
						while ( sibling = walker.previousPONode() ) {
							nodeName = sibling.nodeName;
							if ( nodeName === 'IMG' ||
									( nodeName === '#text' &&
										notWS.test( sibling.data ) ) ) {
								break;
							}
							if ( !isInline( sibling ) ) {
								sibling = null;
								break;
							}
						}
						data = data.replace( /^[ \r\n]+/g, sibling ? ' ' : '' );
					}
					if ( endsWithWS ) {
						walker.currentNode = child;
						while ( sibling = walker.nextNode() ) {
							if ( nodeName === 'IMG' ||
									( nodeName === '#text' &&
										notWS.test( sibling.data ) ) ) {
								break;
							}
							if ( !isInline( sibling ) ) {
								sibling = null;
								break;
							}
						}
						data = data.replace( /[ \r\n]+$/g, sibling ? ' ' : '' );
					}
					if ( data ) {
						child.data = data;
						continue;
					}
				}
				child.remove(  );
				--i;
				--l;
			}
		}
		return node;
	},

	// ---

	removeEmptyInlines = node => {
		let children = node.childNodes,
			l = children.length,
			child;
		while ( l-- ) {
			child = children[l];
			if ( child.nodeType === ELEMENT_NODE && !isLeaf( child ) ) {
				removeEmptyInlines( child );
				if ( isInline( child ) && !child.firstChild ) {
					child.remove(  );
				}
			} else if ( child.nodeType === TEXT_NODE && !child.data ) {
				child.remove(  );
			}
		}
	},

	// ---

	notWSTextNode = node => node.nodeType === ELEMENT_NODE ? node.nodeName === 'BR' : notWS.test( node.data ),
	isLineBreak = ( br, isLBIfEmptyBlock ) => {
		let block = br.parentNode;
		let walker;
		while ( isInline( block ) ) {
			block = block.parentNode;
		}
		walker = createTreeWalker( block, SHOW_ELEMENT_OR_TEXT, notWSTextNode );
		walker.currentNode = br;
		return !!walker.nextNode() ||
			( isLBIfEmptyBlock && !walker.previousNode() );
	},

	// <br> elements are treated specially, and differently depending on the
	// browser, when in rich text editor mode. When adding HTML from external
	// sources, we must remove them, replacing the ones that actually affect
	// line breaks by wrapping the inline text in a <div>. Browsers that want <br>
	// elements at the end of each block will then have them added back in a later
	// fixCursor method call.
	cleanupBRs = ( node, root, keepForBlankLine ) => {
		let brs = node.querySelectorAll( 'BR' );
		let l = brs.length;
		let br, parent;
		while ( l-- ) {
			br = brs[l];
			// Cleanup may have removed it
			parent = br.parentNode;
			if ( !parent ) { continue; }
			// If it doesn't break a line, just remove it; it's not doing
			// anything useful. We'll add it back later if required by the
			// browser. If it breaks a line, wrap the content in div tags
			// and replace the brs.
			if ( !isLineBreak( br, keepForBlankLine ) ) {
				detach( br );
			} else if ( !isInline( parent ) ) {
				fixContainer( parent, root );
			}
		}
	},

	// The (non-standard but supported enough) innerText property is based on the
	// render tree in Firefox and possibly other browsers, so we must insert the
	// DOM node into the document to ensure the text part is correct.
	setClipboardData =
			( event, contents, root ) => {
		let clipboardData = event.clipboardData;
		let body = doc.body;
		let node = createElement( 'div' );
		let html, text;

		node.append( contents );

		html = node.innerHTML;

		// Firefox will add an extra new line for BRs at the end of block when
		// calculating innerText, even though they don't actually affect
		// display, so we need to remove them first.
		cleanupBRs( node, root, true );
		node.setAttribute( 'style',
			'position:fixed;overflow:hidden;bottom:100%;right:100%;' );
		body.append( node );
		text = node.innerText || node.textContent;
		text = text.replace( NBSP, ' ' ); // Replace nbsp with regular space
		node.remove(  );

		if ( text !== html ) {
			clipboardData.setData( 'text/html', html );
		}
		clipboardData.setData( 'text/plain', text );
		event.preventDefault();
	},

	mergeObjects = ( base, extras, mayOverride ) => {
		base = base || {};
		extras && Object.entries(extras).forEach(([prop,value])=>{
			if ( mayOverride || !( prop in base ) ) {
				base[ prop ] = ( value && value.constructor === Object ) ?
					mergeObjects( base[ prop ], value, mayOverride ) :
					value;
			}
		});
		return base;
	},

	// --- Events ---

	// Subscribing to these events won't automatically add a listener to the
	// document node, since these events are fired in a custom manner by the
	// editor code.
	customEvents = {
		pathChange: 1, select: 1, input: 1, undoStateChange: 1
	},

	// --- Workaround for browsers that can't focus empty text nodes ---

	// WebKit bug: https://bugs.webkit.org/show_bug.cgi?id=15256

	// Walk down the tree starting at the root and remove any ZWS. If the node only
	// contained ZWS space then remove it too. We may want to keep one ZWS node at
	// the bottom of the tree so the block can be selected. Define that node as the
	// keepNode.
	removeZWS = ( root, keepNode ) => {
		let walker = createTreeWalker( root, SHOW_TEXT );
		let parent, node, index;
		while ( node = walker.nextNode() ) {
			while ( ( index = node.data.indexOf( ZWS ) ) > -1  &&
					( !keepNode || node.parentNode !== keepNode ) ) {
				if ( node.length === 1 ) {
					do {
						parent = node.parentNode;
						node.remove(  );
						node = parent;
						walker.currentNode = parent;
					} while ( isInline( node ) && !getLength( node ) );
					break;
				} else {
					node.deleteData( index, 1 );
				}
			}
		}
	},

	// --- Bookmarking ---

	startSelectionId = 'squire-selection-start',
	endSelectionId = 'squire-selection-end',

	createBookmarkNodes = () => [
		createElement( 'INPUT', {
			id: startSelectionId,
			type: 'hidden'
		}),
		createElement( 'INPUT', {
			id: endSelectionId,
			type: 'hidden'
		})
	],

	// --- Block formatting ---

	tagAfterSplit = {
		DT:  'DD',
		DD:  'DT',
		LI:  'LI',
		PRE: 'PRE'
	},

	splitBlock = ( self, block, node, offset ) => {
		let splitTag = tagAfterSplit[ block.nodeName ] || self._config.blockTag,
			nodeAfterSplit = split( node, offset, block.parentNode, self._root );

		// Make sure the new node is the correct type.
		if ( !hasTagAttributes( nodeAfterSplit, splitTag, {} ) ) {
			block = createElement( splitTag );
			if ( nodeAfterSplit.dir ) {
				block.dir = nodeAfterSplit.dir;
			}
			nodeAfterSplit.replaceWith( block );
			block.append( empty( nodeAfterSplit ) );
			nodeAfterSplit = block;
		}
		return nodeAfterSplit;
	},

	getListSelection = ( range, root ) => {
		// Get start+end li in single common ancestor
		let list = range.commonAncestorContainer;
		let startLi = range.startContainer;
		let endLi = range.endContainer;
		while ( list && list !== root && !/^[OU]L$/.test( list.nodeName ) ) {
			list = list.parentNode;
		}
		if ( !list || list === root ) {
			return null;
		}
		if ( startLi === list ) {
			startLi = startLi.childNodes[ range.startOffset ];
		}
		if ( endLi === list ) {
			endLi = endLi.childNodes[ range.endOffset ];
		}
		while ( startLi && startLi.parentNode !== list ) {
			startLi = startLi.parentNode;
		}
		while ( endLi && endLi.parentNode !== list ) {
			endLi = endLi.parentNode;
		}
		return [ list, startLi, endLi ];
	},

	makeList = ( self, frag, type ) => {
		let walker = getBlockWalker( frag, self._root ),
			node, tag, prev, newLi;

		while ( node = walker.nextNode() ) {
			if ( node.parentNode.nodeName === 'LI' ) {
				node = node.parentNode;
				walker.currentNode = node.lastChild;
			}
			if ( node.nodeName !== 'LI' ) {
				newLi = createElement( 'LI' );
				if ( node.dir ) {
					newLi.dir = node.dir;
				}

				// Have we replaced the previous block with a new <ul>/<ol>?
				if ( ( prev = node.previousSibling ) && prev.nodeName === type ) {
					prev.append( newLi );
					detach( node );
				}
				// Otherwise, replace this block with the <ul>/<ol>
				else {
					node.replaceWith(
						createElement( type, null, [
							newLi
						])
					);
				}
				newLi.append( empty( node ) );
				walker.currentNode = newLi;
			} else {
				node = node.parentNode;
				tag = node.nodeName;
				if ( tag !== type && ( /^[OU]L$/.test( tag ) ) ) {
					node.replaceWith(
						createElement( type, null, [ empty( node ) ] )
					);
				}
			}
		}

		return frag;
	},

	linkRegExp = /\b(?:((?:(?:ht|f)tps?:\/\/|www\d{0,3}[.]|[a-z0-9][a-z0-9.-]*[.][a-z]{2,}\/)(?:[^\s()<>]+|\([^\s()<>]+\))+(?:[^\s?&`!()[\]{};:'".,<>«»“”‘’]|\([^\s()<>]+\)))|([\w\-.%+]+@(?:[\w-]+\.)+[a-z]{2,}\b(?:[?][^&?\s]+=[^\s?&`!()[\]{};:'".,<>«»“”‘’]+(?:&[^&?\s]+=[^\s?&`!()[\]{};:'".,<>«»“”‘’]+)*)?))/i,

	addLinks = ( frag, root ) => {
		let walker = createTreeWalker( frag, SHOW_TEXT, node => !getClosest( node, root, 'A' ) );
		let node, data, parent, match, index, endIndex, child;
		while (( node = walker.nextNode() )) {
			data = node.data;
			parent = node.parentNode;
			while (( match = linkRegExp.exec( data ) )) {
				index = match.index;
				endIndex = index + match[0].length;
				if ( index ) {
					child = doc.createTextNode( data.slice( 0, index ) );
					parent.insertBefore( child, node );
				}
				child = createElement( 'A', mergeObjects({
					href: match[1] ?
						/^(?:ht|f)tps?:/i.test( match[1] ) ?
							match[1] :
							'http://' + match[1] :
						'mailto:' + match[0]
				}, null, false ));
				child.textContent = data.slice( index, endIndex );
				parent.insertBefore( child, node );
				node.data = data = data.slice( endIndex );
			}
		}
	},

	escapeHTML = text => text.replace( '&', '&amp;' )
	   .replace( '<', '&lt;' )
	   .replace( '>', '&gt;' )
	   .replace( '"', '&quot;' ),

	removeFormatting = ( self, root, clean ) => {
		let node, next;
		for ( node = root.firstChild; node; node = next ) {
			next = node.nextSibling;
			if ( isInline( node ) ) {
				if ( node.nodeType === TEXT_NODE || node.nodeName === 'BR' || node.nodeName === 'IMG' ) {
					clean.append( node );
					continue;
				}
			} else if ( isBlock( node ) ) {
				clean.append( self.createDefaultBlock([
					removeFormatting(
						self, node, doc.createDocumentFragment() )
				]));
				continue;
			}
			removeFormatting( self, node, clean );
		}
		return clean;
	};

let contentWalker,
	nodeCategoryCache = new WeakMap();

// Previous node in post-order.
TreeWalker.prototype.previousPONode = function () {
	let current = this.currentNode,
		root = this.root,
		nodeType = this.nodeType, // whatToShow?
		filter = this.filter,
		node;
	while ( current ) {
		node = current.lastChild;
		while ( !node && current && current !== root) {
			node = current.previousSibling;
			if ( !node ) { current = current.parentNode; }
		}
		if ( node && ( typeToBitArray[ node.nodeType ] & nodeType ) && filter( node ) ) {
			this.currentNode = node;
			return node;
		}
		current = node;
	}
	return null;
};

function onKey ( event ) {
	if ( event.defaultPrevented ) {
		return;
	}

	let key = event.key.toLowerCase(),
		modifiers = '',
		range = this.getSelection();

	// We need to apply the backspace/delete handlers regardless of
	// control key modifiers.
	if ( key !== 'backspace' && key !== 'delete' ) {
		if ( event.altKey  ) { modifiers += 'alt-'; }
		if ( event[osKey] ) { modifiers += ctrlKey; }
		if ( event.shiftKey ) { modifiers += 'shift-'; }
	}

	key = modifiers + key;

	if ( this._keyHandlers[ key ] ) {
		this._keyHandlers[ key ]( this, event, range );
	// !event.isComposing stops us from blatting Kana-Kanji conversion in Safari
	} else if ( !range.collapsed && !event.isComposing &&
			!event[osKey] &&
			key.length === 1 ) {
		// Record undo checkpoint.
		this.saveUndoState( range );
		// Delete the selection
		deleteContentsOfRange( range, this._root );
		this._ensureBottomLine();
		this.setSelection( range );
		this._updatePath( range, true );
	}
}

function onCut ( event ) {
	let range = this.getSelection();
	let root = this._root;
	let self = this;
	let startBlock, endBlock, copyRoot, contents, parent, newContents;

	// Nothing to do
	if ( range.collapsed ) {
		event.preventDefault();
		return;
	}

	// Save undo checkpoint
	this.saveUndoState( range );

	// Edge only seems to support setting plain text as of 2016-03-11.
	if ( event.clipboardData ) {
		// Clipboard content should include all parents within block, or all
		// parents up to root if selection across blocks
		startBlock = getStartBlockOfRange( range, root );
		endBlock = getEndBlockOfRange( range, root );
		copyRoot = ( ( startBlock === endBlock ) && startBlock ) || root;
		// Extract the contents
		contents = deleteContentsOfRange( range, root );
		// Add any other parents not in extracted content, up to copy root
		parent = range.commonAncestorContainer;
		if ( parent.nodeType === TEXT_NODE ) {
			parent = parent.parentNode;
		}
		while ( parent && parent !== copyRoot ) {
			newContents = parent.cloneNode( false );
			newContents.append( contents );
			contents = newContents;
			parent = parent.parentNode;
		}
		// Set clipboard data
		setClipboardData( event, contents, root );
	} else {
		setTimeout( () => {
			try {
				// If all content removed, ensure div at start of root.
				self._ensureBottomLine();
			} catch ( error ) {
				didError( error );
			}
		}, 0 );
	}

	this.setSelection( range );
}

function onCopy ( event ) {
	// Edge only seems to support setting plain text as of 2016-03-11.
	if ( event.clipboardData ) {
		let range = this.getSelection(), root = this._root,
			// Clipboard content should include all parents within block, or all
			// parents up to root if selection across blocks
			startBlock = getStartBlockOfRange( range, root ),
			endBlock = getEndBlockOfRange( range, root ),
			copyRoot = ( ( startBlock === endBlock ) && startBlock ) || root,
			contents, parent, newContents;
		// Clone range to mutate, then move up as high as possible without
		// passing the copy root node.
		range = range.cloneRange();
		moveRangeBoundariesDownTree( range );
		moveRangeBoundariesUpTree( range, copyRoot, copyRoot, root );
		// Extract the contents
		contents = range.cloneContents();
		// Add any other parents not in extracted content, up to copy root
		parent = range.commonAncestorContainer;
		if ( parent.nodeType === TEXT_NODE ) {
			parent = parent.parentNode;
		}
		while ( parent && parent !== copyRoot ) {
			newContents = parent.cloneNode( false );
			newContents.append( contents );
			contents = newContents;
			parent = parent.parentNode;
		}
		// Set clipboard data
		setClipboardData( event, contents, root );
	}
}

// Need to monitor for shift key like this, as event.shiftKey is not available
// in paste event.
function monitorShiftKey ( event ) {
	this.isShiftDown = event.shiftKey;
}

function onPaste ( event ) {
	let clipboardData = event.clipboardData;
	let items = clipboardData && clipboardData.items;
	let choosePlain = this.isShiftDown;
	let fireDrop = false;
	let hasRTF = false;
	let hasImage = false;
	let plainItem = null;
	let htmlItem = null;
	let self = this;
	let l, item, type, types, data;

	// Current HTML5 Clipboard interface
	// ---------------------------------
	// https://html.spec.whatwg.org/multipage/interaction.html
	if ( items ) {
		l = items.length;
		while ( l-- ) {
			item = items[l];
			type = item.type;
			if ( type === 'text/html' ) {
				htmlItem = item;
			// iOS copy URL gives you type text/uri-list which is just a list
			// of 1 or more URLs separated by new lines. Can just treat as
			// plain text.
			} else if ( type === 'text/plain' || type === 'text/uri-list' ) {
				plainItem = item;
			} else if ( type === 'text/rtf' ) {
				hasRTF = true;
			} else if ( /^image\/.*/.test( type ) ) {
				hasImage = true;
			}
		}

		// Treat image paste as a drop of an image file. When you copy
		// an image in Chrome/Firefox (at least), it copies the image data
		// but also an HTML version (referencing the original URL of the image)
		// and a plain text version.
		//
		// However, when you copy in Excel, you get html, rtf, text, image;
		// in this instance you want the html version! So let's try using
		// the presence of text/rtf as an indicator to choose the html version
		// over the image.
		if ( hasImage && !( hasRTF && htmlItem ) ) {
			event.preventDefault();
			this.fireEvent( 'dragover', {
				dataTransfer: clipboardData,
				/*jshint loopfunc: true */
				preventDefault: () => fireDrop = true
				/*jshint loopfunc: false */
			});
			if ( fireDrop ) {
				this.fireEvent( 'drop', {
					dataTransfer: clipboardData
				});
			}
			return;
		}

		// Edge only provides access to plain text as of 2016-03-11 and gives no
		// indication there should be an HTML part. However, it does support
		// access to image data, so we check for that first. Otherwise though,
		// fall through to fallback clipboard handling methods
		event.preventDefault();
		if ( htmlItem && ( !choosePlain || !plainItem ) ) {
			htmlItem.getAsString( html => self.insertHTML( html, true ) );
		} else if ( plainItem ) {
			plainItem.getAsString( text => self.insertPlainText( text, true ) );
		}
		return;
	}

	// Safari (and indeed many other OS X apps) copies stuff as text/rtf
	// rather than text/html; even from a webpage in Safari. The only way
	// to get an HTML version is to fallback to letting the browser insert
	// the content. Same for getting image data. *Sigh*.
	types = clipboardData && clipboardData.types;
	if ( types && (
			types.includes( 'text/html' ) || (
				!isGecko &&
				types.includes( 'text/plain') &&
				!types.includes( 'text/rtf' ))
			)) {
		event.preventDefault();
		// Abiword on Linux copies a plain text and html version, but the HTML
		// version is the empty string! So always try to get HTML, but if none,
		// insert plain text instead. On iOS, Facebook (and possibly other
		// apps?) copy links as type text/uri-list, but also insert a **blank**
		// text/plain item onto the clipboard. Why? Who knows.
		if ( !choosePlain && ( data = clipboardData.getData( 'text/html' ) ) ) {
			this.insertHTML( data, true );
		} else if ( data = clipboardData.getData( 'text/plain' ) || clipboardData.getData( 'text/uri-list' ) ) {
			this.insertPlainText( data, true );
		}
		return;
	}
}

// On Windows you can drag an drop text. We can't handle this ourselves, because
// as far as I can see, there's no way to get the drop insertion point. So just
// save an undo state and hope for the best.
function onDrop ( event ) {
	let types = event.dataTransfer.types;
	let l = types.length;
	let hasData = false;
	while ( l-- ) {
		switch ( types[l] ) {
		case 'text/plain':
		case 'text/html':
			hasData = true;
			break;
		default:
			return;
		}
	}

//	if ( types.includes('text/plain') || types.includes('text/html') ) {
	if ( hasData ) {
		this.saveUndoState();
	}
}

let keyHandlers = {
	// This song and dance is to force iOS to do enable the shift key
	// automatically on enter. When you do the DOM split manipulation yourself,
	// WebKit doesn't reset the IME state and so presents auto-complete options
	// as though you were continuing to type on the previous line, and doesn't
	// auto-enable the shift key. The old trick of blurring and focussing
	// again no longer works in iOS 13, and I tried various execCommand options
	// but they didn't seem to do anything. The only solution I've found is to
	// let iOS handle the enter key, then after it's done that reset the HTML
	// to what it was before and handle it properly in Squire; the IME state of
	// course doesn't reset so you end up in the correct state!
	enter: isIOS ? ( self, event, range ) => {
		self._saveRangeToBookmark( range );
		let html = self._getHTML();
		let restoreAndDoEnter = () => {
			self.removeEventListener( 'keyup', restoreAndDoEnter );
			self._setHTML( html );
			range = self._getRangeAndRemoveBookmark();
			// Ignore the shift key on iOS, as this is for auto-capitalisation.
			handleEnter( self, false, range );
		};
		self.addEventListener( 'keyup', restoreAndDoEnter );
	} : ( self, event, range ) => {
		event.preventDefault();
		handleEnter( self, event.shiftKey, range );
	},

	'shift-enter': ( self, event, range ) => self._keyHandlers.enter( self, event, range ),

	backspace: ( self, event, range ) => {
		let root = self._root;
		self._removeZWS();
		// Record undo checkpoint.
		self.saveUndoState( range );
		// If not collapsed, delete contents
		if ( !range.collapsed ) {
			event.preventDefault();
			deleteContentsOfRange( range, root );
			afterDelete( self, range );
		}
		// If at beginning of block, merge with previous
		else if ( rangeDoesStartAtBlockBoundary( range, root ) ) {
			event.preventDefault();
			let current = getStartBlockOfRange( range, root );
			let previous;
			if ( !current ) {
				return;
			}
			// In case inline data has somehow got between blocks.
			fixContainer( current.parentNode, root );
			// Now get previous block
			previous = getPreviousBlock( current, root );
			// Must not be at the very beginning of the text area.
			if ( previous ) {
				// If not editable, just delete whole block.
				if ( !previous.isContentEditable ) {
					detachUneditableNode( previous, root );
					return;
				}
				// Otherwise merge.
				mergeWithBlock( previous, current, range, root );
				// If deleted line between containers, merge newly adjacent
				// containers.
				current = previous.parentNode;
				while ( current !== root && !current.nextSibling ) {
					current = current.parentNode;
				}
				if ( current !== root && ( current = current.nextSibling ) ) {
					mergeContainers( current, root );
				}
				self.setSelection( range );
			}
			// If at very beginning of text area, allow backspace
			// to break lists/blockquote.
			else if ( current ) {
				let parent = getClosest( current, root, 'UL,OL,BLOCKQUOTE' );
				if (parent) {
					return ( 'BLOCKQUOTE' === parent.nodeName )
						// Break blockquote
						? self.decreaseQuoteLevel( range )
						// Break list
						: self.decreaseListLevel( range );
				}
				self.setSelection( range );
				self._updatePath( range, true );
			}
		}
		// Otherwise, leave to browser but check afterwards whether it has
		// left behind an empty inline tag.
		else {
			self.setSelection( range );
			setTimeout( () => afterDelete( self ), 0 );
		}
	},
	'delete': ( self, event, range ) => {
		let root = self._root;
		let current, next, originalRange,
			cursorContainer, cursorOffset, nodeAfterCursor;
		self._removeZWS();
		// Record undo checkpoint.
		self.saveUndoState( range );
		// If not collapsed, delete contents
		if ( !range.collapsed ) {
			event.preventDefault();
			deleteContentsOfRange( range, root );
			afterDelete( self, range );
		}
		// If at end of block, merge next into this block
		else if ( rangeDoesEndAtBlockBoundary( range, root ) ) {
			event.preventDefault();
			current = getStartBlockOfRange( range, root );
			if ( !current ) {
				return;
			}
			// In case inline data has somehow got between blocks.
			fixContainer( current.parentNode, root );
			// Now get next block
			next = getNextBlock( current, root );
			// Must not be at the very end of the text area.
			if ( next ) {
				// If not editable, just delete whole block.
				if ( !next.isContentEditable ) {
					detachUneditableNode( next, root );
					return;
				}
				// Otherwise merge.
				mergeWithBlock( current, next, range, root );
				// If deleted line between containers, merge newly adjacent
				// containers.
				next = current.parentNode;
				while ( next !== root && !next.nextSibling ) {
					next = next.parentNode;
				}
				if ( next !== root && ( next = next.nextSibling ) ) {
					mergeContainers( next, root );
				}
				self.setSelection( range );
				self._updatePath( range, true );
			}
		}
		// Otherwise, leave to browser but check afterwards whether it has
		// left behind an empty inline tag.
		else {
			// But first check if the cursor is just before an IMG tag. If so,
			// delete it ourselves, because the browser won't if it is not
			// inline.
			originalRange = range.cloneRange();
			moveRangeBoundariesUpTree( range, root, root, root );
			cursorContainer = range.endContainer;
			cursorOffset = range.endOffset;
			if ( cursorContainer.nodeType === ELEMENT_NODE ) {
				nodeAfterCursor = cursorContainer.childNodes[ cursorOffset ];
				if ( nodeAfterCursor && nodeAfterCursor.nodeName === 'IMG' ) {
					event.preventDefault();
					detach( nodeAfterCursor );
					moveRangeBoundariesDownTree( range );
					afterDelete( self, range );
					return;
				}
			}
			self.setSelection( originalRange );
			setTimeout( () => afterDelete( self ), 0 );
		}
	},
	tab: ( self, event, range ) => {
		let root = self._root;
		let node, parent;
		self._removeZWS();
		// If no selection and at start of block
		if ( range.collapsed && rangeDoesStartAtBlockBoundary( range, root ) ) {
			node = getStartBlockOfRange( range, root );
			// Iterate through the block's parents
			while ( ( parent = node.parentNode ) ) {
				// If we find a UL or OL (so are in a list, node must be an LI)
				if ( parent.nodeName === 'UL' || parent.nodeName === 'OL' ) {
					// Then increase the list level
					event.preventDefault();
					self.increaseListLevel( range );
					break;
				}
				node = parent;
			}
		}
	},
	'shift-tab': ( self, event, range ) => {
		let root = self._root;
		let node;
		self._removeZWS();
		// If no selection and at start of block
		if ( range.collapsed && rangeDoesStartAtBlockBoundary( range, root ) ) {
			// Break list
			node = range.startContainer;
			if ( getClosest( node, root, 'UL,OL' ) ) {
				event.preventDefault();
				self.decreaseListLevel( range );
			}
		}
	},
	space: ( self, _, range ) => {
		let node;
		let root = self._root;
		self._recordUndoState( range, false );
		if ( self._config.addLinks ) {
			addLinks( range.startContainer, root, self );
		}
		self._getRangeAndRemoveBookmark( range );

		// If the cursor is at the end of a link (<a>foo|</a>) then move it
		// outside of the link (<a>foo</a>|) so that the space is not part of
		// the link text.
		node = range.endContainer;
		if ( range.collapsed && range.endOffset === getLength( node ) ) {
			do {
				if ( node.nodeName === 'A' ) {
					range.setStartAfter( node );
					break;
				}
			} while ( !node.nextSibling &&
				( node = node.parentNode ) && node !== root );
		}
		// Delete the selection if not collapsed
		if ( !range.collapsed ) {
			deleteContentsOfRange( range, root );
			self._ensureBottomLine();
			self.setSelection( range );
			self._updatePath( range, true );
		}

		self.setSelection( range );
	},
	arrowleft: self => self._removeZWS(),
	arrowright: self => self._removeZWS()
};

// System standard for page up/down on Mac is to just scroll, not move the
// cursor. On Linux/Windows, it should move the cursor, but some browsers don't
// implement this natively. Override to support it.
function _moveCursorTo( self, toStart ) {
	let root = self._root,
		range = createRange( root, toStart ? 0 : root.childNodes.length );
	moveRangeBoundariesDownTree( range );
	self.setSelection( range );
	return self;
}
if ( !isMac ) {
	keyHandlers.pageup = self => _moveCursorTo( self, true );
	keyHandlers.pagedown = self => _moveCursorTo( self, false );
}

keyHandlers[ ctrlKey + 'b' ] = mapKeyToFormat( 'B' );
keyHandlers[ ctrlKey + 'i' ] = mapKeyToFormat( 'I' );
keyHandlers[ ctrlKey + 'u' ] = mapKeyToFormat( 'U' );
keyHandlers[ ctrlKey + 'shift-7' ] = mapKeyToFormat( 'S' );
keyHandlers[ ctrlKey + 'shift-5' ] = mapKeyToFormat( 'SUB', 'SUP' );
keyHandlers[ ctrlKey + 'shift-6' ] = mapKeyToFormat( 'SUP', 'SUB' );
keyHandlers[ ctrlKey + 'shift-8' ] = toggleList( 'UL', 'makeUnorderedList' );
keyHandlers[ ctrlKey + 'shift-9' ] = toggleList( 'OL', 'makeOrderedList' );
keyHandlers[ ctrlKey + '[' ] = changeIndentationLevel( 'decrease' );
keyHandlers[ ctrlKey + ']' ] = changeIndentationLevel( 'increase' );
keyHandlers[ ctrlKey + 'd' ] = mapKeyTo( 'toggleCode' );
keyHandlers[ ctrlKey + 'y' ] = mapKeyTo( 'redo' );
keyHandlers[ 'redo' ] = mapKeyTo( 'redo' );
keyHandlers[ ctrlKey + 'z' ] = mapKeyTo( 'undo' );
keyHandlers[ 'undo' ] = mapKeyTo( 'undo' );
keyHandlers[ ctrlKey + 'shift-z' ] = mapKeyTo( 'redo' );

class EditStack extends Array
{
	constructor(squire) {
		super();
		this.squire = squire;
		this.index = -1;
		this.inUndoState = false;

		this.threshold = -1; // -1 means no threshold
		this.limit = -1; // -1 means no limit
	}

	clear() {
		this.index = -1;
		this.length = 0;
	}

	stateChanged ( /*canUndo, canRedo*/ ) {
		this.squire.fireEvent( 'undoStateChange', {
			canUndo: this.index > 0,
			canRedo: this.index + 1 < this.length
		});
		this.squire.fireEvent( 'input' );
	}

	docWasChanged () {
		if ( this.inUndoState ) {
			this.inUndoState = false;
			this.stateChanged ( /*true, false*/ );
		} else
			this.squire.fireEvent( 'input' );
	}

	// Leaves bookmark
	recordUndoState ( range, replace ) {
		replace = replace !== false && this.inUndoState;
		// Don't record if we're already in an undo state
		if ( !this.inUndoState || replace ) {
			// Advance pointer to new position
			let undoIndex = this.index;
			let undoThreshold = this.threshold;
			let undoLimit = this.limit;
			let squire = this.squire;
			let html;

			if ( !replace ) {
				++undoIndex;
			}
			undoIndex = Math.max(0, undoIndex);

			// Truncate stack if longer (i.e. if has been previously undone)
			this.length = Math.min(undoIndex + 1, this.length);

			// Get data
			if ( range ) {
				squire._saveRangeToBookmark( range );
			}
			html = squire._getHTML();

			// If this document is above the configured size threshold,
			// limit the number of saved undo states.
			// Threshold is in bytes, JS uses 2 bytes per character
			if ( undoThreshold > -1 && html.length * 2 > undoThreshold
			 && undoLimit > -1 && undoIndex > undoLimit ) {
				this.splice( 0, undoIndex - undoLimit );
			}

			// Save data
			this[ undoIndex ] = html;
			this.index = undoIndex;
			this.inUndoState = true;
		}
	}

	saveUndoState ( range ) {
		let squire = this.squire;
		if ( range === undefined ) {
			range = squire.getSelection();
		}
		this.recordUndoState( range );
		squire._getRangeAndRemoveBookmark( range );
	}

	undo () {
		let squire = this.squire,
			undoIndex = this.index - 1;
		// Sanity check: must not be at beginning of the history stack
		if ( undoIndex >= 0 || !this.inUndoState ) {
			// Make sure any changes since last checkpoint are saved.
			this.recordUndoState( squire.getSelection(), false );
			this.index = undoIndex;
			squire._setHTML( this[ undoIndex ] );
			let range = squire._getRangeAndRemoveBookmark();
			if ( range ) {
				squire.setSelection( range );
			}
			this.inUndoState = true;
			this.stateChanged ( /*undoIndex > 0, true*/ );
		}
	}

	redo () {
		// Sanity check: must not be at end of stack and must be in an undo state.
		let squire = this.squire,
			undoIndex = this.index + 1;
		if ( undoIndex < this.length && this.inUndoState ) {
			this.index = undoIndex;
			squire._setHTML( this[ undoIndex ] );
			let range = squire._getRangeAndRemoveBookmark();
			if ( range ) {
				squire.setSelection( range );
			}
			this.stateChanged ( /*true, undoIndex + 1 < this.length*/ );
		}
	}
}

class Squire
{
	constructor (root, config) {
		this._root = root;

		this._events = {};

		this._isFocused = false;
		this._lastRange = null;

		this._hasZWS = false;

		this._lastAnchorNode = null;
		this._lastFocusNode = null;
		this._path = '';

		let _willUpdatePath;
		const selectionchange = () => {
			if (root.contains(doc.activeElement)) {
				if ( this._isFocused && !_willUpdatePath ) {
					_willUpdatePath = setTimeout( () => {
						_willUpdatePath = 0;
						this._updatePath( this.getSelection() );
					}, 0 );
				}
			} else {
				this.removeEventListener('selectionchange', selectionchange);
			}
		};
		this.addEventListener('selectstart', () => this.addEventListener('selectionchange', selectionchange));

		this.editStack = new EditStack(this);
		this._ignoreChange = false;
		this._ignoreAllChanges = false;

		this._mutation = new MutationObserver( ()=>this._docWasChanged() );
		this._mutation.observe( root, {
			childList: true,
			attributes: true,
			characterData: true,
			subtree: true
		});

		// On blur, restore focus except if the user taps or clicks to focus a
		// specific point. Can't actually use click event because focus happens
		// before click, so use mousedown/touchstart
		this._restoreSelection = false;
		// https://caniuse.com/mdn-api_document_pointerup_event
		this.addEventListener( 'blur', () => this._restoreSelection = true )
			.addEventListener( 'pointerdown mousedown touchstart', () => this._restoreSelection = false )
			.addEventListener( 'focus', () => this._restoreSelection && this.setSelection( this._lastRange ) )
			.addEventListener( 'cut', onCut )
			.addEventListener( 'copy', onCopy )
			.addEventListener( 'keydown keyup', monitorShiftKey )
			.addEventListener( 'paste', onPaste )
			.addEventListener( 'drop', onDrop )
			.addEventListener( 'keydown', onKey )
			.addEventListener( 'pointerup keyup mouseup touchend', ()=>this.getSelection() );

		// Add key handlers
		this._keyHandlers = Object.create( keyHandlers );

		// Override default properties
		this.setConfig( config );

		root.setAttribute( 'contenteditable', 'true' );
		// Grammarly breaks the editor, *sigh*
		root.setAttribute( 'data-gramm', 'false' );

		// Remove Firefox's built-in controls
		try {
			doc.execCommand( 'enableObjectResizing', false, 'false' );
			doc.execCommand( 'enableInlineTableEditing', false, 'false' );
		} catch ( error ) {}

		root.__squire__ = this;

		// Need to register instance before calling setHTML, so that the fixCursor
		// function can lookup any default block tag options set.
		this.setHTML( '' );
	}

	setConfig ( config ) {
		config = mergeObjects({
			blockTag: 'DIV',
			addLinks: true
		}, config, true );

		if ( typeof config.sanitizeToDOMFragment !== 'function' ) {
			config.sanitizeToDOMFragment = null;
		}

		// Users may specify block tag in lower case
		config.blockTag = config.blockTag.toUpperCase();

		this._config = config;

		return this;
	}

	createDefaultBlock ( children ) {
		let config = this._config;
		return fixCursor(
			createElement( config.blockTag, null, children ),
			this._root
		);
	}

	getRoot () {
		return this._root;
	}

	modifyDocument ( modificationCallback ) {
		let mutation = this._mutation;
		if ( mutation ) {
			if ( mutation.takeRecords().length ) {
				this._docWasChanged();
			}
			mutation.disconnect();
		}

		this._ignoreAllChanges = true;
		modificationCallback();
		this._ignoreAllChanges = false;

		if ( mutation ) {
			mutation.observe( this._root, {
				childList: true,
				attributes: true,
				characterData: true,
				subtree: true
			});
			this._ignoreChange = false;
		}
	}

	// --- Events ---

	fireEvent ( type, event ) {
		let handlers = this._events[ type ];
		let isFocused, l, obj;
		// UI code, especially modal views, may be monitoring for focus events and
		// immediately removing focus. In certain conditions, this can cause the
		// focus event to fire after the blur event, which can cause an infinite
		// loop. So we detect whether we're actually focused/blurred before firing.
		if ( /^(?:focus|blur)/.test( type ) ) {
			isFocused = this._root === doc.activeElement;
			if ( type === 'focus' ) {
				if ( !isFocused || this._isFocused ) {
					return this;
				}
				this._isFocused = true;
			} else {
				if ( isFocused || !this._isFocused ) {
					return this;
				}
				this._isFocused = false;
			}
		}
		if ( handlers ) {
			if ( !event ) {
				event = {};
			}
			if ( event.type !== type ) {
				event.type = type;
			}
			// Clone handlers array, so any handlers added/removed do not affect it.
			handlers = handlers.slice();
			l = handlers.length;
			while ( l-- ) {
				obj = handlers[l];
				try {
					if ( obj.handleEvent ) {
						obj.handleEvent( event );
					} else {
						obj.call( this, event );
					}
				} catch ( error ) {
					error.details = 'Squire: fireEvent error. Event type: ' + type;
					didError( error );
				}
			}
		}
		return this;
	}

	handleEvent ( event ) {
		this.fireEvent( event.type, event );
	}

	addEventListener ( type, fn ) {
		type.split(/\s+/).forEach(type=>{
			let handlers = this._events[ type ],
				target = this._root;
			if ( !fn ) {
				didError({
					name: 'Squire: addEventListener with null or undefined fn',
					message: 'Event type: ' + type
				});
				return this;
			}
			if ( !handlers ) {
				handlers = this._events[ type ] = [];
				if ( !customEvents[ type ] ) {
					if ( type === 'selectionchange' ) {
						target = doc;
					}
					target.addEventListener( type, this, {capture:true,passive:'touchstart'===type} );
				}
			}
			handlers.push( fn );
		});
		return this;
	}

	removeEventListener ( type, fn ) {
		let handlers = this._events[ type ];
		let target = this._root;
		let l;
		if ( handlers ) {
			if ( fn ) {
				l = handlers.length;
				while ( l-- ) {
					if ( handlers[l] === fn ) {
						handlers.splice( l, 1 );
					}
				}
			} else {
				handlers.length = 0;
			}
			if ( !handlers.length ) {
				delete this._events[ type ];
				if ( !customEvents[ type ] ) {
					if ( type === 'selectionchange' ) {
						target = doc;
					}
					target.removeEventListener( type, this, true );
				}
			}
		}
		return this;
	}

	// --- Selection and Path ---

	getCursorPosition ( range ) {
		if ( ( !range && !( range = this.getSelection() ) ) ||
				!range.getBoundingClientRect ) {
			return null;
		}
		// Get the bounding rect
		let rect = range.getBoundingClientRect();
		let node, parent;
		if ( rect && !rect.top ) {
			this._ignoreChange = true;
			node = createElement( 'SPAN' );
			node.textContent = ZWS;
			insertNodeInRange( range, node );
			rect = node.getBoundingClientRect();
			parent = node.parentNode;
			node.remove(  );
			mergeInlines( parent, range );
		}
		return rect;
	}

	setSelection ( range ) {
		if ( range ) {
			this._lastRange = range;
			// If we're setting selection, that automatically, and synchronously, // triggers a focus event. So just store the selection and mark it as
			// needing restore on focus.
			if ( this._isFocused ) {
				// iOS bug: if you don't focus the iframe before setting the
				// selection, you can end up in a state where you type but the input
				// doesn't get directed into the contenteditable area but is instead
				// lost in a black hole. Very strange.
				if ( isIOS ) {
					win.focus();
				}
				let sel = win.getSelection();
				if ( sel && sel.setBaseAndExtent ) {
					sel.setBaseAndExtent(
						range.startContainer,
						range.startOffset,
						range.endContainer,
						range.endOffset,
					);
				} else if ( sel ) {
					// This is just for IE11
					sel.removeAllRanges();
					sel.addRange( range );
				}
			} else {
				this._restoreSelection = true;
			}
		}
		return this;
	}

	getSelection () {
		let sel = win.getSelection();
		let root = this._root;
		let range, startContainer, endContainer, node;
		// If not focused, always rely on cached range; another function may
		// have set it but the DOM is not modified until focus again
		if ( this._isFocused && sel && sel.rangeCount ) {
			range = sel.getRangeAt( 0 ).cloneRange();
			startContainer = range.startContainer;
			endContainer = range.endContainer;
			// FF can return the range as being inside an <img>. WTF?
			if ( startContainer && isLeaf( startContainer ) ) {
				range.setStartBefore( startContainer );
			}
			if ( endContainer && isLeaf( endContainer ) ) {
				range.setEndBefore( endContainer );
			}
		}
		if ( range && root.contains( range.commonAncestorContainer ) ) {
			this._lastRange = range;
		} else {
			range = this._lastRange;
			node = range.commonAncestorContainer;
			// Check the editor is in the live document; if not, the range has
			// probably been rewritten by the browser and is bogus
			if ( !doc.contains( node ) ) {
				range = null;
			}
		}
		return range || createRange( root.firstChild, 0 );
	}

	getSelectionClosest (selector) {
		let range = this.getSelection();
		return range && getClosest(range.commonAncestorContainer, this._root, selector);
	}

	selectionContains (selector) {
		let range = this.getSelection(),
			node = range && range.commonAncestorContainer;
		if (node && !range.collapsed) {
			node = node.querySelector ? node : node.parentElement;
			// TODO: isNodeContainedInRange( range, node ) for real selection match?
			return !!(node && node.querySelector(selector));
		}
		return false;
	}

	getSelectedText () {
		let range = this.getSelection();
		if ( !range || range.collapsed ) {
			return '';
		}
		let walker = createTreeWalker(
			range.commonAncestorContainer,
			SHOW_ELEMENT_OR_TEXT,
			node => isNodeContainedInRange( range, node )
		);
		let startContainer = range.startContainer;
		let endContainer = range.endContainer;
		let node = walker.currentNode = startContainer;
		let textContent = '';
		let addedTextInBlock = false;
		let value;

		if ( filterAccept != walker.filter.acceptNode( node ) ) {
			node = walker.nextNode();
		}

		while ( node ) {
			if ( node.nodeType === TEXT_NODE ) {
				value = node.data;
				if ( value && ( /\S/.test( value ) ) ) {
					if ( node === endContainer ) {
						value = value.slice( 0, range.endOffset );
					}
					if ( node === startContainer ) {
						value = value.slice( range.startOffset );
					}
					textContent += value;
					addedTextInBlock = true;
				}
			} else if ( node.nodeName === 'BR' ||
					addedTextInBlock && !isInline( node ) ) {
				textContent += '\n';
				addedTextInBlock = false;
			}
			node = walker.nextNode();
		}

		return textContent;
	}

	getPath () {
		return this._path;
	}

	// --- Workaround for browsers that can't focus empty text nodes ---

	// WebKit bug: https://bugs.webkit.org/show_bug.cgi?id=15256

	_addZWS () {
		this._hasZWS = isWebKit;
		return doc.createTextNode( isWebKit ? ZWS : '' );
	}
	_removeZWS () {
		if ( this._hasZWS ) {
			removeZWS( this._root );
			this._hasZWS = false;
		}
	}

	// --- Path change events ---

	_updatePath ( range, force ) {
		if ( !range ) {
			return;
		}
		let anchor = range.startContainer,
			focus = range.endContainer,
			newPath;
		if ( force || anchor !== this._lastAnchorNode ||
				focus !== this._lastFocusNode ) {
			this._lastAnchorNode = anchor;
			this._lastFocusNode = focus;
			newPath = ( anchor && focus ) ? ( anchor === focus ) ?
				getPath( focus, this._root, this._config ) : '(selection)' : '';
			if ( this._path !== newPath ) {
				this._path = newPath;
				this.fireEvent( 'pathChange', { path: newPath } );
			}
		}
		this.fireEvent( range.collapsed ? 'cursor' : 'select', {
			range: range
		});
	}

	// --- Focus ---

	focus () {
		this._root.focus({ preventScroll: true });
		return this;
	}

	blur () {
		this._root.blur();
		return this;
	}

	// --- Bookmarking ---

	_saveRangeToBookmark ( range ) {
		let [startNode, endNode] = createBookmarkNodes(this),
			temp;

		insertNodeInRange( range, startNode );
		range.collapse( false );
		insertNodeInRange( range, endNode );

		// In a collapsed range, the start is sometimes inserted after the end!
		if ( startNode.compareDocumentPosition( endNode ) &
				DOCUMENT_POSITION_PRECEDING ) {
			startNode.id = endSelectionId;
			endNode.id = startSelectionId;
			temp = startNode;
			startNode = endNode;
			endNode = temp;
		}

		range.setStartAfter( startNode );
		range.setEndBefore( endNode );
	}

	_getRangeAndRemoveBookmark ( range ) {
		let root = this._root,
			start = root.querySelector( '#' + startSelectionId ),
			end = root.querySelector( '#' + endSelectionId );

		if ( start && end ) {
			let startContainer = start.parentNode,
				endContainer = end.parentNode,
				startOffset = indexOf( startContainer.childNodes, start ),
				endOffset = indexOf( endContainer.childNodes, end );

			if ( startContainer === endContainer ) {
				--endOffset;
			}

			detach( start );
			detach( end );

			if ( !range ) {
				range = doc.createRange();
			}
			range.setStart( startContainer, startOffset );
			range.setEnd( endContainer, endOffset );

			// Merge any text nodes we split
			mergeInlines( startContainer, range );
			if ( startContainer !== endContainer ) {
				mergeInlines( endContainer, range );
			}

			// If we didn't split a text node, we should move into any adjacent
			// text node to current selection point
			if ( range.collapsed ) {
				startContainer = range.startContainer;
				if ( startContainer.nodeType === TEXT_NODE ) {
					endContainer = startContainer.childNodes[ range.startOffset ];
					if ( !endContainer || endContainer.nodeType !== TEXT_NODE ) {
						endContainer =
							startContainer.childNodes[ range.startOffset - 1 ];
					}
					if ( endContainer && endContainer.nodeType === TEXT_NODE ) {
						range.setStart( endContainer, 0 );
						range.collapse( true );
					}
				}
			}
		}
		return range || null;
	}

	// --- Undo ---

	_docWasChanged () {
		nodeCategoryCache = new WeakMap();
		if ( !this._ignoreAllChanges ) {
			if ( this._ignoreChange ) {
				this._ignoreChange = false;
			} else {
				this.editStack.docWasChanged();
			}
		}
	}

	_recordUndoState ( range, replace ) {
		this.editStack.recordUndoState( range, replace );
	}

	saveUndoState ( range ) {
		this.editStack.saveUndoState( range );
	}

	undo () {
		this.editStack.undo();
	}

	redo () {
		this.editStack.redo();
	}

	// --- Inline formatting ---

	// Looks for matching tag and attributes, so won't work
	// if <strong> instead of <b> etc.
	hasFormat ( tag, attributes, range ) {
		// 1. Normalise the arguments and get selection
		tag = tag.toUpperCase();
		if ( !attributes ) { attributes = {}; }
		if ( !range && !( range = this.getSelection() ) ) {
			return false;
		}

		// Sanitize range to prevent weird IE artifacts
		if ( !range.collapsed &&
				range.startContainer.nodeType === TEXT_NODE &&
				range.startOffset === range.startContainer.length &&
				range.startContainer.nextSibling ) {
			range.setStartBefore( range.startContainer.nextSibling );
		}
		if ( !range.collapsed &&
				range.endContainer.nodeType === TEXT_NODE &&
				range.endOffset === 0 &&
				range.endContainer.previousSibling ) {
			range.setEndAfter( range.endContainer.previousSibling );
		}

		// If the common ancestor is inside the tag we require, we definitely
		// have the format.
		let root = this._root;
		let common = range.commonAncestorContainer;
		let walker, node;
		if ( getNearest( common, root, tag, attributes ) ) {
			return true;
		}

		// If common ancestor is a text node and doesn't have the format, we
		// definitely don't have it.
		if ( common.nodeType === TEXT_NODE ) {
			return false;
		}

		// Otherwise, check each text node at least partially contained within
		// the selection and make sure all of them have the format we want.
		walker = createTreeWalker( common, SHOW_TEXT, node => isNodeContainedInRange( range, node ) );

		let seenNode = false;
		while ( node = walker.nextNode() ) {
			if ( !getNearest( node, root, tag, attributes ) ) {
				return false;
			}
			seenNode = true;
		}

		return seenNode;
	}

	// Extracts the font-family and font-size (if any) of the element
	// holding the cursor. If there's a selection, returns an empty object.
	getFontInfo ( range ) {
		let fontInfo = {
			color: undefined,
			backgroundColor: undefined,
			family: undefined,
			size: undefined
		};
		let seenAttributes = 0;
		let element, style, attr;

		if ( !range && !( range = this.getSelection() ) ) {
			return fontInfo;
		}

		element = range.commonAncestorContainer;
		if ( range.collapsed || element.nodeType === TEXT_NODE ) {
			if ( element.nodeType === TEXT_NODE ) {
				element = element.parentNode;
			}
			while ( seenAttributes < 4 && element ) {
				if ( style = element.style ) {
					if ( !fontInfo.color && ( attr = style.color ) ) {
						fontInfo.color = attr;
						++seenAttributes;
					}
					if ( !fontInfo.backgroundColor &&
							( attr = style.backgroundColor ) ) {
						fontInfo.backgroundColor = attr;
						++seenAttributes;
					}
					if ( !fontInfo.family && ( attr = style.fontFamily ) ) {
						fontInfo.family = attr;
						++seenAttributes;
					}
					if ( !fontInfo.size && ( attr = style.fontSize ) ) {
						fontInfo.size = attr;
						++seenAttributes;
					}
				}
				element = element.parentNode;
			}
		}
		return fontInfo;
	}

	_addFormat ( tag, attributes, range ) {
		// If the range is collapsed we simply insert the node by wrapping
		// it round the range and focus it.
		let root = this._root;
		let el, walker, startContainer, endContainer, startOffset, endOffset,
			node, needsFormat, block;

		if ( range.collapsed ) {
			el = fixCursor( createElement( tag, attributes ), root );
			insertNodeInRange( range, el );
			range.setStart( el.firstChild, el.firstChild.length );
			range.collapse( true );

			// Clean up any previous formats that may have been set on this block
			// that are unused.
			block = el;
			while ( isInline( block ) ) {
				block = block.parentNode;
			}
			removeZWS( block, el );
		}
		// Otherwise we find all the textnodes in the range (splitting
		// partially selected nodes) and if they're not already formatted
		// correctly we wrap them in the appropriate tag.
		else {
			// Create an iterator to walk over all the text nodes under this
			// ancestor which are in the range and not already formatted
			// correctly.
			//
			// In Blink/WebKit, empty blocks may have no text nodes, just a <br>.
			// Therefore we wrap this in the tag as well, as this will then cause it
			// to apply when the user types something in the block, which is
			// presumably what was intended.
			//
			// IMG tags are included because we may want to create a link around
			// them, and adding other styles is harmless.
			walker = createTreeWalker(
				range.commonAncestorContainer,
				SHOW_ELEMENT_OR_TEXT,
				node => ( node.nodeType === TEXT_NODE ||
							node.nodeName === 'BR' ||
							node.nodeName === 'IMG'
						) && isNodeContainedInRange( range, node )
			);

			// Start at the beginning node of the range and iterate through
			// all the nodes in the range that need formatting.
			startContainer = range.startContainer;
			startOffset = range.startOffset;
			endContainer = range.endContainer;
			endOffset = range.endOffset;

			// Make sure we start with a valid node.
			walker.currentNode = startContainer;
			if ( filterAccept != walker.filter.acceptNode( startContainer ) ) {
				startContainer = walker.nextNode();
				startOffset = 0;
			}

			// If there are no interesting nodes in the selection, abort
			if ( !startContainer ) {
				return range;
			}

			do {
				node = walker.currentNode;
				needsFormat = !getNearest( node, root, tag, attributes );
				if ( needsFormat ) {
					// <br> can never be a container node, so must have a text node
					// if node == (end|start)Container
					if ( node === endContainer && node.length > endOffset ) {
						node.splitText( endOffset );
					}
					if ( node === startContainer && startOffset ) {
						node = node.splitText( startOffset );
						if ( endContainer === startContainer ) {
							endContainer = node;
							endOffset -= startOffset;
						}
						startContainer = node;
						startOffset = 0;
					}
					el = createElement( tag, attributes );
					node.replaceWith( el );
					el.append( node );
				}
			} while ( walker.nextNode() );

			// If we don't finish inside a text node, offset may have changed.
			if ( endContainer.nodeType !== TEXT_NODE ) {
				if ( node.nodeType === TEXT_NODE ) {
					endContainer = node;
					endOffset = node.length;
				} else {
					// If <br>, we must have just wrapped it, so it must have only
					// one child
					endContainer = node.parentNode;
					endOffset = 1;
				}
			}

			// Now set the selection to as it was before
			range = createRange(
				startContainer, startOffset, endContainer, endOffset );
		}
		return range;
	}

	_removeFormat ( tag, attributes, range, partial ) {
		// Add bookmark
		this._saveRangeToBookmark( range );

		// We need a node in the selection to break the surrounding
		// formatted text.
		let fixer;
		if ( range.collapsed ) {
			fixer = this._addZWS();
			insertNodeInRange( range, fixer );
		}

		// Find block-level ancestor of selection
		let root = range.commonAncestorContainer;
		while ( isInline( root ) ) {
			root = root.parentNode;
		}

		// Find text nodes inside formatTags that are not in selection and
		// add an extra tag with the same formatting.
		let startContainer = range.startContainer,
			startOffset = range.startOffset,
			endContainer = range.endContainer,
			endOffset = range.endOffset,
			toWrap = [],
			examineNode = ( node, exemplar ) => {
				// If the node is completely contained by the range then
				// we're going to remove all formatting so ignore it.
				if ( isNodeContainedInRange( range, node, false ) ) {
					return;
				}

				let isText = ( node.nodeType === TEXT_NODE ),
					child, next;

				// If not at least partially contained, wrap entire contents
				// in a clone of the tag we're removing and we're done.
				if ( !isNodeContainedInRange( range, node ) ) {
					// Ignore bookmarks and empty text nodes
					if ( node.nodeName !== 'INPUT' &&
							( !isText || node.data ) ) {
						toWrap.push([ exemplar, node ]);
					}
					return;
				}

				// Split any partially selected text nodes.
				if ( isText ) {
					if ( node === endContainer && endOffset !== node.length ) {
						toWrap.push([ exemplar, node.splitText( endOffset ) ]);
					}
					if ( node === startContainer && startOffset ) {
						node.splitText( startOffset );
						toWrap.push([ exemplar, node ]);
					}
				}
				// If not a text node, recurse onto all children.
				// Beware, the tree may be rewritten with each call
				// to examineNode, hence find the next sibling first.
				else {
					for ( child = node.firstChild; child; child = next ) {
						next = child.nextSibling;
						examineNode( child, exemplar );
					}
				}
			},
			formatTags = Array.prototype.filter.call(
				root.getElementsByTagName( tag ),
				el => isNodeContainedInRange( range, el ) && hasTagAttributes( el, tag, attributes )
			);

		if ( !partial ) {
			formatTags.forEach( node => examineNode( node, node ) );
		}

		// Now wrap unselected nodes in the tag
		toWrap.forEach( item => {
			// [ exemplar, node ] tuple
			let el = item[0].cloneNode( false ),
				node = item[1];
			node.replaceWith( el );
			el.append( node );
		});
		// and remove old formatting tags.
		formatTags.forEach( el => el.replaceWith( empty( el ) ) );

		// Merge adjacent inlines:
		this._getRangeAndRemoveBookmark( range );
		if ( fixer ) {
			range.collapse( false );
		}
		mergeInlines( root, range );

		return range;
	}

	toggleTag ( name, remove ) {
		let range = this.getSelection();
		if ( this.hasFormat( name, null, range ) ) {
			this.changeFormat ( null, { tag: name }, range );
		} else {
			this.changeFormat ( { tag: name }, remove ? { tag: remove } : null, range );
		}
	}

	changeFormat ( add, remove, range, partial ) {
		// Normalise the arguments and get selection
		if ( !range && !( range = this.getSelection() ) ) {
			return this;
		}

		// Save undo checkpoint
		this.saveUndoState( range );

		if ( remove ) {
			range = this._removeFormat( remove.tag.toUpperCase(),
				remove.attributes || {}, range, partial );
		}

		if ( add ) {
			range = this._addFormat( add.tag.toUpperCase(),
				add.attributes || {}, range );
		}

		this.setSelection( range );
		this._updatePath( range, true );

		return this;
	}

	// --- Block formatting ---

	forEachBlock ( fn, range ) {
		if ( !range && !( range = this.getSelection() ) ) {
			return this;
		}

		// Save undo checkpoint
		this.saveUndoState( range );

		let root = this._root;
		let start = getStartBlockOfRange( range, root );
		let end = getEndBlockOfRange( range, root );
		if ( start && end ) {
			do {
				if ( fn( start ) || start === end ) { break; }
			} while ( start = getNextBlock( start, root ) );
		}

		this.setSelection( range );

		// Path may have changed
		this._updatePath( range, true );

		return this;
	}

	modifyBlocks ( modify, range ) {
		if ( !range && !( range = this.getSelection() ) ) {
			return this;
		}

		// 1. Save undo checkpoint and bookmark selection
		this._recordUndoState( range );

		let root = this._root;
		let frag;

		// 2. Expand range to block boundaries
		expandRangeToBlockBoundaries( range, root );

		// 3. Remove range.
		moveRangeBoundariesUpTree( range, root, root, root );
		frag = extractContentsOfRange( range, root, root );

		// 4. Modify tree of fragment and reinsert.
		insertNodeInRange( range, modify.call( this, frag ) );

		// 5. Merge containers at edges
		if ( range.endOffset < range.endContainer.childNodes.length ) {
			mergeContainers( range.endContainer.childNodes[ range.endOffset ], root );
		}
		mergeContainers( range.startContainer.childNodes[ range.startOffset ], root );

		// 6. Restore selection
		this._getRangeAndRemoveBookmark( range );
		this.setSelection( range );
		this._updatePath( range, true );

		return this;
	}

	increaseListLevel ( range ) {
		if ( !range && !( range = this.getSelection() ) ) {
			return this.focus();
		}

		let root = this._root;
		let listSelection = getListSelection( range, root );
		if ( !listSelection ) {
			return this.focus();
		}

		let list = listSelection[0];
		let startLi = listSelection[1];
		let endLi = listSelection[2];
		if ( !startLi || startLi === list.firstChild ) {
			return this.focus();
		}

		// Save undo checkpoint and bookmark selection
		this._recordUndoState( range );

		// Increase list depth
		let type = list.nodeName;
		let newParent = startLi.previousSibling;
		let next;
		if ( newParent.nodeName !== type ) {
			newParent = createElement( type );
			startLi.before( newParent );
		}
		do {
			next = startLi === endLi ? null : startLi.nextSibling;
			newParent.append( startLi );
		} while ( ( startLi = next ) );
		next = newParent.nextSibling;
		if ( next ) {
			mergeContainers( next, root );
		}

		// Restore selection
		this._getRangeAndRemoveBookmark( range );
		this.setSelection( range );
		this._updatePath( range, true );

		return this.focus();
	}

	decreaseListLevel ( range ) {
		if ( !range && !( range = this.getSelection() ) ) {
			return this.focus();
		}

		let root = this._root;
		let listSelection = getListSelection( range, root );
		if ( !listSelection ) {
			return this.focus();
		}

		let list = listSelection[0];
		let startLi = listSelection[1];
		let endLi = listSelection[2];
		let newParent, next, insertBefore, makeNotList;
		if ( !startLi ) {
			startLi = list.firstChild;
		}
		if ( !endLi ) {
			endLi = list.lastChild;
		}

		// Save undo checkpoint and bookmark selection
		this._recordUndoState( range );

		if ( startLi ) {
			// Find the new parent list node
			newParent = list.parentNode;

			// Split list if necesary
			insertBefore = !endLi.nextSibling ?
				list.nextSibling :
				split( list, endLi.nextSibling, newParent, root );

			if ( newParent !== root && newParent.nodeName === 'LI' ) {
				newParent = newParent.parentNode;
				while ( insertBefore ) {
					next = insertBefore.nextSibling;
					endLi.append( insertBefore );
					insertBefore = next;
				}
				insertBefore = list.parentNode.nextSibling;
			}

			makeNotList = !/^[OU]L$/.test( newParent.nodeName );
			do {
				next = startLi === endLi ? null : startLi.nextSibling;
				startLi.remove(  );
				if ( makeNotList && startLi.nodeName === 'LI' ) {
					startLi = this.createDefaultBlock([ empty( startLi ) ]);
				}
				newParent.insertBefore( startLi, insertBefore );
			} while (( startLi = next ));
		}

		if ( !list.firstChild ) {
			detach( list );
		}

		if ( insertBefore ) {
			mergeContainers( insertBefore, root );
		}

		// Restore selection
		this._getRangeAndRemoveBookmark( range );
		this.setSelection( range );
		this._updatePath( range, true );

		return this.focus();
	}

	_ensureBottomLine () {
		let root = this._root;
		let last = root.lastElementChild;
		if ( !last ||
				last.nodeName !== this._config.blockTag || !isBlock( last ) ) {
			root.append( this.createDefaultBlock() );
		}
	}

	// --- Get/Set data ---

	_getHTML () {
		return this._root.innerHTML;
	}

	_setHTML ( html ) {
		let root = this._root;
		let node = root;
		let sanitizeToDOMFragment = this._config.sanitizeToDOMFragment;
		if ( sanitizeToDOMFragment ) {
			empty( node );
			node.appendChild( sanitizeToDOMFragment( html, false, this ) );
		} else {
			node.innerHTML = html;
		}
		do {
			fixCursor( node, root );
		} while ( node = getNextBlock( node, root ) );
		this._ignoreChange = true;
	}

	getHTML ( withBookMark ) {
		let html, range;
		if ( withBookMark && ( range = this.getSelection() ) ) {
			this._saveRangeToBookmark( range );
		}
		html = this._getHTML().replace( /\u200B/g, '' );
		if ( range ) {
			this._getRangeAndRemoveBookmark( range );
		}
		return html;
	}

	setHTML ( html ) {
		let config = this._config;
		let sanitizeToDOMFragment = config.sanitizeToDOMFragment;
		let root = this._root;
		let div, frag, child;

		// Parse HTML into DOM tree
		if ( sanitizeToDOMFragment ) {
			frag = sanitizeToDOMFragment( html, false, this );
		} else {
			div = createElement( 'DIV' );
			div.innerHTML = html;
			frag = doc.createDocumentFragment();
			frag.append( empty( div ) );
		}

		cleanTree( frag, config );
		cleanupBRs( frag, root, false );

		fixContainer( frag, root );

		// Fix cursor
		let node, walker = getBlockWalker( frag, root );
		while ( (node = walker.nextNode()) && node !== root ) {
			fixCursor( node, root );
		}

		// Don't fire an input event
		this._ignoreChange = true;

		// Remove existing root children
		while ( child = root.lastChild ) {
			child.remove(  );
		}

		// And insert new content
		root.append( frag );
		fixCursor( root, root );

		// Reset the undo stack
		this.editStack.clear();

		// Record undo state
		let range = this._getRangeAndRemoveBookmark() ||
			createRange( root.firstChild, 0 );
		this.saveUndoState( range );
		// IE will also set focus when selecting text so don't use
		// setSelection. Instead, just store it in lastSelection, so if
		// anything calls getSelection before first focus, we have a range
		// to return.
		this._lastRange = range;
		this._restoreSelection = true;
		this._updatePath( range, true );

		return this;
	}

	insertElement ( el, range ) {
		if ( !range ) {
			range = this.getSelection();
		}
		range.collapse( true );
		if ( isInline( el ) ) {
			insertNodeInRange( range, el );
			range.setStartAfter( el );
		} else {
			// Get containing block node.
			let root = this._root;
			let splitNode = getStartBlockOfRange( range, root ) || root;
			let parent, nodeAfterSplit;
			// While at end of container node, move up DOM tree.
			while ( splitNode !== root && !splitNode.nextSibling ) {
				splitNode = splitNode.parentNode;
			}
			// If in the middle of a container node, split up to root.
			if ( splitNode !== root ) {
				parent = splitNode.parentNode;
				nodeAfterSplit = split( parent, splitNode.nextSibling, root, root );
			}
			if ( nodeAfterSplit ) {
				nodeAfterSplit.before( el );
			} else {
				root.append( el );
				// Insert blank line below block.
				nodeAfterSplit = this.createDefaultBlock();
				root.append( nodeAfterSplit );
			}
			range.setStart( nodeAfterSplit, 0 );
			range.setEnd( nodeAfterSplit, 0 );
			moveRangeBoundariesDownTree( range );
		}
		this.focus();
		this.setSelection( range );
		this._updatePath( range );

		return this;
	}

	insertImage ( src, attributes ) {
		let img = createElement( 'IMG', mergeObjects({
			src: src
		}, attributes, true ));
		this.insertElement( img );
		return img;
	}

	// Insert HTML at the cursor location. If the selection is not collapsed
	// insertTreeFragmentIntoRange will delete the selection so that it is replaced
	// by the html being inserted.
	insertHTML ( html, isPaste ) {
		let config = this._config;
		let sanitizeToDOMFragment = config.sanitizeToDOMFragment;
		let range = this.getSelection();
		let startFragmentIndex, endFragmentIndex;
		let div, frag, root, node, event;

		// Edge doesn't just copy the fragment, but includes the surrounding guff
		// including the full <head> of the page. Need to strip this out.
		if ( isPaste ) {
			startFragmentIndex = html.indexOf( '<!--StartFragment-->' );
			endFragmentIndex = html.lastIndexOf( '<!--EndFragment-->' );
			if ( startFragmentIndex > -1 && endFragmentIndex > -1 ) {
				html = html.slice( startFragmentIndex + 20, endFragmentIndex );
			}
		}
		if ( sanitizeToDOMFragment ) {
			frag = sanitizeToDOMFragment( html, isPaste, this );
		} else {
			// Wrap with <tr> if html contains dangling <td> tags
			if ( /<\/td>((?!<\/tr>)[\s\S])*$/i.test( html ) ) {
				html = '<TR>' + html + '</TR>';
			}
			// Wrap with <table> if html contains dangling <tr> tags
			if ( /<\/tr>((?!<\/table>)[\s\S])*$/i.test( html ) ) {
				html = '<TABLE>' + html + '</TABLE>';
			}
			// Parse HTML into DOM tree
			div = createElement( 'DIV' );
			div.innerHTML = html;
			frag = doc.createDocumentFragment();
			frag.append( empty( div ) );
		}

		// Record undo checkpoint
		this.saveUndoState( range );

		try {
			root = this._root;
			node = frag;
			event = {
				fragment: frag,
				preventDefault: function () {
					this.defaultPrevented = true;
				},
				defaultPrevented: false
			};

			addLinks( frag, frag, this );
			cleanTree( frag, config );
			cleanupBRs( frag, root, false );
			removeEmptyInlines( frag );
			frag.normalize();

			while ( node = getNextBlock( node, frag ) ) {
				fixCursor( node, root );
			}

			if ( isPaste ) {
				this.fireEvent( 'willPaste', event );
			}

			if ( !event.defaultPrevented ) {
				insertTreeFragmentIntoRange( range, event.fragment, root );
				range.collapse( false );

				// After inserting the fragment, check whether the cursor is inside
				// an <a> element and if so if there is an equivalent cursor
				// position after the <a> element. If there is, move it there.
				moveRangeBoundaryOutOf( range, 'A', root );

				this._ensureBottomLine();
			}

			this.setSelection( range );
			this._updatePath( range, true );
			// Safari sometimes loses focus after paste. Weird.
			if ( isPaste ) {
				this.focus();
			}
		} catch ( error ) {
			didError( error );
		}
		return this;
	}

	insertPlainText ( plainText, isPaste ) {
		let range = this.getSelection();
		if ( range.collapsed &&
				getClosest( range.startContainer, this._root, 'PRE' ) ) {
			let node = range.startContainer;
			let offset = range.startOffset;
			let text, event;
			if ( !node || node.nodeType !== TEXT_NODE ) {
				text = doc.createTextNode( '' );
				node && node.childNodes[ offset ].before( text );
				node = text;
				offset = 0;
			}
			event = {
				text: plainText,
				preventDefault: function () {
					this.defaultPrevented = true;
				},
				defaultPrevented: false
			};
			if ( isPaste ) {
				this.fireEvent( 'willPaste', event );
			}

			if ( !event.defaultPrevented ) {
				plainText = event.text;
				node.insertData( offset, plainText );
				range.setStart( node, offset + plainText.length );
				range.collapse( true );
			}
			this.setSelection( range );
			return this;
		}
		let lines = plainText.split( '\n' );
		let config = this._config;
		let tag = config.blockTag;
		let closeBlock  = '</' + tag + '>';
		let openBlock = '<' + tag + '>';
		let i, l, line;

		for ( i = 0, l = lines.length; i < l; ++i ) {
			line = lines[i];
			line = escapeHTML( line ).replace( / (?= )/g, '&nbsp;' );
			// We don't wrap the first line in the block, so if it gets inserted
			// into a blank line it keeps that line's formatting.
			// Wrap each line in <div></div>
			if ( i ) {
				line = openBlock + ( line || '<BR>' ) + closeBlock;
			}
			lines[i] = line;
		}
		return this.insertHTML( lines.join( '' ), isPaste );
	}

	// --- Formatting ---

	addStyles ( styles ) {
		if ( styles ) {
			let head = doc.documentElement.firstChild,
				style = createElement( 'STYLE', {
					type: 'text/css'
				});
			style.append( doc.createTextNode( styles ) );
			head.append( style );
		}
		return this;
	}

	makeLink ( url, attributes ) {
		let range = this.getSelection();
		if ( range.collapsed ) {
			let protocolEnd = url.indexOf( ':' ) + 1;
			if ( protocolEnd ) {
				while ( url[ protocolEnd ] === '/' ) { ++protocolEnd; }
			}
			insertNodeInRange(
				range,
				doc.createTextNode( url.slice( protocolEnd ) )
			);
		}
		attributes = mergeObjects(
			mergeObjects({
				href: url
			}, attributes, true ),
			null,
			false
		);

		this.changeFormat({
			tag: 'A',
			attributes: attributes
		}, {
			tag: 'A'
		}, range );
		return this.focus();
	}

	removeLink () {
		this.changeFormat( null, {
			tag: 'A'
		}, this.getSelection(), true );
		return this.focus();
	}

	setStyle ( style ) {
		let range = this.getSelection();
		let start = range ? range.startContainer : 0;
		let end = range ? range.endContainer : 0;
		// When the selection is all the text inside an element, set style on the element itself
		if ( start && TEXT_NODE === start.nodeType && 0 === range.startOffset && start === end && end.length === range.endOffset ) {
			this.saveUndoState( range );
			setStyle( start.parentNode, style );
			this.setSelection( range );
			this._updatePath( range, true );
		}
		// Else create a span element
		else {
			this.changeFormat({
				tag: 'SPAN',
				attributes: {
					style: style
				}
			}, null, range);
		}
		return this.focus();
	}

	setTextColor ( color ) {
		return this.setStyle({
			color: color
		});
	}

	setBackgroundColor ( color ) {
		return this.setStyle({
			backgroundColor: color
		});
	}

	setTextAlignment ( alignment ) {
		this.forEachBlock( block => {
			block.style.textAlign = alignment || '';
		} );
		return this.focus();
	}

	setTextDirection ( direction ) {
		this.forEachBlock( block => {
			if ( direction ) {
				block.dir = direction;
			} else {
				block.removeAttribute( 'dir' );
			}
		} );
		return this.focus();
	}

	// ---

	code () {
		let range = this.getSelection();
		if ( range.collapsed || isContainer( range.commonAncestorContainer ) ) {
			this.modifyBlocks( frag => {
				let root = this._root;
				let output = doc.createDocumentFragment();
				let walker = getBlockWalker( frag, root );
				let node;
				// 1. Extract inline content; drop all blocks and contains.
				while (( node = walker.nextNode() )) {
					// 2. Replace <br> with \n in content
					let nodes = node.querySelectorAll( 'BR' );
					let l = nodes.length;
					let br;

					while ( l-- ) {
						br = nodes[l];
						if ( !isLineBreak( br, false ) ) {
							detach( br );
						} else {
							br.replaceWith( doc.createTextNode( '\n' ) );
						}
					}
					// 3. Remove <code>; its format clashes with <pre>
					nodes = node.querySelectorAll( 'CODE' );
					l = nodes.length;
					while ( l-- ) {
						detach( nodes[l] );
					}
					if ( output.childNodes.length ) {
						output.append( doc.createTextNode( '\n' ) );
					}
					output.append( empty( node ) );
				}
				// 4. Replace nbsp with regular sp
				walker = createTreeWalker( output, SHOW_TEXT );
				while (( node = walker.nextNode() )) {
					node.data = node.data.replace( NBSP, ' ' ); // nbsp -> sp
				}
				output.normalize();
				return fixCursor( createElement( 'PRE',
					null, [
						output
					]), root );
			}, range );
		} else {
			this.changeFormat({
				tag: 'CODE'
			}, null, range );
		}
		return this.focus();
	}

	removeCode () {
		let range = this.getSelection();
		let ancestor = range.commonAncestorContainer;
		let inPre = getClosest( ancestor, this._root, 'PRE' );
		if ( inPre ) {
			this.modifyBlocks( frag => {
				let root = this._root;
				let pres = frag.querySelectorAll( 'PRE' );
				let l = pres.length;
				let pre, walker, node, value, contents, index;
				while ( l-- ) {
					pre = pres[l];
					walker = createTreeWalker( pre, SHOW_TEXT );
					while (( node = walker.nextNode() )) {
						value = node.data;
						value = value.replace( / (?= )/g, NBSP ); // sp -> nbsp
						contents = doc.createDocumentFragment();
						while (( index = value.indexOf( '\n' ) ) > -1 ) {
							contents.append(
								doc.createTextNode( value.slice( 0, index ) )
							);
							contents.append( createElement( 'BR' ) );
							value = value.slice( index + 1 );
						}
						node.before( contents );
						node.data = value;
					}
					fixContainer( pre, root );
					pre.replaceWith( empty( pre ) );
				}
				return frag;
			}, range );
		} else {
			this.changeFormat( null, { tag: 'CODE' }, range );
		}
		return this.focus();
	}

	toggleCode () {
		if ( this.hasFormat( 'PRE' ) || this.hasFormat( 'CODE' ) ) {
			this.removeCode();
		} else {
			this.code();
		}
		return this;
	}

	// ---

	removeAllFormatting ( range ) {
		if ( !range && !( range = this.getSelection() ) || range.collapsed ) {
			return this;
		}

		let root = this._root;
		let stopNode = range.commonAncestorContainer;
		while ( stopNode && !isBlock( stopNode ) ) {
			stopNode = stopNode.parentNode;
		}
		if ( !stopNode ) {
			expandRangeToBlockBoundaries( range, root );
			stopNode = root;
		}
		if ( stopNode.nodeType === TEXT_NODE ) {
			return this;
		}

		// Record undo point
		this.saveUndoState( range );

		// Avoid splitting where we're already at edges.
		moveRangeBoundariesUpTree( range, stopNode, stopNode, root );

		// Split the selection up to the block, or if whole selection in same
		// block, expand range boundaries to ends of block and split up to root.
		let startContainer = range.startContainer;
		let startOffset = range.startOffset;
		let endContainer = range.endContainer;
		let endOffset = range.endOffset;

		// Split end point first to avoid problems when end and start
		// in same container.
		let formattedNodes = doc.createDocumentFragment();
		let cleanNodes = doc.createDocumentFragment();
		let nodeAfterSplit = split( endContainer, endOffset, stopNode, root );
		let nodeInSplit = split( startContainer, startOffset, stopNode, root );
		let nextNode, childNodes;

		// Then replace contents in split with a cleaned version of the same:
		// blocks become default blocks, text and leaf nodes survive, everything
		// else is obliterated.
		while ( nodeInSplit !== nodeAfterSplit ) {
			nextNode = nodeInSplit.nextSibling;
			formattedNodes.append( nodeInSplit );
			nodeInSplit = nextNode;
		}
		removeFormatting( this, formattedNodes, cleanNodes );
		cleanNodes.normalize();
		nodeInSplit = cleanNodes.firstChild;
		nextNode = cleanNodes.lastChild;

		// Restore selection
		childNodes = stopNode.childNodes;
		if ( nodeInSplit ) {
			stopNode.insertBefore( cleanNodes, nodeAfterSplit );
			startOffset = indexOf( childNodes, nodeInSplit );
			endOffset = indexOf( childNodes, nextNode ) + 1;
		} else {
			startOffset = indexOf( childNodes, nodeAfterSplit );
			endOffset = startOffset;
		}

		// Merge text nodes at edges, if possible
		range.setStart( stopNode, startOffset );
		range.setEnd( stopNode, endOffset );
		mergeInlines( stopNode, range );

		// And move back down the tree
		moveRangeBoundariesDownTree( range );

		this.setSelection( range );
		this._updatePath( range, true );

		return this.focus();
	}

	changeIndentationLevel (direction) {
		let parent = this.getSelectionClosest('UL,OL,BLOCKQUOTE');
		if (parent || 'increase' === direction) {
			let method = ( !parent || 'BLOCKQUOTE' === parent.nodeName ) ? 'Quote' : 'List';
			this[ direction + method + 'Level' ]();
		}
	}

	increaseQuoteLevel ( range ) {
		this.modifyBlocks(
			frag => createElement( 'BLOCKQUOTE', null, [ frag ]),
			range
		);
		return this.focus();
	}

	decreaseQuoteLevel ( range ) {
		this.modifyBlocks(
			frag => {
				var blockquotes = frag.querySelectorAll( 'blockquote' );
				Array.prototype.filter.call( blockquotes, el =>
					!getClosest( el.parentNode, frag, 'BLOCKQUOTE' )
				).forEach( el => el.replaceWith( empty( el ) ) );
				return frag;
			},
			range
		);
		return this.focus();
	}

	makeUnorderedList () {
		this.modifyBlocks( frag => makeList( this, frag, 'UL' ) );
		return this.focus();
	}

	makeOrderedList () {
		this.modifyBlocks( frag => makeList( this, frag, 'OL' ) );
		return this.focus();
	}

	removeList () {
		this.modifyBlocks( frag => {
			let lists = frag.querySelectorAll( 'UL, OL' ),
				items =  frag.querySelectorAll( 'LI' ),
				root = this._root,
				i, l, list, listFrag, item;
			for ( i = 0, l = lists.length; i < l; ++i ) {
				list = lists[i];
				listFrag = empty( list );
				fixContainer( listFrag, root );
				list.replaceWith( listFrag );
			}

			for ( i = 0, l = items.length; i < l; ++i ) {
				item = items[i];
				if ( isBlock( item ) ) {
					item.replaceWith(
						this.createDefaultBlock([ empty( item ) ])
					);
				} else {
					fixContainer( item, root );
					item.replaceWith( empty( item ) );
				}
			}

			return frag;
		});
		return this.focus();
	}

	bold () { this.toggleTag( 'B' ); }
	italic () { this.toggleTag( 'I' ); }
	underline () { this.toggleTag( 'U' ); }
	strikethrough () { this.toggleTag( 'S' ); }
	subscript () { this.toggleTag( 'SUB', 'SUP' ); }
	superscript () { this.toggleTag( 'SUP', 'SUB' ); }
}

win.Squire = Squire;

})( document );

/* eslint max-len: 0 */

(doc => {

const
	removeElements = 'HEAD,LINK,META,NOSCRIPT,SCRIPT,TEMPLATE,TITLE',
	allowedElements = 'A,B,BLOCKQUOTE,BR,DIV,FONT,H1,H2,H3,H4,H5,H6,HR,IMG,LI,OL,P,SPAN,STRONG,TABLE,TD,TH,TR,U,UL',
	allowedAttributes = 'abbr,align,background,bgcolor,border,cellpadding,cellspacing,class,color,colspan,dir,face,frame,height,href,hspace,id,lang,rowspan,rules,scope,size,src,style,target,type,usemap,valign,vspace,width'.split(','),

	i18n = (str, def) => rl.i18n(str) || def,

	ctrlKey = shortcuts.getMetaKey() + ' + ',

	createElement = name => doc.createElement(name),

	tpl = createElement('template'),
	clr = createElement('input'),

	trimLines = html => html.trim().replace(/^(<div>\s*<br\s*\/?>\s*<\/div>)+/, '').trim(),
	htmlToPlain = html => rl.Utils.htmlToPlain(html).trim(),
	plainToHtml = text => rl.Utils.plainToHtml(text),

	getFragmentOfChildren = parent => {
		let frag = doc.createDocumentFragment();
		frag.append(...parent.childNodes);
		return frag;
	},

	SquireDefaultConfig = {
/*
		blockTag: 'P',
		undo: {
			documentSizeThreshold: -1, // -1 means no threshold
			undoLimit: -1 // -1 means no limit
		},
		addLinks: true // allow_smart_html_links
*/
		sanitizeToDOMFragment: (html, isPaste/*, squire*/) => {
			tpl.innerHTML = (html||'')
				.replace(/<\/?(BODY|HTML)[^>]*>/gi,'')
				.replace(/<!--[^>]+-->/g,'')
				.replace(/<span[^>]*>\s*<\/span>/gi,'')
				.trim();
			tpl.querySelectorAll('a:empty,span:empty').forEach(el => el.remove());
			tpl.querySelectorAll('[data-x-div-type]').forEach(el => el.replaceWith(getFragmentOfChildren(el)));
			if (isPaste) {
				tpl.querySelectorAll(removeElements).forEach(el => el.remove());
				tpl.querySelectorAll('*').forEach(el => {
					if (!el.matches(allowedElements)) {
						el.replaceWith(getFragmentOfChildren(el));
					} else if (el.hasAttributes()) {
						[...el.attributes].forEach(attr => {
							let name = attr.name.toLowerCase();
							if (!allowedAttributes.includes(name)) {
								el.removeAttribute(name);
							}
						});
					}
				});
			}
			return tpl.content;
		}
	};

clr.type = "color";
clr.style.display = 'none';
doc.body.append(clr);

class SquireUI
{
	constructor(container) {
		const
			doClr = fn => () => {
				clr.value = '';
				clr.onchange = () => squire[fn](clr.value);
				clr.click();
			},

			actions = {
				mode: {
					plain: {
						html: '〈〉',
						cmd: () => this.setMode('plain' == this.mode ? 'wysiwyg' : 'plain'),
						hint: i18n('EDITOR/TEXT_SWITCHER_PLAINT_TEXT', 'Plain')
					}
				},
				font: {
					fontFamily: {
						select: {
							'sans-serif': {
								Arial: "'Nimbus Sans L', 'Liberation sans', 'Arial Unicode MS', Arial, Helvetica, Garuda, Utkal, FreeSans, sans-serif",
								Tahoma: "'Luxi Sans', Tahoma, Loma, Geneva, Meera, sans-serif",
								Trebuchet: "'DejaVu Sans Condensed', Trebuchet, 'Trebuchet MS', sans-serif",
								Lucida: "'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', 'Bitstream Vera Sans', 'DejaVu LGC Sans', sans-serif",
								Verdana: "'DejaVu Sans', Verdana, Geneva, 'Bitstream Vera Sans', 'DejaVu LGC Sans', sans-serif"
							},
							monospace: {
								Courier: "'Liberation Mono', 'Courier New', FreeMono, Courier, monospace",
								Lucida: "'DejaVu Sans Mono', 'DejaVu LGC Sans Mono', 'Bitstream Vera Sans Mono', 'Lucida Console', Monaco, monospace"
							},
							sans: {
								Times: "'Nimbus Roman No9 L', 'Times New Roman', Times, FreeSerif, serif",
								Palatino: "'Bitstream Charter', 'Palatino Linotype', Palatino, Palladio, 'URW Palladio L', 'Book Antiqua', Times, serif",
								Georgia: "'URW Palladio L', Georgia, Times, serif"
							}
						},
						cmd: s => squire.setStyle({ fontFamily: s.value })
					},
					fontSize: {
						select: ['11px','13px','16px','20px','24px','30px'],
						cmd: s => squire.setStyle({ fontSize: s.value })
					}
				},
				colors: {
					textColor: {
						html: 'A<sub>▾</sub>',
						cmd: doClr('setTextColor'),
						hint: 'Text color'
					},
					backgroundColor: {
						html: '🎨', /* ▧ */
						cmd: doClr('setBackgroundColor'),
						hint: 'Background color'
					},
				},
/*
				bidi: {
					bdoLtr: {
						html: '&lrm;𝐁',
						cmd: () => this.doAction('bold','B'),
						hint: 'Bold'
					},
					bdoRtl: {
						html: '&rlm;𝐁',
						cmd: () => this.doAction('bold','B'),
						hint: 'Bold'
					}
				},
*/
				inline: {
					bold: {
						html: 'B',
						cmd: () => this.doAction('bold'),
						key: 'B',
						hint: 'Bold'
					},
					italic: {
						html: 'I',
						cmd: () => this.doAction('italic'),
						key: 'I',
						hint: 'Italic'
					},
					underline: {
						html: '<u>U</u>',
						cmd: () => this.doAction('underline'),
						key: 'U',
						hint: 'Underline'
					},
					strike: {
						html: '<s>S</s>',
						cmd: () => this.doAction('strikethrough'),
						key: 'Shift + 7',
						hint: 'Strikethrough'
					},
					sub: {
						html: 'Xₙ',
						cmd: () => this.doAction('subscript'),
						key: 'Shift + 5',
						hint: 'Subscript'
					},
					sup: {
						html: 'Xⁿ',
						cmd: () => this.doAction('superscript'),
						key: 'Shift + 6',
						hint: 'Superscript'
					}
				},
				block: {
					ol: {
						html: '#',
						cmd: () => this.doList('OL'),
						key: 'Shift + 8',
						hint: 'Ordered list'
					},
					ul: {
						html: '⋮',
						cmd: () => this.doList('UL'),
						key: 'Shift + 9',
						hint: 'Unordered list'
					},
					quote: {
						html: '"',
						cmd: () => {
							let parent = this.getParentNodeName('UL,OL');
							(parent && 'BLOCKQUOTE' == parent) ? squire.decreaseQuoteLevel() : squire.increaseQuoteLevel();
						},
						hint: 'Blockquote'
					},
					indentDecrease: {
						html: '⇤',
						cmd: () => squire.changeIndentationLevel('decrease'),
						key: ']',
						hint: 'Decrease indent'
					},
					indentIncrease: {
						html: '⇥',
						cmd: () => squire.changeIndentationLevel('increase'),
						key: '[',
						hint: 'Increase indent'
					}
				},
				targets: {
					link: {
						html: '🔗',
						cmd: () => {
							if ('A' === this.getParentNodeName()) {
								squire.removeLink();
							} else {
								let url = prompt("Link","https://");
								url != null && url.length && squire.makeLink(url);
							}
						},
						hint: 'Link'
					},
					imageUrl: {
						html: '🖼️',
						cmd: () => {
							if ('IMG' === this.getParentNodeName()) {
//								squire.removeLink();
							} else {
								let src = prompt("Image","https://");
								src != null && src.length && squire.insertImage(src);
							}
						},
						hint: 'Image URL'
					},
					imageUpload: {
						html: '📂️',
						cmd: () => browseImage.click(),
						hint: 'Image select',
					}
				},
/*
				table: {
					// TODO
				},
*/
				changes: {
					undo: {
						html: '↶',
						cmd: () => squire.undo(),
						key: 'Z',
						hint: 'Undo'
					},
					redo: {
						html: '↷',
						cmd: () => squire.redo(),
						key: 'Y',
						hint: 'Redo'
					}
				}
			},

			plain = createElement('textarea'),
			wysiwyg = createElement('div'),
			toolbar = createElement('div'),
			browseImage = createElement('input'),
			squire = new Squire(wysiwyg, SquireDefaultConfig);

		browseImage.type = 'file';
		browseImage.accept = 'image/*';
		browseImage.style.display = 'none';
		browseImage.onchange = () => {
			if (browseImage.files.length) {
				let reader = new FileReader();
				reader.readAsDataURL(browseImage.files[0]);
				reader.onloadend = () => reader.result && squire.insertImage(reader.result);
			}
		}

		plain.className = 'squire-plain';
		wysiwyg.className = 'squire-wysiwyg';
		this.mode = ''; // 'plain' | 'wysiwyg'
		this.__plain = {
			getRawData: () => this.plain.value,
			setRawData: plain => this.plain.value = plain
		};

		this.container = container;
		this.squire = squire;
		this.plain = plain;
		this.wysiwyg = wysiwyg;

		toolbar.className = 'squire-toolbar btn-toolbar';
		let group, action, touchTap;
		for (group in actions) {
/*
			if ('bidi' == group && !rl.settings.app('allowHtmlEditorBitiButtons')) {
				continue;
			}
*/
			let toolgroup = createElement('div');
			toolgroup.className = 'btn-group';
			toolgroup.id = 'squire-toolgroup-'+group;
			for (action in actions[group]) {
				let cfg = actions[group][action], input, ev = 'click';
				if (cfg.input) {
					input = createElement('input');
					input.type = cfg.input;
					ev = 'change';
				} else if (cfg.select) {
					input = createElement('select');
					input.className = 'btn';
					if (Array.isArray(cfg.select)) {
						cfg.select.forEach(value => {
							var option = new Option(value, value);
							option.style[action] = value;
							input.append(option);
						});
					} else {
						Object.entries(cfg.select).forEach(([label, options]) => {
							let group = createElement('optgroup');
							group.label = label;
							Object.entries(options).forEach(([text, value]) => {
								var option = new Option(text, value);
								option.style[action] = value;
								group.append(option);
							});
							input.append(group);
						});
					}
					ev = 'input';
				} else {
					input = createElement('button');
					input.type = 'button';
					input.className = 'btn';
					input.innerHTML = cfg.html;
					input.action_cmd = cfg.cmd;
					input.addEventListener('touchstart', () => touchTap = input, {passive:true});
					input.addEventListener('touchmove', () => touchTap = null, {passive:true});
					input.addEventListener('touchcancel', () => touchTap = null);
					input.addEventListener('touchend', e => {
						if (touchTap === input) {
							e.preventDefault();
							cfg.cmd(input);
						}
						touchTap = null;
					});
				}
				input.addEventListener(ev, () => cfg.cmd(input));
				if (cfg.hint) {
					input.title = cfg.key ? cfg.hint + ' (' + ctrlKey + cfg.key + ')' : cfg.hint;
				} else if (cfg.key) {
					input.title = ctrlKey + cfg.key;
				}
				input.dataset.action = action;
				input.tabIndex = -1;
				cfg.input = input;
				toolgroup.append(input);
			}
			toolgroup.children.length && toolbar.append(toolgroup);
		}

		let changes = actions.changes;
		changes.undo.input.disabled = changes.redo.input.disabled = true;
		squire.addEventListener('undoStateChange', state => {
			changes.undo.input.disabled = !state.canUndo;
			changes.redo.input.disabled = !state.canRedo;
		});

		squire.addEventListener('focus', () => shortcuts.off());
		squire.addEventListener('blur', () => shortcuts.on());

		container.append(toolbar, wysiwyg, plain);

/*
		squire.addEventListener('dragover', );
		squire.addEventListener('drop', );
		squire.addEventListener('pathChange', );
		squire.addEventListener('cursor', );
		squire.addEventListener('select', );
		squire.addEventListener('input', );
		squire.addEventListener('willPaste', );
		squire.addEventListener( 'keydown keyup', monitorShiftKey )
		squire.addEventListener( 'keydown', onKey )
*/

		// CKEditor gimmicks used by HtmlEditor
		this.plugins = {
			plain: true
		};
		this.focusManager = {
			hasFocus: () => squire._isFocused,
			blur: () => squire.blur()
		};
	}

	doAction(name) {
		this.squire[name]();
		this.squire.focus();
	}

	getParentNodeName(selector) {
		let parent = this.squire.getSelectionClosest(selector);
		return parent ? parent.nodeName : null;
	}

	doList(type) {
		let parent = this.getParentNodeName('UL,OL'),
			fn = {UL:'makeUnorderedList',OL:'makeOrderedList'};
		(parent && parent == type) ? this.squire.removeList() : this.squire[fn[type]]();
	}

	testPresenceinSelection(format, validation) {
		return validation.test(this.squire.getPath()) || this.squire.hasFormat(format);
	}

	setMode(mode) {
		if (this.mode != mode) {
			let cl = this.container.classList;
			cl.remove('squire-mode-'+this.mode);
			if ('plain' == mode) {
				this.plain.value = htmlToPlain(this.squire.getHTML(), true);
			} else {
				this.setData(plainToHtml(this.plain.value, true));
				mode = 'wysiwyg';
			}
			this.mode = mode; // 'wysiwyg' or 'plain'
			cl.add('squire-mode-'+mode);
			this.onModeChange && this.onModeChange();
			setTimeout(()=>this.focus(),1);
		}
	}

	// CKeditor gimmicks used by HtmlEditor
	on(type, fn) {
		if ('mode' == type) {
			this.onModeChange = fn;
		} else {
			this.squire.addEventListener(type, fn);
			this.plain.addEventListener(type, fn);
		}
	}

	execCommand(cmd, cfg) {
		if ('insertSignature' == cmd) {
			cfg = Object.assign({
				clearCache: false,
				isHtml: false,
				insertBefore: false,
				signature: ''
			}, cfg);

			if (cfg.clearCache) {
				this._prev_txt_sig = null;
			} else try {
				const signature = cfg.isHtml ? htmlToPlain(cfg.signature) : cfg.signature;
				if ('plain' === this.mode) {
					let
						text = this.plain.value,
						prevSignature = this._prev_txt_sig;
					if (prevSignature) {
						text = text.replace(prevSignature, '').trim();
					}
					this.plain.value = cfg.insertBefore ? '\n\n' + signature + '\n\n' + text : text + '\n\n' +  signature;
				} else {
					const root = this.squire.getRoot(),
						div = createElement('div');
					div.className = 'rl-signature';
					div.innerHTML = cfg.isHtml ? cfg.signature : plainToHtml(cfg.signature);
					root.querySelectorAll('div.rl-signature').forEach(node => node.remove());
					cfg.insertBefore ? root.prepend(div) : root.append(div);
				}
				this._prev_txt_sig = signature;
			} catch (e) {
				console.error(e);
			}
		}
	}

	getData() {
		return trimLines(this.squire.getHTML());
	}

	setData(html) {
//		this.plain.value = html;
		this.squire.setHTML(trimLines(html));
	}

	focus() {
		('plain' == this.mode ? this.plain : this.squire).focus();
	}
}

this.SquireUI = SquireUI;

})(document);
