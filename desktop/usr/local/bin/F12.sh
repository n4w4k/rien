#!/bin/bash
#
# Copyright (c) 2018 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# screenshot directory setting is first argument
SCREENSHOT_DIR="$1"
# if not set, just put files in HOME
if [[ ! -d "$SCREENSHOT_DIR" ]]; then SCREENSHOT_DIR=~; fi

# version without xdotool: allow to click and select the target window.
#   drawback: might mess up some specific windows (full screen games, etc).
# if we really want this feature, we could try with xdotool selectwindow
# 
# 
# # fine for xwininfo as shipped by x11-utils 7.7+4
# XWININFOSTRING="xwininfo: Window id: "
# # grep window id with xwininfo
# GRABBED_XWINFO=`xwininfo | grep "$XWININFOSTRING" | sed s/"$XWININFOSTRING"//`
# # extract id
# GRABBED_ID=`echo "$GRABBED_XWINFO" | cut -f 1 -d " "`
# # remove quotes andid to keep the window name
# GRABBED_NAME=`echo "$GRABBED_XWINFO" | sed s/\"//g | sed s/"$GRABBED_ID"\ *//g`
# # if urlize.pl is installed, use it
# urlizepl=`which urlize.pl`
# if [ -x "$urlizepl" ]; then GRABBED_NAME=`urlize -e "$GRABBED_NAME"`; fi

# xdotool version
# exit if missing 
if [ ! -x "`which xdotool`" ]; then echo "Please install xdotool." && exit ; fi

# get ID
GRABBED_ID=`xdotool getwindowfocus`
# get name, shorten it, remove slashes clean it with urlize if available
GRABBED_NAME=`xdotool getwindowname "$GRABBED_ID"`
GRABBED_NAME_CLEANED=`echo "$GRABBED_NAME" | sed s@/@-@g | cut -c -20`
if [ -x "`which urlize.pl`" ]; then GRABBED_NAME_CLEANED=`urlize -e "$GRABBED_NAME_CLEANED"`; fi
# get geometry
GRABBED_GEOMETRY=`xdotool getwindowgeometry "$GRABBED_ID" | tr -d " " | tr '[:upper:]' '[:lower:]' | grep geometry | cut -f 2 -d :`

# create output name
OUTPUT="$GRABBED_NAME_CLEANED-`date +%y%m%d_%H%M%S`-$GRABBED_GEOMETRY.png"
# remove repeated characrters
OUTPUT=`echo "$OUTPUT" | tr -s '[:punct:]' | tr -s '[:space:]'`

# actually capture the window
import -window "$GRABBED_ID" "$SCREENSHOT_DIR/$OUTPUT"

# build watermark - is that useful?
#TMP=`mktemp`.png
#WATERMARK_WIDTH=`echo "$GRABBED_GEOMETRY" | cut -f 1 -d x`
#WATERMARK_HEIGHT=$((3 * $WATERMARK_WIDTH / 100))
#convert -size $WATERMARK_WIDTH"x"$WATERMARK_HEIGHT -gravity NorthEast -stroke "#454545" -fill "#c8c8c8" -background "transparent" -strokewidth 1 label:"$GRABBED_NAME" "$TMP"
# add watermark
#convert  "$SCREENSHOT_DIR/$OUTPUT" -coalesce -gravity NorthEast null: "$TMP" -layers composite "$SCREENSHOT_DIR/$OUTPUT"


# warn user message
echo "$SCREENSHOT_DIR/$OUTPUT saved"
if [ -x "`which notify-send`" ]; then notify-send --icon=video-display "$SCREENSHOT_DIR/$OUTPUT saved"; fi
				      
# EOF
#"label:$GRABBED_NAME"
