#!/bin/bash
#
# Related torrent-watch.pl: to use with web browser in order to feed magnet
# links to torrent-watch.pl through a temporary file.
#
# firefox/palemoon setup:
#
#   about:config ->
#       new boolean network.protocol-handler.expose.magnet = false

[ -z $1 ] && echo "What are you trying to achieve? Pass a magnet link as argument." && exit;

# default wild assumption
TORRENT_WATCH=/home/torrent/watch

# source /etc/bashrc.d so TORRENT_WATCHDIR should be properly
# set (at least with stalag13-utils package), but escaping any Bash specific
# stuff by unsetting BASH_VERSION
BASH_VERSION=""
[ -z "$ETC_BASHRC_SOURCED" ] && for i in /etc/bashrc.d/*.sh ; do if [ -r "$i" ];
 then . $i; fi; done

echo "$1" > $TORRENT_WATCH/magnetfilesh`date +%s``cat /dev/urandom | tr -dc [:alnum:] | head -c5`.magnet

# EOF
