#!/bin/bash
#
# wrapper to install/start docfetcher
# named docftcher instead of docfetcher to ease pgrep of real docfetcher process
#

CURRENT_RELEASE=1.1.22
DIR=$HOME/.docfetcher
SUBDIR=$DIR/DocFetcher-$CURRENT_RELEASE

if [ ! -e $SUBDIR ]; then
    # missing current release
    if [ -e $DIR ]; then
	# main dir exists already? probably an update
	echo "updating docfetcher to release $CURRENT_RELEASE"
    else
	# otherwise create from scratch
	mkdir -pv $DIR
	echo "installing docfetcher $CURRENT_RELEASE"
    fi
    # in any case, copy the current version
    cp -rv /usr/local/share/DocFetcher-$CURRENT_RELEASE $SUBDIR
    # symlink it to current
    rm -fv $DIR/current
    ln -sv $DIR/DocFetcher-$CURRENT_RELEASE $DIR/current
fi

# start only one at a time per user
pgrep -f docfetcher -c --uid $UID >/dev/null || $SUBDIR/DocFetcher-GTK3.sh 

# EOF





