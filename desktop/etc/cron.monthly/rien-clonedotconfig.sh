#!/bin/bash
#
#
# Copyright (c) 2022 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# on mixed SSD / HDD setup, with /home being on HDD except ~/.config bound to /home.config/ :
# update a clone of ~/.config that resides on HDD, some home actually keep touch with config changes over time

SSD_DIR=/home.config
[ ! -d $SSD_DIR ] && exit
cd $SSD_DIR

for user in *; do
    # skip if not related to an actual user
    [ ! -d /home/$user/.config ] && continue
    # otherwise clone current config, assuming /home/$user/.config is bound to $SDD_DIR/$user
    # in /etc/fstab
    /usr/bin/rsync --archive --delete /home/$user/.config/ /home/$user/.config.bak/
    date > /home/$user/.config.bak/.lastclone
done

# EOF
    
