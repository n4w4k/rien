#!/usr/bin/perl
#
# Copyright (c) 2017 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Fcntl qw(:flock);
use POSIX qw(strftime);
use File::HomeDir;

my $binmpc = "/usr/bin/mpc";
my $filecurrent = File::HomeDir->my_home()."/.mpc-current-playlist";

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

# Get currently played one
# (as far I understand, mpd has no such notion, so we handle a specific .current file)
my $current;
if (-e $filecurrent) {
    open(CURRENT, "< $filecurrent");
    while (<CURRENT>) {
	chomp();
	$current = $_;
    }    
    close(CURRENT);
}


# go thru the playlist list and pick the next
open(MPC_LISTPLAYLISTS, "$binmpc lsplaylists |");
my $playnext = 0;
my $next;
while (<MPC_LISTPLAYLISTS>) {
    chomp();

    # next to the currently played?
    if ($playnext) {
	# select it and break out the loop
	$next = $_;
	last;
    }

    # set next to the first available if not yet set
    # (so we have the first one as fallback)
    $next = $_ unless $next;
    
    # currently played? mark that the next in list is to be played
    $playnext = 1 if $_ eq $current;

}
close(MPC_LISTPLAYLISTS);

# nothing to do is current match the next (single playlist?)
exit if $current eq $next;

# otherwise load the next one
system($binmpc, "--quiet", "clear");
system($binmpc, "--quiet", "load", $next);
system($binmpc, "--quiet", "play");

# and save status
open(CURRENT, "> $filecurrent");
print CURRENT $next;
close(CURRENT);

# EOF
