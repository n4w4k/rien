#!/usr/bin/perl
#
# Copyright (c) 2017 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Fcntl qw(:flock);
use POSIX qw(strftime);
use File::Basename;


my $binmpc = "/usr/bin/mpc";
my $dirplaylist = "/var/lib/mpd/playlists"; # defaut raspbian ; user must have write access here!
                                            # adduser pi audio

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

# update mpc database
system($binmpc, "--quiet", "update");

# slurp list of files
my %playlist;
open(MPC_LISTALL, "$binmpc listall |");
while (<MPC_LISTALL>) {
    chomp();
    # always add in general playlist
    push(@{$playlist{"All"}}, $_);

    # if in subdir, also register in special list
    my $dir = dirname($_);
    next if $dir eq ".";
    push(@{$playlist{$dir}}, $_);
}
close(MPC_LISTALL);

# for the remaining part, we need to work in playlist directory
# (not using mpc so not to mess with currently played songs)
chdir($dirplaylist) 
    or die "Unable to enter $dirplaylist";

# build new playlists 
foreach my $list (sort(keys %playlist)) {
    open(M3U, "> $list.m3u");
    foreach (@{$playlist{$list}}) {
	print M3U "$_\n";
    }
    close(M3U);
}

# remove possibly outdated playlists
for my $list (glob("*.m3u")) {
    # skip if still valid
    next if ($playlist{basename($list, ".m3u")});
    # otherwise get rid of it
    unlink($list);
}


# EOF
